<?php get_header(); 
	
	if ( is_rtl() ) {
		$rtl = ' sm-rtl';
	} else {
		$rtl = '';
	}
	
?>

<div id="primary" class="site-content errorpage-404">
	<div id="content" role="main">

		<div id="post-0" class="post error404 no-results not-found">
		
		<div class="row">
		  <div class="col-xs-12 col-md-4">
			<div class="error-header">
				<h1><?php esc_html_e( '404', 'kiwi' ); ?></h1>
				<h4><?php esc_html_e( 'This is somewhat embarrassing, isn\'t it?', 'kiwi' ); ?></h4>
			</div>
		  </div>
		  
		   <div class="col-xs-12 col-md-4">
			<?php esc_html_e( 'The page you are looking for might have been moved, had its name changed, deleted or never existed in the first place, you may have mistyped the url, or it\'s simply temporarily unavailable!', 'kiwi' ); ?>
		   
			<div class="sep"></div>					
				<span class="report"><?php esc_html_e( 'If you feel that this an error that needs to be addressed, feel free to contact the administrator of this website through the contact page.', 'kiwi' ); ?></span>
			</div>
		  
		  <div class="col-xs-12 col-md-4 block">
			<div><?php esc_html_e( 'Perhaps searching, or one of the links below, can help.', 'kiwi' ); ?></div>				
			<div>
				<?php if ( has_nav_menu( '404_navigation' ) ) { ?>                    
					<?php wp_nav_menu(array('theme_location' => '404_navigation',  'container' => false,  'menu_class' => 'kiwi-nav nav navbar-nav'. esc_attr( $rtl ) .'', 'walker' => new wp_bootstrap_navwalker())); ?>
				<?php } ?>
			</div>
			<?php get_search_form(); ?>
		  </div>
		  
		</div>
		
		</div>

	</div>
</div>

<?php get_footer(); ?>
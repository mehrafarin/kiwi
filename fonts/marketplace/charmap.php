<?php $icons = array();
$icons['Marketplace-Additional-Icons']['mobile'] = array("class"=>'mobile',"tags"=>'mobile,tablet,phone,device',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['laptop'] = array("class"=>'laptop',"tags"=>'laptop,computer,screen',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['desktop'] = array("class"=>'desktop',"tags"=>'desktop,screen,display,computer',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['tablet'] = array("class"=>'tablet',"tags"=>'tablet,screen,mobile',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['phone'] = array("class"=>'phone',"tags"=>'phone,mobile,contact,telephone',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['document'] = array("class"=>'document',"tags"=>'document,file,paper,text',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['documents'] = array("class"=>'documents',"tags"=>'documents,files,papers',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['search'] = array("class"=>'search',"tags"=>'search,lookup,find,magnifier',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['clipboard'] = array("class"=>'clipboard',"tags"=>'clipboard,paste,board',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['newspaper'] = array("class"=>'newspaper',"tags"=>'newspaper,paper',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['notebook'] = array("class"=>'notebook',"tags"=>'notebook,book',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['book-open'] = array("class"=>'book-open',"tags"=>'book-open',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['browser'] = array("class"=>'browser',"tags"=>'browser,window,software,program',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['calendar'] = array("class"=>'calendar',"tags"=>'calendar,date,schedule,week',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['presentation'] = array("class"=>'presentation',"tags"=>'presentation,board',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['picture'] = array("class"=>'picture',"tags"=>'picture,image,photo',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['pictures'] = array("class"=>'pictures',"tags"=>'pictures,images,photos',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['video'] = array("class"=>'video',"tags"=>'video,movie,film',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['camera'] = array("class"=>'camera',"tags"=>'camera,photo,picture,image',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['printer'] = array("class"=>'printer',"tags"=>'printer',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['toolbox'] = array("class"=>'toolbox',"tags"=>'toolbox,briefcase',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['briefcase'] = array("class"=>'briefcase',"tags"=>'briefcase',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['wallet'] = array("class"=>'wallet',"tags"=>'wallet,money',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['gift'] = array("class"=>'gift',"tags"=>'gift',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['bargraph'] = array("class"=>'bargraph',"tags"=>'bargraph,bars,graph,chart,stats,statistics',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['grid'] = array("class"=>'grid',"tags"=>'grid,squares',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['expand'] = array("class"=>'expand',"tags"=>'expand,fullscreen,enlarge',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['focus'] = array("class"=>'focus',"tags"=>'focus',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['edit'] = array("class"=>'edit',"tags"=>'edit,pencil,write,blog',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['adjustments'] = array("class"=>'adjustments',"tags"=>'adjustments,settings,options,equalizer,preferences',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['ribbon'] = array("class"=>'ribbon',"tags"=>'ribbon,bookmark',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['hourglass'] = array("class"=>'hourglass',"tags"=>'hourglass,loading,busy,time',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['lock'] = array("class"=>'lock',"tags"=>'lock,security,secure,privacy,encryption',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['megaphone'] = array("class"=>'megaphone',"tags"=>'megaphone,news,announcements,advertisement',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['shield'] = array("class"=>'shield',"tags"=>'shield,secure',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['trophy'] = array("class"=>'trophy',"tags"=>'trophy,cup,winner,tournament,race,award',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['flag'] = array("class"=>'flag',"tags"=>'flag,report,spam',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['map'] = array("class"=>'map',"tags"=>'map,location',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['puzzle'] = array("class"=>'puzzle',"tags"=>'puzzle,piece,plugin,extension,game',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['basket'] = array("class"=>'basket',"tags"=>'basket,cart,checkout,ecommerce',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['envelope'] = array("class"=>'envelope',"tags"=>'envelope,mail,email,letter,message',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['streetsign'] = array("class"=>'streetsign',"tags"=>'streetsign,address,direction',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['telescope'] = array("class"=>'telescope',"tags"=>'telescope,astronomy',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['gears'] = array("class"=>'gears',"tags"=>'gears,settings,preferences',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['key'] = array("class"=>'key',"tags"=>'key,password,unlock,access',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['paperclip'] = array("class"=>'paperclip',"tags"=>'paperclip,attachment',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['attachment'] = array("class"=>'attachment',"tags"=>'attachment,chain,link',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['pricetags'] = array("class"=>'pricetags',"tags"=>'pricetags,tags,prices,shop,ecommerce',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['lightbulb'] = array("class"=>'lightbulb',"tags"=>'lightbulb,light,night,idea,thought',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['layers'] = array("class"=>'layers',"tags"=>'layers,stack,planes,manage',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['pencil'] = array("class"=>'pencil',"tags"=>'pencil,edit,blog,write',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['tools'] = array("class"=>'tools',"tags"=>'tools,measure,design,ruler,pencil',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['tools-2'] = array("class"=>'tools-2',"tags"=>'tools-2,wrench,screwdriver',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['scissors'] = array("class"=>'scissors',"tags"=>'scissors,cut,blade',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['paintbrush'] = array("class"=>'paintbrush',"tags"=>'paintbrush,color,fill,brush',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['magnifying-glass'] = array("class"=>'magnifying-glass',"tags"=>'magnifying-glass,search,glass,magnifier,lookup',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['circle-compass'] = array("class"=>'circle-compass',"tags"=>'circle-compass,distance',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['linegraph'] = array("class"=>'linegraph',"tags"=>'linegraph,graph,statistics',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['mic'] = array("class"=>'mic',"tags"=>'mic,microphone,record,voice,audio',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['strategy'] = array("class"=>'strategy',"tags"=>'strategy,horse,chess,piece',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['beaker'] = array("class"=>'beaker',"tags"=>'beaker,laboratory,test,beta,experiment',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['caution'] = array("class"=>'caution',"tags"=>'caution,warning,exclamation,note,notice',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['recycle'] = array("class"=>'recycle',"tags"=>'recycle,arrows',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['anchor'] = array("class"=>'anchor',"tags"=>'anchor,link,attach',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['profile-male'] = array("class"=>'profile-male',"tags"=>'profile-male,persona,profile,male,user,avatar',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['profile-female'] = array("class"=>'profile-female',"tags"=>'profile-female,female,woman,user,profile,avatar',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['bike'] = array("class"=>'bike',"tags"=>'bike,bicycle,ride',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['wine'] = array("class"=>'wine',"tags"=>'wine,glass,drink',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['hotairballoon'] = array("class"=>'hotairballoon',"tags"=>'hotairballoon,balloon,flight,air',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['globe'] = array("class"=>'globe',"tags"=>'globe,earth',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['genius'] = array("class"=>'genius',"tags"=>'genius,atom,nuclear',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['map-pin'] = array("class"=>'map-pin',"tags"=>'map-pin,pin,location',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['dial'] = array("class"=>'dial',"tags"=>'dial',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['chat'] = array("class"=>'chat',"tags"=>'chat,bubbles,talk,message,conversation',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['heart'] = array("class"=>'heart',"tags"=>'heart,love,like,favorite',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['cloud'] = array("class"=>'cloud',"tags"=>'cloud,weather',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['upload'] = array("class"=>'upload',"tags"=>'upload,save',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['download'] = array("class"=>'download',"tags"=>'download,load',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['target'] = array("class"=>'target',"tags"=>'target,goal',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['hazardous'] = array("class"=>'hazardous',"tags"=>'hazardous,dangerous',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['piechart'] = array("class"=>'piechart',"tags"=>'piechart,chart,statistic',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['speedometer'] = array("class"=>'speedometer',"tags"=>'speedometer,meter,gauge,measurement',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['global'] = array("class"=>'global',"tags"=>'global,sphere',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['compass'] = array("class"=>'compass',"tags"=>'compass,direction,guide',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['lifesaver'] = array("class"=>'lifesaver',"tags"=>'lifesaver,lifebuoy',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['clock'] = array("class"=>'clock',"tags"=>'clock,time,schedule',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['aperture'] = array("class"=>'aperture',"tags"=>'aperture,diaphragm',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['quote'] = array("class"=>'quote',"tags"=>'quote',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['scope'] = array("class"=>'scope',"tags"=>'scope,target',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['alarmclock'] = array("class"=>'alarmclock',"tags"=>'alarmclock,clock',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['refresh'] = array("class"=>'refresh',"tags"=>'refresh,sync,reload,loading',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['happy'] = array("class"=>'happy',"tags"=>'happy,smiley,emoticon',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['sad'] = array("class"=>'sad',"tags"=>'sad,upset,smiley,emoticon',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['facebook'] = array("class"=>'facebook',"tags"=>'facebook,social',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['twitter'] = array("class"=>'twitter',"tags"=>'twitter,social',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['googleplus'] = array("class"=>'googleplus',"tags"=>'googleplus,social',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['rss'] = array("class"=>'rss',"tags"=>'rss,feed,atom',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['tumblr'] = array("class"=>'tumblr',"tags"=>'tumblr,social',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['linkedin'] = array("class"=>'linkedin',"tags"=>'linkedin,social',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['dribbble'] = array("class"=>'dribbble',"tags"=>'dribbble,social',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['number5'] = array("class"=>'number5',"tags"=>'number,four',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['number6'] = array("class"=>'number6',"tags"=>'number,three',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['number7'] = array("class"=>'number7',"tags"=>'number,two',"unicode"=>'');
$icons['Marketplace-Additional-Icons']['number8'] = array("class"=>'number8',"tags"=>'number,one',"unicode"=>'');
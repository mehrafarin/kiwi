<?php 

/*===============================================================
	Widget :: Marketplace :: Categories 2 
===============================================================*/
class Marketplace_Widget_Categories_Two extends WP_Widget {
	/** Constructor */
	function __construct() {
		parent::__construct( 'edd_categories_normal_widget', esc_html__( 'Marketplace :: Categories 2', 'kiwi' ), array( 'description' => esc_html__( 'Display categories', 'kiwi' ) ) );
	}

	/** @see WP_Widget::widget */
	function widget( $args, $instance ) {
		// Set defaults
		$args['id']           = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_categories_normal_widget';
		$instance['title']    = ( isset( $instance['title'] ) ) ? $instance['title'] : '';
		
		$title      = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );
		$count      = isset( $instance['count'] ) && $instance['count'] == 'on' ? 1 : 0;
		$hide_empty = isset( $instance['hide_empty'] ) && $instance['hide_empty'] == 'on' ? 1 : 0;
		
		$hide_months = isset( $instance['hide_months'] ) ? $instance['hide_months'] : false;
		
		$include 	= isset( $instance['include'] ) ? $instance['include'] : '';
		$exclude 	= isset( $instance['exclude'] ) ? $instance['exclude'] : '';
				
		if ( $instance['hide_months'] == '1' ) {	
			if ( is_singular( 'download' ) ) {
				return false;	
			}						
		}
		
		echo $args['before_widget'];
		
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		$custom_args = array(
			'taxonomy'           => 'download_category',
			'show_count'         => $count,
			'hide_empty'         => $hide_empty,
			'orderby'            => 'name',
			'order'              => 'ASC',
			'title_li'           => '',
			'depth'              => 1,	
		);		
		
		if ( isset( $instance['include'] ) && $instance['include'] != '' ) {	
			$custom_args['include'] = explode(',', $include);
		}
		
		if ( $instance['exclude'] != '' ) {	
			$custom_args['exclude'] = explode(',', $exclude);
		}
		
		do_action( 'edd_before_category_widget' );
		
		add_filter('wp_list_categories', 'cat_count_span');
		
			echo '<ul class="main-menu edd-taxonomy-widget">';
				 wp_list_categories($custom_args);
			echo '</ul>';	
		
		do_action( 'edd_after_category_widget' );
		
			echo $args['after_widget'];
	}

	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['count'] = isset( $new_instance['count'] ) ? $new_instance['count'] : '';
		$instance['hide_empty'] = isset( $new_instance['hide_empty'] ) ? $new_instance['hide_empty'] : '';
		
		$instance['hide_months'] = isset( $new_instance['hide_months'] ) ? (bool) $new_instance['hide_months'] : false;
		
		$instance['include'] = isset( $new_instance['include'] ) ? $new_instance['include'] : '';
		$instance['exclude'] = isset( $new_instance['exclude'] ) ? $new_instance['exclude'] : '';
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance ) {
		// Set up some default widget settings.
		$defaults = array(
			'title'         => '',
			'taxonomy'      => 'download_category',
			'count'         => 'off',
			'hide_empty'    => 'off'
		);

		$hide_months = isset( $instance['hide_months'] ) ? (bool) $instance['hide_months'] : false;
		
		$include = isset( $instance['include'] ) ? esc_attr( $instance['include'] ) : '';
		$exclude = isset( $instance['exclude'] ) ? esc_attr( $instance['exclude'] ) : '';
		
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		
		
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
		</p>
		<p>
			<input <?php checked( $instance['count'], 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>" type="checkbox" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"><?php esc_html_e( 'Show Count', 'kiwi' ); ?></label>
		</p>
		<p>
			<input <?php checked( $instance['hide_empty'], 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_empty' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_empty' ) ); ?>" type="checkbox" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_empty' ) ); ?>"><?php esc_html_e( 'Hide Empty Categories', 'kiwi' ); ?></label>
		</p>
		
		<!-- Hide -->		
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $hide_months ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_months' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>"><?php esc_html_e( 'Hide on item description page?', 'kiwi' ); ?></label>
		</p>
		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>"><?php esc_html_e( 'Include items<br><small>(Use ID\'s seperated by a comma.)</small>', 'kiwi' ); ?></label>
			<input class="widefat" size="3" id="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'include' ) ); ?>" type="text" value="<?php echo esc_attr( $include ); ?>"/>
		</p>
		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'exclude' ) ); ?>"><?php esc_html_e( 'Exclude items<br><small>(Use ID\'s seperated by a comma.)</small>', 'kiwi' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'exclude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'exclude' ) ); ?>" type="text" value="<?php echo esc_attr( $exclude ); ?>"/>
		</p>	
		
	<?php
	}
}
<?php 
/*===============================================================
	Widget :: Marketplace :: Categories
===============================================================*/
class Marketplace_Widget_Categories extends WP_Widget {
	/** Constructor */
	function __construct() {
		parent::__construct( 'edd_categories_cat_widget', esc_html__( 'Marketplace :: Categories', 'kiwi' ), 
		array( 'description' => esc_html__( 'Display categories (accordion style)', 'kiwi' ) ) );
	}

	/** @see WP_Widget::widget */
	function widget( $args, $instance ) {
		// Set defaults
		$args['id']           = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_categories_cat_widget';
		$instance['title']    = ( isset( $instance['title'] ) ) ? $instance['title'] : '';
		
		$title      = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );
		$count      = isset( $instance['count'] ) && $instance['count'] == 'on' ? 1 : 0;
		$hide_empty = isset( $instance['hide_empty'] ) && $instance['hide_empty'] == 'on' ? 1 : 0;
		
		$hide_months = isset( $instance['hide_months'] ) ? $instance['hide_months'] : false;
				
		if ( $instance['hide_months'] == '1' ) {	
			if ( is_singular( 'download' ) ) {
				return false;	
			}						
		}
		
		wp_enqueue_script( 'dcjqaccordion', get_template_directory_uri() . '/js/cat/jquery.dcjqaccordion.2.7.min.js', array(), '2.7', true );
		wp_enqueue_script( 'hoverintent', get_template_directory_uri() . '/js/cat/jquery.hoverIntent.minified.js', array(), '1.0.0', true );
		wp_enqueue_script( 'accordion-cookie', get_template_directory_uri() . '/js/cat/jquery.cookie.js', array(), '1.0.0', true );

		
		echo $args['before_widget'];
		
		add_filter('wp_list_categories', 'cat_count_span');
		
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		if ( $instance['count'] == 'on' ) {	
			$script_count = 'showCount: true,';						
		} else {
			$script_count = 'showCount: false,';
		}
		
		$uniqid = uniqid('accordion-');		
	 
		$script = "<script type='text/javascript'>
		jQuery( document ).ready( function($) {
			$(document).ready(function() {
		$('#$uniqid').dcAccordion({
			eventType: 'click',
			autoClose: false,
			saveState: true,
			disableLink: true,
			speed: 'slow',
			$script_count
			autoExpand: true,
			autoClose : false,
			menuClose: true,
			cookie	: 'dcjq-accordion',
			classExpand	 : 'dcjq-current-parent'
				});
			});
        }
     );
    </script>";

		do_action( 'edd_before_category_widget' );
		
			echo $script;
			echo '<ul id="'.$uniqid.'" class="main-menu edd-taxonomy-widget accordion">';
				 wp_list_categories( 'title_li=&taxonomy=download_category&show_count=' . $count . '&hide_empty=' . $hide_empty );			
			echo '</ul>';	
		
		do_action( 'edd_after_category_widget' );
		
			echo $args['after_widget'];
	}

	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['count'] = isset( $new_instance['count'] ) ? $new_instance['count'] : '';
		$instance['hide_empty'] = isset( $new_instance['hide_empty'] ) ? $new_instance['hide_empty'] : '';
		
		$instance['hide_months'] = isset( $new_instance['hide_months'] ) ? (bool) $new_instance['hide_months'] : false;
		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance ) {
		// Set up some default widget settings.
		$defaults = array(
			'title'         => '',
			'taxonomy'      => 'download_category',
			'count'         => 'off',
			'hide_empty'    => 'off'
		);

		$hide_months = isset( $instance['hide_months'] ) ? (bool) $instance['hide_months'] : false;
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
		</p>
		<p>
			<input <?php checked( $instance['count'], 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>" type="checkbox" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"><?php esc_html_e( 'Show Count', 'kiwi' ); ?></label>
		</p>
		<p>
			<input <?php checked( $instance['hide_empty'], 'on' ); ?> id="<?php echo $this->get_field_id( 'hide_empty' ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_empty' ) ); ?>" type="checkbox" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_empty' ) ); ?>"><?php esc_html_e( 'Hide Empty Categories', 'kiwi' ); ?></label>
		</p>
		
		<!-- Hide -->		
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $hide_months ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_months' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>"><?php esc_html_e( 'Hide on item description page?', 'kiwi' ); ?></label>
		</p>
	<?php
	}
}

<?php 

/*===============================================================
	Widget :: Marketplace :: Tags
===============================================================*/
class Marketplace_Widget_Tags extends WP_Widget {
	/** Constructor */
	function __construct() {
		parent::__construct( 'Marketplace_Widget_Tags', esc_html__( 'Marketplace :: Tags', 'kiwi' ), 
		array( 'description' => esc_html__( 'Display tags of marketplace items', 'kiwi' ) ) );
	}
	
	/** @see WP_Widget::widget */
	function widget( $args, $instance ) {
		// Set defaults
		$args['id']           = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_categories_tags_widget';
		$instance['title']    = ( isset( $instance['title'] ) ) ? $instance['title'] : '';
		
		$title      = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );
		$count      = isset( $instance['count'] ) && $instance['count'] == 'on' ? 1 : 0;
		$hide_empty = isset( $instance['hide_empty'] ) && $instance['hide_empty'] == 'on' ? 1 : 0;
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 20;
					if ( ! $number )
						$number = 20;

		$hide_months = isset( $instance['hide_months'] ) ? $instance['hide_months'] : false;		
		
		$orderby = empty($instance['orderby']) ? '' : $instance['orderby'];
		$order = empty($instance['order']) ? '' : $instance['order'];
				
		if ( $instance['hide_months'] == '1' ) {	
			if ( is_singular( 'download' ) ) {
				return false;	
			}						
		}			
					
					
		echo $args['before_widget'];
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
			 

		do_action( 'edd_before_taxonomy_widget' );

		echo '<ul class="marketplace-tags">';			
			remove_filter('wp_list_categories', 'cat_count_span'); 
				wp_list_categories( 'title_li=&taxonomy=download_tag&show_count=' . $count . '&hide_empty=' . $hide_empty . '&number=' . $number . '&orderby=' . $orderby . '&order=' . $order . '');	
		echo '</ul>';
		

		do_action( 'edd_after_taxonomy_widget' );
		
		echo $args['after_widget'];
	}

	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );
		$instance['count'] = isset( $new_instance['count'] ) ? $new_instance['count'] : '';
		$instance['hide_empty'] = isset( $new_instance['hide_empty'] ) ? $new_instance['hide_empty'] : '';
		
		$instance['hide_months'] = isset( $new_instance['hide_months'] ) ? (bool) $new_instance['hide_months'] : false;
				
		$instance['orderby'] = $new_instance['orderby'];
		$instance['order'] = $new_instance['order'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance ) {
		// Set up some default widget settings.
		$defaults = array(
			'title'         => '',
			'taxonomy'      => 'download_tag',
			'count'         => 'on',
			'hide_empty'    => 'off',
			'number'  		=> '20', 
			'orderby'		=> 'name',
			'order'			=> 'DESC'
		);

		$instance = wp_parse_args( (array) $instance, $defaults ); 
		$hide_months = isset( $instance['hide_months'] ) ? (bool) $instance['hide_months'] : false; 
		
		$orderby = $instance['orderby']; 
		$order = $instance['order']; 
		
		?>
		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of tags to show:', 'kiwi' ); ?></label>
			<input size="3" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['number'] ); ?>"/>
		</p>		
	
		
		<p>
		  <label for="<?php echo esc_attr( $this->get_field_id('text') ); ?>"><?php esc_html_e( 'Order by:', 'kiwi' ); ?>
			<select class='widefat' id="<?php echo esc_attr( $this->get_field_id('orderby') ); ?>"
					name="<?php echo esc_attr( $this->get_field_name('orderby') ); ?>" type="text">
			  <option value='ID'<?php echo ($orderby=='ID')?'selected':''; ?>><?php esc_html_e( 'ID', 'kiwi' ); ?></option>
			  <option value='name'<?php echo ($orderby=='name')?'selected':''; ?>><?php esc_html_e( 'Name', 'kiwi' ); ?></option>
			  <option value='slug'<?php echo ($orderby=='slug')?'selected':''; ?>><?php esc_html_e( 'Slug', 'kiwi' ); ?></option>
			  <option value='count'<?php echo ($orderby=='count')?'selected':''; ?>><?php esc_html_e( 'Count', 'kiwi' ); ?></option>
			</select>                
		  </label>
		 </p>
		
		<p>
		  <label for="<?php echo esc_attr( $this->get_field_id('text') ); ?>"><?php esc_html_e( 'Order:', 'kiwi' ); ?>
			<select class='widefat' id="<?php echo esc_attr( $this->get_field_id('order') ); ?>"
					name="<?php echo esc_attr( $this->get_field_name('order') ); ?>" type="text">
			  <option value='ASC'<?php echo ($order=='ASC')?'selected':''; ?>><?php esc_html_e( 'ASC', 'kiwi' ); ?></option>
			  <option value='DESC'<?php echo ($order=='DESC')?'selected':''; ?>><?php esc_html_e( 'DESC', 'kiwi' ); ?></option>
			</select>                
		  </label>
		 </p>

		
		<p>
			<input <?php checked( $instance['count'], 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>" type="checkbox" />
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php esc_html_e( 'Show Count:', 'kiwi' ); ?></label>
		</p>
		<p>
			<input <?php checked( $instance['hide_empty'], 'on' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_empty' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_empty' ) ); ?>" type="checkbox" />
			<label for="<?php echo $this->get_field_id( 'hide_empty' ); ?>"><?php esc_html_e( 'Hide Empty Tags', 'kiwi' ); ?></label>
		</p>
		
		<!-- Hide -->		
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $hide_months ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_months' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>"><?php esc_html_e( 'Hide on item description page?', 'kiwi' ); ?></label>
		</p>
		
	<?php
	}
}
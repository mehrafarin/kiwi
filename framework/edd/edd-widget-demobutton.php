<?php

class Marketplace_Item_Demolink extends WP_Widget {

    /** Constructor */
	public function __construct() {
			parent::__construct( 'Marketplace_Item_Demolink', esc_html__( 'Marketplace :: Demo Link', 'kiwi' ), 
			array( 'description' => esc_html__( 'Display a button with a link to an item\'s demo.', 'kiwi' ) ) );
		}	


    /** @see WP_Widget::widget */
    public function widget( $args, $instance ) {
		$args['id'] = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_download_item_stats';

        if ( ! is_singular( 'download' ) ) {
        	return;
        }

       	// set correct download ID
        if ( is_singular( 'download' ) ) {
        	$download_id = get_the_ID();
        } else {
        	$download_id = absint( $instance['download_id'] );
        }

        // Variables from widget settings
		// $title = 'apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] )';
		$title = '';
      	
		$demo_title = isset( $instance['demo_title'] ) ? $instance['demo_title'] : '';
		
		$mp_demo_url 	= get_post_meta( $download_id, '_cmb2_marketplace_demo_url', true );
		$fes_demo_url 	= get_post_meta( $download_id, 'demo_url', true );
		
		if ( !empty( $mp_demo_url ) || !empty( $fes_demo_url ) ) { 		
		
			// Used by themes. Opens the widget
			echo $args['before_widget'];

			// Display the widget title
			if( $title ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}		

			do_action( 'edd_product_item_stats_before_title' , $instance , $download_id );		
						
			if (!class_exists('EDD_Front_End_Submissions') && !empty( $mp_demo_url) || class_exists( 'EDD_Front_End_Submissions' ) && empty( $fes_demo_url ) && !empty( $mp_demo_url ) ){ 			
					echo '<div class="demo">
						<a href="'. esc_url( $mp_demo_url ).'" target="_blank"><i class="fa fa-globe"></i> <span>'. esc_html( $demo_title ).'</span></a>
					</div>';					
			} 
			
			if (class_exists( 'EDD_Front_End_Submissions' ) && !empty( $fes_demo_url )) { 	
					echo '<div class="demo">
						<a href="'. esc_url( $fes_demo_url ).'" target="_blank"><i class="fa fa-globe"></i> <span class="label">'. esc_html__('Live demo', 'kiwi').'</span></a>
					</div>';					
			} 	

			do_action( 'edd_product_item_stats_before_end', $instance, $download_id );

			// Used by themes. Closes the widget
			echo $args['after_widget'];
		}
	
	} 
   	/** @see WP_Widget::form */
    public function form( $instance ) {
        // Set up some default widget settings.
        $defaults = array(
            'title' 			=> sprintf( esc_html__( '%s Details', 'kiwi' ), edd_get_label_singular() ),
            'download_id' 		=> 'current',
        );
		
		$demo_title = isset( $instance['demo_title'] ) ? esc_attr( $instance['demo_title'] ) : '';
		
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        		
		<p>
			<label for="<?php echo $this->get_field_id( 'demo_title' ); ?>"><?php echo wp_kses(__('Text inside the button, e.g. <em>Live Demo</em>', 'kiwi'), wp_kses_allowed_html( 'post' ) ); ?><input class="widefat" id="<?php echo $this->get_field_id( 'demo_title' ); ?>" name="<?php echo $this->get_field_name( 'demo_title' ); ?>" type="text" value="<?php echo $demo_title; ?>" />
		</p> 
		
		
        
        <?php do_action( 'edd_product_item_stats_form' , $instance ); ?>
    <?php }

    /** @see WP_Widget::update */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title']           = strip_tags( $new_instance['title'] );
        $instance['demo_title'] 	 = strip_tags( $new_instance['demo_title'] );
		
        do_action( 'edd_product_item_stats_update', $instance );
        
        return $instance;
    } 

}


<?php

class Marketplace_File_Info extends WP_Widget {

    /** Constructor */
	public function __construct() {
			parent::__construct( 'Marketplace_File_Info', esc_html__( 'Marketplace :: Item information', 'kiwi' ), array( 'description' => esc_html__( 'Display item information, such as release/modified date, file format/size, etc.', 'kiwi' ) ) );
		}	


    /** @see WP_Widget::widget */
    public function widget( $args, $instance ) {
		$args['id'] = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_download_item_stats';

        if ( ! is_singular( 'download' ) ) {
        	return;
        }

       	// set correct download ID
        if ( 'current' == $instance['download_id'] && is_singular( 'download' ) ) {
        	$download_id = get_the_ID();
        } else {
        	$download_id = absint( $instance['download_id'] );
        }

        // Variables from widget settings
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );
      	
	
        // Used by themes. Opens the widget
        echo $args['before_widget'];

        // Display the widget title
        if( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
		}
		
		$show_releasedate = isset( $instance['show_releasedate'] ) && $instance['show_releasedate'] === 1 ? 1 : 0;
		$show_modifieddate = isset( $instance['show_modifieddate'] ) && $instance['show_modifieddate'] === 1 ? 1 : 0;
		$show_itemversion = isset( $instance['show_itemversion'] ) && $instance['show_itemversion'] === 1 ? 1 : 0;
		$show_filesize = isset( $instance['show_filesize'] ) && $instance['show_filesize'] === 1 ? 1 : 0; 
		$show_highres = isset( $instance['show_highres'] ) && $instance['show_highres'] === 1 ? 1 : 0; 
		$show_responsive = isset( $instance['show_responsive'] ) && $instance['show_responsive'] === 1 ? 1 : 0;
		$show_widgetready = isset( $instance['show_widgetready'] ) && $instance['show_widgetready'] === 1 ? 1 : 0; 
		$show_retinaready = isset( $instance['show_retinaready'] ) && $instance['show_retinaready'] === 1 ? 1 : 0; 
		$show_browser = isset( $instance['show_browser'] ) && $instance['show_browser'] === 1 ? 1 : 0; 
		$show_plugincom = isset( $instance['show_plugincom'] ) && $instance['show_plugincom'] === 1 ? 1 : 0;
		$show_frameworkcom = isset( $instance['show_frameworkcom'] ) && $instance['show_frameworkcom'] === 1 ? 1 : 0; 
		$show_fileformats = isset( $instance['show_fileformats'] ) && $instance['show_fileformats'] === 1 ? 1 : 0; 
		$show_itemsupport = isset( $instance['show_itemsupport'] ) && $instance['show_itemsupport'] === 1 ? 1 : 0; 
		$show_supporturl = isset( $instance['show_supporturl'] ) && $instance['show_supporturl'] === 1 ? 1 : 0; 
		$show_changelogurl = isset( $instance['show_changelogurl'] ) && $instance['show_changelogurl'] === 1 ? 1 : 0; 
		$show_documentation = isset( $instance['show_documentation'] ) && $instance['show_documentation'] === 1 ? 1 : 0; 
		$show_itemcat = isset( $instance['show_itemcat'] ) && $instance['show_itemcat'] === 1 ? 1 : 0; 
		$show_itemtags = isset( $instance['show_itemtags'] ) && $instance['show_itemtags'] === 1 ? 1 : 0; 
		$show_demourl = isset( $instance['show_demourl'] ) && $instance['show_demourl'] === 1 ? 1 : 0;

		
        do_action( 'edd_product_fileinfo_before_title' , $instance , $download_id );	
		
		
		remove_filter( "term_links-download_category", 'limit_terms');
		$catlist = get_the_term_list( $download_id, 'download_category', '', ', ', '' );
		$taglist = get_the_term_list( $download_id, 'download_tag', '', ', ', '' );
		$frame = get_the_term_list( $download_id, 'framework', '', ', ', '' );
		$plugins = get_the_term_list( $download_id, 'plugins', '', ', ', '' );
		$fformats = get_the_term_list( $download_id, 'fileformats', '', ', ', '' );
		$itemversion = get_post_meta( $download_id, '_cmb2_marketplace_item_version', true );
		$highres = get_post_meta( $download_id, '_cmb2_marketplace_item_highres', true );
		$responsive = get_post_meta( $download_id, '_cmb2_marketplace_item_responsive', true );
		$widget = get_post_meta( $download_id, '_cmb2_marketplace_item_widget', true );
		$retina = get_post_meta( $download_id, '_cmb2_marketplace_item_retina', true );
		$browser = get_post_meta( $download_id, '_cmb2_marketplace_item_browser', true );
		$filesize = get_post_meta( $download_id, '_cmb2_marketplace_item_filesize', true );
		$support = get_post_meta( $download_id, '_cmb2_marketplace_item_support', true );
		$support_url = get_post_meta( $download_id, '_cmb2_marketplace_item_support_url', true );
		$changelog = get_post_meta( $download_id, '_cmb2_marketplace_item_changelog_url', true );
		$demo_url = get_post_meta( $download_id, '_cmb2_marketplace_demo_url', true );
		$doc_url = get_post_meta( $download_id, '_cmb2_marketplace_item_documentation_url', true );
		
		
		echo '<table class="mp-file-information-widget">';
		
			if( $show_releasedate === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Release date:', 'kiwi' ).'</td>
					<td itemprop="dateCreated datePublished">'. get_post_time('M j, Y', true).'</td>
				</tr>'; 
			}
			
			if( $show_modifieddate === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Last updated:', 'kiwi' ).'</td>
					<td itemprop="dateModified">'. get_post_modified_time('M j, Y', true).'</td>
				</tr>'; 
			}
			
			if( $itemversion && $show_itemversion === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Item Version:', 'kiwi' ).'</td>
					<td>'. esc_html( $itemversion ).'</td>
				</tr>'; 
			}
			
			if( $filesize && $show_filesize === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'File size:', 'kiwi' ).'</td>
					<td>'. esc_html( $filesize ).'</td>
				</tr>'; 
			}
		
			if( $highres && $show_highres === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'High Resolution:', 'kiwi' ).'</td>
					<td>'. esc_html( $highres ).'</td>
				</tr>'; 
			}
		
			if( $responsive && $show_responsive === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Fully responsive:', 'kiwi' ).'</td>
					<td>'. esc_html( $responsive ).'</td>
				</tr>'; 
			}
		
			if( $widget && $show_widgetready === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Widget Ready:', 'kiwi' ).'</td>
					<td>'. esc_html( $widget ).'</td>
				</tr>'; 
			}
			
			if( $retina && $show_retinaready === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Retina-Ready:', 'kiwi' ).'</td>
					<td>'. esc_html( $retina ).'</td>
				</tr>'; 
			}
		
			if( $browser && $show_browser === 1 ) {
				$browser_res = array();
				foreach($browser as $key => $value) {
					//do something
					$browser_res[] = $value;
				}
				echo '<tr>
				<td>'.esc_html__( 'Browsers Compatibility:', 'kiwi' ).'</td><td>';
				echo implode(', ', $browser_res);
				echo '</td>
				</tr>';
			} 
		
		
			if( $plugins && $show_plugincom === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Plugins Compatibility:', 'kiwi' ).'</td>
					<td>'. $plugins.'</td>
				</tr>'; 
			}
			
			if( $frame && $show_frameworkcom === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Framework Compatibility:', 'kiwi' ).'</td>
					<td>'. $frame.'</td>
				</tr>'; 
			}
			
			if( $fformats && $show_fileformats === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'File formats:', 'kiwi' ).'</td>
					<td>'. $fformats.'</td>
				</tr>'; 
			}
						
			if( $support && $show_itemsupport === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Item Support:', 'kiwi' ).'</td>
					<td>'. esc_html( $support ).'</td>
				</tr>'; 
			}	
			
			if( $support_url && $show_supporturl === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Support:', 'kiwi' ).'</td>
					<td><a href="'. esc_url( $support_url ).'" target="_blank">'.esc_html__( 'Link', 'kiwi' ).'</a></td>
				</tr>'; 
			}
			
			if( $changelog && $show_changelogurl === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Changelog:', 'kiwi' ).'</td>
					<td><a href="'. esc_url( $changelog ).'" target="_blank">'.esc_html__( 'Link', 'kiwi' ).'</a></td>
				</tr>'; 
			}			
			
			if( $doc_url && $show_documentation === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Documentation:', 'kiwi' ).'</td>
					<td><a href="'. esc_url( $doc_url ).'" target="_blank">'.esc_html__( 'Link', 'kiwi' ).'</a></td>
				</tr>'; 
			}
			
			if( $catlist && $show_itemcat === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Item Category:', 'kiwi' ).'</td>
					<td>'. $catlist.'</td>
				</tr>'; 
			}
			
			if( $taglist && $show_itemtags === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Item Tags:', 'kiwi' ).'</td>
					<td>'. $taglist.'</td>
				</tr>'; 
			}
			
			if( $demo_url && $show_demourl === 1 ) {
				echo '<tr>
					<td>'.esc_html__( 'Demo:', 'kiwi' ).'</td>
					<td><a href="'. esc_url( $demo_url ).'" target="_blank">'.esc_html__( 'Link', 'kiwi' ).'</a></td>
				</tr>'; 
			}

			echo '</table>';
			
        
        do_action( 'edd_product_fileinfo_before_end', $instance, $download_id );

        // Used by themes. Closes the widget
        echo $args['after_widget'];
    }

   	/** @see WP_Widget::form */
    public function form( $instance ) {
        // Set up some default widget settings.
        $defaults = array(
            'title' 			=> sprintf( esc_html__( '%s Details', 'kiwi' ), edd_get_label_singular() ),
            'download_id' 		=> 'current',
        );

        $instance = wp_parse_args( (array) $instance, $defaults ); 
		
		
		$show_releasedate = isset( $instance['show_releasedate'] ) ? esc_attr( $instance['show_releasedate'] ) : 0; 
		$show_modifieddate = isset( $instance['show_modifieddate'] ) ? esc_attr( $instance['show_modifieddate'] ) : 0; 
		$show_itemversion = isset( $instance['show_itemversion'] ) ? esc_attr( $instance['show_itemversion'] ) : 0; 
		$show_filesize = isset( $instance['show_filesize'] ) ? esc_attr( $instance['show_filesize'] ) : 0; 
		$show_highres = isset( $instance['show_highres'] ) ? esc_attr( $instance['show_highres'] ) : 0; 
		$show_responsive = isset( $instance['show_responsive'] ) ? esc_attr( $instance['show_responsive'] ) : 0; 
		$show_widgetready = isset( $instance['show_widgetready'] ) ? esc_attr( $instance['show_widgetready'] ) : 0; 
		$show_retinaready = isset( $instance['show_retinaready'] ) ? esc_attr( $instance['show_retinaready'] ) : 0; 
		$show_browser = isset( $instance['show_browser'] ) ? esc_attr( $instance['show_browser'] ) : 0; 
		$show_plugincom = isset( $instance['show_plugincom'] ) ? esc_attr( $instance['show_plugincom'] ) : 0; 
		$show_frameworkcom = isset( $instance['show_frameworkcom'] ) ? esc_attr( $instance['show_frameworkcom'] ) : 0; 
		$show_fileformats = isset( $instance['show_fileformats'] ) ? esc_attr( $instance['show_fileformats'] ) : 0; 
		$show_itemsupport = isset( $instance['show_itemsupport'] ) ? esc_attr( $instance['show_itemsupport'] ) : 0; 
		$show_supporturl = isset( $instance['show_supporturl'] ) ? esc_attr( $instance['show_supporturl'] ) : 0; 
		$show_changelogurl = isset( $instance['show_changelogurl'] ) ? esc_attr( $instance['show_changelogurl'] ) : 0; 
		$show_documentation = isset( $instance['show_documentation'] ) ? esc_attr( $instance['show_documentation'] ) : 0; 
		$show_itemcat = isset( $instance['show_itemcat'] ) ? esc_attr( $instance['show_itemcat'] ) : 0; 
		$show_itemtags = isset( $instance['show_itemtags'] ) ? esc_attr( $instance['show_itemtags'] ) : 0; 
		$show_demourl = isset( $instance['show_demourl'] ) ? esc_attr( $instance['show_demourl'] ) : 0;
		
		
		
		?>
        
        <!-- Title -->
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ) ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo $instance['title']; ?>" />
        </p>

			<!-- Download -->
         <?php 
            $args = array( 
	            'post_type'      => 'download', 
	            'posts_per_page' => -1, 
	            'post_status'    => 'publish', 
	        );
	        $downloads = get_posts( $args );
			
			
        ?>
         <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'download_id' ) ); ?>"><?php printf( esc_html__( '%s', 'kiwi' ), edd_get_label_singular() ); ?></label>
            <select class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'download_id' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'download_id' ) ); ?>">
            	<option value="current"><?php esc_html_e( 'Use current', 'kiwi' ); ?></option>
            </select>
        </p> 
		
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_releasedate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_releasedate' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_releasedate ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_releasedate' ) ); ?>"><?php esc_html_e( 'Display release date?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_modifieddate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_modifieddate' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_modifieddate ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_modifieddate' ) ); ?>"><?php esc_html_e( 'Display modified date?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_itemversion' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_itemversion' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_itemversion ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_itemversion' ) ); ?>"><?php esc_html_e( 'Display item version?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_filesize' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_filesize' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_filesize ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_filesize' ) ); ?>"><?php esc_html_e( 'Display file size?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_highres' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_highres' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_highres ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_highres' ) ); ?>"><?php esc_html_e( 'Display high resolution?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_responsive' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_responsive' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_responsive ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_responsive' ) ); ?>"><?php esc_html_e( 'Display responsive?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_widgetready' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_widgetready' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_widgetready ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_widgetready' ) ); ?>"><?php esc_html_e( 'Display widget ready?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_retinaready' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_retinaready' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_retinaready ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_retinaready' ) ); ?>"><?php esc_html_e( 'Display retina ready?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_browser' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_browser' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_browser ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_browser' ) ); ?>"><?php esc_html_e( 'Display browser?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_plugincom' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_plugincom' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_plugincom ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_plugincom' ) ); ?>"><?php esc_html_e( 'Display plugin compatibility?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_frameworkcom' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_frameworkcom' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_frameworkcom ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_frameworkcom' ) ); ?>"><?php esc_html_e( 'Display framework compatibility?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_fileformats' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_fileformats' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_fileformats ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_fileformats' ) ); ?>"><?php esc_html_e( 'Display file formats?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_itemsupport' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_itemsupport' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_itemsupport ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_itemsupport' ) ); ?>"><?php esc_html_e( 'Display item support?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_supporturl' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_supporturl' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_supporturl ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_supporturl' ) ); ?>"><?php esc_html_e( 'Display support url?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_changelogurl' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_changelogurl' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_changelogurl ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_changelogurl' ) ); ?>"><?php esc_html_e( 'Display changelog?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_documentation' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_documentation' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_documentation ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_documentation' ) ); ?>"><?php esc_html_e( 'Display documentation?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_itemcat' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_itemcat' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_itemcat ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_itemcat' ) ); ?>"><?php esc_html_e( 'Display item categories?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo $this->get_field_id( 'show_itemtags' ); ?>" name="<?php echo $this->get_field_name( 'show_itemtags' ); ?>" type="checkbox" value="1" <?php checked( '1', $show_itemtags ); ?>/>
			<label for="<?php echo $this->get_field_id( 'show_itemtags' ); ?>"><?php esc_html_e( 'Display item tags?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo $this->get_field_id( 'show_demourl' ); ?>" name="<?php echo $this->get_field_name( 'show_demourl' ); ?>" type="checkbox" value="1" <?php checked( '1', $show_demourl ); ?>/>
			<label for="<?php echo $this->get_field_id( 'show_demourl' ); ?>"><?php esc_html_e( 'Display demo url?', 'kiwi' ); ?></label> 
		</p>
		
        
        
        <?php do_action( 'edd_product_item_stats_form' , $instance ); ?>
    <?php }

    /** @see WP_Widget::update */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title']           = strip_tags( $new_instance['title'] );
        $instance['download_id']     = strip_tags( $new_instance['download_id'] );		
		
		$instance['show_releasedate'] = strip_tags( $new_instance['show_releasedate'] );
        $instance['show_releasedate'] = $instance['show_releasedate'] === '1' ? 1 : 0;
		
		$instance['show_modifieddate'] = strip_tags( $new_instance['show_modifieddate'] );
        $instance['show_modifieddate'] = $instance['show_modifieddate'] === '1' ? 1 : 0;
		
		$instance['show_itemversion'] = strip_tags( $new_instance['show_itemversion'] );
        $instance['show_itemversion'] = $instance['show_itemversion'] === '1' ? 1 : 0;
		
		$instance['show_filesize'] = strip_tags( $new_instance['show_filesize'] );
        $instance['show_filesize'] = $instance['show_filesize'] === '1' ? 1 : 0;
		
		$instance['show_highres'] = strip_tags( $new_instance['show_highres'] );
        $instance['show_highres'] = $instance['show_highres'] === '1' ? 1 : 0;
		
		$instance['show_responsive'] = strip_tags( $new_instance['show_responsive'] );
        $instance['show_responsive'] = $instance['show_responsive'] === '1' ? 1 : 0;
		
		$instance['show_widgetready'] = strip_tags( $new_instance['show_widgetready'] );
        $instance['show_widgetready'] = $instance['show_widgetready'] === '1' ? 1 : 0;
		
		$instance['show_retinaready'] = strip_tags( $new_instance['show_retinaready'] );
        $instance['show_retinaready'] = $instance['show_retinaready'] === '1' ? 1 : 0;
		
		$instance['show_browser'] = strip_tags( $new_instance['show_browser'] );
        $instance['show_browser'] = $instance['show_browser'] === '1' ? 1 : 0;
		
		$instance['show_plugincom'] = strip_tags( $new_instance['show_plugincom'] );
        $instance['show_plugincom'] = $instance['show_plugincom'] === '1' ? 1 : 0;
		
		$instance['show_frameworkcom'] = strip_tags( $new_instance['show_frameworkcom'] );
        $instance['show_frameworkcom'] = $instance['show_frameworkcom'] === '1' ? 1 : 0;
		
		$instance['show_fileformats'] = strip_tags( $new_instance['show_fileformats'] );
        $instance['show_fileformats'] = $instance['show_fileformats'] === '1' ? 1 : 0;
		
		$instance['show_itemsupport'] = strip_tags( $new_instance['show_itemsupport'] );
        $instance['show_itemsupport'] = $instance['show_itemsupport'] === '1' ? 1 : 0;
		
		$instance['show_supporturl'] = strip_tags( $new_instance['show_supporturl'] );
        $instance['show_supporturl'] = $instance['show_supporturl'] === '1' ? 1 : 0;
		
		$instance['show_changelogurl'] = strip_tags( $new_instance['show_changelogurl'] );
        $instance['show_changelogurl'] = $instance['show_changelogurl'] === '1' ? 1 : 0;
		
		$instance['show_documentation'] = strip_tags( $new_instance['show_documentation'] );
        $instance['show_documentation'] = $instance['show_documentation'] === '1' ? 1 : 0;
		
		$instance['show_itemcat'] = strip_tags( $new_instance['show_itemcat'] );
        $instance['show_itemcat'] = $instance['show_itemcat'] === '1' ? 1 : 0;
		
		$instance['show_itemtags'] = strip_tags( $new_instance['show_itemtags'] );
        $instance['show_itemtags'] = $instance['show_itemtags'] === '1' ? 1 : 0;
		
		$instance['show_demourl'] = strip_tags( $new_instance['show_demourl'] );
        $instance['show_demourl'] = $instance['show_demourl'] === '1' ? 1 : 0;
        
        do_action( 'edd_product_item_stats_update', $instance );
        
        return $instance;
    } 

}





/*===============================================================
		Register Widget
===============================================================*/
function mp_register_widget_fileinfo() {
	register_widget('Marketplace_File_Info');
}
add_action( 'widgets_init', 'mp_register_widget_fileinfo' );
<?php
/*===============================================================
	Widget :: Marketplace :: Purchase block 2
===============================================================*/
class Marketplace_Widget_Details_Two extends WP_Widget {

    /** Constructor */
	public function __construct() {
		parent::__construct(
			'edd_product_details widget-style-two', sprintf( esc_html__( 'Marketplace :: Purchase block 2', 'kiwi' ), edd_get_label_singular() ), array( 'description' => sprintf( esc_html__( 'Display the purchase block of a specific item', 'kiwi' ), edd_get_label_singular() ), )
		);
	}

/** @see WP_Widget::widget */
    public function widget( $args, $instance ) {

		if ( ! is_singular( 'download' )  ) {
        	return;
        }

		global $kiwi_theme_option;

		// set correct download ID
        if ( 'current' == $instance['download_id'] && is_singular( 'download' ) ) {
        	$download_id = get_the_ID();
        } else {
        	$download_id = absint( $instance['download_id'] );
        }

		if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && edd_coming_soon_is_active() ) {
			$variable_class = '<div class="mp-comingsoon-class-active">';
			$variable_class_end = '</div>';
		} else {
			$variable_class = '<div itemscope itemtype="http://schema.org/Product"><span itemprop="name" class="mp-ande">' . get_the_title( get_the_ID() ) . '</span>';
			$variable_class_end = '</div>';
		}

		$args['id'] = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_download_details_widget';

        /* if ( ! isset( $instance['download_id'] ) || ( 'current' == $instance['download_id'] && ! is_singular( 'download' ) ) ) {
        	return;
        } */

		if ( edd_display_tax_rate() && edd_prices_include_tax() || edd_display_tax_rate() && ! edd_prices_include_tax() ) {
			$tax_enabled = ' mp-tax-enabled';
		} else {
			$tax_enabled = ' mp-tax-disabled';
		}



        // Variables from widget settings
		remove_filter( 'edd_purchase_download_form', 'mp_filter_checkout' );

		remove_filter( 'edd_cs_vote_description', 'mp_shortcode_removal_comingsoon' );
		remove_filter( 'edd_cs_vote_submission', 'mp_shortcode_removal_comingsoon' );
		remove_filter( 'edd_coming_soon_voted_message', 'mp_shortcode_removal_comingsoon' );

        // Used by themes. Opens the widget
        echo $args['before_widget'];

		echo $variable_class;

        do_action( 'edd_purchase_widget_before_title' , $instance , $download_id );

     	$price = get_post_meta( get_the_ID(), 'edd_price', true );



	if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) {
		/* Coming Soon Plugin */
		$cs_active = edd_coming_soon_is_active( get_the_ID() );
		$votes_enabled = edd_coming_soon_voting_enabled( get_the_ID() );
		$votes = get_post_meta( get_the_ID(), '_edd_coming_soon_votes', true );

		$custom_text = get_post_meta( get_the_ID(), 'edd_coming_soon_text', true );
		$custom_text = !empty ( $custom_text ) ? $custom_text : apply_filters( 'edd_cs_coming_soon_text', esc_html__( 'Coming Soon', 'kiwi' ) );


			if ( 0 == $price && !edd_coming_soon_is_active() ){
				echo '<span class="price free">';

					if ( $kiwi_theme_option['marketplace-free-enable'] == '1' ) {
						echo $kiwi_theme_option['marketplace-free-customtext'];
					} else {
						echo edd_price( get_the_ID(), false );
					}
				echo '</span>';
			}  elseif ( edd_coming_soon_is_active() ){

				echo apply_filters( 'edd_coming_soon_display_text', '<p><strong>' . $custom_text . '</strong></p>' );

					if ( $cs_active && ( $votes_enabled ) ) {
						echo '<div class="mp-votes">';
						echo sprintf( _n( '%s vote', '%s votes', $votes, 'kiwi' ), $votes );
						echo '</div>';
					}
			}  else {
					// echo '<div class="price-button div-flex" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
					// echo '<meta itemprop="priceCurrency" content="' . edd_get_currency() . '" />';
					// echo '<meta itemprop="price" content="' . edd_format_amount( edd_get_download_price( get_the_ID() ) ) . '" />';
					// echo '<span class="price left-column align-left">' . edd_price( get_the_ID(), false ) . '</span>';
					//
					// echo '<div class="right-column align-right'.$tax_enabled.'">';
					// echo '<span class="text">'. esc_html__( 'Item Price', 'kiwi' ).'</span>';
					//
					// if ( edd_display_tax_rate() && edd_prices_include_tax() ) {
					// 	echo '<span class="edd_purchase_tax_rate">' . sprintf( __( 'VAT %1$s&#37; included', 'kiwi' ), edd_get_tax_rate() * 100 ) . '</span>';
					// } elseif ( edd_display_tax_rate() && ! edd_prices_include_tax() ) {
					// 	echo '<span class="edd_purchase_tax_rate">' . sprintf( __( 'VAT %1$s&#37; applicable', 'kiwi' ), edd_get_tax_rate() * 100 ) . '</span>';
					// }
					//
					// echo '</div>';

					?>
					<div class="custom-box" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
						<meta itemprop="priceCurrency" content="<?php echo edd_get_currency();?>" />
						<meta itemprop="price" content="<?php echo edd_format_amount( edd_get_download_price( get_the_ID() ) );?>" />
						<li class="custom-header"><?php echo esc_html__( 'Item Price', 'kiwi' ); ?></li>
						<li class="custom-body"><?php echo edd_price( get_the_ID(), false );?></li>
					</div>
					<?php
					//echo '<meta itemprop="name" content="' . get_the_title( get_the_ID() ) . '">';


				echo '</div><div class="clear"></div>';
			}

			if( $kiwi_theme_option['marketplace-disclaimer-enable'] == '1' && !edd_coming_soon_is_active() ) {
				echo '<div class="disclaimer">' . $kiwi_theme_option['marketplace-disclaimer'] . '</div>';
			}


			if ( class_exists( 'EDD_Points_Renderer' ) ) {
				echo '<div class="mp-points-rewards">' . do_action( 'mp_edd_points_rewards', get_the_ID() ) . '</div>';
			}


	}


	if ( !is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) {

		if ( 0 == $price ){
			echo '<span class="price free">';

					if ( $kiwi_theme_option['marketplace-free-enable'] == '1' ) {
						echo $kiwi_theme_option['marketplace-free-customtext'];
					} else {
						echo edd_price( get_the_ID(), false );
					}
				echo '</span>';
		}  else {
				echo '<div class="price-button div-flex" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
				echo '<meta itemprop="priceCurrency" content="' . edd_get_currency() . '" />';
				echo '<meta itemprop="price" content="' . edd_format_amount( edd_get_download_price( get_the_ID() ) ) . '" />';
				echo '<span class="price left-column align-left">' . edd_price( get_the_ID(), false ) . '</span>';
				echo '<div class="right-column align-right'.$tax_enabled.'">';
					echo '<span class="text">'. esc_html__( 'Item Price', 'kiwi' ).'</span>';

					if ( edd_display_tax_rate() && edd_prices_include_tax() ) {
						echo '<span class="edd_purchase_tax_rate">' . sprintf( __( 'VAT %1$s&#37; included', 'kiwi' ), edd_get_tax_rate() * 100 ) . '</span>';
					} elseif ( edd_display_tax_rate() && ! edd_prices_include_tax() ) {
						echo '<span class="edd_purchase_tax_rate">' . sprintf( __( 'VAT %1$s&#37; applicable', 'kiwi' ), edd_get_tax_rate() * 100 ) . '</span>';
					}

				echo '</div>';
				?>
				<div class="custom-box" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					<meta itemprop="priceCurrency" content="<?php echo edd_get_currency();?>" />
					<meta itemprop="price" content="<?php echo edd_format_amount( edd_get_download_price( get_the_ID() ) );?>" />
					<li class="custom-header"><?php echo esc_html__( 'Item Price', 'kiwi' ); ?></li>
					<li class="custom-body"><?php echo edd_price( get_the_ID(), false );?></li>
				</div>
				<?php
				//echo '<meta itemprop="name" content="' . get_the_title( get_the_ID() ) . '">';
			echo '</div><div class="clear"></div>';
		}

		if( $kiwi_theme_option['marketplace-disclaimer-enable'] == '1' ) {
            echo '<div class="disclaimer">' . $kiwi_theme_option['marketplace-disclaimer'] . '</div>';
		}
	}


        do_action( 'edd_purchase_widget_before_purchase_button' , $instance , $download_id );

		do_action( 'mp_purchase_widget_before_button');

		// purchase button
 		#---TAKEXPERT start changelog---#
 		/*
 		echo edd_get_purchase_link( array( 'price' => '0', 'class' => 'addcart', 'download_id' => get_the_ID() ) );
 		echo '<div class="clear"></div>';
		*/
		#---TAKEXPERT stop changelog---#
    	echo '<div class="clear"></div>';

		do_action( 'mp_purchase_widget_after_button');
		do_action( 'edd_product_details_widget_after_purchase_button' , $instance , $download_id );

		do_action('takexpert_dynamic_edd_purchase_button');


		echo $variable_class_end;

        // Used by themes. Closes the widget
        echo $args['after_widget'];
    }

   	/** @see WP_Widget::form */
    public function form( $instance ) {
        // Set up some default widget settings.
        $defaults = array(
            'download_id' 		=> 'current'
        );

        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <!-- Show purchase button -->
        <p><?php esc_html_e( 'No options to choose from.', 'kiwi' )?></p>

        <?php do_action( 'edd_purchase_widget_form' , $instance ); ?>

    <?php }

    /** @see WP_Widget::update */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title']           = strip_tags( $new_instance['title'] );
        $instance['download_id']     = strip_tags( $new_instance['download_id'] );

        do_action( 'edd_purchase_widget_update', $instance );

        return $instance;
    }

}

<?php

class Marketplace_Fes_Specs extends WP_Widget {

    /** Constructor */
	public function __construct() {
			parent::__construct( 'Marketplace_Fes_Specs', esc_html__(  'Marketplace :: FES Specs', 'kiwi' ), 
			array( 'description' => esc_html__(  'Display submission fields.', 'kiwi' ) ) );
		}	


    /** @see WP_Widget::widget */
    public function widget( $args, $instance ) {
		$args['id'] = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_download_item_stats';

        if ( ! is_singular( 'download' ) ) {
        	return;
        }

       	// set correct download ID
        if ( is_singular( 'download' ) ) {
        	$download_id = get_the_ID();
        } else {
        	$download_id = absint( $instance['download_id'] );
        }

        // Variables from widget settings
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );
		
		$show_releasedate = isset( $instance['show_releasedate'] ) && $instance['show_releasedate'] === 1 ? 1 : 0;
		$show_modifieddate = isset( $instance['show_modifieddate'] ) && $instance['show_modifieddate'] === 1 ? 1 : 0;
		$show_itemcat = isset( $instance['show_itemcat'] ) && $instance['show_itemcat'] === 1 ? 1 : 0; 
		$show_itemtags = isset( $instance['show_itemtags'] ) && $instance['show_itemtags'] === 1 ? 1 : 0;
		$show_changelog = isset( $instance['show_changelog'] ) && $instance['show_changelog'] === 1 ? 1 : 0;
		
		
		remove_filter( "term_links-download_category", 'limit_terms');
		
		$catlist = get_the_term_list( $download_id, 'download_category', '', ', ', '' );
		$taglist = get_the_term_list( $download_id, 'download_tag', '', ', ', '' );
		$changelog = get_post_meta( $download_id, 'changelog_url', true );
		
		
        // Used by themes. Opens the widget
        echo $args['before_widget'];

        // Display the widget title
        if( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
		}
		
       global $post;

		$show_custom = EDD_FES()->helper->get_option( 'fes-show-custom-meta', false );
		$form_id     = EDD_FES()->helper->get_option( 'fes-submission-form', false );

		if ( !$show_custom || !$form_id ) {
			echo '';
		}
		
		//$form = EDD_FES()->helper->get_form_by_id( $form_id, $post->ID );
		$form = EDD_FES()->helper->get_form_by_id( $form_id, $download_id );
		
		$html = $form->display_fields();
		
		
		echo '<table class="mp-fes-file-information-widget">';
		
			if( $show_releasedate === 1 ) {
				echo '<tr class="table-releasedate">
					<td>'.esc_html__(  'Release date:', 'kiwi' ).'</td>
					<td itemprop="dateCreated datePublished">'. esc_attr( date_i18n( get_option( 'date_format' ), get_post_time( 'U' ) ) ).'</td>
				</tr>'; 
			}
			
			if( $show_modifieddate === 1 ) {
				echo '<tr class="table-lastupldated">
					<td>'.esc_html__(  'Last updated:', 'kiwi' ).'</td>
					<td itemprop="dateModified">'. esc_attr( date_i18n( get_option( 'date_format' ), get_post_modified_time( 'U' ) ) ).'</td>
				</tr>'; 
			}			
			
		echo '</table>';
		echo '<div class="clear"></div>';		
		
		echo $html;
				
		echo '<table class="mp-fes-file-information-widget second">';
			
			if( $catlist && $show_itemcat === 1 ) {
				echo '<tr class="table-categories">
					<td>'.esc_html__(  'Item Category:', 'kiwi' ).'</td>
					<td>'. $catlist.'</td>
				</tr>'; 
			}
			
			if( $taglist && $show_itemtags === 1 ) {
				echo '<tr class="table-itemtags">
					<td>'.esc_html__(  'Item Tags:', 'kiwi' ).'</td>
					<td>'. $taglist.'</td>
				</tr>'; 
			}
			
			if( $changelog && $show_changelog && $show_changelog === 1 ) {
				echo '<tr class="table-changelog">
					<td>'.esc_html__(  'Changelog:', 'kiwi' ).'</td>
					<td><a href="'. esc_url( $changelog ).'" target="_blank">'.esc_html__(  'Link', 'kiwi' ).'</a></td>
				</tr>'; 
			} 
			
		echo '</table>';
		echo '<div class="clear"></div>';

        // Used by themes. Closes the widget
        echo $args['after_widget'];
    }

   	/** @see WP_Widget::form */
    public function form( $instance ) {
        // Set up some default widget settings.
        $defaults = array(
            'title' 			=> sprintf( esc_html__(  '%s Details', 'kiwi' ), edd_get_label_singular() ),
            'download_id' 		=> 'current',
        );
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$show_releasedate = isset( $instance['show_releasedate'] ) ? esc_attr( $instance['show_releasedate'] ) : 0; 
		$show_modifieddate = isset( $instance['show_modifieddate'] ) ? esc_attr( $instance['show_modifieddate'] ) : 0;
		$show_itemcat = isset( $instance['show_itemcat'] ) ? esc_attr( $instance['show_itemcat'] ) : 0; 
		$show_itemtags = isset( $instance['show_itemtags'] ) ? esc_attr( $instance['show_itemtags'] ) : 0;
		$show_changelog = isset( $instance['show_changelog'] ) ? esc_attr( $instance['show_changelog'] ) : 0;
		
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'kiwi' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_releasedate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_releasedate' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_releasedate ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_releasedate' ) ); ?>"><?php esc_html_e( 'Display release date?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_modifieddate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_modifieddate' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_modifieddate ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_modifieddate' ) ); ?>"><?php esc_html_e( 'Display modified date?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_itemcat' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_itemcat' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_itemcat ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_itemcat' ) ); ?>"><?php esc_html_e( 'Display item categories?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_itemtags' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_itemtags' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_itemtags ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_itemtags' ) ); ?>"><?php esc_html_e( 'Display item tags?', 'kiwi' ); ?></label> 
		</p>
		<p>
			<input id="<?php echo esc_attr( $this->get_field_id( 'show_changelog' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_changelog' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_changelog ); ?>/>
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_changelog' ) ); ?>"><?php esc_html_e( 'Show changelog URL?', 'kiwi' ); ?></label> 
		</p>   
        <?php do_action( 'edd_product_fes_stats_form' , $instance ); ?>
		
    <?php }

    /** @see WP_Widget::update */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title']           = strip_tags( $new_instance['title'] );
		$instance['show_releasedate'] = strip_tags( $new_instance['show_releasedate'] );
        $instance['show_releasedate'] = $instance['show_releasedate'] === '1' ? 1 : 0;
		
		$instance['show_modifieddate'] = strip_tags( $new_instance['show_modifieddate'] );
        $instance['show_modifieddate'] = $instance['show_modifieddate'] === '1' ? 1 : 0;
		
		$instance['show_itemcat'] = strip_tags( $new_instance['show_itemcat'] );
        $instance['show_itemcat'] = $instance['show_itemcat'] === '1' ? 1 : 0;
		
		$instance['show_itemtags'] = strip_tags( $new_instance['show_itemtags'] );
        $instance['show_itemtags'] = $instance['show_itemtags'] === '1' ? 1 : 0;
		
		$instance['show_changelog'] = strip_tags( $new_instance['show_changelog'] );
        $instance['show_changelog'] = $instance['show_changelog'] === '1' ? 1 : 0;
		
        do_action( 'edd_product_fes_stats_update', $instance );
        
        return $instance;
    } 

}
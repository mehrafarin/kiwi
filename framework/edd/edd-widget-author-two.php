<?php 
/*===============================================================
	Widget :: Marketplace :: Author widget 2
===============================================================*/
class Marketplace_Author_Widget_Two extends WP_Widget {
     
    function __construct() {
			parent::__construct( 'edd_author_widget_two', esc_html__( 'Marketplace :: Author 2', 'kiwi' ), 
			array( 'description' => esc_html__( 'Display author info.', 'kiwi' ) ) );
		}


	function widget( $args, $instance ) {
		
		global $post;
		
		$title = apply_filters( 'widget_title', $instance['title'] ? $instance['title'] : '' );	
         
		 if ( ! is_singular( 'download' )  ) {
        	return;
        }
		
		$post_id 			= $post->ID;
		
		$post_author_id 	= get_post_field( 'post_author', $post_id );
		$post_count 		= count_user_posts( $post_author_id , "download"  );
		
		$name 				= get_the_author_meta('display_name', $post->post_author);
		$website 			= get_the_author_meta('user_url', $post->post_author);
		
		$description1 		= get_the_author_meta( 'description', $post->post_author );
		$description 		= wp_trim_words( $description1, 20, '...' );		
		
		$date  				= date_i18n( get_option('date_format'), strtotime( get_the_author_meta( 'user_registered', $post_author_id ) ) );		
		$avatar				= get_avatar( $post->post_author, 120 );
		
		echo $args['before_widget'];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
				
		if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
			$constant = EDD_FES()->helper->get_option( 'fes-vendor-constant', '' );
			$constant = ( isset( $constant ) && $constant != '' ) ? $constant : esc_html__( 'vendor', 'kiwi' );
            $constant = apply_filters( 'fes_vendor_constant_singular_lowercase', $constant ); 

		/* Vendor Specs */		
		//$author = get_user_by( 'slug', get_query_var( 'vendor' ) );
		//$post_author_id 	= get_post_field( 'post_author', $post_id );
		$fes_portfolio 				= EDD_FES()->vendors->get_all_products_count($post_author_id, 'publish'); 		
		$vendor_location 			= get_the_author_meta( 'your_location', $post_author_id );
		$vendor_collaboration_old 	= get_the_author_meta( 'available_for_collaboration_and_co-authoring?', $post_author_id );		
		$vendor_collaboration 		= get_the_author_meta( 'available_for_collaboration_and_co-authoring', $post_author_id );
		$vendor_freelance_old 		= get_the_author_meta( 'available_for_freelance_work?', $post_author_id );
		$vendor_freelance 			= get_the_author_meta( 'available_for_freelance_work', $post_author_id );
		$vendor_hourlyrate 			= get_the_author_meta( 'hourly_rate', $post_author_id );		
		$vendor_revenue 			= get_the_author_meta( 'sales_share_revenue', $post_author_id );
		
		}		
		/****/		
		 
		do_action( 'edd_before_author_widget' );
				
			
			echo '<div class="widget-style-two author">';
			
			echo '<div class="author-avatar">'.$avatar.'</div>'; 
						
			if ( class_exists( 'EDD_Front_End_Submissions' ) ) {					 
				$vendor_name = get_the_author_meta( 'user_login' );
				echo '<h4><a rel="author" href="'. esc_url( site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name) ).'">'. esc_html( $name ) .'</a></h4>'; 
			} else {
				echo '<h4 rel="author">'.esc_html( $name ).'</h4>';
			}
			
			echo '<div class="author-location">';	
						
				if ( isset($vendor_location) && !empty( $vendor_location ) ) {
					echo '<i class="fa fa-map-marker"></i> '. esc_html( $vendor_location ).'</span>';
				} else {
					echo '';
				}	
			
			echo '</div>';	
			
			if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
				echo '<a class="portfolio-link" href="'.site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name).'">'. esc_html__( 'View Portfolio', 'kiwi' ).'</a>';
			} 
			
			echo '<table class="table author-specs">
				  <tbody>					
					<tr>          
					  <td>'. esc_html__( 'Joined', 'kiwi' ).'</td>
					  <td>'. esc_html( $date ).'</td>
					</tr>';
					
				if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
					echo '<tr>          
					  <td>'. esc_html__( 'Portfolio', 'kiwi' ).'</td>
					  <td>'. sprintf( _n( '%s item', '%s items', $fes_portfolio, 'kiwi' ), $fes_portfolio ) .'</td>					  
					</tr>';
				} else {
					echo '<tr>          
					  <td>'. esc_html__( 'Portfolio', 'kiwi' ).'</td>
					  <td>'. sprintf( _n( '%s item', '%s items', $post_count, 'kiwi' ), $post_count ) .'</td>
					</tr>';
				}

					echo '<tr>          
					  <td>'. esc_html__( 'Website', 'kiwi' ).'</td>
					  <td><a href="'.esc_url( $website ) .'" target="_blank">'.esc_html__( 'Link', 'kiwi' ).'</a></td>
					</tr>					
				  </tbody>
				</table>';
			
			
			echo '<div class="clear"></div>';
			echo '</div>';
			
			
			
	if ( !empty( $vendor_collaboration ) ||  !empty( $vendor_freelance ) || ! empty( $vendor_collaboration_old ) || !empty( $vendor_collaboration ) ||  !empty( $vendor_freelance_old ) || ! empty( $vendor_collaboration_old ) ) {
		
			echo '<div class="widget-style-two author author-freelancework">';
			
			if ( isset($vendor_collaboration) && !empty( $vendor_collaboration ) && empty( $vendor_revenue ) || isset($vendor_collaboration_old) && !empty( $vendor_collaboration ) && empty( $vendor_revenue )) {
				echo '<div class="first">';
				echo esc_html__('Author is available for collaboration and co-authoring.', 'kiwi');
				echo '</div>';
			} else {
				echo '';
			}
			
			if ( isset($vendor_collaboration) && !empty( $vendor_collaboration ) && isset($vendor_revenue) && !empty( $vendor_revenue ) || isset($vendor_collaboration_old) && !empty( $vendor_collaboration ) && isset($vendor_revenue) && !empty( $vendor_revenue ) ) {
				echo '<div class="first">';
				printf( esc_html__( 'Author is available for collaboration and co-authoring and wish to receive a %1$s percent sales share revenue depending on the work.', 'kiwi' ), '<span>' . $vendor_revenue . '</span>' );
				echo '</div>';
			} else {
				echo '';
			}
			
			if ( isset($vendor_freelance) && !empty( $vendor_freelance ) && empty( $vendor_hourlyrate ) || isset($vendor_freelance_old) && !empty( $vendor_freelance_old ) && empty( $vendor_hourlyrate )) {
				echo '<div>'.esc_html__('Author is available for freelance work.', 'kiwi').'</div>';
			} else {
				echo '';
			}
			
			if ( isset($vendor_freelance) && !empty( $vendor_freelance ) && isset($vendor_hourlyrate) && !empty( $vendor_hourlyrate ) || isset($vendor_freelance_old) && !empty( $vendor_freelance_old ) && isset($vendor_hourlyrate) && !empty( $vendor_hourlyrate ) ) {
				echo '<div>';
				printf( esc_html__( 'Author is available for freelance work and wish to receive %1$s an hour.', 'kiwi' ), '<span>' . $vendor_hourlyrate . '</span>' );
				echo '</div>';
			} else {
				echo '';
			}
			
			echo '</div>';
	
	}

		do_action( 'edd_after_author_widget' );

		echo $args['after_widget'];
		
    }

    
    
     
    function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );		
		
		return $instance;
	}
    
	
	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';		
				
	?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
				
	<?php
	}
     
}
<?php 
/*===============================================================
	Widget :: Marketplace :: Author portfolio widget
===============================================================*/
class Marketplace_Author_Portfolio_Widget extends WP_Widget {
     
    function __construct() {
			parent::__construct( 'edd_author_portfolio_widget', esc_html__( 'Marketplace :: Author Portfolio', 'kiwi' ), array( 'description' => esc_html__( 'Display author\'s portfolio items.', 'kiwi' ) ) );
		}

	        function widget( $args, $instance ){
				
			if ( ! is_singular( 'download' )  ) {
				return;
			}	
								
			global $kiwi_theme_option, $post;
			
            $title = apply_filters( 'widget_title', $instance['title'] ? $instance['title'] : '' );
            $limit = isset( $instance['limit'] ) ? $instance['limit'] : 4;             
            $show_price = isset( $instance['show_price'] ) && $instance['show_price'] === 1 ? 1 : 0;
			$show_date = isset( $instance['show_date'] ) && $instance['show_date'] === 1 ? 1 : 0;
			$show_sales = isset( $instance['show_sales'] ) && $instance['show_sales'] === 1 ? 1 : 0; 
			$thumbnail = isset( $instance['thumbnail'] ) && $instance['thumbnail'] === 1 ? 1 : 0;
			$thumbnail_size = isset( $instance['thumbnail_size'] ) ? $instance['thumbnail_size'] : 80;
            $show_reviews = isset( $instance['show_reviews'] ) && $instance['show_reviews'] === 1 ? 1 : 0;
            
			 
			$post_id = $post->ID;
			$post_author_id 	= get_post_field( 'post_author', $post_id );
			
			do_action( 'edd_before_price_widget' );
			
             // set the params
             $params = array( 
                 'post_type'     		=> 'download',
				 'post__not_in' 		=> array($post_id),
				 'posts_per_page'	  	=> $limit,  
                 'post_status'   		=> 'publish', 
                 'orderby'        		=> 'rand', 
				 'author' 				=> $post_author_id,
             );

		$grid_mp_items = new WP_Query($params);
		
		$have_results = $grid_mp_items->have_posts();
		
		if ( empty( $have_results ) ) {
        	return;
        }
		
		$mp_widget_audio = '';
		
		
        echo $args['before_widget'];
			 
		 if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		echo '<ul class="widget-mk-kiwi widget-mp-pack mp-author-portfolio">';
		
		if ( !empty( $have_results ) && $grid_mp_items->have_posts() ) : while ( $grid_mp_items->have_posts() ) : $grid_mp_items->the_post();
				
			$release_date = get_the_time('M j, Y', get_the_ID() );
			$sales = edd_get_download_sales_stats( get_the_ID() );

			if ( $thumbnail === 1 && function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( get_the_ID() ) ) {
				$thumbnail_class = 'widget-download-with-thumbnail';
			} else {
				$thumbnail_class = 'widget-download-without-thumbnail';
			}
						
			echo '<li class="'.$thumbnail_class.'">';			
			
			if ( $thumbnail === 1 && function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( get_the_ID() ) ) {
				
				$mp_featured_value = get_post_meta( get_the_ID(), 'mp_featured_item', true);
				
// ****************
if ( class_exists( 'EDD_Front_End_Submissions' ) ) { 
	$vendor_thumbnail = get_post_meta( get_the_ID(), 'upload_item_thumbnail', true );		
}

			if ( class_exists( 'EDD_Front_End_Submissions' ) && !empty( $vendor_thumbnail ) ) { 
				foreach($vendor_thumbnail as $key=>$val){
					$url = wp_get_attachment_image_src( $val, 'full' ); 
					$post_thumbnail = '<img src="'. esc_url( $url[0] ).'" width="'.$url[1].'" height="'.$url[2].'">';
				}
			} else {
				
				$mp_cmb_embedded = get_post_meta( get_the_ID(), '_cmb2_edd_thumbnail', true );					
				if ( !empty($mp_cmb_embedded) ) {				
					$post_thumbnail = '<img src="'. esc_url( $mp_cmb_embedded ).'" width="'.$thumbnail_size.'" height="'.$thumbnail_size.'">';	
				} else {				
					$post_thumbnail = get_the_post_thumbnail( get_the_ID(), apply_filters( 'edd_widgets_kiwipack_thumbnail_size', array( $thumbnail_size,$thumbnail_size ) ) );
				}
				
			}	
// ****************	
			if( function_exists('mp_media_class') ){ 
				echo '<div class="mp-widget-avatar'.mp_media_class().'">';
			} else {
				echo '<div class="mp-widget-avatar">';
			}
			
				if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
					echo'<div class="featured-item-ribbon"><span></span></div>';				
				}
				
				echo '<div class="mp-post-thumbnail">'.$post_thumbnail.'</div>';
				echo apply_filters('vc_mp_audio_only', $mp_widget_audio);
			echo '</div>';
		} 
		
		echo '<div class="mp-widget-content">';
		
		if ( $show_date === 1 ) { echo '<span class="time">' . $release_date . '</span>'; }
		if ( $show_sales === 1 ) {	echo '<span class="pull-right"><i class="fa fa-download"></i> <span>' .$sales. '</span></span>'; }
		
		echo '<h3><a href="'. get_permalink().'">'.get_the_title().'</a></h3>';		
			
			 if ( $show_reviews === 1 && class_exists( 'EDD_Reviews' ) ) {	
				echo '<div class="mp-widget-reviews">';			
				 mp_extension_review_function_core();			
				echo '</div><br />';
			}
					
			if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) {	
				
				$cs_active = edd_coming_soon_is_active( get_the_ID() );	
				
				$custom_text = get_post_meta( get_the_ID(), 'edd_coming_soon_text', true );
				$custom_text = !empty ( $custom_text ) ? $custom_text : apply_filters( 'edd_cs_coming_soon_text', esc_html__( 'Coming Soon', 'kiwi' ) );
			}
			
			
		if ($show_price === 1) {
			
			if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && $cs_active ) {			
					
				$price = apply_filters( 'edd_coming_soon_display_text', '<p><strong>' . $custom_text . '</strong></p>' );
				
				echo sprintf( '<span class="widget-download-price cs-active">%s</span>', $price );
			
			} else {
				
				remove_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_get_custom_status_text', 30, 2 );
				
				$price = get_post_meta( get_the_ID(), 'edd_price', true );
				
							if ( $price == 0 && $kiwi_theme_option['marketplace-free-enable'] == '1' && !edd_has_variable_prices( get_the_ID() ) ) {
								echo  '<span class="price free">';
									$price = $kiwi_theme_option['marketplace-free-customtext'];
								echo  '</span>';
							} elseif ( edd_has_variable_prices( get_the_ID() ) ) {
								 $price = edd_price_range( get_the_ID() );
							} else {
								$price = edd_price( get_the_ID(), false );
							}
							
				echo sprintf( '<span class="widget-download-price">%s</span>', $price );	
				
			}
				
		}
		
		echo '</div>';
		echo '</li>';
		
		endwhile; 
			wp_reset_postdata();
		endif;

		echo '</ul>';
		
		echo $args['after_widget'];	
		
    }

        
        /**
         * Update
         *
         * @return   array
         * @since    1.0
        */

        function update( $new_instance, $old_instance )
        {
            $instance = $old_instance;

            // sanitize title
            $instance['title'] = strip_tags( $new_instance['title'] );
			
			 // sanitize limit
            $instance['limit'] = strip_tags( $new_instance['limit'] );
            $instance['limit'] = ( ( bool ) preg_match( '/^\-?[0-9]+$/', $instance['limit'] ) ) && $instance['limit'] > -2 ? $instance['limit'] : 4;


            // sanitize show price
            $instance['show_price'] = strip_tags( $new_instance['show_price'] );
            $instance['show_price'] = $instance['show_price'] === '1' ? 1 : 0;
			
			// sanitize show date
            $instance['show_date'] = strip_tags( $new_instance['show_date'] );
            $instance['show_date'] = $instance['show_date'] === '1' ? 1 : 0;
			
			// sanitize show reviews
            $instance['show_reviews'] = strip_tags( $new_instance['show_reviews'] );
            $instance['show_reviews'] = $instance['show_reviews'] === '1' ? 1 : 0;
			
			// sanitize show sales
            $instance['show_sales'] = strip_tags( $new_instance['show_sales'] );
            $instance['show_sales'] = $instance['show_sales'] === '1' ? 1 : 0;
            
            // sanitize thumbnail
            $instance['thumbnail'] = strip_tags( $new_instance['thumbnail'] );
            $instance['thumbnail'] = $instance['thumbnail'] === '1' ? 1 : 0;

            // sanitize thumbnail size
            $instance['thumbnail_size'] = strip_tags( $new_instance['thumbnail_size'] );
            $instance['thumbnail_size'] = ( ( bool ) preg_match( '/^[0-9]+$/', $instance['thumbnail_size'] ) ) ? $instance['thumbnail_size'] : 80;
           // $instance['hide_months'] = isset( $new_instance['hide_months'] ) ? (bool) $new_instance['hide_months'] : false;
            return $instance;
        }


        /**
         * Form
         *
         * @return   void
         * @since    1.0
        */

        function form( $instance ) {
            $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
			$limit = isset( $instance['limit'] ) ? esc_attr( $instance['limit'] ) : 4;
            $show_price = isset( $instance['show_price'] ) ? esc_attr( $instance['show_price'] ) : 0;
			$show_reviews = isset( $instance['show_reviews'] ) ? esc_attr( $instance['show_reviews'] ) : 0;
			$show_date = isset( $instance['show_date'] ) ? esc_attr( $instance['show_date'] ) : 0;
			$show_sales = isset( $instance['show_sales'] ) ? esc_attr( $instance['show_sales'] ) : 0;			
            $thumbnail = isset( $instance['thumbnail'] ) ? esc_attr( $instance['thumbnail'] ) : 0;
            $thumbnail_size = isset( $instance['thumbnail_size'] ) ? esc_attr( $instance['thumbnail_size'] ) : 80;
            ?>
                <p>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
                    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
                </p>
				
				<p>
                    <label for="<?php echo esc_attr($this->get_field_id( 'limit' ) ); ?>"><?php printf( esc_html__('Number of %s to show:', 'kiwi' ), edd_get_label_plural( true ) ); ?></label>
                    <input class="small" size="3" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>"/>
                </p>
				
                <p>
                    <input id="<?php echo esc_attr( $this->get_field_id( 'show_price' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_price' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_price ); ?>/>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'show_price' ) ); ?>"><?php esc_html_e( 'Display price?', 'kiwi' ); ?></label> 
                </p>
                <p>
                    <input id="<?php echo esc_attr( $this->get_field_id( 'thumbnail' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'thumbnail' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $thumbnail ); ?>/>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'thumbnail' ) ); ?>"><?php esc_html_e( 'Display thumbnails?', 'kiwi' ); ?></label> 
                </p>
				
				<p>
                    <input id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_date ); ?>/>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>"><?php esc_html_e( 'Display date?', 'kiwi' ); ?></label> 
                </p>
				<p>
                    <input id="<?php echo esc_attr( $this->get_field_id( 'show_sales' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_sales' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_sales ); ?>/>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'show_sales' ) ); ?>"><?php esc_html_e( 'Display sales?', 'kiwi' ); ?></label> 
                </p>
				<p>
					<input id="<?php echo esc_attr( $this->get_field_id( 'show_reviews' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_reviews' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_reviews ); ?>/>
					<label for="<?php echo esc_attr( $this->get_field_id( 'show_reviews' ) ); ?>"><?php esc_html_e( 'Display Reviews?', 'kiwi' ); ?></label> 
				</p>
                <p>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'thumbnail_size' ) ); ?>"><?php esc_html_e( 'Size of the thumbnails, e.g. 80 = 80x80px', 'kiwi' ); ?></label> 
                    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'thumbnail_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'thumbnail_size' ) ); ?>" type="text" value="<?php echo esc_attr( $thumbnail_size ); ?>" />
                </p>
            <?php
        }
        
    }
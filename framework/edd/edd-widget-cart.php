<?php 
/*===============================================================
	Widget :: Marketplace Cart
===============================================================*/
class Marketplace_Widget_Cart extends WP_Widget {
	/** Constructor */
	function __construct() {
		parent::__construct( 'edd_cart_widget', esc_html__( 'Marketplace :: Cart', 'kiwi' ), array( 'description' => esc_html__( 'Display shopping cart', 'kiwi' ) ) );
	}

	/** @see WP_Widget::widget */
	function widget( $args, $instance ) {

		if ( isset( $instance['hide_on_checkout'] ) && edd_is_checkout() ) {
			return;
		}

		$args['id']        = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_cart_widget';
		$instance['title'] = ( isset( $instance['title'] ) ) ? $instance['title'] : '';

		$title = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );

		
		$hide_months = isset( $instance['hide_months'] ) ? $instance['hide_months'] : false;
				
		if ( $instance['hide_months'] == '1' ) {	
			if ( is_singular( 'download' ) ) {
				return false;	
			}						
		}
		
		echo $args['before_widget'];

		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		do_action( 'edd_before_cart_widget' );
		
		edd_shopping_cart( true );

		do_action( 'edd_after_cart_widget' );

		echo $args['after_widget'];
	}

	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']            = strip_tags( $new_instance['title'] );
		$instance['hide_on_checkout'] = isset( $new_instance['hide_on_checkout'] );
		$instance['hide_months'] = isset( $new_instance['hide_months'] ) ? (bool) $new_instance['hide_months'] : false;
		return $instance;
	}

	/** @see WP_Widget::form */
	function form( $instance ) {

		$defaults = array(
			'title'            => '',
			'hide_on_checkout' => ''
		);

		$instance = wp_parse_args( (array) $instance, $defaults ); 
		$hide_months = isset( $instance['hide_months'] ) ? (bool) $instance['hide_months'] : false;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
		</p>

		<!-- Hide on Checkout Page -->
		<p>
			<input <?php checked( $instance['hide_on_checkout'], true ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_on_checkout' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_on_checkout' ) ); ?>" type="checkbox" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_on_checkout' ) ); ?>"><?php esc_html_e( 'Hide on Checkout Page', 'kiwi' ); ?></label>
		</p>
		
		<!-- Hide -->		
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $hide_months ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_months' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>"><?php esc_html_e( 'Hide on item description page?', 'kiwi' ); ?></label>
		</p>

		<?php
	}
}
<?php

class Marketplace_Item_Ratings extends WP_Widget {

    /** Constructor */
	public function __construct() {
			parent::__construct( 'Marketplace_Item_Ratings', esc_html__( 'Marketplace :: Item Rating', 'kiwi' ), 
			array( 'description' => esc_html__( 'Display total item rating.', 'kiwi' ) ) );
		}	


    /** @see WP_Widget::widget */
    public function widget( $args, $instance ) {
		$args['id'] = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_download_ratings';

        if ( ! is_singular( 'download' ) ) {
        	return;
        }
	
        // Used by themes. Opens the widget
        echo $args['before_widget'];
		
		remove_action( 'pre_get_comments',   array( edd_reviews(), 'hide_reviews'                           ) );
		remove_filter( 'comments_clauses',   array( edd_reviews(), 'hide_reviews_from_comment_feeds_compat' ), 10, 2 );
		remove_filter( 'comment_feed_where', array( edd_reviews(), 'hide_reviews_from_comment_feeds'        ), 10, 2 );
		
		
		$post = get_queried_object();
		if ( !$post || !$post->ID ) {
			return;
		}
		$post_id = $post->ID;
		
		/* */
		$args = array(
			'post_id' => $post_id,
			'count' => true, 
			'meta_key' => 'edd_rating',
		);
		$comments = get_comments($args);

		$rating_count = 0;
		$total_rating = 0;
		/* */

		$reviews = get_comments(
			apply_filters(
				'widget_edd_per_product_reviews_args',
				array(
					//'number' => $number,
					'status' => 'approve',
					'post_status' => 'publish',
					'post_type' => 'download',
					'post_id' => $post_id,
				)
			)
		);

		

		if ( $reviews && $comments != 0) {	
		
		 echo '<div itemscope itemtype="http://schema.org/Product">';
		 echo '<span itemprop="name" class="mp-ande">' . get_the_title( $post_id ) . '</span>';
		 echo ' <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';

			foreach ( (array) $reviews as $review ) {				
				$total_rating = $total_rating + get_comment_meta( $review->comment_ID, 'edd_rating', true );
				$rating_count = $rating_count + 1;
			}
			
			$average = $total_rating / $comments;
			$number = sprintf('%0.2f', $average);
		
			echo '<div class="edd_reviews_rating_box" role="img" aria-label="'. $number . ' ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo '<div class="edd_star_rating custom" style="float: none; width: ' . ( 19 * $number ) . 'px;"></div>';
			echo '</div>';
			
			echo '<span class="mp-rating">';
			
			echo '<meta itemprop="ratingValue" content="' . $number . '" />';
			echo '<meta itemprop="bestRating" content="5" />';
			echo '<meta itemprop="worstRating" content="1" />';
			echo '<meta itemprop="reviewCount" content="' . $comments . '" />';
			
			printf(  esc_html__( 'An average of %s based on %s ratings.' , 'kiwi' ), $number, $comments );
			echo '</span>';
			
			echo '</div>';
			echo '</div>';
			
		} else {
			
			echo '<div class="edd_reviews_rating_box" role="img" aria-label="0 ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo '<div class="edd_star_rating custom" style="float: none; width:0"></div>';
			echo '</div>';
					
			echo '<span class="mp-rating-no-reviews">' . esc_html__( 'There are no reviews yet.', 'kiwi' ) . '</span>';
		}
			
		
		echo '</div>';
		

    }

   	/** @see WP_Widget::form */
    public function form( $instance ) {
        // Set up some default widget settings.
       
		?>
        
        <p><?php esc_html_e( 'No options to choose from.', 'kiwi' )?></p>
        
        <?php do_action( 'edd_product_item_stats_form' , $instance ); ?>
		
    <?php }

    /** @see WP_Widget::update */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        
        do_action( 'edd_product_item_stats_update', $instance );
        
        return $instance;
    } 

}
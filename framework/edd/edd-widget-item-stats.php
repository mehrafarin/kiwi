<?php

class Marketplace_Item_Stats extends WP_Widget {

    /** Constructor */
	public function __construct() {
			parent::__construct( 'Marketplace_Item_Stats', esc_html__( 'Marketplace :: Item stats', 'kiwi' ), array( 'description' => esc_html__( 'Display item stats such as number of comments and purchases.', 'kiwi' ) ) );
		}	


    /** @see WP_Widget::widget */
    public function widget( $args, $instance ) {
		$args['id'] = ( isset( $args['id'] ) ) ? $args['id'] : 'edd_download_item_stats';

        if ( ! is_singular( 'download' ) ) {
        	return;
        }
		
		
       	// set correct download ID
        if ( is_singular( 'download' ) ) {
        	$download_id = get_the_ID();
        } else {
        	$download_id = absint( $instance['download_id'] );
        }

        // Variables from widget settings
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $args['id'] );      	
	
        // Used by themes. Opens the widget
        echo $args['before_widget'];

        // Display the widget title
        if( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
		}
		
        do_action( 'edd_product_item_stats_before_title' , $instance , $download_id );
		
		$sales = edd_get_download_sales_stats( $download_id );
		
	 	$params = array(
				'post_id' 	=> $download_id,
				'post_type' => 'download',
				'meta_query' => array(
								array( 
									'key' => 'edd_rating', 
									'value' => '', 
									'compare' => 'NOT EXISTS'
								), 
							),
				'count' 	=> true, 
		);
		$comment = get_comments($params);
		
		//$comment = get_comments_number( $download_id );
			
			echo '<div class="btn-group stats" role="group">';
				echo '<button type="button" class="btn btn-default purchases"><i class="fa fa-download"></i> ' . esc_html( $sales ) . '</button>';
				echo '<button type="button" class="btn btn-default comments"><i class="fa fa-comments"></i> ' . esc_html( $comment ) . '</button>';
			echo '</div>';         
        
        do_action( 'edd_product_item_stats_before_end', $instance, $download_id );

        // Used by themes. Closes the widget
        echo $args['after_widget'];
    }

   	/** @see WP_Widget::form */
    public function form( $instance ) {
        // Set up some default widget settings.
        $defaults = array(
            'title' 			=> sprintf( esc_html__( '%s Details', 'kiwi' ), edd_get_label_singular() ),
            'download_id' 		=> 'current',
        );

        $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        
        <!-- Title -->
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ) ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo $instance['title']; ?>" />
        </p>

			<!-- Download -->
         <?php 
            $args = array( 
	            'post_type'      => 'download', 
	            'posts_per_page' => -1, 
	            'post_status'    => 'publish', 
	        );
	        $downloads = get_posts( $args );
        ?>
        
        
        <?php do_action( 'edd_product_item_stats_form' , $instance ); ?>
    <?php }

    /** @see WP_Widget::update */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title']           = strip_tags( $new_instance['title'] );
        
		do_action( 'edd_product_item_stats_update', $instance );
        
        return $instance;
    } 

}
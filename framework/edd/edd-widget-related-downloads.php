<?php
/** EDD Related Downloads Widget */

class MP_Widget_Related_Downloads extends WP_Widget {

        /**
         * Construct
         *
         * @return   void
         * @since    1.0
        */

        function __construct()
        {
             parent::__construct( false, sprintf( esc_html__( 'Marketplace :: Related Items', 'kiwi' ), edd_get_label_plural() ), 
			 array( 'description' => sprintf( esc_html__( 'Display related items.', 'kiwi' ), edd_get_label_plural( true ) ) ) );
        }


        /**
         * Widget
         *
         * @return   void
         * @since    1.0
        */

        function widget( $args, $instance ) {
            
			global $post,$kiwi_theme_option;
            
            if ( ! is_singular( 'download' ) )
            return;
				
            $title = apply_filters( 'widget_title', $instance['title'] ? $instance['title'] : '' );
			$offset = $instance['offset'] ? $instance['offset'] : 0;
			$limit = isset( $instance['limit'] ) ? $instance['limit'] : 4;
			$show_price = isset( $instance['show_price'] ) && $instance['show_price'] === 1 ? 1 : 0;
			$show_date = isset( $instance['show_date'] ) && $instance['show_date'] === 1 ? 1 : 0;
			$show_reviews = isset( $instance['show_reviews'] ) && $instance['show_reviews'] === 1 ? 1 : 0;
			$show_sales = isset( $instance['show_sales'] ) && $instance['show_sales'] === 1 ? 1 : 0;
			$thumbnail = isset( $instance['thumbnail'] ) && $instance['thumbnail'] === 1 ? 1 : 0;
			$thumbnail_size = isset( $instance['thumbnail_size'] ) ? $instance['thumbnail_size'] : 80;
			$number = isset( $instance['number'] ) ? esc_attr( $instance['number'] ) : '';
			$sortby = isset( $instance['sortby'] ) ? $instance['sortby'] : '';
			$relatedby = isset( $instance['relatedby'] ) ? $instance['relatedby'] : '';
			
			$mp_widget_audio = '';
			
			do_action( 'edd_before_related_widget' );
			
			remove_filter( "term_links-download_category", 'limit_terms');
			
			$tags = get_the_term_list( $post->ID, 'download_tag', '', ', ', '' );
			$catlist = get_the_term_list( $post->ID, 'download_category', '', ', ', '' );			
			
			$params = array( 
				'post_type'      => 'download',
				'posts_per_page' => $limit,
				'post_status'    => 'publish',
				'offset'         => $offset,
				'post__not_in' 	 => array( $post->ID ),
			);
				
			if ( $relatedby == 'related_tags' ) { 
				$params['tax_query'][] = array(
					'taxonomy' => 'download_tag', 
					'field' => 'slug', 
					'terms' => explode(',', $tags ),
				);	
			}
			
			if ( $relatedby == 'related_categories' ) { 				
				$params['tax_query'][] = array(
					'taxonomy' => 'download_category', 
					'field' => 'slug', 
					'terms' => explode(',', $catlist ),
				);	
			}

			if ( $sortby == 'most_recent' ) { 
				$params['orderby'] = 'date'; 
				$params['order'] = 'DESC';
			}
			
			if ( $sortby == 'random_items' ) { $params['orderby'] = 'rand'; }	
			
			
			if ( $sortby == 'featured_items' ) {
				$params['orderby'] = 'meta_value_num';
				$params['meta_query'][] = array(
						'key'   => 'mp_featured_item',
						'value' => 'yes'
					);		 
			}
				
			if ( $sortby == 'recently_viewed' ) {
				$params['orderby'] = 'meta_value';
				$params['order'] = 'DESC';
				$params['meta_key'] = 'mp_last_viewed';		
			}
				
				
			if ( $sortby == 'top_sellers' ) {
				$params['orderby'] = 'meta_value_num';	
				$params['meta_query'][] = array(
					'key'     => '_edd_download_sales',
					'compare' => '>',
					'value'   => 0,
					'order'   => 'DESC',
					'relation' => 'AND',
				);
			} 
			 
			if ( $instance['exclude_free'] ) {
				$params['meta_query'][] = array(
					'key'     => 'edd_price',
					'value'   => 0.00,
					'type'    => 'numeric',
					'compare' => '!='
				);
			}	
				
				
			$grid_mp_items = new WP_Query($params);
            
			$have_results = $grid_mp_items->have_posts();
			
			if ( empty( $have_results ) ) {
				return;
			}
			
			echo $args['before_widget'];
		
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		echo '<ul class="widget-mk-kiwi widget-mp-pack mp-vc-items">';
		
		if ( !empty( $have_results ) && $grid_mp_items->have_posts() ) : while ( $grid_mp_items->have_posts() ) : $grid_mp_items->the_post();
		
			$release_date = get_the_time('M j, Y', get_the_ID() );
			$sales = edd_get_download_sales_stats( get_the_ID() );

			if ( $thumbnail === 1 && function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( get_the_ID() ) ) {
				$thumbnail_class = 'widget-download-with-thumbnail';
			} else {
				$thumbnail_class = 'widget-download-without-thumbnail';
			}
			
			
			echo '<li class="'.$thumbnail_class.'">';			
			
			// get the post thumbnail
			if ( $thumbnail === 1 && function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( get_the_ID() ) ) {
				
			$mp_featured_value = get_post_meta( get_the_ID(), 'mp_featured_item', true);
				
// ****************
if ( class_exists( 'EDD_Front_End_Submissions' ) ) { 
	$vendor_thumbnail = get_post_meta( get_the_ID(), 'upload_item_thumbnail', true );		
}

			if ( class_exists( 'EDD_Front_End_Submissions' ) && !empty( $vendor_thumbnail ) ) { 
				foreach($vendor_thumbnail as $key=>$val){
					$url = wp_get_attachment_image_src( $val, 'full' ); 
					$post_thumbnail = '<img src="'. esc_url( $url[0] ).'" width="'.$url[1].'" height="'.$url[2].'">';
				}
			} else {
				
				$mp_cmb_embedded = get_post_meta( get_the_ID(), '_cmb2_edd_thumbnail', true );					
				if ( !empty($mp_cmb_embedded) ) {				
					$post_thumbnail = '<img src="'. esc_url( $mp_cmb_embedded ).'" width="'.$thumbnail_size.'" height="'.$thumbnail_size.'">';	
				} else {				
					$post_thumbnail = get_the_post_thumbnail( get_the_ID(), apply_filters( 'edd_widgets_kiwipack_thumbnail_size', array( $thumbnail_size,$thumbnail_size ) ) );
				}
			
			}	
// ****************	
			if( function_exists('mp_media_class') ){ 
				echo '<div class="mp-widget-avatar'.mp_media_class().'">';
			} else {
				echo '<div class="mp-widget-avatar">';
			}
			
				if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
					echo'<div class="featured-item-ribbon"><span></span></div>';				
				}
				
				echo '<div class="mp-post-thumbnail">'.$post_thumbnail.'</div>';
				echo apply_filters('vc_mp_audio_only', $mp_widget_audio);
			echo '</div>';
		} 
		
		echo '<div class="mp-widget-content">';
		
		
		if ( $show_date === 1 ) { echo '<span class="time">' . esc_html( $release_date ) . '</span>'; }
		if ( $show_sales === 1 ) {	echo '<span class="pull-right"><i class="fa fa-download"></i> <span>' . esc_html( $sales ). '</span></span>'; }
		
		echo '<h3><a href="'. get_permalink().'">'.get_the_title().'</a></h3>';		
			
			 if ( $show_reviews === 1 && class_exists( 'EDD_Reviews' ) ) {	
				echo '<div class="mp-widget-reviews">';			
				 mp_extension_review_function_core();			
				echo '</div><br />';
			}
					
			if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) {	
				
				$cs_active = edd_coming_soon_is_active( get_the_ID() );	
				
				$custom_text = get_post_meta( get_the_ID(), 'edd_coming_soon_text', true );
				$custom_text = !empty ( $custom_text ) ? $custom_text : apply_filters( 'edd_cs_coming_soon_text', esc_html__( 'Coming Soon', 'kiwi' ) );
			}
			
			
		if ($show_price === 1) {
			
			if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && $cs_active ) {			
					
				$price = apply_filters( 'edd_coming_soon_display_text', '<p><strong>' . $custom_text . '</strong></p>' );
				
				echo sprintf( '<span class="widget-download-price cs-active">%s</span>', $price );
			
			} else {
				
				remove_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_get_custom_status_text', 30, 2 );
				
				$price = get_post_meta( get_the_ID(), 'edd_price', true );
				
							if ( $price == 0 && $kiwi_theme_option['marketplace-free-enable'] == '1' && !edd_has_variable_prices( get_the_ID() ) ) {
								echo  '<span class="price free">';
									$price = $kiwi_theme_option['marketplace-free-customtext'];
								echo  '</span>';
							} elseif ( edd_has_variable_prices( get_the_ID() ) ) {
								 $price = edd_price_range( get_the_ID() );
							} else {
								$price = edd_price( get_the_ID(), false );
							}
							
				echo sprintf( '<span class="widget-download-price">%s</span>', $price );	
				
			}
				
		}
		
		echo '</div>';
					   
			echo '</li>';
		
		endwhile; 
			wp_reset_postdata();
		endif;
		
		echo '</ul>';
		
		
		echo $args['after_widget'];
		
		
}


        /**
         * Update
         *
         * @return   array
         * @since    1.0
        */

        function update( $new_instance, $old_instance )
        {
            $instance = $old_instance;

            $instance['title'] = strip_tags( $new_instance['title'] );
			$instance['limit'] = strip_tags( $new_instance['limit'] );
            $instance['limit'] = ( ( bool ) preg_match( '/^\-?[0-9]+$/', $instance['limit'] ) ) && $instance['limit'] > -2 ? $instance['limit'] : 4;
			
			$instance['offset'] = strip_tags( $new_instance['offset'] );
            $instance['offset'] = ( ( bool ) preg_match( '/^[0-9]+$/', $instance['offset'] ) ) ? $instance['offset'] : 4;
           
		    $instance['exclude_free'] = isset( $new_instance['exclude_free'] ) ? (bool) $new_instance['exclude_free'] : 0;
			
		   
			
		    $instance['show_price'] = strip_tags( $new_instance['show_price'] );
            $instance['show_price'] = $instance['show_price'] === '1' ? 1 : 0; 
			$instance['show_date'] = strip_tags( $new_instance['show_date'] );
            $instance['show_date'] = $instance['show_date'] === '1' ? 1 : 0;			
			$instance['show_reviews'] = strip_tags( $new_instance['show_reviews'] );
            $instance['show_reviews'] = $instance['show_reviews'] === '1' ? 1 : 0;			
			$instance['show_sales'] = strip_tags( $new_instance['show_sales'] );
            $instance['show_sales'] = $instance['show_sales'] === '1' ? 1 : 0;			
            $instance['thumbnail'] = strip_tags( $new_instance['thumbnail'] );
            $instance['thumbnail'] = $instance['thumbnail'] === '1' ? 1 : 0;
			
			$instance['thumbnail_size'] = strip_tags( $new_instance['thumbnail_size'] );
            $instance['thumbnail_size'] = ( ( bool ) preg_match( '/^[0-9]+$/', $instance['thumbnail_size'] ) ) ? $instance['thumbnail_size'] : 80;
			$instance['sortby'] = strip_tags( $new_instance['sortby'] );
            $instance['sortby'] = $instance['sortby'] ? $instance['sortby'] : 'most_recent';
			$instance['relatedby'] = strip_tags( $new_instance['relatedby'] );
            $instance['relatedby'] = $instance['relatedby'] ? $instance['relatedby'] : 'related_tags';
            return $instance;
        }




        /**
         * Form
         *
         * @return   void
         * @since    1.0
        */

        function form( $instance )
        {
            $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$offset = isset( $instance['offset'] ) ? esc_attr( $instance['offset'] ) : 0;
		$limit = isset( $instance['limit'] ) ? esc_attr( $instance['limit'] ) : 4;
		$show_price = isset( $instance['show_price'] ) ? esc_attr( $instance['show_price'] ) : 0;		
		$exclude_free = isset( $instance['exclude_free'] ) ? esc_attr( $instance['exclude_free'] ) : 0;		 
		$show_date = isset( $instance['show_date'] ) ? esc_attr( $instance['show_date'] ) : 0;
		$show_reviews = isset( $instance['show_reviews'] ) ? esc_attr( $instance['show_reviews'] ) : 0;
		$show_sales = isset( $instance['show_sales'] ) ? esc_attr( $instance['show_sales'] ) : 0;		
		$thumbnail = isset( $instance['thumbnail'] ) ? esc_attr( $instance['thumbnail'] ) : 0;
		$thumbnail_size = isset( $instance['thumbnail_size'] ) ? esc_attr( $instance['thumbnail_size'] ) : 80;		
		$sortby = isset( $instance['sortby'] ) ? esc_attr( $instance['sortby'] ) : 'most_recent';		
		$relatedby = isset( $instance['relatedby'] ) ? esc_attr( $instance['relatedby'] ) : 'related_tags';
		
            ?>
                <p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'relatedby' ) ); ?>"><?php esc_html_e( 'Relate by:','kiwi' ); ?></label>
				<select name="<?php echo esc_attr( $this->get_field_name( 'relatedby' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'relatedby' ) ); ?>" class="widefat">
					<option value="related_tags"<?php if ( 'related_tags' == $instance['relatedby'] ) echo 'selected="selected"'; ?>><?php esc_html_e('Tags','kiwi'); ?></option>
					<option value="related_categories"<?php if ( 'related_categories' == $instance['relatedby'] ) echo 'selected="selected"'; ?>><?php esc_html_e('Categories','kiwi'); ?></option>
				</select>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'sortby' ) ); ?>"><?php esc_html_e( 'Sort by:','kiwi' ); ?></label>
				<select name="<?php echo esc_attr( $this->get_field_name( 'sortby' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'sortby' ) ); ?>" class="widefat">
					<option value="most_recent" <?php if ( 'most_recent' == $instance['sortby'] ) echo 'selected="selected"'; ?>><?php esc_html_e('Most Recent','kiwi'); ?></option>
					<option value="random_items" <?php if ( 'random_items' == $instance['sortby'] ) echo 'selected="selected"'; ?>><?php esc_html_e('Random Items','kiwi'); ?></option>
					<option value="top_sellers" <?php if ( 'top_sellers' == $instance['sortby'] ) echo 'selected="selected"'; ?>><?php esc_html_e( 'Top Sellers','kiwi' ); ?></option>
					<option value="featured_items" <?php if ( 'featured_items' == $instance['sortby'] ) echo 'selected="selected"'; ?>><?php esc_html_e( 'Featured Items','kiwi' ); ?></option>
					<option value="recently_viewed" <?php if ( 'recently_viewed' == $instance['sortby'] ) echo 'selected="selected"'; ?>><?php esc_html_e( 'Recently Viewed','kiwi' ); ?></option>
				</select>
			</p>
			<p>
                    <label for="<?php echo esc_attr( $this->get_field_id( 'offset' ) ); ?>"><?php printf( esc_html__('Number of %s to skip:', 'kiwi' ), edd_get_label_plural( true ) ); ?></label>
                    <input class="small" size="3" id="<?php echo esc_attr( $this->get_field_id( 'offset' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'offset' ) ); ?>" type="text" value="<?php echo esc_attr( $offset ); ?>"/>
                </p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php printf( esc_html__('Number of %s to show:', 'kiwi' ), edd_get_label_plural( true ) ); ?></label>
				<input class="small" size="3" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>"/>
			</p>
			<p>
				<input id="<?php echo esc_attr( $this->get_field_id( 'show_price' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_price' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_price ); ?>/>
				<label for="<?php echo esc_attr( $this->get_field_id( 'show_price' ) ); ?>"><?php esc_html_e( 'Display price?', 'kiwi' ); ?></label>
			</p>
			<p>
				<input id="<?php echo esc_attr( $this->get_field_id( 'thumbnail' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'thumbnail' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $thumbnail ); ?>/>
				<label for="<?php echo esc_attr( $this->get_field_id( 'thumbnail' ) ); ?>"><?php esc_html_e( 'Display thumbnails?', 'kiwi' ); ?></label>
			</p>	
			<p>
				<input id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_date ); ?>/>
				<label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>"><?php esc_html_e( 'Display date?', 'kiwi' ); ?></label> 
			</p>
			<p>
				<input id="<?php echo esc_attr( $this->get_field_id( 'show_sales' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_sales' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_sales ); ?>/>
				<label for="<?php echo esc_attr( $this->get_field_id( 'show_sales' ) ); ?>"><?php esc_html_e( 'Display sales?', 'kiwi' ); ?></label> 
			</p>				
			<p>
				<input id="<?php echo esc_attr( $this->get_field_id( 'exclude_free' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'exclude_free' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $exclude_free ); ?>/>
				<label for="<?php echo esc_attr( $this->get_field_id( 'exclude_free' ) ); ?>"><?php printf( esc_html__( 'Exclude free %s?', 'kiwi' ), strtolower( edd_get_label_plural() ) ); ?></label>
			</p>
			<p>
				<input id="<?php echo esc_attr( $this->get_field_id( 'show_reviews' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_reviews' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $show_reviews ); ?>/>
				<label for="<?php echo esc_attr( $this->get_field_id( 'show_reviews' ) ); ?>"><?php esc_html_e( 'Display Reviews?', 'kiwi' ); ?></label> 
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'thumbnail_size' ) ); ?>"><?php esc_html_e( 'Size of the thumbnails, e.g. 80 = 80x80px', 'kiwi' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'thumbnail_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'thumbnail_size' ) ); ?>" type="text" value="<?php echo esc_attr( $thumbnail_size ); ?>" />
			</p>
            <?php
        }
        
    }
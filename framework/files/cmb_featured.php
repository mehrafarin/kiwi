<?php 

/* Usage: featured_image_get_meta( 'mp_featured_item' ) */

function featured_image_get_meta( $value ) {
	global $post;

	$field = get_post_meta( $post->ID, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}

function featured_image_add_meta_box() {
	add_meta_box(
		'featured_image-featured-image',
		esc_html__( 'Featured Item', 'kiwi' ),
		'featured_image_html',
		'download',
		'side',
		'default'
	);
}
add_action( 'add_meta_boxes', 'featured_image_add_meta_box' );



function featured_image_html( $post) {
	wp_nonce_field( '_featured_image_nonce', 'featured_image_nonce' ); ?>

	<p>
		<input type="radio" name="mp_featured_item" id="mp_featured_item_0" value="Yes" <?php echo ( featured_image_get_meta( 'mp_featured_item' ) === 'Yes' ) ? 'checked' : ''; ?>>
		<label for="mp_featured_item_0"><?php esc_html_e( 'Yes', 'kiwi' ); ?></label><br />
		
		<input type="radio" name="mp_featured_item" id="mp_featured_item_1" value="No" <?php echo ( featured_image_get_meta( 'mp_featured_item' ) === 'No' ) ? 'checked' : ''; ?>>
		<label for="mp_featured_item_1"><?php esc_html_e( 'No', 'kiwi' ); ?></label><br />
	</p><?php
}

function featured_image_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['featured_image_nonce'] ) || ! wp_verify_nonce( $_POST['featured_image_nonce'], '_featured_image_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['mp_featured_item'] ) )
		update_post_meta( $post_id, 'mp_featured_item', esc_attr( $_POST['mp_featured_item'] ) );
}
add_action( 'save_post', 'featured_image_save' );

?>
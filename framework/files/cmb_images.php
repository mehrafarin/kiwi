<?php

function mp_metabox_item_images() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'mp_item_images',
		'title'         => esc_html__( 'Item Images', 'kiwi' ),
		'object_types'  => array( 'download', ), // Post type
		'context'   	=> 'normal',
		'priority'   	=> 'high',
	) );
	
	$cmb_demo->add_field( array(
		'id'   => $prefix . 'mp_output_item_images',
		'name' => 'Item Images',
		'desc' => '',    
		'type' => 'file_list',
		'preview_size' => array( 50, 50 ), 	
	) );
	
	
	
	
	
}
add_action( 'cmb2_init', 'mp_metabox_item_images' );


/*===============================================================
	Sample template tag function for outputting a cmb2 file_list
===============================================================*/ 
function cmb2_output_file_list( $file_list_meta_key, $img_size = 'mp-slider-product-page' ) {
	
	global $kiwi_theme_option;
	
    $files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );

    echo '<div class="mp-display-image flex-edd-images"><ul class="slides none">';
   
   
   foreach ( (array) $files as $attachment_id => $attachment_url ) {
       if(!empty($kiwi_theme_option['mp-lightbox'] )) {	
			echo '<li><a href="'. esc_url( $attachment_url ).'" '.$kiwi_theme_option['mp-lightbox'].'>';
		} else {
			echo '<li><a href="'. esc_url( $attachment_url ).'">';
		}
	   
        echo wp_get_attachment_image( $attachment_id, $img_size );
        echo '</a></li>';
    }
    echo '</ul>';
	
	echo '<ul class="flex-direction-nav">
			<li><a href="#" class="flex-prev"></a></li>
			<li><a href="#" class="flex-next"></a></li>
		</ul>';
		
	echo '</div>';
}

?>
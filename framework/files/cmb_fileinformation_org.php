<?php

	function mp_metabox_fallback_demolink() {

		$prefix = '_cmb2_';

		$cmb_demo = new_cmb2_box( array(
			'id'            => $prefix . 'metabox_demo_url',
			'title'         => esc_html__( 'File information', 'kiwi' ),
			'object_types'  => array( 'download', ), // Post type
			'context'   	=> 'normal',
			'priority'   	=> 'high',
		) );
		
		$cmb_demo->add_field( array(
			'name' => esc_html__( 'Thumbnail', 'kiwi' ),
			'desc' => esc_html__( 'Upload a thumbnail. (e.g.: 80x80px)', 'kiwi' ),
			'id'   => $prefix . 'edd_thumbnail',
			'type' => 'file',
			'preview_size' => array( 50, 50 ), 	
		) );
		
		$cmb_demo->add_field( array(
			'name'    => esc_html__( 'Item version', 'kiwi' ),
			'desc'    => esc_html__( 'E.g.: 1.2.7', 'kiwi' ),
			'id'      => $prefix . 'marketplace_item_version',
			'type'    => 'text'
		) );
		
			$cmb_demo->add_field( array(
				'name'             => esc_html__( 'High Resolution', 'kiwi' ),
				'id' 				=> $prefix . 'marketplace_item_highres',
				'type'             => 'radio_inline',
				'show_option_none' => false,
				'options'          => array(
					'yes' 		=> esc_html__( 'Yes', 'kiwi' ),
					'no'   		=> esc_html__( 'No', 'kiwi' ),
					'na'   		=> esc_html__( 'N/A', 'kiwi' ),
				),
			) );
			
			$cmb_demo->add_field( array(
				'name'             => esc_html__( 'Fully responsive', 'kiwi' ),
				'id' 			   => $prefix . 'marketplace_item_responsive',
				'type'             => 'radio_inline',
				'show_option_none' => false,
				'options'          => array(
					'yes' 		=> esc_html__( 'Yes', 'kiwi' ),
					'no'   		=> esc_html__( 'No', 'kiwi' ),
					'na'   		=> esc_html__( 'N/A', 'kiwi' ),
				),
			) );
			
			$cmb_demo->add_field( array(
				'name'             => esc_html__( 'Widget Ready', 'kiwi' ),
				'id' 			   => $prefix . 'marketplace_item_widget',
				'type'             => 'radio_inline',
				'show_option_none' => false,
				'options'          => array(
					'yes' 		=> esc_html__( 'Yes', 'kiwi' ),
					'no'   		=> esc_html__( 'No', 'kiwi' ),
					'na'   		=> esc_html__( 'N/A', 'kiwi' ),
				),
			) );
			
			
			$cmb_demo->add_field( array(
				'name'             => esc_html__( 'Retina-Ready', 'kiwi' ),
				'id' 				=> $prefix . 'marketplace_item_retina',
				'type'             => 'radio_inline',
				'show_option_none' => false,
				'options'          => array(
					'yes' 		=> esc_html__( 'Yes', 'kiwi' ),
					'no'   		=> esc_html__( 'No', 'kiwi' ),
					'na'   		=> esc_html__( 'N/A', 'kiwi' ),
				),
			) );
			
			
			$cmb_demo->add_field( array(
				'name'             => esc_html__( 'Browsers Compatibility', 'kiwi' ),
				'desc'             => esc_html__( 'Select multiple options if applicable.', 'kiwi' ),
				'id'               => $prefix . 'marketplace_item_browser',
				'type'             => 'multicheck_inline',
				'options'          => array(
					'IE6' 		 	=> esc_html__( 'IE6', 'kiwi' ),
					'IE7'  			=> esc_html__( 'IE7', 'kiwi' ),
					'IE8'     		=> esc_html__( 'IE8', 'kiwi' ),
					'IE9' 			=> esc_html__( 'IE9', 'kiwi' ),
					'IE10'  		=> esc_html__( 'IE10', 'kiwi' ),
					'IE11'     		=> esc_html__( 'IE11', 'kiwi' ),
					'Firefox' 		=> esc_html__( 'Firefox', 'kiwi' ),
					'Safari'   		=> esc_html__( 'Safari', 'kiwi' ),
					'Opera'     	=> esc_html__( 'Opera', 'kiwi' ),
					'Chrome'    	=> esc_html__( 'Chrome', 'kiwi' ),
				),
			) );	
		
		
		$cmb_demo->add_field( array(
			'name'    => esc_html__( 'File size', 'kiwi' ),
			'desc'    => esc_html__( 'E.g.: 14.21MB', 'kiwi' ),
			//'default' => '80px auto 80px auto',
			'id'      => $prefix . 'marketplace_item_filesize',
			'type'    => 'text'
		) );
		
		$cmb_demo->add_field( array(
				'name'             => esc_html__( 'Item Support', 'kiwi' ),
				'id' 				=> $prefix . 'marketplace_item_support',
				'type'             => 'radio_inline',
				'show_option_none' => false,
				'options'          => array(
					'yes' 		=> esc_html__( 'Yes', 'kiwi' ),
					'no'   		=> esc_html__( 'No', 'kiwi' ),
					'na'   		=> esc_html__( 'N/A', 'kiwi' ),
				),
			) );
			
		$cmb_demo->add_field( array(
			'name'   	 => esc_html__( 'Support URL', 'kiwi' ),
			//'desc'    => 'http://',
			//'default' => '80px auto 80px auto',
			'id'      => $prefix . 'marketplace_item_support_url',
			'type'    => 'text_url'
		) );
		
		$cmb_demo->add_field( array(
			'name'    => 	esc_html__( 'Changelog URL', 'kiwi' ),
			//'desc'    => 'http://',
			//'default' => '80px auto 80px auto',
			'id'      => $prefix . 'marketplace_item_changelog_url',
			'type'    => 'text_url'
		) );
		
		$cmb_demo->add_field( array(
			'name'    => esc_html__( 'Documentation URL', 'kiwi' ),
			//'desc'    => 'http://',
			//'default' => '80px auto 80px auto',
			'id'      => $prefix . 'marketplace_item_documentation_url',
			'type'    => 'text_url'
		) );
		
		
		$cmb_demo->add_field( array(
			'name'    => esc_html__( 'Demo URL', 'kiwi' ),
			'desc'    => esc_html__( 'http://', 'kiwi' ),
			//'default' => '80px auto 80px auto',
			'id'      => $prefix . 'marketplace_demo_url',
			'type'    => 'text_url'
		) );
		
		
		
		$cmb_demo->add_field( array(
			'name' => esc_html__( 'Audio file', 'kiwi' ),
			'desc' => esc_html__( 'Upload an audio file.', 'kiwi' ),
			'id'   => $prefix . 'edd_audio_upload',
			'type' => 'file_list',
		) );
		
		
		$cmb_demo->add_field( array(
			'name' => esc_html__( 'Video file', 'kiwi' ),
			'desc' => esc_html__( 'Upload an video file.', 'kiwi' ),
			'id'   => $prefix . 'edd_video_upload',
			'type' => 'file_list',
		) );
		
		$cmb_demo->add_field( array(
			'name' => esc_html__( 'Embed audio/video', 'kiwi' ),
			'desc' => __( 'Enter a youtube, souncloud, or vimeo URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'kiwi' ),
			'id'   => $prefix . 'edd_media_embed',
			'type' => 'oembed',
			// 'repeatable' => true,
		) );
		
		
		/* $cmb_demo->add_field( array(
			'name' => esc_html__( 'Preview Images (slider)', 'kiwi' ),
			//'desc' => __( 'Enter a youtube, souncloud, or vimeo URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'kiwi' ),
			'id'   => $prefix . 'mp_output_slider_images',
			'type' => 'file_list',
			'preview_size' => array( 50, 50 ), 	
		) ); */
		
		
	}
	add_action( 'cmb2_init', 'mp_metabox_fallback_demolink' );	
	
	
/*===============================================================
	Register new taxonomies
===============================================================*/ 	
add_action( 'init', 'create_new_download_taxonomies', 0 );

function create_new_download_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => esc_html_x( 'Frameworks', 'taxonomy general name', 'kiwi' ),
		'singular_name'     => esc_html_x( 'Framework', 'taxonomy singular name', 'kiwi' ),
		'search_items'      => esc_html__( 'Search Frameworks', 'kiwi' ),
		'all_items'         => esc_html__( 'All Frameworks', 'kiwi' ),
		'parent_item'       => esc_html__( 'Parent Framework', 'kiwi' ),
		'parent_item_colon' => esc_html__( 'Parent Framework:', 'kiwi' ),
		'edit_item'         => esc_html__( 'Edit Framework', 'kiwi' ),
		'update_item'       => esc_html__( 'Update Framework', 'kiwi' ),
		'add_new_item'      => esc_html__( 'Add New Framework', 'kiwi' ),
		'new_item_name'     => esc_html__( 'New Framework Name', 'kiwi' ),
		'menu_name'         => esc_html__( 'Framework', 'kiwi' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'framework' ),
	);

	register_taxonomy( 'framework', array( 'download' ), $args );

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => esc_html_x( 'Plugins', 'taxonomy general name', 'kiwi' ),
		'singular_name'              => esc_html_x( 'Plugin', 'taxonomy singular name', 'kiwi' ),
		'search_items'               => esc_html__( 'Search Plugins', 'kiwi' ),
		'popular_items'              => esc_html__( 'Popular Plugins', 'kiwi' ),
		'all_items'                  => esc_html__( 'All Plugins', 'kiwi' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => esc_html__( 'Edit Plugin', 'kiwi' ),
		'update_item'                => esc_html__( 'Update Plugin', 'kiwi' ),
		'add_new_item'               => esc_html__( 'Add New Plugin', 'kiwi' ),
		'new_item_name'              => esc_html__( 'New Plugin Name', 'kiwi' ),
		'separate_items_with_commas' => esc_html__( 'Separate plugin with commas', 'kiwi' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove plugins', 'kiwi' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used plugins', 'kiwi' ),
		'not_found'                  => esc_html__( 'No plugins found.', 'kiwi' ),
		'menu_name'                  => esc_html__( 'Plugins', 'kiwi' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'plugin' ),
	);

	register_taxonomy( 'plugins', 'download', $args );
	
	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => esc_html_x( 'File Formats', 'taxonomy general name', 'kiwi' ),
		'singular_name'              => esc_html_x( 'File Format', 'taxonomy singular name', 'kiwi' ),
		'search_items'               => esc_html__( 'Search File Formats', 'kiwi' ),
		'popular_items'              => esc_html__( 'Popular File Formats', 'kiwi' ),
		'all_items'                  => esc_html__( 'All File Formats', 'kiwi' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => esc_html__( 'Edit File Format', 'kiwi' ),
		'update_item'                => esc_html__( 'Update File Format', 'kiwi' ),
		'add_new_item'               => esc_html__( 'Add New File Format', 'kiwi' ),
		'new_item_name'              => esc_html__( 'New File Format Name', 'kiwi' ),
		'separate_items_with_commas' => esc_html__( 'Separate file format with commas', 'kiwi' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove file formats', 'kiwi' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used file formats', 'kiwi' ),
		'not_found'                  => esc_html__( 'No file formats found.', 'kiwi' ),
		'menu_name'                  => esc_html__( 'File Formats', 'kiwi' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'fileformat' ),
	);

	register_taxonomy( 'fileformats', 'download', $args );
	
	
}	
	
?>
<?php 
 
/*===============================================================
	Credits: http://fellowtuts.com/wordpress/wordpress-custom-breadcrumbs-without-plugin/
===============================================================*/

function ft_custom_breadcrumbs(array $options = array() ) {
	
	// default values assigned to options
	$options = array_merge(array(
    'crumbId' => 'nav_crumb', // id for the breadcrumb Div
	'crumbClass' => 'nav_crumb', // class for the breadcrumb Div
	'beginningText' => '', // text showing before breadcrumb starts
	'showOnHome' => 1,// 1 - show breadcrumbs on the homepage, 0 - don't show
	'delimiter' => ' &raquo; ', // delimiter between crumbs
	'homePageText' => esc_html__( 'Home', 'kiwi' ), // text for the 'Home' link
	'showCurrent' => 1, // 1 - show current post/page title in breadcrumbs, 0 - don't show
	'beforeTag' => '<span class="current">', // tag before the current breadcrumb
	'afterTag' => '</span>', // tag after the current crumb
	'showTitle'=> 1 // showing post/page title or slug if title to show then 1
   ), $options);
   
    $crumbId = $options['crumbId'];
	$crumbClass = $options['crumbClass'];
	$beginningText = $options['beginningText'] ;
	$showOnHome = $options['showOnHome'];
	$delimiter = $options['delimiter']; 
	$homePageText = $options['homePageText']; 
	$showCurrent = $options['showCurrent']; 
	$beforeTag = $options['beforeTag']; 
	$afterTag = $options['afterTag']; 
	$showTitle =  $options['showTitle']; 
	
	$marketplaceslug = esc_html__( 'Items', 'kiwi' );
	$marketplaceitemdesc = esc_html__( 'Item', 'kiwi' );
	$media = esc_html__( 'Media Library', 'kiwi' );
	
	global $post;

	$wp_query = $GLOBALS['wp_query'];

	$homeLink = home_url();

	$snippet_item = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$snippet_name = '<span itemprop="name">';
	
	$snippet_item_end = '</span>';
	$snippet_item_link = ' itemprop="item" ';

	
	
	// echo '<div id="'.$crumbId.'" class="'.$crumbClass.'" >'.$beginningText;
	echo ''.$beginningText;
	
	if (is_home() || is_front_page()) {
	
	  if ($showOnHome == 1)

		  echo $beforeTag . $homePageText . $afterTag;

	} else { 
	
	  echo ''.$snippet_item.'<a'.$snippet_item_link.'href="' . esc_url( $homeLink ). '">' . $snippet_name . $homePageText . $snippet_item_end . '</a> ' . $delimiter . $snippet_item_end . ' ';
	
	  if ( is_category() ) {
		$thisCat = get_category(get_query_var('cat'), false);
		if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
		//echo $beforeTag . 'Category Archives: ' . single_cat_title('', false) . ' ' . $afterTag;
			echo $beforeTag . ''. esc_html__( 'Category Archives: ', 'kiwi' ). '' . single_cat_title('', false) . ' ' . $afterTag;
	
	
	  } elseif ( is_tax() ) {
		  $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		  $parents = array();
		  $parent = $term->parent;
		  while ( $parent ) {
			  $parents[] = $parent;
			  $new_parent = get_term_by( 'id', $parent, get_query_var( 'taxonomy' ) );
			  $parent = $new_parent->parent;

		  }		  
		  if ( ! empty( $parents ) ) {
			  $parents = array_reverse( $parents );
			  foreach ( $parents as $parent ) {
				  $item = get_term_by( 'id', $parent, get_query_var( 'taxonomy' ));
				  echo   ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_term_link( $item->slug, get_query_var( 'taxonomy' ) ) . '">' . $snippet_name . $item->name . $snippet_item_end . '</a>'  . $delimiter . $snippet_item_end . ' ';
			  }
		  }
		  
		  

		  $queried_object = $wp_query->get_queried_object();
		  echo $beforeTag . $queried_object->name . $afterTag;	  
		  } elseif ( is_search() ) {
		 echo $beforeTag . ''. esc_html__( 'Search results for: ', 'kiwi' ). '' . $snippet_name . esc_html( get_search_query(false) ) . $snippet_item_end . '' . $afterTag;
		
	
	  } elseif ( is_day() ) {
		echo ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_year_link(get_the_time('Y')) . '">' . $snippet_name . get_the_time('Y') .  $snippet_item_end . '</a> ' . $delimiter  . $snippet_item_end . ' ';
		echo ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' .  $snippet_name . get_the_time('F') . $snippet_item_end . '</a> ' . $delimiter . $snippet_item_end  . ' ';
		echo $beforeTag . get_the_time('d') . $afterTag;
	
	  } elseif ( is_month() ) {
		echo ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_year_link(get_the_time('Y')) . '">' . $snippet_name . get_the_time('Y') . $snippet_item_end . '</a> ' . $delimiter . $snippet_item_end  . ' ';
		echo $beforeTag . get_the_time('F') . $afterTag;
	
	  } elseif ( is_year() ) {
		echo $beforeTag . get_the_time('Y') . $afterTag;
	
	  } elseif ( is_single() && !is_attachment() ) {
		  
		     if($showTitle)
			   $title = get_the_title();
			  else
			  $title =  $post->post_name;
		  
					 if ( get_post_type() == 'product' ) { // it is for custom post type with custom taxonomies like
					// Breadcrumb would be : Home Furnishings > Bed Covers > Cotton Quilt King Kantha Bedspread 
					// product = Cotton Quilt King Kantha Bedspread, custom taxonomy product_cat (Home Furnishings -> Bed Covers)
					// show  product with category on single page
					  if ( $terms = wp_get_object_terms( $post->ID, 'product_cat' ) ) {
		
						  $term = current( $terms );
		
						  $parents = array();
		
						  $parent = $term->parent;
						  while ( $parent ) {
		
							  $parents[] = $parent;
		
							  $new_parent = get_term_by( 'id', $parent, 'product_cat' );
		
							  $parent = $new_parent->parent;
		
						  }
						  if ( ! empty( $parents ) ) {			
		
							  $parents = array_reverse($parents);
		
							  foreach ( $parents as $parent ) {
		
								  $item = get_term_by( 'id', $parent, 'product_cat');
		
								  echo  ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_term_link( $item->slug, 'product_cat' ) . '">' . $snippet_name . $item->name . $snippet_item_end . '</a>'  . $delimiter . $snippet_item_end  . ' ';
		
							  }
		
						  }
						  echo  ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_term_link( $term->slug, 'product_cat' ) . '">' . $snippet_name . $term->name . $snippet_item_end . '</a>'  . $delimiter . $snippet_item_end  . ' ';
					  }
					  echo $beforeTag . $title . $afterTag;
				  }  elseif ( get_post_type() != 'post' ) {
				  $post_type = get_post_type_object(get_post_type());
				  $slug = $post_type->rewrite;
				  
				  
				if (!is_singular( 'download' )) { 
					echo ''.$snippet_item.'<a'.$snippet_item_link.'href="' . $homeLink . '/' . $slug['slug'] . '/">' . $snippet_name . $post_type->labels->singular_name . $snippet_item_end . '</a>' . $snippet_item_end  . ' ';
				}
				
				if (is_singular( 'download' )) { 
					echo ''.$snippet_item.'<a'.$snippet_item_link.'href="' . $homeLink . '/' . $slug['slug'] . '/">' . $snippet_name . $marketplaceslug . $snippet_item_end . '</a>' . $snippet_item_end  . ' ';
				}
								
				 if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $beforeTag . $title . $afterTag;
				} else {
				  $cat = get_the_category(); $cat = $cat[0];
				  $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				  if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
				  echo '' . $snippet_item . '<a'.$snippet_item_link.'href="' . get_category_link( $cat->term_id ) . '" title="' . sprintf( esc_html__( "View all posts in %s",'kiwi' ), $cat->name ) . '" ' . '>'. $snippet_name . $cat->name . $snippet_item_end . '</a></span>' . $delimiter  . ' '; 
				  if ($showCurrent == 1) echo $beforeTag . $title . $afterTag;
				
				}
}  

// elseif ( !is_single() && !is_page() && get_post_type() != 'post' && get_post_type() != 'download' && !is_404() ) {
   elseif ( !is_single() && !is_page() && get_post_type() != 'post' && get_post_type() != 'download' && !is_404() ) {
  
$post_type = get_post_type_object(get_post_type());

echo $beforeTag . $post_type->labels->singular_name . $afterTag;			 
}  	 

elseif ( !is_single() && !is_page() && get_post_type() != 'post' && get_post_type() == 'download' && !is_404() ) {		  

$post_type = get_post_type_object(get_post_type());		

echo $beforeTag . $marketplaceslug . $afterTag; 
} 

	 
	 elseif ( is_attachment() ) {
			 
		//$parent = get_post($post->post_parent);
		//$cat = get_the_category($parent->ID); $cat = $cat[0];
		//echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
		//echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
		
		echo $media;
		if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $beforeTag . get_the_title() . $afterTag;	
		  
		} elseif ( is_page() && !$post->post_parent ) {
			$title =($showTitle)? get_the_title():$post->post_name;
			  
		if ($showCurrent == 1) echo $beforeTag .  $title . $afterTag;

	  } elseif ( is_page() && $post->post_parent ) {
		$parent_id  = $post->post_parent;
		$breadcrumbs = array();
		while ($parent_id) {									
		  $page = get_page($parent_id);
		  $breadcrumbs[] = ''.$snippet_item.'<a'.$snippet_item_link.'href="' . get_permalink($page->ID) . '">' . $snippet_name . get_the_title($page->ID) . '</a>' . $snippet_item_end  . ' ';
		  $parent_id  = $page->post_parent;
		}
		$breadcrumbs = array_reverse($breadcrumbs);
		for ($i = 0; $i < count($breadcrumbs); $i++) {
		  echo $breadcrumbs[$i];
		  if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
		}
			$title =($showTitle)? get_the_title():$post->post_name;
		   
	if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $beforeTag . $title . $afterTag;

	  } elseif ( is_tag() ) {

		// echo $beforeTag . 'Posts tagged "' . single_tag_title('', false) . '"' . $afterTag;
		echo $beforeTag . ''. esc_html__( 'Posts tagged with: ', 'kiwi' ). '' . single_tag_title('', false) . '' . $afterTag;
		
		
		
	  } elseif ( is_author() ) {
		 global $author;
		$userdata = get_userdata($author);

		//echo $beforeTag . 'Articles posted by ' . $userdata->display_name . $afterTag;
		echo $beforeTag . ''. esc_html__( 'Articles posted by: ', 'kiwi' ). '' . $userdata->display_name . $afterTag;
		
		

	  } elseif ( is_404() ) {
		  
		//echo $beforeTag . 'Error 404' . $afterTag;
		  echo $beforeTag . ''. esc_html__( 'Error 404', 'kiwi' ). '' . $afterTag;
			
	  }

	  if ( get_query_var('paged') ) {
		if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() || is_tax() ) echo '';
		//echo __('Page','kiwi') . ' ' . get_query_var('paged');
		echo $delimiter, esc_html__('Page ', 'kiwi'), get_query_var('paged'); 
		if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() || is_tax() ) echo '';
	  }
	}
//	echo '</div>';
  }
// end ft_custom_breadcrumb


?>
<?php 
 
function function_social_media(){
	
	global $kiwi_theme_option;
	$kiwi_social_media = $kiwi_theme_option['topbar-contentleft'] . $kiwi_theme_option['topbar-contentright'] . $kiwi_theme_option['footer-contentright']; 
	
	echo '<ul class="socialmedia">';
	if($kiwi_social_media >= '2'){
		if ( !empty($kiwi_theme_option['socialmedia-facebook'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-facebook'] ) .'><i class="fa fa-facebook"></i></a></li>';	
		}		
		if ( !empty($kiwi_theme_option['socialmedia-twitter'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-twitter'] ) .'><i class="fa fa-twitter"></i></a></li>';	
		}		
		if ( !empty($kiwi_theme_option['socialmedia-tumblr'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-tumblr'] ) .'><i class="fa fa-tumblr"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-linkedin'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-linkedin'] ) .'><i class="fa fa-linkedin"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-google'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-google'] ) .'><i class="fa fa-google-plus"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-rss'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-rss'] ) .'><i class="fa fa-rss"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-flickr'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-flickr'] ) .'><i class="fa fa-flickr"></i></a></li>';	
		}		
		if ( !empty($kiwi_theme_option['socialmedia-dribbble'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-dribbble'] ) .'><i class="fa fa-dribbble"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-pinterest'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-pinterest'] ) .'><i class="fa fa-pinterest"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-skype'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-skype'] ) .'><i class="fa fa-skype"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-youtube'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-youtube'] ) .'><i class="fa fa-youtube-play"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-vimeo'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-vimeo'] ) .'><i class="fa fa-vimeo-square"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-behance'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-behance'] ) .'><i class="fa fa-behance"></i></a></li>';	
		}		
		if ( !empty($kiwi_theme_option['socialmedia-spotify'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-spotify'] ) .'><i class="fa fa-spotify"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-instagram'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-instagram'] ) .'><i class="fa fa-instagram"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-github'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-github'] ) .'><i class="fa fa-github"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-stackexchange'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-stackexchange'] ) .'><i class="fa fa-stack-exchange"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-soundcloud'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-soundcloud'] ) .'><i class="fa fa-soundcloud"></i></a></li>';	
		}
		if ( !empty($kiwi_theme_option['socialmedia-vk'])) { 
		echo '<li><a target="_blank" href='. esc_url( $kiwi_theme_option['socialmedia-vk'] ) .'><i class="fa fa-vk"></i></a></li>';	
		}		
	}
	echo '</ul>'; 
}
add_action('kiwi_social_media', 'function_social_media');

?>
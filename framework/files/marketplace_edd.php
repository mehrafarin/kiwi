<?php


/*===============================================================
	Change slug name
===============================================================*/
if ( ! defined( 'EDD_SLUG' ) ) { 
	define( 'EDD_SLUG', apply_filters( 'kiwi_edd_slug', 'item' ) ); 
};

/*===============================================================
	EDD .htaccess rules
===============================================================*/
function mp_edd_filetypes($arr) {
	$arr = array('jpg','png','gif','mp3','ogg','mp4','m4v','mov','wmv','avi','mpg','ogv','3gp','3g2');
	return $arr;
}
add_filter( 'edd_protected_directory_allowed_filetypes', 'mp_edd_filetypes', 10, 1 );


/*===============================================================
	Change post type labels
===============================================================*/
function set_download_labels($labels) {
	$labels = apply_filters( 'kiwi_posttype_labels', array(
		'name'				 	=> esc_html_x('Marketplace', 'post type general name', 'kiwi'),
		'singular_name' 		=> esc_html_x('Marketplace', 'post type singular name', 'kiwi'),
		'add_new' 				=> esc_html__('Add New', 'kiwi'),
		'add_new_item' 			=> esc_html__('Add New Item', 'kiwi'),
		'edit_item' 			=> esc_html__('Edit Item', 'kiwi'),
		'new_item' 				=> esc_html__('New Item', 'kiwi'),
		'all_items'				=> esc_html__('All Items', 'kiwi'),
		'view_item' 			=> esc_html__('View Item', 'kiwi'),
		'search_items' 			=> esc_html__('Search Items', 'kiwi'),
		'not_found' 			=> esc_html__('No Items found', 'kiwi'),
		'not_found_in_trash' 	=> esc_html__('No Items found in Trash', 'kiwi'), 
		'parent_item_colon' 	=> '',
		'menu_name' 			=> esc_html__('Marketplace', 'kiwi')
	));
	return $labels;
}
add_filter('edd_download_labels', 'set_download_labels');


/*===============================================================
	Change all EDD labels
===============================================================*/
function pw_edd_product_labels( $labels ) {
	$labels = array(
	   'singular' 	=> esc_html__('Item', 'kiwi'),
	   'plural' 	=> esc_html__('Items', 'kiwi')
	);
	return $labels;
}
add_filter('edd_default_downloads_name', 'pw_edd_product_labels');




/*===============================================================
	Cookie for displaying recently viewed inside widget
===============================================================*/
function complete_cookie() {
	
	$viewed_posts = array();	
	
	if(isset($_COOKIE['mp_recently_viewed']) && $_COOKIE['mp_recently_viewed']!='') { 
		  
		$viewed_posts =  unserialize($_COOKIE['mp_recently_viewed']);
		
		if (! is_array($viewed_posts)) {			
				$viewed_posts = array(get_the_ID()); 
		} else{			 
		 $viewed_posts = array_diff($viewed_posts, array(get_the_ID())); // for removing current post in cookie 
			array_unshift($viewed_posts,get_the_ID());// update cookie with current post
		}
		
	 } else {
		  $viewed_posts = array(get_the_ID());
	 }
	 
	 setcookie( 'mp_recently_viewed', serialize($viewed_posts), strtotime( '+7 days' ), COOKIEPATH, COOKIE_DOMAIN, false, false );
	 
}
add_action( 'get_header', 'complete_cookie' );



/*===============================================================
	Removal add to cart button under item description
===============================================================*/
remove_action( 'edd_after_download_content', 'edd_append_purchase_link' );

function marketplace_purchase_link( $download_id ) {
	
	global $kiwi_theme_option;
	
	if ( ! get_post_meta( $download_id, '_edd_hide_purchase_link', true ) && $kiwi_theme_option['marketplace-hidepurchasebutton'] == '1' ) {
		echo edd_get_purchase_link( 
			array( 
				'download_id' 	=> $download_id, 
				// 'class' 	=> 'edd-submit my-new-class', // add your new classes here
				'price' 	=> 0 // turn the price off
			)
		);
	}
	
	if ( $kiwi_theme_option['marketplace-hidepurchasebutton'] == '1' ) {			
		echo edd_get_purchase_link( 
			array( 
				'download_id' 	=> $download_id, 
			// 	'class' 	=> 'edd-submit my-new-class', // add your new classes here
				'price' 	=> 0 // turn the price off
			)
		);
	}

}
add_action( 'edd_after_download_content', 'marketplace_purchase_link' );


/*===============================================================
	Adding Comments Support to item description
===============================================================*/
 function mp_comment_support($supports) {
	$supports[] = 'comments';
	return $supports;	
}
add_filter('edd_download_supports', 'mp_comment_support');
 

/*===============================================================
	Adding Author Support
===============================================================*/
function mp_author_support($supports) {
	$supports[] = 'author';
	return $supports;	
}
add_filter('edd_download_supports', 'mp_author_support');
 
 
/*===============================================================
	Marketplace :: Stats
===============================================================*/ 
if ( ! function_exists( 'kiwi_marketplace_stats' ) && class_exists( 'Easy_Digital_Downloads' ) ) :

function kiwi_marketplace_stats() {
	
	global $kiwi_theme_option, $edd_logs;
	
	$downloads_count = wp_count_posts( 'download' );
	$count = $edd_logs->get_log_count( '*', 'sale' );
	
	 
		echo '<ul class="km-stats">';
		echo sprintf( _n( '<li><span>%1$s</span> item for sale</li> ', '<li><span>%1$s</span> items for sale</li> ', esc_html( $downloads_count->publish ), 'kiwi' ), esc_html( $downloads_count->publish ) );
		echo sprintf( _n( '<li><span>%1$s</span> item sold </li> ', '<li><span>%1$s</span> items sold</li>', esc_html( $count ), 'kiwi' ), esc_html( $count ) );	
		echo sprintf( _n( '<li><span>%1$s</span> transaction</li>', '<li>and <span>%1$s</span> transactions</li>', esc_html( edd_count_total_customers() ), 'kiwi' ), esc_html( edd_count_total_customers() ));		
		echo '</ul>';
		
 }
endif;


/*===============================================================
	Marketplace :: Stats shortcodes
===============================================================*/ 

function mpstats_one() {	
	$downloads_count = wp_count_posts( 'download' );	
	return '<span>' . esc_html( $downloads_count->publish ) . '</span>';
}
add_shortcode('mpstats-totalitems', 'mpstats_one');


function mpstats_two() {
	
	global $edd_logs;
	$count = $edd_logs->get_log_count( '*', 'sale' );	
	
	return '<span>' . esc_html( $count ). '</span>';
}
add_shortcode('mpstats-totalsales', 'mpstats_two');


function mpstats_three() {	
	return '<span>' . esc_html( edd_count_total_customers() ) . '</span>';
}
add_shortcode('mpstats-totalcustomers', 'mpstats_three');


function mpstats_four() {	

	$count_users = count_users();
	$number_users = $count_users['total_users'];
	
	return '<span>' . esc_html( $number_users ). '</span>';
}
add_shortcode('mpstats-totalusers', 'mpstats_four');


function mpstats_five() {	

	$count_vendors = count_users();
	$show = array('frontend_vendor');
	//$show = array('subscriber', 'administrator');		
	
	$total_vendors = 0;
	
	foreach($count_vendors['avail_roles'] as $role => $count){
		if( in_array($role, $show)) {		
			$total_vendors = $count;		
		} 
	}  
	
	//if( $total_vendors != '0' ) {
		return esc_html( $total_vendors );
	/* } else {
		return '0'; 
	} */
}
add_shortcode('mpstats-totalvendors', 'mpstats_five');








/*===============================================================
	Marketplace :: Variable number of posts per page
===============================================================*/
 function mp_archive_items( $query ) {
	
	global $kiwi_theme_option;

    if ( 
	/* is_singular( 'download' ) && $query->is_main_query() && !is_admin() */ /* this line cause conflict on single blog post */
		is_tax( 'download_category' ) && $query->is_main_query() && !is_admin() ||  is_tax( 'download_tag' ) && $query->is_main_query() && !is_admin() || is_post_type_archive( 'download' ) && $query->is_main_query() && !is_admin() || is_page_template('page-marketplace.php') && $query->is_main_query() && !is_admin() || is_page_template('page-themes.php') && $query->is_main_query() && !is_admin() ) {
			 
        $query->set( 'posts_per_page', $kiwi_theme_option['marketplace-sidebar-archive-postcount'] );
        return;		
    }
}
add_action( 'pre_get_posts', 'mp_archive_items' ); 


/*===============================================================
	Marketplace :: Categories ordered by parent-child category relationship
===============================================================*/
function marketplace_cat_terms() { 

	global $post;

$taxonomy 	= 'download_category';
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );


$prefix = esc_html__( 'Posted in: ', 'kiwi' );
$separator = esc_html__( ' &raquo; ', 'kiwi' ); 

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

		$term_ids = implode( ',' , $post_terms );
		$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
		$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
		
		// display post categories
		echo '<span class="categories">';
		echo $prefix . $terms;
		echo '</span>';
	}

 }
 
 
/*===============================================================
	Marketplace :: Remove Decimals
===============================================================*/
function mp_remove_decimals( $decimals, $amount ) {	

global $kiwi_theme_option;

	if( floor( $amount ) == $amount && $kiwi_theme_option['marketplace-decimal-enable'] == '1' ) {
		$decimals = 0;
	}
	
	if( floor( $amount ) == $amount && $kiwi_theme_option['marketplace-decimal-enable'] == '0' ) {
		$decimals = 2;
	}
	
	return $decimals;
}

add_filter( 'edd_format_amount_decimals', 'mp_remove_decimals', 10, 2 ); 


/*===============================================================
	Marketplace :: Add span + move count inside link
===============================================================*/
function cat_count_span($links) {	
    $links = str_replace('</a> (', '<span class="dcjq-count ver">', $links);
	$links = str_replace(')', '</span></a>', $links);
	return $links;    
} 
add_filter('wp_list_categories', 'cat_count_span', 10, 1);


/*===============================================================
	Marketplace :: Custom search template on EDD pages
===============================================================*/
 function mp_template_chooser($template) {
  global $wp_query;
  $post_type = get_post_type();
  if( $wp_query->is_search && $post_type == 'download' )
  {
    return locate_template('archive-' . $post_type . '.php');
  }
  return $template;
}
add_filter('template_include', 'mp_template_chooser');




 /*===============================================================
	Marketplace :: Checkout icon
===============================================================*/
function mp_filter_checkout( $result ) {
		
		$search  		= esc_html__( 'Checkout', 'kiwi' );
		$replacewith	= __( '<i class="fa fa-sign-out"></i>', 'kiwi' );		
		$result			= str_replace( $search, $replacewith, $result );

		return $result;
	
}
add_filter( 'edd_purchase_download_form', 'mp_filter_checkout' ); 

  
 /*===============================================================
	Marketplace :: Filter [download] shortcode
===============================================================*/
function mp_filter_download( $result ) {
		$search  		= '<div style="clear:both;"></div>';
		$replacewith	= ' ';		
		return str_replace( $search, $replacewith, $result );
}
add_filter( 'downloads_shortcode', 'mp_filter_download', 10, 2 );


/* Add class clear before pagination */
function mp_filter_downloadtwo( $result ) {	
		$search  		= '<div id="edd_download_pagination" class="navigation">';
		$replacewith	= '<div class="clear"></div><div id="edd_download_pagination" class="navigation">';	
		
		return str_replace( $search, $replacewith, $result );
}
add_filter( 'downloads_shortcode', 'mp_filter_downloadtwo', 10, 2 );


function mp_filter_download_add_class( $result ) {
		$search  		= 'edd_downloads_list';
		$replacewith	= 'edd_downloads_list mp-vc-items';		
		return str_replace( $search, $replacewith, $result );
}
add_filter( 'downloads_shortcode', 'mp_filter_download_add_class', 10, 2 );






















/*===============================================================
	Marketplace :: Filter cart widget
===============================================================*/ 
function mp_filter_widget( $item, $id ) {
	 
	$search  	= '<span class="edd-cart-item-separator">-</span>';
	//$replace	= '<span class="variable-option">';
	$replace	= ' ';
	
	return str_replace( $search, $replace, $item );
}
add_filter( 'edd_cart_item', 'mp_filter_widget', 10, 2 );  
  
 


/*===============================================================
	Marketplace :: Login/Register/Dashboard links inside topmenu bar
===============================================================*/
function add_nav_menu_items( $items, $args ) {
	
	global $kiwi_theme_option, $user;
	
	$user = wp_get_current_user();
		
	$loginmessage = '<li class="mp-login-message">'.esc_html__( 'Hello,', 'kiwi' ).' '.$user->display_name.'</li>';
	$logout = '<li class="mp-logout"><a href="'. esc_url( wp_logout_url( home_url() ) ) .'">'.esc_html__( 'Log out', 'kiwi' ).'</a></li>';
		
	$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
	$buyer_link = get_page_link ($url_id);
	
	$url_id_vendor = $kiwi_theme_option['marketplace-enablelinks-dashboard'];
	$vendor_link = get_page_link ($url_id_vendor);
	
	$overlay_search = '';
	
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
					
					if ( class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor() ) {
						
							if ( isset($kiwi_theme_option['marketplace-enablelinks-dashboard']) && !empty($kiwi_theme_option['marketplace-enablelinks-dashboard']) ) {	
									$dashboard = '<li class="mp-dashboard"><a href="'. esc_url( $vendor_link ).'">'.esc_html__( 'Dashboard', 'kiwi' ).'</a></li>';
								} else {
									$dashboard = '';
								}	
							
					} else {								
								
							if ( isset($kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber']) && !empty($kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber']) ) {	
									$dashboard = '<li class="mp-dashboard"><a href="'. esc_url( $buyer_link ).'">'.esc_html__( 'Dashboard', 'kiwi' ).'</a></li>';
								} else {
									$dashboard = '';
								}
								
					}	
	}	
	
	if ( isset($kiwi_theme_option['marketplace-enablelinks-register']) && !empty($kiwi_theme_option['marketplace-enablelinks-register']) ) {	
		$register = '<li class="mp-register"><a href="'. get_page_link ($kiwi_theme_option['marketplace-enablelinks-register']) .'">'.esc_html__( 'Register', 'kiwi' ).'</a></li>';
	} else {
		$register = '<li class="mp-register"><a href="' . esc_url( wp_registration_url() ). '">'.esc_html__( 'Register', 'kiwi' ).'</a></li>';
	}
	
	if ( isset($kiwi_theme_option['marketplace-enablelinks-login']) && !empty($kiwi_theme_option['marketplace-enablelinks-login']) ) {	
		$login = '<li class="mp-login"><a href="'. get_page_link ($kiwi_theme_option['marketplace-enablelinks-login']) .'">'.esc_html__( 'Log in', 'kiwi' ).'</a></li>';
	} else {
		$login = '<li class="mp-login"><a href="' . esc_url( wp_login_url() ). '">'.esc_html__( 'Log in', 'kiwi' ).'</a></li>';
	}	
	
		
		 if( $kiwi_theme_option['header-expandable-search-topbar'] == '1' ){			
			if( $args->theme_location == 'top_navigation' || $args->theme_location == 'nonlogged_navigation' || 
			$args->theme_location == 'logged_navigation' || $args->theme_location == 'vendor_navigation' ) {
				$overlay_search .= '<li class="menu-item menu-item-search search-form">';
				$overlay_search .= ' <a id="overlay-menu"><i class="search-field"></i></a>';	
				$overlay_search .= '</li>'; 
			 }
		} else {
			$overlay_search .= '';
		} 
		
	
	if ( $kiwi_theme_option['marketplace-enablelinks-topbar'] == '1' && $kiwi_theme_option['marketplace-empty-userroles'] == '0') {
		
		if (is_user_logged_in() && $args->theme_location == 'top_navigation' || is_user_logged_in() && $args->theme_location == 'logged_navigation' ||
		is_user_logged_in() && $args->theme_location == 'vendor_navigation') {
			
			 if ( $kiwi_theme_option['marketplace-enablelinks-username'] == '1' ) { 
				$items = $loginmessage . $dashboard . $logout . $items;
			} 
			
			if ( $kiwi_theme_option['marketplace-enablelinks-username'] == '0' ) { 
				$items = $dashboard . $logout . $items;	
			} 
		
		} elseif (!is_user_logged_in() && $args->theme_location == 'top_navigation' || !is_user_logged_in() && $args->theme_location == 'nonlogged_navigation' ) {
			$items = $login . $register  . $items;			
		}    	
	}
	
		if ( $kiwi_theme_option['marketplace-enablelinks-topbar'] == '1' && $kiwi_theme_option['marketplace-empty-userroles'] == '1' ) { 
			$items = $items;					
		} 
	
	
	if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
		 return $overlay_search . $items;
	} else {
		 return $items . $overlay_search;
	}
	
   
}
add_filter( 'wp_nav_menu_items', 'add_nav_menu_items', 10, 2 );

 

 
/*===============================================================
	Marketplace :: Plugin Wishlist
===============================================================*/ 

/**
 * Pass in the correct download ID to the wish list function responsible for outputting the 'add to wish list' button
 */
function mp_load_wish_list_link( $instance, $download_id ) {	
	if ( function_exists ( 'easy_share_buttons' ) && function_exists( 'edd_wl_load_wish_list_link' ) ) {
		edd_wl_load_wish_list_link( $download_id );
	}
}
 
function mp_remove_wl_action() {	
	if ( function_exists ( 'easy_share_buttons' )) {	
		// remove link from all carts
		remove_action( 'edd_purchase_link_top', 'edd_wl_load_wish_list_link' );	
		// add link inside widget :: purchase block
		add_action( 'edd_product_details_widget_after_purchase_button', 'mp_load_wish_list_link', 10, 2 );
		}
}
add_action( 'template_redirect', 'mp_remove_wl_action' );



 
 
/*===============================================================
	Marketplace :: Plugin :: Content restriction
===============================================================*/ 
/* remove pages inside receipt */ 
if ( class_exists ( 'EDD_Content_Restriction' )) {

	function mp_remove_restriction_plugin() {					
		remove_action( 'edd_payment_receipt_after', 'edd_cr_add_to_receipt', 1, 2 );
		}

add_action( 'template_redirect', 'mp_remove_restriction_plugin' );
}




/*===============================================================
	Marketplace :: Plugin :: Currency Converter
===============================================================*/ 
/* remove pages inside receipt */ 
if ( class_exists ( 'EDD_Currency_Public' )) {

	function mp_edd_extension_currency_price_format_append($output) {
			$search  = array(' ( ', ' ) ');
			$replace = array('<span class="edd-currency-price-append">', '</span>'); 
			
			return str_replace( $search, $replace, $output );
		}	
	add_filter( 'edd_currency_change_before_format', 'mp_edd_extension_currency_price_format_append', 10, 1 ); 
	add_filter( 'edd_currency_change_after_format', 'mp_edd_extension_currency_price_format_append', 10, 1 );

}




/*===============================================================
	Marketplace :: Plugin :: File Upload
===============================================================*/ 
if( class_exists( 'EDD_Upload_File' ) ) {

	function mp_ext_file_upload_before() {		
		echo '<div class="mp-ext-edd-file-upload">';	
	}
	add_action( 'edd_upload_file_before', 'mp_ext_file_upload_before' );
	 
	 
	function mp_ext_file_upload_after() {		
		echo '</div>';	
	}
	add_action( 'edd_upload_file_after', 'mp_ext_file_upload_after' );
	
	// remove standard actions 
	remove_action( 'edd_payment_receipt_after_table', 'edd_upload_file_print_uploaded_files', 11, 2 );
	remove_action( 'edd_payment_receipt_after_table', 'edd_upload_file_receipt_upload_field', 12, 2 );
	
	// add them inside a special created hook
	add_action( 'edd_payment_receipt_after_table_upload_field', 'edd_upload_file_print_uploaded_files', 10, 2 );
	add_action( 'edd_payment_receipt_after_table_upload_field', 'edd_upload_file_receipt_upload_field', 10, 2 );
	
	
	
	
	/**
	 * EDD Cross-sell & Upsell - Removing the excerpt
	*/
	function mp_ext_file_upload_excerpt() {		
			return false;
	}
	//add_filter( 'edd_csau_show_excerpt', 'mp_ext_file_upload_excerpt' );
	add_filter( 'edd_csau_show_price', 'mp_ext_file_upload_excerpt' );
	add_filter( 'edd_csau_upsell_show_button', 'mp_ext_file_upload_excerpt' );
	
	
	
	function mp_filter_cross_sell($output) {
		
		$search  = array('<h2>', '</h2>');
		$replace = array('<div class="edd_widget_title"><h3>', '</h3></div>'); 
		
		return str_replace( $search, $replace, $output );
		}
	add_filter( 'edd_csau_html', 'mp_filter_cross_sell', 10, 2 );

	
	
	if ( ! function_exists( 'mp_cross_sell_items' ) ) :
	function mp_cross_sell_items() {
		
		$products = get_post_meta( get_the_ID(), '_edd_csau_cross_sell_products', true );	
		$edd_cross_sells = edd_csau_html();
		
		if( ! empty( $products ) ) {
			
			echo '<div class="mp-product-description mp-edd-cross-sell">';
			echo $edd_cross_sells;	
			echo '</div>';
			
		}	
	}
	endif;
	
	

}


/*===============================================================
	Marketplace :: Title filter :: Social Discount
===============================================================*/
if ( class_exists ( 'EDD_Social_Discounts' )) {
	
	function mp_social_share_css_class($output) {
		
		$search  = array('<h3 class="edd-sd-title">', '</h3>');
		$replace = array('<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
		
		return str_replace( $search, $replace, $output );
		}
	add_filter( 'edd_social_discounts_share_title', 'mp_social_share_css_class', 10, 2 );

}



/*===============================================================
	Marketplace :: Extension :: Recommended Downloads
===============================================================*/ 
if ( class_exists ( 'EDDRecommendedDownloads' )) {
	
	if ( ! function_exists( 'mp_edd_ext_recommened_items' ) ) :
		function mp_edd_ext_recommened_items() {
			
			global $post;
			
			$rec_items = EDD()->session->set( 'edd_has_recommendations', $post->ID );					

			if( ! empty( $rec_items ) ) {		
				edd_rp_get_template_part( 'single_recommendations' );	
			}	
				
		}
	endif;	
	
}


/*===============================================================
	Marketplace :: Title filter :: Purchase Rewards
===============================================================*/
if ( class_exists ( 'EDD_Purchase_Rewards' )) {	

	function mp_purchase_rewards_css_class($output) {
			$search  = array('<h2 class="share-title">', '</h2>');
			$replace = array('<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
	 add_filter( 'edd_purchase_rewards_sharing_icons', 'mp_purchase_rewards_css_class', 10, 2 );
	 
	 
	 
	  function mp_purchase_rewards_discount_one($output) {
			$search  = array('<h3>','</h3>','<h2>','</h2>');
			$replace = array('<h3 class="purchase-rewards-discount-code">','</h3>', '<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
	 add_filter( 'edd_purchase_rewards_show_reward', 'mp_purchase_rewards_discount_one', 10, 2 ); 
 
}



/*===============================================================
	Marketplace :: FES
===============================================================*/
function mp_fes_header_register($output) {
			$search  = array('<h1 class="fes-headers" id="fes-registration-form-title">','</h1>');
			$replace = array('<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
add_filter( 'fes_registration_form_header', 'mp_fes_header_register', 10, 2 ); 


function mp_fes_header_login($output) {
			$search  = array('<h1 class="fes-headers" id="fes-login-form-title">','</h1>');
			$replace = array('<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
add_filter( 'fes_login_form_header', 'mp_fes_header_login', 10, 2 ); 



function mp_fes_header_contact($output) {
			$search  = array('<h1 class="fes-headers" id="fes-contact-form-title">','</h1>');
			$replace = array('<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
add_filter( 'fes_vendor_contact_form_header', 'mp_fes_header_contact', 10, 2 ); 


function mp_fes_header_profile($output) {
			$search  = array('<h1 class="fes-headers" id="fes-profile-page-title">','</h1>');
			$replace = array('<div class="edd_widget_title"><h3 class="plugin">', '</h3></div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
add_filter( 'fes_profile_form_header', 'mp_fes_header_profile', 10, 2 ); 
		


/* Additional dashboard links */
if ( class_exists( 'EDD_Front_End_Submissions' ) ) {
	function mp_fes_dashboard_menus() {
		
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		
		$menu_items = array();
		
			// Dashboard tab
		$menu_items['home'] = array(
			"icon" => "home",
			"task" => array( 'dashboard', '' ),
			"name" => esc_html__( 'Dashboard', 'kiwi' ),
		);
		
		// Profile tab
		$menu_items['profile'] = array(
			"icon" => "user",
			"task" => array( 'profile' ),
			"name" => esc_html__( 'Profile', 'kiwi' ),
		);
		
		// "Products" tab
		$menu_items['my_products'] = array(
			"icon" => "list",
			"task" => array( 'products' ),
			"name" => EDD_FES()->helper->get_product_constant_name( $plural = true, $uppercase = true ),
		);
		
		// Add "Product" tab
		if ( EDD_FES()->vendors->vendor_can_create_product() ) {
			$menu_items['new_product'] = array(
				"icon" => "pencil",
				"task" => array( 'new-product' ),
				"name" => esc_html__( 'Add', 'kiwi' ) . ' ' . EDD_FES()->helper->get_product_constant_name( $plural = false, $uppercase = true ),
			);
		}
		
		// Earnings tab
		if ( EDD_FES()->integrations->is_commissions_active() ) {
			$menu_items['earnings'] = array(
				"icon" => "earnings",
				"task" => array( 'earnings' ),
				"name" => esc_html__( 'Earnings', 'kiwi' ),
			);
		}
		
		// Orders tab
		if ( EDD_FES()->vendors->vendor_can_view_orders() ){
			$menu_items['orders'] = array(
				"icon" => "gift",
				"task" => array( 'orders' ),
				"name" => esc_html__( 'Orders', 'kiwi' ),
			);
		}
		
		// Support for EDD Wishlists
		if( class_exists( 'EDD_Wish_Lists' ) ) {
			$menu_items[ 'wishlists' ] = array(
				"task" => array( 'wishlist' ),
				"icon" => "wishlist",
				"name" => esc_html__( 'Wishlists', 'kiwi' )
			);
		}

		// Support for EDD Points and Rewards
		if( class_exists( 'EDD_Points_Renderer' ) ) {
			$menu_items[ 'points' ] = array(
				"task" => array( 'points' ),
				"icon" => "points",
				"name" => esc_html__( 'Points', 'kiwi' )
			);
		}


		
		// Download tab :: Purchased items for download
		$menu_items[ 'downloads' ] = array(
			"task" => array( 'download' ),
			"icon" => "purchases",
			"name" => esc_html__( 'Purchases', 'kiwi' )
		);

		// Invoice tab
			$menu_items[ 'purchases' ] = array(
				"task" => array( 'purchases' ),
				"icon" => "invoices",
				"name" => esc_html__( 'Invoices', 'kiwi' )
			);
			
		
		// Support for EDD_Reviews
		if( class_exists( 'EDD_Reviews' ) ) {
			$menu_items['edd_reviews_vendor_feedback'] = array(
				'task' => 'vendor-feedback',
				'name' => esc_html__( 'Reviews', 'kiwi' ),
				'icon' => apply_filters( 'edd_reviews_vendor_feedback_icon', 'author-feedback' )				
			);
		}
		
		
		

		// Support for EDD BOOKING
		if( is_plugin_active('edd-bookings/edd-bookings.php' ) ) {
			
			$menu_items['bookings'] = array(
				'task' => 'bookings',
				'name' => esc_html__( 'Bookings', 'kiwi' ),
				'icon' => 'booking',				
			);
			
			$menu_items['bookings-calendar'] = array(
				'task' => 'bookings-calendar',
				'name' => esc_html__( 'Calendar', 'kiwi' ),
				'icon' => 'booking-calendar',				
			);
		}
		

		
		// Support for EDD Licensing software
		if( class_exists( 'EDD_Software_Licensing' ) ) {
			$menu_items[ 'licenses' ] = array(
				"task" => array( 'licenses' ),
				"icon" => "licenses",
				"name" => esc_html__( 'Licenses', 'kiwi' )
			);
		}	
		
		
		// Support for EDD WP Affiliate
		if( class_exists( 'Affiliate_WP' ) ) {
			$menu_items[ 'wp_affiliate' ] = array(
				"task" => array( 'wp_affiliate' ),
				"icon" => "wp_affiliate",
				"name" => esc_html__( 'Affiliate', 'kiwi' )
			);
		}	
		
		// Logout tab
			 $menu_items['logout'] = array(
				"icon" => "off",
				"task" => array( 'logout' ),
				"name" => esc_html__( 'Logout', 'kiwi' ),
			); 	
	
			return $menu_items;
	}

add_filter( 'fes_vendor_dashboard_menu', 'mp_fes_dashboard_menus', 10, 2 ); 
}		


function mp_custom_task_wishlist($custom,$task){
	
		if( 'wishlist' == $task ) {
			$custom = 'wishlist';
		} 
		
		if( 'download' == $task ) {
			$custom = 'download';
		}
		
		if( 'purchases' == $task ) {
			$custom = 'purchases';
		}
		
		if( 'points' == $task ) {
			$custom = 'points';
		}
		
		if( 'licenses' == $task ) {
			$custom = 'licenses';
		}
		
		if( 'wp_affiliate' == $task && class_exists( 'Affiliate_WP' ) ) {
			$custom = 'wp_affiliate';
		}
		
		if( 'vendor-feedback' == $task && class_exists( 'EDD_Reviews' ) ) {
			$custom = 'vendor-feedback';
		}

return $custom; 
}

add_filter('fes_signal_custom_task', 'mp_custom_task_wishlist', 10, 2 ); 
		
	

// FIX for "Cannot modify header information" when redirecting to affiliate dashboard	
function mp_output_buffer() {
        ob_start();
}
add_action('init', 'mp_output_buffer');



 function mp_custom_template_wishlist($task){
	 
	 
	 if($_GET[ 'task' ] == 'wp_affiliate' && class_exists( 'Affiliate_WP' ) ) {			
			$dashboard_affiliate = esc_url( add_query_arg( 'tab', 'urls', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) );
			$template = wp_redirect( $dashboard_affiliate ); exit;	
		return $template;
	} 
	
	
	if($_GET[ 'task' ] == 'wishlist') {		
		$template = EDD_FES()->templates->fes_get_template_part( 'frontend', 'wishlist' );	
		return $template;
	} 
	
	if($_GET[ 'task' ] == 'download') {		
		$template = EDD_FES()->templates->fes_get_template_part( 'frontend', 'download' );	
		return $template;
	} 
	
	if($_GET[ 'task' ] == 'purchases') {		
		$template = EDD_FES()->templates->fes_get_template_part( 'frontend', 'invoices' );	
		return $template;
	} 
	
	if($_GET[ 'task' ] == 'points') {		
		$template = EDD_FES()->templates->fes_get_template_part( 'frontend', 'points' );	
		return $template;
	}
	
	if($_GET[ 'task' ] == 'licenses') {		
		$template = EDD_FES()->templates->fes_get_template_part( 'frontend', 'licenses' );	
		return $template;
	}
	
	
}
add_action('fes_custom_task_wishlist', 'mp_custom_template_wishlist', 10, 2 ); 
add_action('fes_custom_task_download', 'mp_custom_template_wishlist', 10, 2 );
add_action('fes_custom_task_purchases', 'mp_custom_template_wishlist', 10, 2 );
add_action('fes_custom_task_points', 'mp_custom_template_wishlist', 10, 2 );
add_action('fes_custom_task_licenses', 'mp_custom_template_wishlist', 10, 2 );
add_action('fes_custom_task_wp_affiliate', 'mp_custom_template_wishlist', 10, 2 );

/*===============================================================
	Marketplace :: FES Filter the TOS field to remove the textarea so you can link to a separate page in a new window
===============================================================*/

if ( isset($kiwi_theme_option['marketplace-enable-termsconditions']) && !empty($kiwi_theme_option['marketplace-enable-termsconditions']) ) {
	
function mp_fes_tos( $field, $form_vars, $id, $type, $form_id, $read_only, $args ) {
	
	global $kiwi_theme_option;	
	
	ob_start();
	
	echo '<div class="fes-fields">';
	echo '<span data-required="yes" data-type="radio"></span>';
	echo '<a href="'. get_page_link ($kiwi_theme_option['marketplace-enable-termsconditions']) .'" target="_blank">'.esc_html__( 'Click here to read the Terms and Conditions.', 'kiwi' ).'</a></li>';
	echo '<br /><label><input type="checkbox" name="fes_accept_toc" required="required" />';
	esc_html_e( 'I have read, understood and agree to comply with the terms and conditions stated herein.', 'kiwi' );
	echo '</label>';
	echo '</div>';
	
	return ob_get_clean();
		
}
add_filter( 'fes_forms_toc_wrap', 'mp_fes_tos', 10, 7 );
 
}


/*===============================================================
	Marketplace :: Notification message if an item is previously purchased
===============================================================*/
/*  function mp_feature_already_purchased_message( $purchase_form, $args ) {

	$user_id 		= get_current_user_id();
	$download_id 	= $args['download_id'];
	$downloads 		= array( $download_id );

	// only show file download links on the single download page
	 if ( ! is_singular( 'download' ) ) {
		return $purchase_form;
	} 

	// return if user has not purchased the file before or the user is not logged in
	 if ( ! edd_has_user_purchased( $user_id, $downloads ) || ! is_user_logged_in() ) {
		return $purchase_form;
	} 

	$purchases = edd_get_users_purchases( $user_id, -1, false, 'complete' );

	$found_payment = false;

	if ( $purchases ) {
		foreach ( $purchases as $payment ) {
			$downloads      = edd_get_payment_meta_cart_details( $payment->ID, true );
			$purchase_data  = edd_get_payment_meta( $payment->ID );
			$email          = edd_get_payment_user_email( $payment->ID );

			// loop though downloads and find the current download ID
			if ( $downloads ) {
				foreach ( $downloads as $download ) {
					if ( get_the_ID() == $download['id'] ) {
						// found it
						$found_payment  = $purchase_data ? $purchase_data : false;
						$found_download = $download ? $download : false;
						break;
					}
				}
			}
		}

		 if ( ! $found_payment ) {
			return $purchase_form; 
		} 
	
		$payment_id = edd_get_purchase_id_by_key( $found_payment['key'] );

		$price_id 		= edd_get_cart_item_price_id( $found_download );
		$download_files = edd_get_download_files( $found_download['id'], $price_id );

		$email = edd_get_payment_user_email( $payment_id );
		$download_id = $found_download['id'];

		if ( edd_is_payment_complete( $payment_id ) ) :

			if ( $download_files ) :
			
			foreach ( $download_files as $filekey => $file ) :
			
			$download_url = edd_get_download_file_url( $found_payment['key'], $email, $filekey, $download_id );
			
			$message = esc_html__( 'You already bought this item.', 'kiwi' ); 
			
			//$download_name = esc_html( $file['name'] );
			$download_name = esc_html__( 'Download file here.', 'kiwi' ); 
			$download_link = ' <a href="'. esc_url( $download_url ).'" class="edd_download_file_link">'.$download_name.'</a>'; 
			
			echo '<div class="mp-already-purchased">';
			echo $message . $download_link;
			echo '</div>';
			
				endforeach;

			else :
				//_e( 'No downloadable files found.', 'kiwi' );
			endif; // End if payment complete

		else : ?>
			<span class="mp-already-purchased">
				<?php printf( __( 'Payment status is %s', 'kiwi' ), edd_get_payment_status( $payment, true ) ); ?>
			</span>
			<?php
		endif; // End if $download_files
		
	}

}
add_action( 'mp_item_desc_previously_purchased', 'mp_feature_already_purchased_message', 10, 2 );
 */					
 
 
 


/*===============================================================
	Marketplace :: Coming Soon
===============================================================*/
function edd_coming_soon_modify_text( $output ) {
  
			$search  = array('<p><strong>', '</strong></p>');
			$replace = array('<div class="mp-coming-soon">', '</div>'); 
			
			return str_replace( $search, $replace, $output );			
}
add_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_modify_text' );


function mp_comingsoon_output($output) {
			$search  = array('<br /><strong>', '</strong>');
			$replace = array('<div class="mp-votes">', '</div>'); 
			
			return str_replace( $search, $replace, $output );
		}	
add_filter( 'edd_download_price', 'mp_comingsoon_output', 40, 2 ); 
add_filter( 'edd_price_range', 'mp_comingsoon_output', 40, 2 );



function edd_coming_soon_modify_button( $output ) {
  
			$search  = array('<span class="dashicons dashicons-heart">');
			$replace = array('<span class="fa fa-heart">'); 
			
			return str_replace( $search, $replace, $output );			
}
add_filter( 'edd_cs_btn_icon', 'edd_coming_soon_modify_button' );





function mp_shortcode_removal_comingsoon() {  
			echo '<i class="fa fa-clock-o"></i>';			
}
add_filter( 'edd_cs_vote_description', 'mp_shortcode_removal_comingsoon', 10, 2 );
add_filter( 'edd_coming_soon_voted_message', 'mp_shortcode_removal_comingsoon', 10, 2 );
add_filter( 'edd_cs_vote_submission', 'mp_shortcode_removal_comingsoon', 10, 2 );


/*===============================================================
	Marketplace :: Grid style 1 :: Term filter
===============================================================*/
function limit_terms($val) {
    return array_splice($val, 0, 1);
}
add_filter( "term_links-download_category", 'limit_terms'); 


/*===============================================================
	Marketplace :: Software Specs Plugin :: Add tags
===============================================================*/
if (  class_exists('EDD_Software_Specs' ) ) {
	function mp_add_tags_to_specs() {
		
		global $post;
		
		$taglist = get_the_term_list( $post->ID, 'download_tag', '', ', ', '' );
		
		remove_filter( "term_links-download_category", 'limit_terms');
		
		$catlist = get_the_term_list( $post->ID, 'download_category', '', ', ', '' );
		
		
		
		if ( !empty( $catlist ) ){		
		    echo '<tr itemprop="category" class="mp-tags-itemdesc categories">';
			echo '<td>' . esc_html__( 'Categories:', 'kiwi' ) . '</td>';
			echo '<td>' . $catlist . '</td>';
			echo '</tr>';
		}	
		
		if ( !empty( $taglist ) ){	
			echo '<tr class="mp-tags-itemdesc">';
			echo '<td>' . esc_html__( 'Tags:', 'kiwi' ) . '</td>';
			echo '<td>' . $taglist . '</td>';
			echo '</tr>';
		}	
			
			
	}
	add_action( 'eddss_add_specs_table_row', 'mp_add_tags_to_specs', 10, 1 );
}



/*===============================================================
	Marketplace :: profile editor :: add extra fields
===============================================================*/
function mp_extra_profile_fields( $user_id, $userdata ) {
	
	$biographical_info = isset( $_POST['edd_biographical_info'] ) ? $_POST['edd_biographical_info'] : '';	
		update_user_meta( $user_id, 'description', $biographical_info );
	
		
	if( isset( $_POST['edd_user_url'] )  )	{
		$profile_url = array('ID'=>$user_id,'user_url'=>$_POST['edd_user_url']);
			wp_update_user ($profile_url);
		}
		
}
add_action( 'edd_pre_update_user_profile', 'mp_extra_profile_fields', 10, 2 );



/*** WP AVATAR ***/
if ( class_exists ( 'WP_User_Avatar' )) {
	
	remove_action('wpua_before_avatar', 'wpua_do_before_avatar');
	remove_action('wpua_after_avatar', 'wpua_do_after_avatar');

	function mp_extra_avatar_fields() {
		
		echo do_shortcode('[avatar_upload]');
		
	}
	add_action( 'edd_profile_editor_before', 'mp_extra_avatar_fields', 10, 2 );


	function my_before_avatar() {
	  echo '<div class="edd_widget_title mp-avatar">
				<h3 class="plugin">'. esc_html__('Avatar Settings', 'kiwi' ).'</h3>
				<div class="clear"></div></div>';
	}
	add_action('wpua_before_avatar', 'my_before_avatar');


	function my_after_avatar() {
	  echo '';
	}
	add_action('wpua_after_avatar', 'my_after_avatar');
}

/*===============================================================
	Marketplace :: RVI + Coming Soon Compatibility + EDD shortcode
===============================================================*/

if ( ! function_exists( 'mp_compatibility_rvi_cm' ) ) :

function mp_compatibility_rvi_cm() {
	
if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && edd_coming_soon_is_active() ) {	

		$variable_class = '<span class="cart mp-comingsoon-class"><i class="fa fa-clock-o"></i></span>';			
	} else {
		$link = edd_get_purchase_link( array( 'textafter' => '<i class="fa fa-sign-out"></i>','text' => '<i class="fa fa-cart-arrow-down"></i>','price' => '0','download_id' => get_the_ID() ) );
		$variable_class = '<span class="cart">'.$link.'</span>';
	}	
	 ?>
	 
<div class="mp-button styleblock">
	<span class="viewmore"><a href="<?php the_permalink(); ?>"><i class="fa fa-eye"></i></a></span>
	<?php echo $variable_class; ?>
</div>
	<?php 
}
endif;



/*===============================================================
	Marketplace :: Changelog shortcode
===============================================================*/
function mp_changelog( $atts, $content = null ) {	
	$changelog = esc_html__( 'Changelog', 'kiwi' );	
    return '<h4 class="none">'. esc_html( $changelog ).'</h4><div class="mp-pre">'.do_shortcode($content).'</div>';
}
add_shortcode("changelog", "mp_changelog");



function changelog_fix() {
	$fixed = esc_html__( 'fixed', 'kiwi' );
    return '<span class="badge fix">'. esc_html( $fixed ).'</span>';
}
add_shortcode('fix', 'changelog_fix');


	function changelog_updated() {
		$updated = esc_html__( 'updated', 'kiwi' );
		return '<span class="badge updated">'. esc_html( $updated ).'</span>';
	}
	add_shortcode('updated', 'changelog_updated');

	
function changelog_new() {
	$new = esc_html__( 'new', 'kiwi' );
    return '<span class="badge new">'. esc_html( $new ).'</span>';
}
add_shortcode('new', 'changelog_new');


	function changelog_date( $atts, $content = null ) {			
		return '<div class="changelog-date">'.$content.'</div>';
	}
	add_shortcode('item_date', 'changelog_date');

	
function item_note( $atts, $content = null ) {			
	return '<div class="item-note">'.$content.'</div>';
}
add_shortcode('note', 'item_note');
	
	
/*===============================================================
	Marketplace :: Wishlist
===============================================================*/

if ( class_exists( 'EDD_Wish_Lists' ) ) {	
	remove_action( 'edd_purchase_link_top', 'edd_wl_load_wish_list_link' );
	add_action( 'mp_purchase_widget_after_button', 'edd_wl_load_wish_list_link' );
}


/*===============================================================
	Marketplace :: Customer Dashboard :: NON-FES
===============================================================*/
if ( class_exists( 'EDD_Customer_Dashboard' ) ) {	

	function mp_customer_dashboard_before() {		
		echo '<div class="container-fluid mp-fes-vendor-menu"><div class="container align-center"><div class="row">';	
		}
	add_action( 'edd_cd_before_menu', 'mp_customer_dashboard_before', 10, 1 );


	function mp_customer_dashboard_after() {		
		echo '</div></div></div><div class="clear"></div><div class="container mp-full-dashboard"> ';
		}
	add_action( 'edd_cd_after_menu', 'mp_customer_dashboard_after', 10, 1 );	


	function mp_customer_dashboard_after_content() {		
		echo '</div>';	
		}
	add_action( 'edd_cd_after_content', 'mp_customer_dashboard_after_content', 10, 1 );



	/* function mp_user_dashboard_menu_names( $menu ) {
		$menu['purchases']['name'] 	= esc_html__( 'Invoices', 'kiwi' );
		$menu['downloads']['name'] 	= esc_html__( 'Purchases', 'kiwi' );
		
			if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
				$menu[ 'fes' ] = array(
					'task' => 'fes_become_vendor',
					'name' => esc_html__( 'Vendor', 'edd_customer_dashboard' )
				);
			}	
	 
			return $menu;
	}
	add_filter( 'edd_customer_dashboard_menu_links', 'mp_user_dashboard_menu_names' ); */
	
	
	function mp_user_dashboard_menu_names( $menu ) {
		
			$menu = array();
			
			$menu[ 'default' ] = array(
				'task' => '',
				'name' => esc_html__( 'Dashboard', 'kiwi' )
			);
			
			$menu[ 'profile' ] = array(
				'task' => 'profile',
				'name' => esc_html__( 'Profile', 'kiwi' )
			);

			$menu[ 'purchases' ] = array(
				'task' => 'purchases',
				'name' => esc_html__( 'Invoices', 'kiwi' )
			);

			$menu[ 'downloads' ] = array(
				'task' => 'download',
				'name' => esc_html__( 'Purchases', 'kiwi' )
			);

			if( class_exists( 'EDD_Wish_Lists' ) ) {
				$menu[ 'wishlists' ] = array(
					'task'=> 'wishlist',
					'name' => esc_html__( 'Wishlists', 'kiwi' )
				);
			}

			if( class_exists( 'EDD_Points_Renderer' ) ) {
				$menu[ 'points' ] = array(
					'task'=> 'points',
					'name' => esc_html__( 'Points', 'kiwi' )
				);
			}

			if( class_exists( 'EDD_Software_Licensing' ) ) {
				$menu[ 'licenses' ] = array(
					'task'=> 'licenses',
					'name' => esc_html__( 'Licenses', 'kiwi' )
				);
			}	
			
			if ( class_exists( 'EDD_Front_End_Submissions' ) ) {
				if ( EDD_FES()->helper->get_option( 'fes-allow-registrations', true ) || EDD_FES()->helper->get_option( 'fes-allow-applications', true ) ) {
					$menu[ 'fes' ] = array(
						'task' => 'fes_become_vendor',
						'name' => esc_html__( 'Vendor', 'kiwi' )
					);
				}
			}

			if ( class_exists( 'Affiliate_WP' ) ) {	
				$menu[ 'wp_affiliate' ] = array(
					'task' => 'wp_affiliate',
					'name' => esc_html__( 'Affiliate', 'kiwi' )
				);
			}	
		
	 
			return $menu;
	}
	add_filter( 'edd_customer_dashboard_menu_links', 'mp_user_dashboard_menu_names' );



	function mp_custom_task_user_dashboard($custom,$task){	
	
		if( 'default' == $task ) { $custom = 'default'; }
		
		if( class_exists( 'Affiliate_WP' ) ) {	
			if( 'points' == $task ) { $custom = 'points'; }	
		}
		
		if ( class_exists( 'EDD_Software_Licensing' ) ) {	
			if( 'licenses' == $task ) { $custom = 'licenses'; }
		}
		
		if( class_exists( 'Affiliate_WP' ) ) {	
			if( 'wp_affiliate' == $task ) { $custom = 'wp_affiliate'; }
		}
		
		return $custom; 	
	}
	add_filter('edd_cd_custom_task', 'mp_custom_task_user_dashboard', 10, 2 ); 


		
	 function mp_custom_task_user($task){
		 
		 $task       = ! empty( $_GET['task'] ) ? $_GET['task'] : 'dashboard';
			
			if( $_GET[ 'task' ] == 'points' && class_exists( 'EDD_Points_Renderer' ) ) {	
				$template = get_template_part( 'fes_templates/frontend', 'points' );
				return $template;
			} elseif( $_GET[ 'task' ] == 'licenses' && class_exists( 'EDD_Software_Licensing' ) ) {	
				$template = get_template_part( 'fes_templates/frontend', 'licenses' );
				return $template;
			} elseif( $_GET[ 'task' ] == 'wp_affiliate' && class_exists( 'Affiliate_WP' ) ) {
				
				$dashboard_affiliate = esc_url( add_query_arg( 'tab', 'urls', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) );
				$template = wp_redirect( $dashboard_affiliate ); exit;			
				
				return $template;
			}else {
				$template = get_template_part( 'templates/user', 'dashboard' );	
				return $template;
			}
			
			
	}
	add_filter('edd_cd_custom_task_content', 'mp_custom_task_user', 10, 2 ); 

	
	/** Get the total spent for a customer. **/
	function mp_total_spent( $user_id = '' ) {
		
		if ( ! $user_id ) {
			$user_id = get_current_user_id();
		}

		$customers = EDD()->customers->get_customers(
			array( 
				'number'  => -1,
				'user_id' => $user_id
			)
		);

		$purchase_values = array();

		if ( $customers ) {
			foreach ( $customers as $customer ) {
				$purchase_values[] = $customer->purchase_value;
			}
		}
		
		remove_filter( 'edd_usd_currency_filter_before', 'mp_format_currency', 10, 3 );
		remove_filter( 'edd_usd_currency_filter_before', 'mp_format_currency_commissions_dashboard', 10, 3 );
		
		$total_spent = edd_currency_filter( edd_format_amount( array_sum( $purchase_values ) ) );
		
		return $total_spent;
	}


	
}



/*===============================================================
	Marketplace :: Review extension
===============================================================*/
if ( class_exists( 'EDD_Reviews' ) ) {
	
	remove_filter( 'the_content', array( edd_reviews(), 'load_frontend' ) );
	
	if ( ! function_exists( 'mp_edd_reviews_body' ) ) :
		
		function mp_edd_reviews_body( $comment, $depth, $args) {
		
		global $comment;
		
		$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
		
		if ( edd_reviews()->has_user_purchased( $comment->user_id, $comment->comment_post_ID ) ) {
			$verified = '<span class="edd-reviews-verified-purchase">' . __( 'Verified Buyer', 'kiwi' ) . '</span>';
		} else {
			$verified = ' ';
		}
		
		ob_start();
		do_action( 'edd_reviews_before_review' ); 
		
		
		$rating = get_comment_meta( $comment->comment_ID, 'edd_rating', true );
		?>
		
		<<?php echo $tag; ?> id="edd-review-<?php comment_ID(); ?>" <?php comment_class( $args['has_children'] ? 'parent' : '' ); ?>>
		<div id="div-edd-review-<?php comment_ID(); ?>" class="edd-review-body">
				<div class="edd-review-meta">
				
					<div class="edd-review-author vcard">
					
						<?php echo get_avatar( $comment->comment_author_email, 48 )  ?>
						
						<?php if ( 0 == $args['depth'] ) { ?>
						<b><?php echo get_comment_meta( $comment->comment_ID, 'edd_review_title', true ); ?></b>
						
						<?php if ( 0 != $rating ) { ?>
						<span class="mp-edd-review-meta-rating">						
							<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="star-rating">
								<div class="edd_reviews_rating_box" role="img" aria-label="<?php echo $rating . ' ' . __( 'stars', 'kiwi' ); ?>">
									<div class="edd_star_rating" style="width: <?php echo ( 19 * $rating ); ?>px"></div>
								</div>
								<div style="display:none" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
									<meta itemprop="worstRating" content="1" />
									<span itemprop="ratingValue"><?php echo $rating; ?></span>
									<span itemprop="bestRating">5</span>
								</div>
							</div>						
						</span>
						<?php } ?>
						
						<?php } ?>
						
					</div>

					<div class="edd-review-metadata">
						<p>
							<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
							<?php echo sprintf( '<span class="author">By %s</span>', get_comment_author_link() ); ?> on
							<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
								<time datetime="<?php comment_time( 'c' ); ?>">
									<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'kiwi' ), get_comment_date(), get_comment_time() ); ?>
								</time>
							</a> <?php echo $verified; ?> <?php edit_comment_link( __( 'Edit', 'kiwi' ), '<span class="edit-link">', '</span>' ); ?>
						</p>
					</div>

					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="edd-review-awaiting-moderation"><i><?php _e( 'Your review is awaiting moderation.', 'kiwi' ); ?></i></p>
					<?php endif; ?>

					<?php edd_reviews()->comment_rating( $comment ); ?>
					<div class="clearfix"></div>
				</div>
				

				<div class="edd-review-content"><?php comment_text(); ?></div>

				<?php echo edd_reviews()->get_comment_helpful_output( $comment ); ?>

				<?php
				edd_reviews()->reviews_reply_link( array_merge( (array) $args, array(
					'depth'      => $depth,
					'max_depth'  => $args['max_depth'],
				) ) );
				?>
			</div>
		
		<?php
		do_action( 'edd_reviews_after_review' );
		$output = ob_get_contents();
		ob_end_clean();
		
		echo $output;
			
		}
		add_filter('edd_reviews_body', 'mp_edd_reviews_body', 10, 3);	
		
				
	endif;
	
	
	
	if ( ! function_exists( 'mp_vendor_review_feedback_avarage' ) ) :
		
		function mp_vendor_review_feedback_avarage( $author_id = 0 ) {			
	
		remove_action( 'pre_get_comments',   array( edd_reviews(), 'hide_reviews'                           ) );
		remove_filter( 'comments_clauses',   array( edd_reviews(), 'hide_reviews_from_comment_feeds_compat' ), 10, 2 );
		remove_filter( 'comment_feed_where', array( edd_reviews(), 'hide_reviews_from_comment_feeds'        ), 10, 2 );
		
		remove_action( 'pre_get_comments', array( edd_reviews()->fes, 'hide_feedback' ) );
		
		$args = array(
			'count' => true, 
			'type' => 'edd_vendor_feedback',
			'meta_key' => 'edd_rating',			
			'post_author' => $author_id,
		);
		$comments = get_comments($args);

		$rating_count = 0;
		$total_rating = 0;
		/* */

		$reviews = get_comments(
			apply_filters(
				'widget_edd_per_product_reviews_args',
				array(
					'status' => 'approve',
					'post_status' => 'publish',
					'type' => 'edd_vendor_feedback',
					'meta_key' => 'edd_rating',			
					'post_author' => $author_id,
				)
			)
		);
		
		if ( $reviews && $comments != 0) {	

			foreach ( (array) $reviews as $review ) {				
				$total_rating = $total_rating + get_comment_meta( $review->comment_ID, 'edd_rating', true );
				$rating_count = $rating_count + 1;
			}
			
			$average = $total_rating / $comments;
			$number = sprintf('%0.2f', $average);
		
			echo '<div class="edd_reviews_rating_box mp-author-avarage-rating" role="img" aria-label="'. $number . ' ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo '<div class="edd_star_rating custom" style="float: none; width: ' . ( 19 * $number ) . 'px;"></div>';
			echo '</div>';
						
		} else {
			
			echo '<div class="edd_reviews_rating_box mp-author-avarage-rating" role="img" aria-label="0 ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo '<div class="edd_star_rating custom" style="float: none; width:0"></div>';
			echo '</div>';
					
		}
			
		}
				
	endif;
	
	
	
	
	/* Widget :: EDD Featured Review */	
	function mp_date_format_review($date_fmt) {
		return 'M j, Y';
	};
	add_filter('edd_reviews_widget_date_format', 'mp_date_format_review');	
	
	function mp_date_format_avatar($size) {
		return 60;
	};
	add_filter('edd_reviews_widget_avatar_size', 'mp_date_format_avatar');	

	
	
	function mp_edd_reviews_review_title_default($title) {
		
		$title .= mp_extension_review_function_colors();
		return $title;
	}
	add_filter( 'edd_reviews_review_title_default', 'mp_edd_reviews_review_title_default' );
		
	 

	/* Widget :: EDD Reviews */
	function change_links($arr) {
		$arr = array(
				'meta_key' => 'edd_rating',
			);
		return $arr;
	}
	add_filter( 'widget_edd_reviews_args', 'change_links', 10, 1 );	
	
	
	
	
	if ( ! function_exists( 'mp_extension_reviews_content' ) ) :
		
		function mp_extension_reviews_content( $echo = true ) {	

			global $post;
			
			$content = '';

			if ( $post && $post->post_type == 'download' && is_singular( 'download' ) && is_main_query() && ! post_password_required() ) {
				ob_start();
				edd_get_template_part( 'reviews' );
				if ( get_option( 'thread_comments' ) ) {
					edd_get_template_part( 'reviews-reply' );
				}
				$content .= ob_get_contents();
				ob_end_clean();
			}

			if ( $echo ) {
				echo $content;
			} else {
				return $content;
			}
			
		}
				
	endif;
	
	
	
	
	
	
	
	function mp_extension_review_function_core() {
		
		remove_action( 'pre_get_comments',   array( edd_reviews(), 'hide_reviews'                           ) );
		remove_filter( 'comments_clauses',   array( edd_reviews(), 'hide_reviews_from_comment_feeds_compat' ), 10, 2 );
		remove_filter( 'comment_feed_where', array( edd_reviews(), 'hide_reviews_from_comment_feeds'        ), 10, 2 );
		
	
		$args = array(
			'post_id' => get_the_ID(),
			'count' => true, 
			'meta_key' => 'edd_rating',
		);
		$comments = get_comments($args);

		$rating_count = 0;
		$total_rating = 0;
		/* */
		
		$reviews = get_comments(
			apply_filters(
				'widget_edd_per_product_reviews_args',
				array(
					'status' => 'approve',
					'post_status' => 'publish',
					'post_type' => 'download',
					'post_id' => get_the_ID(),
				)
			)
		);
	
		if ( $reviews ) {		

			foreach ( (array) $reviews as $review ) {				
				$total_rating = $total_rating + get_comment_meta( $review->comment_ID, 'edd_rating', true );
				$rating_count = $rating_count + 1;
			}
			
		if($total_rating != 0) {	
			$average = $total_rating / $comments;
			$number = sprintf('%0.2f', $average);
		} else {
			$number = 0;
		}
		
			echo  '<div class="edd_reviews_rating_box" role="img" aria-label="'. $number . ' ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo  '<div class="edd_star_rating custom" style="float: none; width: ' . ( 19 * $number ) . 'px;"></div>';
			echo  '</div>';
			
		} else {
			
			echo  '<div class="edd_reviews_rating_box" role="img" aria-label="0' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo  '<div class="edd_star_rating custom" style="float: none; width:0;"></div>';
			echo  '</div>';
		}
			 
	}
	
	
/* Product description page :: Item description block :: Ratings */	
	function mp_extension_review_function_colors() {	
		
		remove_action( 'pre_get_comments',   array( edd_reviews(), 'hide_reviews'                           ) );
		remove_filter( 'comments_clauses',   array( edd_reviews(), 'hide_reviews_from_comment_feeds_compat' ), 10, 2 );
		remove_filter( 'comment_feed_where', array( edd_reviews(), 'hide_reviews_from_comment_feeds'        ), 10, 2 );
		
		$args = array(
			'post_id' => get_the_ID(),
			'count' => true, 
			'meta_key' => 'edd_rating',
		);
		$comments = get_comments($args);

		$rating_count = 0;
		$total_rating = 0;
		/* */
		
		$reviews = get_comments(
			apply_filters(
				'widget_edd_per_product_reviews_args',
				array(
					'status' => 'approve',
					'post_status' => 'publish',
					'post_type' => 'download',
					'post_id' => get_the_ID(),
				)
			)
		);
	
		if ( $reviews ) {		

			foreach ( (array) $reviews as $review ) {				
				$total_rating = $total_rating + get_comment_meta( $review->comment_ID, 'edd_rating', true );
				$rating_count = $rating_count + 1;
			}
			
		if($total_rating != 0) {	
			$average = $total_rating / $comments;
			$number = sprintf('%0.2f', $average);
		} else {
			$number = 0;
		}
		
		if($number <= 1.66) {	
			$rating_result = 'result-low';
		} elseif($number >= 1.67 && $number <= 3.32 ) {	
			$rating_result = 'result-good';
		} else {
			$rating_result = 'result-excellent';
		}
		
		
	if($total_rating != 0) {	
			echo '<span class="number">';
			echo sprintf( _n( '(%s vote)', '(%s votes)', $comments, 'kiwi' ), $comments );
			echo '<span class="'. esc_attr( $rating_result ).'">'. esc_html( $number ).'</span>';
			echo '</span>';
			
		} else {
			echo '';
		}
		
		
	}
		
	}	
	
}
			

 /*===============================================================
	Marketplace :: Redirects based on roles
===============================================================*/
global $kiwi_theme_option;

if ( $kiwi_theme_option['marketplace-enable-redirectroles'] == '1' ) {
	
		function my_login_redirect( $redirect_to, $request, $user ) {
			
			global $kiwi_theme_option, $user;
			
			$edd_vendor = $kiwi_theme_option['marketplace-redirect-shopvendor'];
			$edd_worker = $kiwi_theme_option['marketplace-redirect-shopworker'];
			$edd_accountant = $kiwi_theme_option['marketplace-redirect-shopaccountant'];
			$edd_manager = $kiwi_theme_option['marketplace-redirect-shopmanager'];
			
			$wp_buyer = $kiwi_theme_option['marketplace-redirect-subscriber'];
						
			if ( isset( $user->roles ) && is_array( $user->roles ) ) {
				
				if ( in_array( 'administrator', $user->roles ) ) {
					return $redirect_to;
				} elseif ( in_array( 'shop_vendor', $user->roles ) ) {
					return get_page_link( $edd_vendor );
				} elseif ( in_array( 'shop_worker', $user->roles ) ) {
					return get_page_link( $edd_worker );
				} elseif ( in_array( 'shop_accountant', $user->roles ) ) {
					return get_page_link( $edd_accountant );
				} elseif ( in_array( 'shop_manager', $user->roles ) ) {
					return get_page_link( $edd_manager );
				} elseif ( in_array( 'subscriber', $user->roles ) ) {
					return get_page_link( $wp_buyer );
				} else {
					return esc_url ( home_url() );
					//return 'nothing';
				}
			} else {
				return $redirect_to;
			}
			
		}

		add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
}

 /*===============================================================
	Marketplace :: Dropdown menu features
===============================================================*/

if( ! function_exists( 'wpse_return_template_part' ) ) {
    function wpse_return_template_part( $first = '', $second = '' ) {
        ob_start();
        get_template_part( $first, $second );
        return ob_get_clean();
    }
}


 function your_custom_menu_item ( $items, $args ) {
	
	global $kiwi_theme_option;	
	
	$layout = $kiwi_theme_option['marketplace-userrole-blocks-vendorlogged']['enabled'];
	$layouttwo = $kiwi_theme_option['marketplace-userrole-blocks-buyerlogged']['enabled'];
	$layoutthree = $kiwi_theme_option['marketplace-userrole-blocks-nonlogged']['enabled'];
	
	$items_icon	= '';
	
	if ($args->theme_location == 'vendor_navigation'  || 
	is_user_logged_in() && $args->theme_location == 'main_navigation' && $kiwi_theme_option['marketplace-enable-userroles-main'] == '1' && 
	class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor()) {
		
		if ($layout): foreach ($layout as $key=>$value) {				 
			switch($key) {				
				case 'dashboard': $items_icon .= wpse_return_template_part( 'templates/topbar', 'roledashboard' ); break;							 		 
				case 'cart': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolecart' ); break;							 		 
				case 'purchases': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolepurchases' ); break;				
				case 'username': $items_icon .= wpse_return_template_part( 'templates/topbar', 'roleuser' ); break;  				
				case 'wishlist': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolewishlist' ); break;
				case 'searching': $items_icon .= wpse_return_template_part( 'templates/topbar', 'fullsearch' ); break;	
			}	 
		}		 
		endif;
						
	} elseif ($args->theme_location == 'logged_navigation' || 
	is_user_logged_in() && $args->theme_location == 'main_navigation' && $kiwi_theme_option['marketplace-enable-userroles-main'] == '1' && 
	class_exists( 'EDD_Front_End_Submissions' ) && !EDD_FES()->vendors->vendor_is_vendor()) {
		
		if ($layouttwo): foreach ($layouttwo as $key=>$value) {
		 
			switch($key) {	 
				case 'cart': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolecart' ); break;							 		 
				case 'purchases': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolepurchases' ); break;				
				case 'username': $items_icon .= wpse_return_template_part( 'templates/topbar', 'roleuser' ); break;  				
				case 'wishlist': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolewishlist' ); break;
				case 'searching': $items_icon .= wpse_return_template_part( 'templates/topbar', 'fullsearch' ); break; 	 
			}	 
		 
		}		 
		endif; 
		
	} elseif ($args->theme_location == 'nonlogged_navigation' || !is_user_logged_in() && $args->theme_location == 'main_navigation' && $kiwi_theme_option['marketplace-enable-userroles-main'] == '1' ) {
		
		if ($layoutthree): foreach ($layoutthree as $key=>$value) {
		 
			switch($key) {	 
				case 'cart': $items_icon .= wpse_return_template_part( 'templates/topbar', 'rolecart' ); break;
				case 'login': $items_icon .= wpse_return_template_part( 'templates/topbar', 'login' ); break;	 
				case 'register': $items_icon .= wpse_return_template_part( 'templates/topbar', 'register' ); break;
				case 'searching': $items_icon .= wpse_return_template_part( 'templates/topbar', 'fullsearch' ); break;	 				
			}	 
		 
		}		 
		endif; 
		
	} else {
		
	}
	
	if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' || $kiwi_theme_option['marketplace-position-topbar-icon'] == '1' || $args->theme_location == 'main_navigation' && $kiwi_theme_option['marketplace-enable-userroles-main'] == '1' ) {
		return $items . $items_icon;
	} else {
		return $items_icon . $items;
	}	
	
			
}
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );


 /*===============================================================
	Marketplace :: checkout tax
===============================================================*/
function my_img_caption_shortcode_filter($label) {
	
	$label = '';

	if( edd_prices_show_tax_on_checkout() ) {
		$label .= '<span class="vat excl">' . sprintf( esc_html__( 'includes %s tax', 'kiwi' ), edd_get_formatted_tax_rate() ).'';
	} else {
		$label .= '<span class="vat incl">' . sprintf( esc_html__( 'excludes %s tax', 'kiwi' ), edd_get_formatted_tax_rate() ).'';
	}
	
	return $label;
}
add_filter('edd_cart_item_tax_description', 'my_img_caption_shortcode_filter',10,3);



/*===============================================================
	Marketplace :: Video Shortcode :: Adding div class
===============================================================*/	
function mp_video_class( $atts, $content = null ) {	
    return '<div class="video-container">'.do_shortcode($content).'</div>';
}
add_shortcode("mp-video", "mp_video_class");



/*===============================================================
	Marketplace :: Video :: Audio :: Featured Image
===============================================================*/ 
if ( ! function_exists( 'mp_audio_video_featured' ) ) :
function mp_audio_video_featured() {
	
	global $kiwi_theme_option;
	
	if (class_exists( 'EDD_Front_End_Submissions' ) ){ 	
	
		$fes_featured_video = get_post_meta( get_the_ID(), 'upload_featured_video', true );
		$fes_featured_audio = get_post_meta( get_the_ID(), 'upload_featured_audio', true );
		$fes_embedded_url 	= get_post_meta( get_the_ID(), 'upload_media_embed', true );
		
		$mp_featured_value 	= get_post_meta( get_the_ID(), 'mp_featured_item', true);
		
		$source = get_template_directory_uri() . '/framework/theme-options/images/img-empty-featuredimage.jpg';
		
		$count_tracks = count( $fes_featured_audio );
		$count_videos = count( $fes_featured_video );
		
		$url_video = '';
		$url_audio = '';
		$url_embed = '';
		
		if (is_array($fes_featured_video)) {
			foreach($fes_featured_video as $key=>$val){	
			
				if ( $count_videos > 1 && is_singular( 'download' ) ) {
					$url_video_playlist  = implode(',', $fes_featured_video);						
				} else {
					$url_video = wp_get_attachment_url( $val );
				}
		
			}	
		}
		
		
		if (is_array($fes_featured_audio)) {	
			foreach($fes_featured_audio as $key=>$val){	
			
				if ( $count_tracks > 1 && is_singular( 'download' ) ) {
					$url_audio_playlist   = implode(',', $fes_featured_audio);						
				} else {
					$url_audio = wp_get_attachment_url( $val );	
				}
				
			}
		}
	
					
				if ( ! empty( $fes_featured_video ) && empty( $fes_featured_audio ) && empty( $fes_embedded_url ) ) {
					
				if ( has_post_thumbnail() ) {	
					$video_preview = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'kiwi-full-image' );
				} else {
					$video_preview = '';
				}	
				
					$attr = array(
						'src'      => strip_tags($url_video),
						'loop'     => '',
						'autoplay' => '',
						'preload'  => 'none',
						'poster' => $video_preview[0],						
					);
					
					$uniqid = uniqid('kiwi-video-');
						
					if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
						echo '<div class="featured-item-ribbon video"><span></span></div>';				
					}
							
					/*  */ 
					if ( $count_videos > 1 && is_singular( 'download' ) ) {
						echo do_shortcode('[playlist type="video" ids="'.$url_video_playlist.'"]');
					} else {
						echo '<div class="video-container '.esc_attr( $uniqid ).'" itemprop="video">' . wp_video_shortcode( $attr ) . ' </div>';
					}
										
				} elseif ( empty( $fes_featured_video ) && empty( $fes_embedded_url ) && !empty( $fes_featured_audio ) ) {
					
					$track_limit = $kiwi_theme_option['marketplace-audio-featuredimage'];
					
					if ( has_post_thumbnail() && is_singular( 'download' ) && $count_tracks > 1 && empty( $track_limit )) { 
					
						if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
							echo '<div class="featured-item-ribbon"><span></span></div>';				
						}
					
						echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive audio-featured playlist' ) ); 
					} elseif ( has_post_thumbnail() && is_singular( 'download' ) && !empty( $track_limit ) && $count_tracks > 1 ) { 
						
						if (  $count_tracks >= $track_limit ) { 
							echo ''; 
						} else {
							
							if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
								echo '<div class="featured-item-ribbon"><span></span></div>';				
							}
							echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive audio-featured playlist tracklimit' ) ); 							
						}
						
					} elseif ( has_post_thumbnail() && is_singular( 'download' ) && $count_tracks <= 1 ) { 
							if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
								echo '<div class="featured-item-ribbon"><span></span></div>';				
							}
						echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive audio-featured' ) ); 
						
					} elseif ( has_post_thumbnail() && !is_singular( 'download' )) {
							if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
								echo '<div class="featured-item-ribbon"><span></span></div>';				
							}
							
						echo '<a href="' . get_the_permalink() . '">';	
						echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive' ) );
						echo '</a>';
					} else {
						echo '<a href="' . get_the_permalink() . '">';
						echo  '<img src="'. esc_url( $source ).'">';
						echo '</a>';
					}	
					
					$attr = apply_filters( 'mp_hook_audio_settings', array(
						'src'      => strip_tags($url_audio),
						'loop'     => 'true',
						'autoplay' => '',
						'preload'  => 'none',
					));
					
					/*  */ 
					if ( $count_tracks > 1 && is_singular( 'download' ) ) {
						echo do_shortcode('[playlist ids="'.$url_audio_playlist.'"]');
					} else {
						echo '<div class="audio-player" itemprop="audio">' . wp_audio_shortcode( $attr ) . ' </div>';
					}
					
					
					
				} elseif ( empty( $fes_featured_video ) && empty( $fes_featured_audio ) && !empty( $fes_embedded_url ) ) {
		
					$embedded_url = $fes_embedded_url;
					$oembed = wp_oembed_get( $embedded_url );
					echo '<div class="video-container">' . $oembed . ' </div>';		
					
				} else {				
					
					if ( has_post_thumbnail() ) { 
					
						if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
								echo '<div class="featured-item-ribbon"><span></span></div>';				
						}
						echo '<a href="' . get_the_permalink() . '">';
						echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive' ) );
						echo '</a>';						
					} else {
						echo '<img src="'. esc_url( $source ).'">';
					}
				}

	} else {

		$mp_cmb_audio = get_post_meta( get_the_ID(), '_cmb2_edd_audio_upload', true );
		$mp_cmb_video = get_post_meta( get_the_ID(), '_cmb2_edd_video_upload', true );
		$mp_cmb_embedded = get_post_meta( get_the_ID(), '_cmb2_edd_media_embed', true );
	
		$mp_featured_value = get_post_meta( get_the_ID(), 'mp_featured_item', true);
		$source = get_template_directory_uri() . '/framework/theme-options/images/img-empty-featuredimage.jpg';
		
	
		$mp_count_tracks = count( $mp_cmb_audio );
		$mp_count_videos = count( $mp_cmb_video );
		
					
		$audio_ids = array();
		$video_ids = array();
		
		
		$mp_url_video = '';
		$mp_url_audio = '';
		
		/* AUDIO */ 
		foreach ((array) $mp_cmb_audio as $id => $url) {	
			$audio_ids[] = $id;
		}	
		
		if ( $mp_count_tracks >= 2 && is_singular( 'download' ) ) {
			$mp_url_audio_playlist = implode(',', $audio_ids );									
		} else {
			$mp_url_audio = wp_get_attachment_url( $id );	
		}
						
		/* VIDEO */ 
		foreach ((array) $mp_cmb_video as $id => $url) {
			$video_ids[] = $id;
		}
		
		if ( $mp_count_videos >= 2 && is_singular( 'download' ) ) {
			$mp_url_video_playlist  = implode(',', $video_ids);						
		} else {
			$mp_url_video = wp_get_attachment_url( $id );
		}
		
		
	/* START */ 
	if ( ! empty( $mp_cmb_video ) && empty( $mp_cmb_audio ) && empty( $mp_cmb_embedded ) ) {

		/* SHOW VIDEO */ 	
		if ( has_post_thumbnail() ) {	
			$video_preview = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'kiwi-full-image' );
		} else {
			$video_preview = '';
		}	
		
		$video_attr = array(
			'src'      => strip_tags($mp_url_video),
			'loop'     => '',
			'autoplay' => '',
			'preload'  => 'none',
			'poster' 	=> $video_preview[0],						
		);
		
		$uniqid = uniqid('kiwi-video-');
			
		if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
			echo '<div class="featured-item-ribbon video"><span></span></div>';				
		}
				
		/*  */ 
		if ( $mp_count_videos >= 2 && is_singular( 'download' ) ) {
			echo do_shortcode('[playlist type="video" ids="'.$mp_url_video_playlist.'"]');
		} else {
			echo '<div class="video-container '.esc_attr( $uniqid ).'" itemprop="video">' . wp_video_shortcode( $video_attr ) . ' </div>';
		}

	} elseif ( empty( $mp_cmb_video ) && empty( $mp_cmb_embedded ) && !empty( $mp_cmb_audio ) ) {


			$track_limit = $kiwi_theme_option['marketplace-audio-featuredimage'];
				
			if ( has_post_thumbnail() && is_singular( 'download' ) && $mp_count_tracks > 1 && empty( $track_limit )) { 
			
				if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
					echo '<div class="featured-item-ribbon"><span></span></div>';				
				}
				echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive audio-featured playlist' ) );
			} elseif ( has_post_thumbnail() && is_singular( 'download' ) && !empty( $track_limit ) && $mp_count_tracks > 1 ) { 
				
				if (  $mp_count_tracks >= $track_limit ) { 
					echo ''; 
				} else {
					
					if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
						echo '<div class="featured-item-ribbon"><span></span></div>';				
					}
					
					echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive audio-featured playlist tracklimit' ) );					
				}
				
			} elseif ( has_post_thumbnail() && is_singular( 'download' ) && $mp_count_tracks <= 1 ) { 
					if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
						echo '<div class="featured-item-ribbon"><span></span></div>';				
					}
					
				echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive audio-featured' ) ); 
				
			} elseif ( has_post_thumbnail() && !is_singular( 'download' )) {
					if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
						echo '<div class="featured-item-ribbon"><span></span></div>';				
					}
				echo '<a href="' . get_the_permalink() . '">';	
				echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive' ) );
				echo '</a>';
			} else {
				echo '<a href="' . get_the_permalink() . '">';
				echo  '<img src="'. esc_url( $source ).'">';
				echo '</a>';
			}	
			
			/* SHOW AUDIO */
			$audio_attr = apply_filters( 'mp_hook_audio_settings', array(
				'src'      => strip_tags($mp_url_audio),
				'loop'     => 'true',
				'autoplay' => '',
				'preload'  => 'none',
			));
			
			/*  */ 
			if ( $mp_count_tracks >= 2 && is_singular( 'download' ) ) {
				echo do_shortcode('[playlist ids="'.$mp_url_audio_playlist.'"]');
			} else {
				echo '<div class="audio-player" itemprop="audio">' . wp_audio_shortcode( $audio_attr ) . ' </div>';
			}


	} elseif ( empty( $mp_cmb_video ) && empty( $mp_cmb_audio ) && !empty( $mp_cmb_embedded ) ) {
		
		$embedded_url = $mp_cmb_embedded;
		$oembed = wp_oembed_get( $embedded_url );
		echo '<div class="video-container">' . $oembed . ' </div>';		
		
	}
		else {				
								
			if ( has_post_thumbnail() ) { 
			
				if ( $mp_featured_value && $mp_featured_value == 'Yes' ) {	
					echo '<div class="featured-item-ribbon"><span></span></div>';				
				}
				echo '<a href="' . get_the_permalink() . '">';
				echo the_post_thumbnail('kiwi-full-image', array( 'class' => 'small-responsive' ) ); 
				echo '</a>';
			} else {
				echo '<a href="' . get_the_permalink() . '">';
				echo  '<img src="'. esc_url( $source ).'">';
				echo'</a>';
			}
		}
		
		
	}	
	
}
endif;

/*===============================================================
	Marketplace :: Audio :: Widgets
===============================================================*/ 
if ( ! function_exists( 'mp_audio_only' ) ) :
function mp_audio_only($mp_widget_audio) {	

	$output = '';
		
	if (class_exists( 'EDD_Front_End_Submissions' ) ){ 	
		
		$fes_featured_audio = get_post_meta( get_the_ID(), 'upload_featured_audio', true );
		
		if (is_array($fes_featured_audio)) {	
			foreach($fes_featured_audio as $key=>$val){						
				$url_audio = wp_get_attachment_url( $val );
			}
		}
		
		if ( !empty( $fes_featured_audio ) ) {
			
			if ( has_post_thumbnail() ) { 
				$output .= get_the_post_thumbnail ('full', array( 'class' => 'small-responsive audio-featured' ) );	
			}
			
			$attr = apply_filters( 'mp_hook_audio_settings', array(
				'src'      => strip_tags($url_audio),
				'loop'     => 'true',
				'autoplay' => '',
				'preload'  => 'none',
			));					
			
			$output .= '<div class="audio-player" itemprop="audio">' . wp_audio_shortcode( $attr ) . ' </div>';
		} 
	} else {
		
		$mp_cmb_audio = get_post_meta( get_the_ID(), '_cmb2_edd_audio_upload', true );

		$audio_ids = array();
		
		foreach ((array) $mp_cmb_audio as $id => $url) {	
			$audio_ids[] = $id;
		}				

		if ( !empty( $mp_cmb_audio  ) ) {
		
			if ( has_post_thumbnail() ) { 
				$output .= get_the_post_thumbnail ('full', array( 'class' => 'small-responsive audio-featured' ) );	
			}
			
			$mp_url_audio = wp_get_attachment_url( $id );
			
			/* SHOW AUDIO */
			$audio_attr = apply_filters( 'mp_hook_audio_settings', array(
				'src'      => strip_tags($mp_url_audio),
				'loop'     => 'true',
				'autoplay' => '',
				'preload'  => 'none',
			));
			
			/*  */ 
			$output .= '<div class="audio-player" itemprop="audio">' . wp_audio_shortcode( $audio_attr ) . ' </div>';
		
	}
		
		
		
	}	
	
	return $output;
	
}

add_filter('vc_mp_audio_only', 'mp_audio_only');
endif;







/*===============================================================
	EDD Extension :: Points and Rewards
===============================================================*/
if ( class_exists( 'EDD_Points_Renderer' ) ) { 
	global $edd_points_render;
	remove_action( 'edd_before_download_content', array( $edd_points_render, 'edd_points_message_content' ), 10 );
	//add_action( 'edd_after_download_content', array( $edd_points_render, 'edd_points_message_content' ), 0 );
	add_action( 'mp_edd_points_rewards', array( $edd_points_render, 'edd_points_message_content' ), 0 );
	
}


/*===============================================================
	Template for comments and pingbacks
===============================================================*/
if ( ! function_exists( 'kiwi_comment_marketplace' ) ) :

function kiwi_comment_marketplace( $comment, $args, $depth ) {
	
	global $post;
	
	if ( class_exists( 'Easy_Digital_Downloads' ) && edd_has_user_purchased( $comment->user_id, $post->ID ) && $comment->user_id != $post->post_author) {
		$alert = '<strong class="fes-purchase-badge fes-purchase-badge-purchased fes-light-green">'.esc_html__('Verified','kiwi').'</strong>';
		$purchased_badge = apply_filters( 'edd_show_has_purchased_item_message', $alert );
	} else {
		$purchased_badge = '';
	}	
	
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php esc_html_e( 'Pingback:', 'kiwi' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'kiwi' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		//global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"> 
	
<div class="mp-comment-author">	
	<?php echo get_avatar( $comment, 48 ); ?><br />
	<?php echo $purchased_badge; ?>
</div>
	
		<article id="comment-<?php comment_ID(); ?>" class="comment">
		
			<header class="comment-meta comment-author vcard">
				<?php
					printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span>' . esc_html__( 'Author', 'kiwi' ) . '</span>' : ''
					);?>
					
					
					<div class="reply-meta">
						<?php printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
							esc_url( get_comment_link( $comment->comment_ID ) ),
							esc_attr( get_comment_time( 'c' ) ),
							/* translators: 1: date, 2: time */
							sprintf( __( '%1$s at %2$s', 'kiwi' ), esc_attr( get_comment_date() ), esc_attr( get_comment_time() ) )
						);
						?>				
						<span class="reply">
							<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'kiwi' ), 'after' => ' ', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
						</span>
					</div>
				
			</header>
			<div class="clear"></div>

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'kiwi' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( esc_html__( 'Edit', 'kiwi' ), '<p class="edit-link">', '</p>' ); ?>
			</section>

			
		</article>
	<?php
		break;
	endswitch; 
}
endif;



/*===============================================================
	Marketplace :: License Extension
===============================================================*/
if ( ! function_exists( 'mp_license_menu' ) ) :

function mp_license_menu() {
	
	global $kiwi_theme_option;
	
if ( is_user_logged_in() ) {
	
	if ( class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor() ) {
		
		$task       = ! empty( $_GET['task'] ) ? $_GET['task'] : 'dashboard';
		$icon_css   = apply_filters( "fes_vendor_dashboard_menu_icon_css", "icon-white" ); //else icon-black/dark
		$menu_items = EDD_FES()->dashboard->get_vendor_dashboard_menu();

		$x = 0;
		
		$url_id_vendor = $kiwi_theme_option['marketplace-enablelinks-dashboard'];
		$vendor_link = get_page_link ($url_id_vendor);
		
		?>
		
		<?php foreach ( $menu_items as $item => $values ) : $values["task"] = isset( $values["task"] ) ? $values["task"] : ''; ?>
		<?php $x++; endforeach; ?>
					
			<?php if ( $x == '9' || $x == '10' || $x == '11'  ) {
					$div_number = 'mp-dashboard-links-limit'; 
				} elseif ( $x == '12' || $x == '13' || $x == '14' ) {
					$div_number = 'mp-dashboard-links-limit-extra'; 
				} elseif ( $x >= '15' ) {
					$div_number = 'mp-dashboard-links-limit-extra more'; 
				} else {
					$div_number = 'mp-dashboard-links'; 
				} 
			?>
			
		<nav class="fes-vendor-menu">
			<ul class="<?php echo esc_attr( $div_number ); ?>">
				<?php foreach ( $menu_items as $item => $values ) : $values["task"] = isset( $values["task"] ) ? $values["task"] : ''; ?>		
					<li class="<?php if ( $task === $values["task"]  ) { echo "active"; } ?>">				
						<a href='<?php echo esc_url( add_query_arg( 'task', $values["task"], $vendor_link ) ); ?>'>
							<i class="icon icon-<?php echo esc_attr( $values["icon"] ); ?> <?php echo esc_attr( $icon_css ); ?>"></i> <span class="hidden-phone hidden-tablet"><?php echo isset( $values["name"] ) ? $values["name"] : $item; ?></span>
						</a>
					</li>
				<?php 
				endforeach; ?>
		</ul>  
		</nav>	
		
		
		<?php
		
	} else {
		
		if ( class_exists( 'EDD_Customer_Dashboard' ) ) {		
			
				$home = home_url();
				$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
				$url = get_page_link ($url_id);
		
		?>
				
		<nav class="customer-dashboard-menu">
			<ul class="menu">
		
		<li class="user-dashboard"><a href="<?php echo esc_url( add_query_arg( 'task', '', $url ) ); ?>"><?php esc_html_e( 'Dashboard', 'kiwi' ); ?></a></li>
		<li class="profile"><a href="<?php echo esc_url( add_query_arg( 'task', 'profile', $url ) ); ?>"><?php esc_html_e( 'Profile', 'kiwi' ); ?></a></li>
		<li class="purchases"><a href="<?php echo esc_url( add_query_arg( 'task', 'purchases', $url ) ); ?>"><?php esc_html_e( 'Invoices', 'kiwi' ); ?></a></li>
		<li class="download"><a href="<?php echo esc_url( add_query_arg( 'task', 'download', $url ) ); ?>"><?php esc_html_e( 'Purchases', 'kiwi' ); ?></a></li>
		
		<?php if( class_exists( 'EDD_Wish_Lists' ) ) { ?>
			<li class="wishlist"><a href="<?php echo esc_url( add_query_arg( 'task', 'wishlist', $url ) ); ?>"><?php esc_html_e( 'Wishlists', 'kiwi' ); ?></a></li>
		<?php } ?>	
		
		<?php if( class_exists( 'EDD_Points_Renderer' ) ) { ?>
			<li class="points"><a href="<?php echo esc_url( add_query_arg( 'task', 'points', $url ) ); ?>"><?php esc_html_e( 'Points', 'kiwi' ); ?></a></li>
		<?php } ?>	
		
		
		<?php if( class_exists( 'EDD_Software_Licensing' ) ) { ?>
			<li class="licenses"><a href="<?php echo esc_url( add_query_arg( 'task', 'licenses', $url ) ); ?>"><?php esc_html_e( 'Licenses', 'kiwi' ); ?></a></li>
		<?php } ?>	
		
		<?php if ( class_exists( 'EDD_Front_End_Submissions' ) ) {?>		
			<li class="fes_become_vendor"><a href="<?php echo esc_url( add_query_arg( 'task', 'fes_become_vendor', $url ) ); ?>"><?php esc_html_e( 'Vendor', 'kiwi' ); ?></a></li>
		<?php } ?>	
		
		<?php if ( class_exists( 'Affiliate_WP' ) ) {?>		
			<li class="wp_affiliate"><a href="<?php echo esc_url( add_query_arg( 'task', 'wp_affiliate', $url ) ); ?>"><?php esc_html_e( 'Affiliate', 'kiwi' ); ?></a></li>
		<?php } ?>
		
		<li><a href="<?php echo esc_url( wp_logout_url( $home ) ); ?>"><?php esc_html_e( 'Logout', 'kiwi' ); ?></a></li>
			</ul>
		</nav>
				
				<?php
				
			} else {
				
			}
		}	
	}	
}
endif;



/*===============================================================
	Marketplace :: FES Redirects
===============================================================*/
if ( class_exists( 'EDD_Front_End_Submissions' ) && $kiwi_theme_option['marketplace-enable-redirectroles'] == '1' ) {
	
	
	function mp_pending_redirect($response, &$output) {
		
			global $kiwi_theme_option;
			
			$fes_pending = $kiwi_theme_option['marketplace-redirect-pendingvendor'];
			
			$response['redirect_to'] = get_page_link($fes_pending);	
			
		return $response;
	}
	add_filter( 'fes_register_form_pending_vendor', 'mp_pending_redirect', 10, 1 );	
		
		
	
	/**  **/
	function mp_fes_redirect($new_output, &$output) {
		
		global $kiwi_theme_option;
		
		$buyer_link = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
		
		$fes_front = $kiwi_theme_option['marketplace-redirect-frontendvendor'];			
		$fes_suspended = $kiwi_theme_option['marketplace-redirect-suspendedvendor'];
		$fes_pending = $kiwi_theme_option['marketplace-redirect-pendingvendor'];
		
		if ( EDD_FES()->vendors->vendor_is_vendor() ) {	
			$new_output['redirect_to'] = get_page_link($fes_front);	
		} elseif ( EDD_FES()->vendors->user_is_status( 'suspended') ) {	
			$new_output['redirect_to'] = get_page_link($fes_suspended);	
		} elseif ( EDD_FES()->vendors->user_is_status( 'pending') ) {
			$new_output['redirect_to'] = get_page_link($fes_pending);	
		} elseif ( !EDD_FES()->vendors->vendor_is_vendor()&& !EDD_FES()->vendors->user_is_status( 'pending') && !EDD_FES()->vendors->user_is_status( 'suspended') ){ 
			$new_output['redirect_to'] = get_page_link($buyer_link);	
		} else {
			$new_output['redirect_to'] = get_permalink( EDD_FES()->helper->get_option( 'fes-vendor-dashboard-page', get_permalink() ) );
		}  

		return $new_output;
	}
	add_filter( 'fes_login_form_success_redirect', 'mp_fes_redirect', 10, 1 );	
		
	
}	



/*===============================================================
	Audio/Video class
===============================================================*/
if ( ! function_exists( 'mp_media_class' ) ) :
		
	function mp_media_class() {
	
		if (class_exists( 'EDD_Front_End_Submissions' ) ){ 	

			$fes_featured_video = get_post_meta( get_the_ID(), 'upload_featured_video', true );
			$fes_featured_audio = get_post_meta( get_the_ID(), 'upload_featured_audio', true );

			if ( ! empty( $fes_featured_video ) && empty( $fes_featured_audio ) ) {
				$container_audio = ' mp-thumb-video';
			} elseif ( empty( $fes_featured_video ) && ! empty( $fes_featured_audio ) ) {
				$container_audio = ' mp-thumb-audio';
			} else {
				$container_audio = '';
			}
			
		} else {
						
			$mp_cmb_audio = get_post_meta( get_the_ID(), '_cmb2_edd_audio_upload', true );
			$mp_cmb_video = get_post_meta( get_the_ID(), '_cmb2_edd_video_upload', true );
			$mp_cmb_embedded = get_post_meta( get_the_ID(), '_cmb2_edd_media_embed', true );

			if ( ! empty( $mp_cmb_video ) && empty( $mp_cmb_audio ) && empty( $mp_cmb_embedded ) || empty( $mp_cmb_video ) && empty( $mp_cmb_audio ) && !empty( $mp_cmb_embedded )) {
				$container_audio = ' mp-thumb-video';
			} elseif ( empty( $mp_cmb_video ) && empty( $mp_cmb_embedded ) && !empty( $mp_cmb_audio ) ) {
				$container_audio = ' mp-thumb-audio';
			} else {
				$container_audio = '';
			}	
			
		}	
		
		return $container_audio;
		
	}
			
endif;





/*===============================================================
	Marketplace :: PDF EDD Extension
===============================================================*/
if ( class_exists( 'EDD_PDF_Invoices' ) ) {
		
		
	/****************************************************************
		Remove filter from edd receipt shortcode
	****************************************************************/
	remove_filter('edd_payment_receipt_after', array( EDD_PDF_Invoices::get_instance(), 'receipt_shortcode_link'));
		
		
	/****************************************************************
		Custom Generated PDF Download Link
	****************************************************************/	
	 if ( ! function_exists( 'mp_edd_ext_pdf_invoices' ) ) :
			
		function mp_edd_ext_pdf_invoices() {
			
			global $edd_receipt_args;
	 
			$payment = get_post( $edd_receipt_args['id'] );
			
			echo '<a class="mp-download-invoice" href="' . esc_url( edd_pdf_invoices()->get_pdf_invoice_url( $payment->ID ) ) . '">' . esc_html__( 'Download Invoice', 'kiwi' ) . '</a>';
			
		}
		add_action( 'mp_hook_invoices_pdf_download','mp_edd_ext_pdf_invoices', 10 );
					
	endif; 
		
}



/*===============================================================
	Marketplace :: Affiliate WP Extension
===============================================================*/
if ( class_exists( 'AffiliateWP_Order_Details_For_Affiliates' ) ) {
	remove_action( 'affwp_affiliate_dashboard_tabs',   array( AffiliateWP_Order_Details_For_Affiliates(), 'add_order_details_tab') );
}


/*===============================================================
	EDD Extension :: Bookings
===============================================================*/
 if( is_plugin_active('edd-bookings/edd-bookings.php' ) ) {
	
	function kiwi_bookings_render_calendar() {
		remove_action('edd_purchase_link_top', array(eddBookings()->getServiceController()->getPostType(),'renderServiceFrontend' ));		
		add_action('edd_purchase_link_top', array(eddBookings()->getServiceController()->getPostType(),'renderServiceFrontend' ), -10);
	}
	add_action( 'init', 'kiwi_bookings_render_calendar' );
		
}	 	





	
?>
<?php 
$attributes = array(   
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Font size', 'kiwi' ),
			'param_name' => 'sepsizefont',
			'value' => '',  
			'description' => esc_html__( 'Font size in px. (e.g., 25px)', 'kiwi' ),
			'group' => 'Typography'	
		),		
		array(        
			'type' => 'colorpicker',        
			'heading' => "Text color",        
			'param_name' => 'sepcolor',        
			'value' => '', 
			'group' => 'Typography',
			'edit_field_class' => 'vc_col-sm-6 vc_column',			
		),	

		array(        
			'type' => 'colorpicker',        
			'heading' => "Background color",        
			'param_name' => 'sepcolorbackground',        
			'value' => '',        
			'group' => 'Typography',
			'edit_field_class' => 'vc_col-sm-6 vc_column',			
		),	
		
		array(        
			'type' => 'textfield',
			'heading' => esc_html__( 'Font family', 'kiwi' ),
			'param_name' => 'sepfontfamily',
			'value' => '',
			'description' => esc_html__( 'Enter the name of the font you wish to use (e.g., Lato, Roboto, or Raleway)', 'kiwi' ),
			'group' => 'Typography'    
		),			
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Letter-spacing', 'kiwi' ),
			'param_name' => 'sepspacing',
			'value' => '',
			'description' => esc_html__( 'Increases or decreases the space between characters in a text; e.g., 1px.', 'kiwi' ),
			'group' => 'Typography'	
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Text padding', 'kiwi' ),
			'param_name' => 'septextpadding',
			'value' => '',
			'description' => esc_html__( 'Add spacing between the text and line seperator (e.g.: 0px 10px 0px 10px)', 'kiwi' ),
			'group' => 'Typography'	
		),

		array(        
			'type' => 'dropdown',        
			'heading' => esc_html__( 'Font style', 'kiwi' ),        
			'param_name' => 'sepfontstyle',        
			'value' => array( "normal", "italic" ),        
			'group' => 'Typography'		
		),		
		array(        
			'type' => 'dropdown',        
			'heading' => esc_html__( 'Text transform', 'kiwi' ),        
			'param_name' => 'septransform',        
			'value' => array( "normal", "uppercase", "lowercase" ),        
			'group' => 'Typography'		
		),		
		array(
		'type' => 'checkbox',
		'heading' => esc_html__( 'Make text bold?', 'kiwi' ),
		'param_name' => 'septextbold',
		//'description' => esc_html__( 'Allow items to be repeated in infinite loop (carousel).', 'kiwi' ),
		'value' => array( esc_html__( 'Yes, please.', 'kiwi' ) => 'yes' ),
		'group' => 'Typography',		
		)		
	);		
vc_add_params('vc_text_separator', $attributes ); 
?>
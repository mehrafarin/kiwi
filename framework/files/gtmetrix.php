<?php 

global $kiwi_theme_option;

/*===============================================================
	Remove query strings from static resources
===============================================================*/

if($kiwi_theme_option['gtmetric-static'] == '1') { 

	function kiwi_remove_script_version( $src ){
		$parts = explode( '?ver', $src );
			return $parts[0];
	}
	add_filter( 'script_loader_src', 'kiwi_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', 'kiwi_remove_script_version', 15, 1 );

}



/*===============================================================
	Defer parsing of JavaScript
===============================================================*/

if($kiwi_theme_option['gtmetric-deferparsing'] == '1') { 

	if (!(is_admin() )) {
		function kiwi_defer_parsing_of_js ( $url ) {
			if ( FALSE === strpos( $url, '.js' ) ) return $url;
			if ( strpos( $url, 'jquery.js' ) ) return $url;
		   
			return "$url' defer onload='";
		}
		add_filter( 'clean_url', 'kiwi_defer_parsing_of_js', 11, 1 );
	}
	
}
 

?>
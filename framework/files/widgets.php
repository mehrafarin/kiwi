<?php 

/*===============================================================
	Register edd widgets
===============================================================*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active('easy-digital-downloads/easy-digital-downloads.php') ) {
	
		require_once( get_template_directory() . '/framework/files/widget_pages.php' );
		
		require_once( get_template_directory() . '/framework/edd/edd-widget-related-downloads.php' );
		require_once( get_template_directory() . '/framework/edd/edd-widget-item-stats.php' );
		require_once( get_template_directory() . '/framework/edd/edd-widget-demobutton.php' );
		require_once( get_template_directory() . '/framework/edd/edd-widget-kiwi-pack.php' );

		require_once( get_template_directory() . '/framework/edd/edd-widget-tags.php');	
		require_once( get_template_directory() . '/framework/edd/edd-widget-cart.php');
		require_once( get_template_directory() . '/framework/edd/edd-widget-categories-one.php');
		require_once( get_template_directory() . '/framework/edd/edd-widget-categories-two.php');
		require_once( get_template_directory() . '/framework/edd/edd-widget-author-two.php');
		require_once( get_template_directory() . '/framework/edd/edd-widget-purchaseblock-two.php');
		require_once( get_template_directory() . '/framework/edd/edd-widget-author-portfolio.php');
		
	if ( class_exists( 'EDD_Reviews' ) ) {	
		require_once( get_template_directory() . '/framework/edd/edd-widget-itemratings.php' );		
		require_once( get_template_directory() . '/framework/edd/edd-widget-review-featured.php' );
		require_once( get_template_directory() . '/framework/edd/edd-widget-reviews-all.php' );
		require_once( get_template_directory() . '/framework/edd/edd-widget-reviews-product.php' );			
	}	
	
	if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
		require_once( get_template_directory() . '/framework/edd/edd-widget-fes-specs.php' );
	}
	
   
	function remove_edd_widgets() {
		
			unregister_widget('edd_categories_tags_widget');
			unregister_widget( 'edd_product_details_widget' );			
			unregister_widget('edd_cart_widget');
			
			register_widget('Marketplace_Widget_Cart');
			register_widget('Marketplace_Widget_Tags');
			
			unregister_widget('WP_Widget_Pages');
			register_widget('Kiwi_Widget_Pages');
			
			register_widget('Marketplace_Widget_Categories');
			register_widget('Marketplace_Widget_Categories_Two');
			register_widget('Marketplace_Widget_Item_Pack');
			register_widget('MP_Widget_Related_Downloads' );
			register_widget('Marketplace_Item_Stats' );
			
			register_widget('Marketplace_Author_Widget_Two' );		
			register_widget('Marketplace_Widget_Details_Two');
			
			register_widget('Marketplace_Author_Portfolio_Widget');
			register_widget('Marketplace_Item_Demolink');


		if ( class_exists( 'EDD_Reviews' ) ) {	
			register_widget('Marketplace_Item_Ratings');
			
			unregister_widget('EDD_Reviews_Widget_Featured_Review');
			unregister_widget('EDD_Reviews_Widget_Reviews');
			unregister_widget('EDD_Reviews_Per_Product_Reviews_Widget');
			
			register_widget('Marketplace_Reviews_Featured_Review');
			register_widget('Marketplace_Reviews_All');
			register_widget('Marketplace_Reviews_Per_Product');
			
		}
		
		if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
			register_widget('Marketplace_Fes_Specs');
		}
		
	}
	add_action( 'widgets_init', 'remove_edd_widgets' );	
	
}



/*===============================================================
	Kiwi Widget :: Comments
===============================================================*/
class Kiwi_Widget_Recent_Comments extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_recent_comments', 'description' => esc_html__( 'Your site\'s most recent comments.', 'kiwi' ) );
		parent::__construct('recent-comments', esc_html__('Kiwi :: Recent comments', 'kiwi'), $widget_ops);
		$this->alt_option_name = 'widget_recent_comments';

		if ( is_active_widget(false, false, $this->id_base) )
			add_action( 'wp_head', array($this, 'recent_comments_style') );

		/* add_action( 'comment_post', array($this, 'flush_widget_cache') );
		add_action( 'edit_comment', array($this, 'flush_widget_cache') );
		add_action( 'transition_comment_status', array($this, 'flush_widget_cache') ); */
	}

	public function recent_comments_style() {

		if ( ! current_theme_supports( 'widgets' ) 
			|| ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) )
			return;
		?>
	
	<?php
	}

	/* public function flush_widget_cache() {
		wp_cache_delete('widget_recent_comments', 'widget');
	} */

	public function widget( $args, $instance ) {
		global $comments, $comment;

		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get('widget_recent_comments', 'widget');
		}
		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		$output = '';

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent Comments', 'kiwi' );

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;

		$comments = get_comments( apply_filters( 'widget_comments_args', array(
			'number'      => $number,
			'status'      => 'approve',
			'post_status' => 'publish'
		) ) );

		$output .= $args['before_widget'];
		
		if ( $title ) {
			$output .= $args['before_title'] . $title . $args['after_title'];
		}
				
		
		if ( $comments ) {
			
			$output .= '<ul id="recentcomments">';
			// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
			$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
			_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

			foreach ( (array) $comments as $comment) {
				$output .= '<li class="recentcomments">';
				$output .= '<div class="flex">';
				/* translators: comments widget: 1: comment author, 2: post link */
				
					if ( isset( $instance['show_thumb']) && !empty( $instance['show_thumb'] ) ) { 
						$output .= '<div class="avatar">' .get_avatar( $comment->comment_author_email, 64 ) . '</div>';										
					} else {
						
					}
					
					$output .= '<a class="author" href="' . get_permalink( $comment->comment_post_ID ) . '#comment-' . $comment->comment_ID . '">';
					$output .= get_comment_author( $comment->comment_ID ) . '</a>';
				
					if ( isset( $instance['show_date']) && !empty( $instance['show_date'] )) { 
						$output .= '<span class="date"> ';
						$output .= sprintf( esc_html__( '%1$s at %2$s', 'kiwi' ), get_comment_date(), get_comment_time() );
						$output .= '</span>';					
					}	
					
					if ( isset( $instance['show_excerpt']) && !empty( $instance['show_excerpt'] ) ) { 
						$output .= '<div class="widget-comment">' . get_comment_excerpt($comment->comment_ID) . '</div>';										
					}
					$output .= '</div>';
					$output .= '</li>';
			}
			$output .= '</ul>';
		} else {
			$output .= esc_html__( 'There are no comments.', 'kiwi' );
		}
		
		
		$output .= $args['after_widget'];

		echo $output;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = $output;
			wp_cache_set( 'widget_recent_comments', $cache, 'widget' );
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = absint( $new_instance['number'] );
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;		
		$instance['show_excerpt'] = isset( $new_instance['show_excerpt'] ) ? (bool) $new_instance['show_excerpt'] : false;
		$instance['show_thumb'] = isset( $new_instance['show_thumb'] ) ? (bool) $new_instance['show_thumb'] : false;
		//$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_comments']) )
			delete_option('widget_recent_comments');

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		$show_excerpt = isset( $instance['show_excerpt'] ) ? (bool) $instance['show_excerpt'] : false;
		$show_thumb = isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : false;
?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of comments to show:', 'kiwi' ); ?></label>
		<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>"><?php esc_html_e( 'Display comment date?', 'kiwi' ); ?></label></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked( $show_excerpt ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_excerpt' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_excerpt' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_excerpt' ) ); ?>"><?php esc_html_e( 'Display comment?', 'kiwi' ); ?></label></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked( $show_thumb ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_thumb' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_thumb' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_thumb' ) ); ?>"><?php esc_html_e( 'Display thumbnail?', 'kiwi' ); ?></label></p>
<?php
	}
}

/*===============================================================
	Kiwi Widget :: Archive
===============================================================*/
class Kiwi_Archive_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_archive theme-kiwi', 'description' => esc_html__( 'A yearly archive of your site\'s posts.', 'kiwi') );
		parent::__construct('Kiwi_Archive_Widget', esc_html__('Kiwi :: Archives', 'kiwi'), $widget_ops);
	}

	public function widget( $args, $instance ) {
		$c = ! empty( $instance['count'] ) ? '1' : '0';

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? esc_html__( 'Archives', 'kiwi' ) : $instance['title'], $instance, $this->id_base );
		$hide_months = isset( $instance['hide_months'] ) ? $instance['hide_months'] : false;
		
		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		global $wpdb;
		
		$year_prev = null;		
        $months = $wpdb->get_results(   "SELECT DISTINCT MONTH( post_date ) AS month ,
                                        YEAR( post_date ) AS year,
                                        COUNT( id ) as post_count FROM $wpdb->posts
                                        WHERE post_status = 'publish' and post_date <= now( )
                                        and post_type = 'post'
                                        GROUP BY month , year
                                        ORDER BY post_date ASC");

        
		if (!function_exists('month_exist')) {
			function month_exist($month, $year, $objects){
				foreach($objects as $object){
					if( ($object->month == $month) && ($object->year == $year) )
					return true;
				}
				return false;
			}
		}
		
		
        $all_month = range(1,12);
        $new_array = $months;
        $i = 0;
        foreach($months as $month){
            $year_current = $month->year;
            if( $year_current != $year_prev ){
                foreach( $all_month as $current_month ){
					
					if ( $instance['hide_months'] == '0' ) {
						  if( !month_exist($current_month, $month->year, $months)){
							$value = new StdClass();
							$value->month = $current_month;
							$value->year = $month->year;
							$value->post_count = 0;
							array_splice($new_array, $i*12+($current_month-1), 0, array($value));
						}  
					}
					
					if ( $instance['hide_months'] == '1' ) {			
						if( !month_exist($current_month, $month->year, $months)) {
						 echo ''; } 						
					}
					
                }
                $i++;
            }
            $year_prev = $month->year;
        }

        $months = $new_array;
        $year_prev = null;
        foreach($months as $month) :
            $year_current = $month->year;
            if ($year_current != $year_prev){
                if ($year_prev != null){?>
		</ul></div>
		<?php } ?>

			
			<div class="full <?php echo esc_html( $month->year ); ?>">
			
				<div class="year">
					<h3><?php echo esc_html( $month->year ); ?></h3>
				</div>
				
				<ul class="kiwi-archive-list none col-top">
				<?php } ?>
				<li>
					<?php echo '<a href="'. esc_url( site_url().'/'.$month->year.'/'. date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ).'/">'; ?>
						<span class="archive-month"><?php echo esc_attr( date("F", mktime(0, 0, 0, $month->month, 1, $month->year)) ); ?></span>
						<span class="archive-count"><?php echo esc_html( $month->post_count ); ?></span>
					</a>
				</li>
				
				<?php $year_prev = $year_current;
				endforeach; ?>
					
			
<?php
		echo $args['after_widget'];
		echo '</div>';
		
	}
		

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['hide_months'] = isset( $new_instance['hide_months'] ) ? (bool) $new_instance['hide_months'] : false;
		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title = strip_tags($instance['title']);
		$hide_months = isset( $instance['hide_months'] ) ? (bool) $instance['hide_months'] : false;
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:', 'kiwi'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked( $hide_months ); ?> id="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_months' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'hide_months' ) ); ?>"><?php esc_html_e( 'Hide months with no posts.', 'kiwi' ); ?></label></p>
<?php
	}
}

/*===============================================================
	Widget :: Recent posts
===============================================================*/ 
class Kiwi_Widget_Recent_Posts extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_recent_entries', 'description' => esc_html__( 'Your site\'s most recent posts.', 'kiwi') );
		parent::__construct('recent-posts', esc_html__('Kiwi :: Recent posts', 'kiwi'), $widget_ops);
		$this->alt_option_name = 'widget_recent_entries';

		/* add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') ); */
	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_posts', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent Posts', 'kiwi' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
		$show_excerpt = isset( $instance['show_excerpt'] ) ? $instance['show_excerpt'] : false;
		$show_thumb = isset( $instance['show_thumb'] ) ? $instance['show_thumb'] : false;

		/**
		 * Filter the arguments for the Recent Posts widget.
		 */
		$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );
		
		echo $args['before_widget'];
			 
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} 
		
		if (  $r->have_posts() ) {
			echo '<ul>';
			while ( $r->have_posts() ) {
				 $r->the_post(); ?>
				 
				 <li>
				 		
				<div class="post-widget-text">
				
					<?php if ( $show_thumb ) : ?>
						<div class="post-widget-image">
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail(); } else {							
								$source = get_template_directory_uri() . '/framework/theme-options/images/img-empty-featuredimage.jpg';							
							echo  '<img src="'. esc_url( $source ).'">';
							}
						?>
						</div>
					<?php endif; ?>
				
					<a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID()); ?>"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></a> 
					
					<?php if ( $show_date ) : ?>
						<div class="post-date"><?php echo esc_html( get_the_date() ); ?></div>					
					<?php endif; ?> 
					
					<?php if ( $show_excerpt ) : ?>
						<div class="post-excerpt"><?php the_excerpt(); ?></div>
					<?php endif; ?> 
					
				</div>
				<div class="clear"></div>
			</li>
				 
				<?php
			}
			echo '</ul>';
			wp_reset_postdata();
		} else {
			esc_html_e( 'There are no posts.', 'kiwi' );
		}
		
		echo $args['after_widget'];
		
		
		
		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;		
		$instance['show_excerpt'] = isset( $new_instance['show_excerpt'] ) ? (bool) $new_instance['show_excerpt'] : false;
		$instance['show_thumb'] = isset( $new_instance['show_thumb'] ) ? (bool) $new_instance['show_thumb'] : false;
		//$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	//public function flush_widget_cache() {
	//	wp_cache_delete('widget_recent_posts', 'widget');
	//}

	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		$show_excerpt = isset( $instance['show_excerpt'] ) ? (bool) $instance['show_excerpt'] : false;
		$show_thumb = isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : false;
	?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'kiwi' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'kiwi' ); ?></label>
		<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>"><?php esc_html_e( 'Display post date?', 'kiwi' ); ?></label></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked( $show_excerpt ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_excerpt' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_excerpt' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_excerpt' ) ); ?>"><?php esc_html_e( 'Display excerpt?', 'kiwi' ); ?></label></p>
		
		<p><input class="checkbox" type="checkbox" <?php checked( $show_thumb ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_thumb' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_thumb' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_thumb' ) ); ?>"><?php esc_html_e( 'Display thumbnail?', 'kiwi' ); ?></label></p>
	<?php
	}
}


/*===============================================================
	Widgets
===============================================================*/
function remove_posts_widget() {
		unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');	

		register_widget('Kiwi_Widget_Recent_Posts');
		register_widget('Kiwi_Archive_Widget');
		register_widget('Kiwi_Widget_Recent_Comments');
	}
add_action( 'widgets_init', 'remove_posts_widget' );



?>
<?php

    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */
	 
	 
	 
    if ( ! class_exists( 'Kiwi_Framework_Config' ) ) {

        class Kiwi_Framework_Config {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;
			
			/* 
							public function overload_edd_license_field_path($field) {
								return dirname(__FILE__).'/edd_license/field_edd_license.php';
							}
							add_filter( "redux/{$YOUR_OPT_NAME}/field/class/edd_license", "overload_edd_license_field_path" );
 */

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }
								
				// If Redux is running as a plugin, this will remove the demo notice and links
				//add_action( 'redux/loaded', 'remove_demo' );
				
				// Function to test the compiler hook and demo CSS output.
				// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
				add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);
				
				// Change the arguments after they've been declared, but before the panel is created
				//add_filter('redux/options/'.$this->args['opt_name'].'/args', 'change_arguments' );
				
				// Change the default value of a field after it's been set, but before it's been useds
				//add_filter('redux/options/'.$this->args['opt_name'].'/defaults', 'change_defaults' );
				
				// Dynamically add a section. Can be also used to modify sections/fields
				//add_filter('redux/options/'.$this->args['opt_name'].'/sections', 'dynamic_section');
				

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }
			
			function compiler_action($options, $css, $changed_values) {
				global $wp_filesystem;
			 
				$filename = get_template_directory() . '/css/dynamic.css';	
								
				if( empty( $wp_filesystem ) ) {
					require_once( ABSPATH .'/wp-admin/includes/file.php' );
					WP_Filesystem();
				}
			 
				if( $wp_filesystem ) {
					$wp_filesystem->put_contents(
						$filename, $css,
						FS_CHMOD_FILE // predefined mode settings for WP files
					);
				}
			}

            public function setSections() {				
				
					$wpkses = array(
						'br' => array(),
						'em' => array(),
						'strong' => array(),
						'small' => array(),
					);

					
                // ACTUAL DECLARATION OF SECTIONS
               $this->sections[] = array(
				'title' => esc_html__( 'General Settings', 'kiwi' ),
				'id'    => 'settings',
				'desc'  => esc_html__( '', 'kiwi' ),
				'icon'  => 'el el-cogs',
				'fields'     => array(		
					array(
                        'id'        => 'settings-headerlogo-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Use logo image', 'kiwi'),
                        'desc'  	=> esc_html__('If left unchecked, plain text will be used instead (generated from site name).', 'kiwi'),
                        'default'   => '1'
                    ),                    
                    array(
                        'id'        => 'settings-headerlogo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Logo', 'kiwi'),
                        'compiler'  => 'true',
                        'subtitle'  => esc_html__('Upload any media using the WordPress native uploader.', 'kiwi'),
                        'default'   => array('url' => get_template_directory_uri(). '/framework/theme-options/images/img-logo-marketplace.png'),                        
						'required' => array('settings-headerlogo-enable','equals','1'),
                    ),                   
                    array(
                        'id'        => 'settings-logoheight',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Image Height', 'kiwi'),
                        'subtitle'  => esc_html__('Numeric only.', 'kiwi'),
                        'desc'      => esc_html__('Use this field to downsize your logo if applicable.', 'kiwi'),
                        'default'   => '34',
						'required' => array('settings-headerlogo-enable','equals','1'),
                    ),
					array(
                        'id'        => 'settings-logomargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '10px 20px 10px 0px',
						'required' => array('settings-headerlogo-enable','equals','1'),
                    ),
					
					
					
					array(
                        'id'        => 'mp-info-invoice-settings',
                        'type'      => 'info',
                        'desc'      => esc_html__('Invoice settings', 'kiwi')
                    ),
					
					 array(
                        'id'        => 'mp-invoice-logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Logo', 'kiwi'),
                        'compiler'  => 'true',
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        //'desc'      => esc_html__('Basic media uploader with disabled URL input field.', 'kiwi'),
                        'subtitle'  => esc_html__('Upload your company\'s logo which will be used on the Invoice.', 'kiwi'),
                       // 'default'   => array('url' => get_template_directory_uri(). '/framework/theme-options/images/img-logo-marketplace-two.png'),                        
                    ),                   
                   		
						
						
					array(				
						'id'       => 'mp-invoice-logo-height',
						'type'     => 'dimensions',
						'units'    => array('em','px','%'),
						'title'    => __('Dimensions logo', 'kiwi'),
						'desc'     => __('Use these fields to downsize your logo if applicable.', 'kiwi'),
						//'width'     => false,
						'default'  => array(
							//'width'   => '200', 
							'height'  => '34',
							'units'  => 'px'
						),
						'compiler'       => array('.mp-invoice-logo img.km-invoice-logo'),
                    ),	
											
					array(
                        'id'        => 'mp-invoice-company-address',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Company Address', 'kiwi'),
                        'subtitle'  => esc_html__('Add here your Company\'s address which will be used on the Invoice.', 'kiwi'),
                        'placeholder'   => 'HTML is allowed in here.',
						'default'	=> '',
                    ),
					
					
					
					
					array(
                        'id'        => 'settings-favicon',
                        'type'      => 'media',
                        'url'       => false,
                        'title'     => esc_html__('Favicon', 'kiwi'),
                        'compiler'  => 'true',
                        'subtitle'  => esc_html__('Upload a 16px16px .png or .gif image that will be your favicon.', 'kiwi'),
                        'default'   => array('url' => get_template_directory_uri(). '/framework/theme-options/images/img-favicon.png'), 						
                    ),	
					
					
					
					array(
                        'id'        => 'settings-pageloader-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Pageloader', 'kiwi'),
                        'desc'  => esc_html__('Check to enable page loader.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					array(
                        'id'        => 'info-settings-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Edit the fields below in case you don\'t use a logo image.', 'kiwi'),
						'required' => array('settings-headerlogo-enable','!=','1'),
                    ),
					array(
                        'id'        => 'settings-sitetitle',
                        'type'      => 'typography',
                        'title'     => esc_html__('Site title font', 'kiwi'),
						'output'    => array('h1.site-title'),
                        'subtitle'  => esc_html__('Specify the site title font properties.', 'kiwi'),
                        'google'    => true,
                        'default'   => array(
                            'color'         => '#18a9c4',
                            'font-size'     => '36px',
                            'font-family'   => 'Roboto,sans-serif',
                            'font-weight'   => 'Normal',
							'line-height'   => 'Normal',
                        ),
						'required' => array('settings-headerlogo-enable','!=','1'),
                    ),
					array(
                        'id'        => 'settings-sitedesc',
                        'type'      => 'typography',
                        'title'     => esc_html__('Site description font', 'kiwi'),
						'output'    => array('h2.site-description'),
                        'subtitle'  => esc_html__('Specify the site description font properties.', 'kiwi'),
                        'google'    => true,
                        'default'   => array(
                            'color'         => '#6d6d6d',
                            'font-size'     => '16px',
                            'font-family'   => 'Roboto,sans-serif',
                            'font-weight'   => 'Normal',
							'line-height'   => 'Normal',
                        ),
						'required' => array('settings-headerlogo-enable','!=','1'),
                    ),
					
					array(
                        'id'        => 'info-settings-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Javascript', 'kiwi')
                    ),                   
										
					array(
						'id' 		=> 'settings-googletracking',
						'type' 		=> 'textarea',
						'title'     => esc_html__('Tracking Code', 'kiwi'),
                        'subtitle'  => esc_html__('This will be added into the footer template of your theme.', 'kiwi'),
                        'desc'      => esc_html__('Please put code inside script tags.', 'kiwi'),
						'placeholder' => 'Paste your Google Analytics/Piwik (or other) tracking code here.',
					),		
					array(
                        'id'        => 'settings-headtag',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Code before &lt;/head>', 'kiwi'),
                        'subtitle'  => esc_html__('Your code will be placed before the closing head tag.', 'kiwi'),
						'placeholder'  => esc_html__('HTML is allowed in here.', 'kiwi'),
                    ),
                    array(
                        'id'        => 'settings-bodytag',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Code before &lt;/body>', 'kiwi'),
                        'subtitle'  => esc_html__('Your code will be placed before the closing body tag.', 'kiwi'),
                        'placeholder'  => esc_html__('HTML is allowed in here.', 'kiwi'),
                    ),
				)
			) ;

             

			 
				
			$this->sections[] = array(
        'title' => esc_html__( 'Site width', 'kiwi' ),
        'id'    => 'sitewidth',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-resize-horizontal',
		'fields'     => array(			
				array(
                        'id'        => 'settings-sitelayout',
                        'type'      => 'select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select your layout mode.', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => 'Full width', 
                            '2' => 'Boxed'
                        ),
                        'default'   => '1'
                    ),
					array(
                        'id'        => 'settings-sitewidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Site width', 'kiwi'),
                        'subtitle'  => esc_html__('This must be numeric only. No need to include "px" in the string.', 'kiwi'),
                      //  'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '1100',
                    ),
					
					
					
					
					array(
                        'id'        => 'settings-boxed-width',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Boxed layout :: Extra settings', 'kiwi'),
						'required' => array('settings-sitelayout','=','2'),
                    //    'desc'      => esc_html__('This is an info notice field with the normal style applied, a header and an icon.', 'kiwi')
                    ),
					
					array(
                        'id'        => 'settings-boxedmargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Boxed layout margin', 'kiwi'),
                    //    'subtitle'  => esc_html__('This must be numeric.', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                    //  'validate'  => 'no_special_chars',
                        'default'   => '40px auto 40px auto',
						'required' => array('settings-sitelayout','=','2'),
                    ),					
					
					array(
						'id'       => 'boxed-border',
						'type'     => 'border',
						'title'    => esc_html__('Border Option', 'kiwi'),
						//'subtitle' => esc_html__('Only color validation can be done on this field type', 'kiwi'),
						'output'   => array('.page-container.boxed'),
						//'desc'     => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
						'default'  => array(
							'border-color'  => '#e4e4e4', 
							'border-style'  => 'solid', 
							'border-top'    => '1px', 
							'border-right'  => '1px', 
							'border-bottom' => '1px', 
							'border-left'   => '1px'
						),
						'required' => array('settings-sitelayout','=','2'),
					),
					
					
					array(
                        'id'        => 'boxed-shadow',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable box shadow', 'kiwi'),
                     //  'desc'     => wp_kses(__('This field only works if <em>boxed layout</em> is selected.', 'kiwi'), $wpkses),
                        'default'   => '0',
						'required' => array('settings-sitelayout','=','2'),
                    ),
					
        )
    ) ;
	
	
   $this->sections[] = array(
        'title' => esc_html__( 'Header', 'kiwi' ),
        'id'    => 'header',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-screenshot',
		'fields'     => array(	
             array(
                        'id'        => 'header-layoutstyle',
                        'type'      => 'select',
                        'title'     => esc_html__('Select header layout', 'kiwi'),
                        'subtitle'  => esc_html__('See below for more customization options.', 'kiwi'),                     
                        'options'   => array(
                            '1' => 'Single row', 
                            '2' => 'Double rows',
							'3' => 'None'
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'header-heightcontrol',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom header padding', 'kiwi'),
                        'subtitle'  => esc_html__('Use this to control the header height.', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                      //  'validate'  => 'no_special_chars',
                        'default'   => '0px 0px 0px 0px',
                    ),
                     
					 
					array(
                        'id'        => 'headerinfo-singlerowoptions',
                        'type'      => 'info',
                        'desc'      => esc_html__('Single row', 'kiwi')
                    ),
										
					 array(
                        'id'        => 'info-header-one',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('First column', 'kiwi'),                   
                    ),
					
					
					
					array(
                        'id'        => 'header-columnone',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>first</strong> column.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '2'
                    ),
					
					array(
                        'id'        => 'header-columnoneeditor',
                        'type'      => 'editor',
                        'title'     => esc_html__('Custom text', 'kiwi'),
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'validate'  => 'html',
						'required' => array('header-columnone','=','7'),
                    ),
									
					array(
                        'id'        => 'header-columnonealign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                        'default'   => 'left'
                    ),
					
					array(
                        'id'        => 'header-columnonefloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),                    
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                        'default'   => 'left'
                    ),
					
					array(
                        'id'        => 'header-columnonecentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  => esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' => array( 
										array('header-columnone','=','2'), 
										array('header-columnone','=','3'),
										array('header-columnone','=','4'), 										
									),
                    ),
					
					array(
                        'id'        => 'header-columnonewidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
                       'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                        'default'   => '',
                    ),
					
					array(
                        'id'        => 'header-columnonemargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '10px 0px 0px 0px',
                    ),
					
					
					/* HEADER :: SECOND COLUMN */
					array(
                        'id'        => 'info-header-two',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Second column', 'kiwi'),
                    ),
					
					array(
                        'id'        => 'header-columntwo',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>second</strong> column.', 'kiwi'), $wpkses),
                       'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
							'8' => 'Leave empty', 
                            '9' => 'Marketplace stats', 
                        ),
                        'default'   => '6' 
                    ),
					
					array(
                        'id'        => 'header-columntwoeditor',
                        'type'      => 'editor',
                        'title'     => esc_html__('Custom text', 'kiwi'),
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'validate'  => 'html',	
						'required' => array('header-columntwo','=','7'),		
                    ),
					
					
									
					array(
                        'id'        => 'header-columntwoalign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                       // 'default'   => 'center'
                    ),
					
					
					array(
                        'id'        => 'header-columntwofloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                        'default'   => 'right'
                    ),
					
					array(
                        'id'        => 'header-columntwocentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  	=> esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' => array( 
										array('header-columntwo','=','2'), 
										array('header-columntwo','=','3'),
										array('header-columntwo','=','4'), 										
									),
                    ),
					
					array(
                        'id'        => 'header-columntwowidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
                       'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                        'default'   => '',
                    ),
					
					array(
                        'id'        => 'header-columntwomargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '13px 0px 13px 0px',
                    ),
					
					
					/* HEADER :: THIRD COLUMN */
					array(
                        'id'        => 'info-header-three',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Third column', 'kiwi'),
                    ),
					
					array(
                        'id'        => 'header-columnthree',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>third</strong> column.', 'kiwi'), $wpkses),                        
                        'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '8'
                    ),
					
					array(
                        'id'        => 'header-columnthreeeditor',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom text', 'kiwi'),                     
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'required' 	=> array('header-columnthree','=','7'),
                    ),
									
					array(
                        'id'        => 'header-columnthreealign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                       // 'default'   => 'center'
                    ),
					
					array(
                        'id'        => 'header-columnthreefloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                       // 'default'   => 'right'
                    ),
					
					array(
                        'id'        => 'header-columnthreecentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  	=> esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' => array( 
										array('header-columnthree','=','2'), 
										array('header-columnthree','=','3'),
										array('header-columnthree','=','4'), 										
									),						
                    ),
					
					array(
                        'id'        => 'header-columnthreewidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
						'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                        'default'   => '',
                    ),
					
					array(
                        'id'        => 'header-columnthreemargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                       // 'default'   => '40px 0px 0px 0px',
                    ),
					
					
					/* HEADER :: FOURTH COLUMN */
					array(
                        'id'        => 'info-header-four',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Fourth column', 'kiwi'),
                    ),
					
					array(
                        'id'        => 'header-columnfour',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>fourth</strong> column.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '8'
                    ),
					
					array(
                        'id'        => 'header-columnfoureditor',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom text', 'kiwi'),
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'required' => array('header-columnfou','=','7'),						
                    ),
									
					array(
                        'id'        => 'header-columnfouralign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                       // 'default'   => 'right'
                    ),
					
					array(
                        'id'        => 'header-columnfourfloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                      //  'default'   => 'left'
                    ),
					
					array(
                        'id'        => 'header-columnfourcentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  => esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' => array( 
										array('header-columnfour','=','2'), 
										array('header-columnfour','=','3'),
										array('header-columnfour','=','4'), 										
									),
                    ),
					
					array(
                        'id'        => 'header-columnfourwidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
						'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                       // 'default'   => '25%',
                    ),
					
					array(
                        'id'        => 'header-columnfourmargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                       // 'default'   => '30px 0px 30px 0px',
                    ),
					
					
					
					
					/* HEADER :: SECOND ROW */
					array(
                        'id'        => 'info-header-five',
                        'type'      => 'info',
                        'desc'      => esc_html__('Second row', 'kiwi'),
						'required' 	=> array('header-layoutstyle','=','2'),
                    ),
					
					
					array(
                        'id'        => 'header-secondrowborder',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable border', 'kiwi'),
                        'subtitle'  => esc_html__('Check to enable a border between the two rows.', 'kiwi'),
                        'default'   => '1',
						'required' 	=> array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'       => 'header-secondrow-border',
						'type'     => 'border',
						'title'    => esc_html__('Border properties', 'kiwi'),
						'subtitle' => esc_html__('Select border color.', 'kiwi'),
						'output'   => array('.kiwi-border'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => false,
						'bottom'   => true,
						'default'  => array(
							'border-color'  => '#eeeeee',
							'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '1px',
							'border-left'   => '0'
							),
						'required' => array( 
									array('header-layoutstyle','=','2'), 
									array('header-secondrowborder','=','1'),										
								),

						
                    ),	
					
					/* HEADER ROW TWO:: FIRST COLUMN */
					 array(
                        'id'        => 'opt-info-normal',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('First column', 'kiwi'),
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					
					
					array(
                        'id'        => 'header-secondrowcolumnone',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>first</strong> column.', 'kiwi'), $wpkses),
						'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '8',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnoneeditor',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom text', 'kiwi'),                     
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'required' => array( 
									array('header-secondrowcolumnone','=','7'), 
									array('header-layoutstyle','=','2'),										
								),
                    ),
						
									
					array(
                        'id'        => 'header-secondrowcolumnonealign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),  
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                      // 'default'   => 'left',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnonefloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                       //'default'   => 'left',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnonecentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  	=> esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' 	=> array( 
										array('header-layoutstyle','=','2'),
										array('header-secondrowcolumnone','=','2'), 
										array('header-secondrowcolumnone','=','3'),
										array('header-secondrowcolumnone','=','4'), 										
									),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnonewidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
                        'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
						//'default'   => '25%',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnonemargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                      //  'default'   => '30px 0px 30px 0px',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					
					/* HEADER ROW TWO:: SECOND COLUMN */
					array(
                        'id'        => 'info-header-six',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Second column', 'kiwi'),
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumntwo',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>second</strong> column.', 'kiwi'), $wpkses),
						'required' => array('header-layoutstyle','=','2'),
                        'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '8',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumntwoeditor',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom text', 'kiwi'),                    
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'required' => array( 
									array('header-secondrowcolumntwo','=','7'), 
									array('header-layoutstyle','=','2'),										
								),	
                    ),
									
					array(
                        'id'        => 'header-secondrowcolumntwoalign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                      //  'default'   => 'center',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumntwofloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'), 
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                      // 'default'   => 'left',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumntwocentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  	=> esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' => array(
										array('header-layoutstyle','=','2'),
										array('header-secondrowcolumntwo','=','2'), 
										array('header-secondrowcolumntwo','=','3'),
										array('header-secondrowcolumntwo','=','4'), 										
									),						
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumntwowidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
                       'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                    //    'default'   => '25%',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumntwomargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                      //  'default'   => '30px 0px 0px 0px',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					
					
					
					/* HEADER ROW TWO:: THIRD COLUMN */
					array(
                        'id'        => 'info-header-seven',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Third column', 'kiwi'),
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnthree',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>third</strong> column.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '8',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnthreeeditor',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom text', 'kiwi'),                  
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'required' => array( 
									array('header-secondrowcolumnthree','=','7'), 
									array('header-layoutstyle','=','2'),										
								),	
                    ),
									
					array(
                        'id'        => 'header-secondrowcolumnthreealign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'), 
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                       // 'default'   => 'center',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnthreecentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  	=> esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' => array( 
										array('header-layoutstyle','=','2'),
										array('header-secondrowcolumnthree','=','2'), 
										array('header-secondrowcolumnthree','=','3'),
										array('header-secondrowcolumnthree','=','4'), 										
									),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnthreefloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                       // 'default'   => 'left',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnthreewidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
                       'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                      //  'default'   => '25%',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnthreemargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                       // 'default'   => '30px 0px 30px 0px',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					
					array(
                        'id'        => 'info-header-eight',
                        'type'      => 'info',
                        'notice'    => true,
                        'title'     => esc_html__('Fourth column', 'kiwi'),
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					/* HEADER ROW TWO:: FOURTH COLUMN */
					array(
                        'id'        => 'header-secondrowcolumnfour',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>fourth</strong> column.', 'kiwi'), $wpkses),
						'required' => array('header-layoutstyle','=','2'),
                        'options'   => array(
                            '1' => 'Logo', 
                            '2' => 'Main navigation', 
                            '3' => 'Header first half', 
                            '4' => 'Header second half',
							'5' => 'Social Media', 
                            '6' => 'Search',
							'7' => 'Custom text', 
                            '8' => 'Leave empty', 							
                            '9' => 'Marketplace stats'
                        ),
                        'default'   => '8',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnfoureditor',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom text', 'kiwi'),                    
                        'desc'      => esc_html__('This field only works when you selected the "custom text."', 'kiwi'),
						'required' => array( 
									array('header-secondrowcolumnfour','=','7'), 
									array('header-layoutstyle','=','2'),										
								),	
                    ),
									
					array(
                        'id'        => 'header-secondrowcolumnfouralign',
                        'type'      => 'select',
                        'title'     => esc_html__('Column text alignment', 'kiwi'),
                    //    'subtitle'  => esc_html__('No validation can be done on this field type', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                      //  'default'   => 'right',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnfourfloat',
                        'type'      => 'select',
                        'title'     => esc_html__('Column block alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left',                             
                            'right' => 'right',
                        ),
                       // 'default'   => 'left',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnfourcentermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Center navigation', 'kiwi'),
                        'desc'  	=> esc_html__('Check to forcefully center the navigation inside the block element.', 'kiwi'),
                        'default'   => '0',
						'required' 	=> array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnfourwidth',
                        'type'      => 'text',
                        'title'     => esc_html__('Column width', 'kiwi'),
                        'desc'      => esc_html__('Controls the width of the first column. Use px or %.', 'kiwi'),
                        //'default'   => '25%',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					array(
                        'id'        => 'header-secondrowcolumnfourmargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Additional margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        //'default'   => '30px 0px 30px 0px',
						'required' => array('header-layoutstyle','=','2'),
                    ),
					
					
					
					
					array(
                        'id'        => 'info-header-nine',
                        'type'      => 'info',
                        'desc'      => esc_html__('Additional header options', 'kiwi')
                    ),
					
					array(
                        'id'        => 'header-sticky',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Fixed header', 'kiwi'),
                        'desc'      => esc_html__('Check to enable a sticky header when scrolling down.', 'kiwi'),
                        'default'   => '0',
                    ),
					
					array(
                        'id'        => 'header-stickymargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom sticky padding', 'kiwi'),
                        'subtitle'  => esc_html__('Use this to control the sticky header height.', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '10px 0px 10px 0px',
                    ),
					
					array(
                        'id'        => 'header-expandable-search',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Search Icon', 'kiwi'),
                        'subtitle'  => esc_html__('Main navigation', 'kiwi'),
                        'desc'      => esc_html__('Check to enable an Full Screen Search Overlay inside the main navigation.', 'kiwi'),
                        'default'   => '0',
                    ),
					
					array(
                        'id'        => 'header-expandable-search-topbar',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Search Icon', 'kiwi'),
                        'subtitle'  => esc_html__('Topbar', 'kiwi'),
                        'desc'      => esc_html__('Check to enable an Full Screen Search Overlay inside the topbar navigation.', 'kiwi'),
                        'default'   => '0'
                    ),
        )
    ) ;
	
	
	
   


  $this->sections[] = array(
        'title' => esc_html__( 'Topbar', 'kiwi' ),
        'id'    => 'topbar',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-edit',
		'fields'     => array(	
		
            array(
                        'id'        => 'info-topbar-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar', 'kiwi')
                    ),
					array(
                        'id'        => 'topbar-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable topbar', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'topbar-position',
                        'type'      => 'select',
                        'title'     => esc_html__('Select position', 'kiwi'),
                        'options'   => array(
                            '1' => 'Above the header', 
                            '2' => 'Below the header'
                        ),
                        'default'   => '1'
                    ),
				
					array(
                        'id'        => 'topbar-custommargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom topbar margin', 'kiwi'),
                        'subtitle'  => esc_html__('Optional', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '0px 0px 0px 0px',
                    ),
				
                   array(
                        'id'        => 'info-topbar-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar :: Right block', 'kiwi')
                    ),
					array(
                        'id'        => 'topbar-contentright',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>right</strong> topbar block.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Topbar navigation', 
                            '2' => 'Social media', 
                            '3' => 'Custom text', 
                            '4' => 'Leave empty',
							'5' => 'Logo',
							'6' => 'Marketplace stats',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'topbar-customtext-right',
                        'type'      => 'editor',
                        'title'     => esc_html__('Topbar custom text', 'kiwi'),
                        'subtitle'  => esc_html__('Right block.', 'kiwi'),
                        'desc'      => esc_html__('This field only works when you selected "custom text".', 'kiwi'),
						'validate'  => 'html',
						'required' => array('topbar-contentright','=','3'),
                    ),
					
					array(
                        'id'        => 'topbar-customtext-rightmargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Extra margin', 'kiwi'),
                        'subtitle'  => esc_html__('Right block.', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '20px 0px 15px 0px',
                    ),
					
					array(
                        'id'        => 'info-topbar-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar :: Left block', 'kiwi')
                    ),
					
					array(
                        'id'        => 'topbar-contentleft',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>left</strong> topbar block.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Topbar navigation', 
                            '2' => 'Social media', 
                            '3' => 'Custom text', 
                            '4' => 'Leave empty',
							'5' => 'Logo',
							'6' => 'Marketplace stats',
                        ),
                        'default'   => '5'
                    ),
					
					array(
                        'id'        => 'topbar-customtext-left',
                        'type'      => 'editor',
                        'title'     => esc_html__('Topbar custom text', 'kiwi'),
                        'subtitle'  => esc_html__('Left block.', 'kiwi'),
                        'desc'      => esc_html__('This field only works when you selected "custom text".', 'kiwi'),
						'validate'  => 'html',
						'default'	=> 'Mauris mollis lectus id nibh mollis semper.',
						'required' => array('topbar-contentleft','=','3'),
                    ),
					
					array(
                        'id'        => 'topbar-customtext-leftmargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Extra margin', 'kiwi'),
                        'subtitle'  => esc_html__('Left block.', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '8px 0px 8px 0px',
                    ),	
					
					
					array(
                        'id'        => 'info-topbar-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar :: Typography', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'topbar-typography',
                        'type'      => 'typography',
                        'title'     => esc_html__('Topbar font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the topbar font properties.', 'kiwi'),
						'output'   	=> array('.topbar .pull-left, .topbar ul li a'),
                        'google'    => true,
					//	'line-height'    => false,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#fff',
                            'font-size'     => '13px',
                            'font-family'   => 'sans-serif',
                            //'font-weight'   => 'Normal',
							//'line-height'   => 'Normal',
                        ),
                    ),
					

					array(
                        'id'        => 'info-topbar-five',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar :: Styling', 'kiwi')
                    ),
					
					

					array(
                        'id'        => 'topbar-styling',
                        'type'      => 'background',
                        'output'    => array('.topbar'),
                        'title'     => esc_html__('Topbar background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color for the topbar.', 'kiwi'),
						'default'  => array( 'background-color' => '#2b2b2b',),
						'preview'	=> true,					
                    ),
					
					 array(
                        'id'        => 'topbar-enableborder',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable topbar border', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'       => 'topbar-topborder',
						'type'     => 'border',
						'title'    => esc_html__('Topbar border', 'kiwi'),
						'subtitle' => esc_html__('Select top border color.', 'kiwi'),
						'output'   => array('.topbar'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => true,
						'bottom'   => false,
						//'style'    => false,
						'default'  => array(
							//'border-color'  => '#e4e4e4',
							//'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0'
						),
						'required' => array('topbar-enableborder','=','1'),						
                    ),
					
					array(
                        'id'       => 'topbar-bottomborder',
						'type'     => 'border',
						'title'    => esc_html__('Topbar border', 'kiwi'),
						'subtitle' => esc_html__('Select bottom border color.', 'kiwi'),
						'output'   => array('.topbar'),
						// 'desc'     => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => false,
						'bottom'   => true,
						//'style'    => false,
						'default'  => array(
							'border-color'  => '#e4e4e4',
							'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '1px',
							'border-left'   => '0'
						),
					'required' => array('topbar-enableborder','=','1'),						
                    ),	

        )
    ) ;
	

  $this->sections[] = array(
        'title' => esc_html__( 'Page title bar', 'kiwi' ),
        'id'    => 'pagetitle',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-screenshot',
		'fields'     => array(	
            array(
                        'id'        => 'pagetitle-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title bar', 'kiwi'),
                        'default'   => '1'
                    ),
					
					 array(
                        'id'        => 'pagetitle-showon',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Show page title bar on:', 'kiwi'),
                        'options'   => array(
                            '1' => 'posts', 
                            '2' => 'pages', 
                            '3' => 'all',
							'4' => 'all except home or static front page',
                        ),
						'default'   => array(
                            '1' => '0', 
                            '2' => '0', 
                            '3' => '0',
							'4' => '1'
                        )
					),
					
					array(
                        'id'        => 'pagetitle-padding',
                        'type'      => 'text',
                        'title'     => esc_html__('Page title bar padding', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '30px 0px 30px 0px',
                    ),
					
					array(
                        'id'        => 'pagetitle-margin',
                        'type'      => 'text',
                        'title'     => esc_html__('Page title bar margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '0px 0px 50px 0px',
                    ),
					
					array(
                        'id'        => 'pagetitle-align',
                        'type'      => 'select',
                        'title'     => esc_html__('Select text alignment', 'kiwi'),
                        'options'   => array(
                            'left' => 'left', 
                            'center' => 'center', 
                            'right' => 'right'
                        ),
                        'default'   => 'left'
                    ),
					
					array(
                        'id'        => 'info-pagetitle-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Page title bar :: Typography', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'pagetitle-typography',
                        'type'      => 'typography',
                        'title'     => esc_html__('Headings font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the headings font properties for pagetitle.', 'kiwi'),
						'output'   	=> array('.pagetitle h1,.pagetitle h2,.pagetitle h3,.pagetitle h4,.pagetitle h5,.pagetitle h6'),
                        'google'    => true,
						'subsets'   => false,
						'font-size'   => false,
						'line-height'   => false,
                        'default'   => array(
                            'color'         => '#fff',
                            'font-family'   => 'Roboto',
                            'font-weight'   => 'Normal'
                        ),
                    ),
					

					array(
                        'id'        => 'info-pagetitle-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Page title bar :: Styling', 'kiwi')
                    ),

					array(
                        'id'        => 'pagetitle-styling',
                        'type'      => 'background',
                        'output'    => array('.pagetitle'),
                        'title'     => esc_html__('Title bar background', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color or image for the page title bar.', 'kiwi'),
                        'default'  => array(
											'background-color' => '#6b953f',
										),
						'preview'	=> true
                    ),
					
					array(
                        'id'       => 'pagetitle-topborder',
						'type'     => 'border',
						'title'    => esc_html__('Title bar border', 'kiwi'),
						'subtitle' => esc_html__('Select top border color.', 'kiwi'),
						'output'   => array('.pagetitle'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => true,
						'bottom'   => false,
						 'default'  => array(
							'border-color'  => '#e4e4e4',
							'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0'
						)					
                    ),
					
					array(
                        'id'       => 'pagetitle-bottomborder',
						'type'     => 'border',
						'title'    => esc_html__('Title bar border', 'kiwi'),
						'subtitle' => esc_html__('Select bottom border color.', 'kiwi'),
						'output'   => array('.pagetitle'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => false,
						'bottom'   => true,
						 'default'  => array(
							'border-color'  => '#e4e4e4',
							'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0'
						)	 				
                    ),		
					
					
					array(
                        'id'        => 'info-pagetitle-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Breadcrumbs', 'kiwi')
                    ),
					 array(
                        'id'        => 'pagetitle-breadcrumbs-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable breadcrumbs inside page title bar', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'pagetitle-breadcrumbs-position',
                        'type'      => 'select',
                        'title'     => esc_html__('Select position', 'kiwi'),
                        'options'   => array(
                            '1' => 'Above the page title', 
                            '2' => 'Below the page title'
                        ),
                        'default'   => '1',
						'required' => array('pagetitle-breadcrumbs-enable','=','1'),	
                    ),
					
					array(
                        'id'        => 'pagetitle-breadcrumbs-margin',
                        'type'      => 'text',
                        'title'     => esc_html__('Breadcrumbs margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '0px 0px 10px 0px',
						'required' => array('pagetitle-breadcrumbs-enable','=','1'),
                    ),
					
					array(
                        'id'        => 'info-pagetitle-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Breadcrumbs + Author copyright :: Typography', 'kiwi'),
                    ),
					
					 array(
                        'id'        => 'pagetitle-breadcrumbs-typography',
                        'type'      => 'typography',
                        'title'     => esc_html__('Breadcrumbs font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the breadcrumbs font properties.', 'kiwi'),
						'output'   	=> array('.breadcrumbs, .author-copyright'),
                        'google'    => true,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#fff',
                            'font-size'     => '12px',
                            'font-family'   => 'Arial,Helvetica,sans-serif',
                            'font-weight'   => 'Normal',
						//	'line-height'   => 'Normal',
							),
						),
						
						
						
						
        )
    ) ;
	
	
	
	
$this->sections[] = array(
        'title' => esc_html__( 'Sidebar', 'kiwi' ),
        'id'    => 'sidebar',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-pause',
		'fields'     => array(	
            array(
                        'id'        => 'info-sidebar-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Posts', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'sidebar-posts-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-posts-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-posts-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
						'default'  => 'post-sidebar'
                    ),
					
					array(
                        'id'        => 'sidebar-posts-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
						'default'  => 'post-sidebar-two',
                    ),
					
					array(
                        'id'        => 'sidebar-posts-sidebarlayout',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select the position of the sidebar.', 'kiwi'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					
					array(
                        'id'        => 'info-sidebar-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Pages', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'sidebar-pages-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'sidebar-pages-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-pages-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'   => 'page-sidebar-sidebar',
                    ),
					
					array(
                        'id'        => 'sidebar-pages-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'  => 'page-sidebar-two',
                    ),
					
					array(
                        'id'        => 'sidebar-pages-sidebarlayout',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					array(
                        'id'        => 'info-sidebar-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Archives (Author,Category,Index,Tags)', 'kiwi')
                    ),
					
					array(
                        'id'        => 'sidebar-archive-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-archive-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'sidebar-archive-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'  => 'post-sidebar',
                    ),
					
					array(
                        'id'        => 'sidebar-archive-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'  => 'post-sidebar-two',
                    ),
					
					array(
                        'id'        => 'sidebar-archive-sidebarlayout',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select the position of the sidebar.', 'kiwi'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					
					
					array(
                        'id'        => 'info-sidebar-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Search page', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'sidebar-search-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-search-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-search-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'  => 'post-sidebar',
                    ),
					
					array(
                        'id'        => 'sidebar-search-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
                    	'default'  => 'post-sidebar-two',
                    ),
					
					array(
                        'id'        => 'sidebar-search-sidebarlayout', 
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select the position of the sidebar.', 'kiwi'),
						'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					array(
                        'id'        => 'info-sidebar-five',
                        'type'      => 'info',
                        'desc'      => esc_html__('Page template :: Blog posts', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'sidebar-blog-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-blog-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-blog-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'  => 'post-sidebar',
                    ),
					
					array(
                        'id'        => 'sidebar-blog-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
					 	'default'  => 'post-sidebar-two',
                    ),
					
					array(
						'id'        => 'sidebar-blog-sidebarlayout', 
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select the position of the sidebar.', 'kiwi'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					array(
                        'id'        => 'sidebar-blog-postcount',
                        'type'      => 'text',
                        'title'     => esc_html__('Post number', 'kiwi'),
                        'subtitle'  => esc_html__('Number of post per page.', 'kiwi'),
                        'default'   => '15',
                    ),
					
					
					
					
        )
    ) ;
	
	
	$this->sections[] = array(
			'title' => esc_html__( 'Third Party Plugins', 'kiwi' ),
			'id'    => 'marketplace-plugins',
			//'desc'  => esc_html__( '', 'kiwi' ),
			'icon'  => 'el el-fire',
			'fields'     => array(

			array(
				'id'       => 'mp-extension-list',
				'type'     => 'checkbox',
				'title'    => esc_html__('Extensions', 'kiwi'), 						
				'desc'     => esc_html__('This Theme Options Panel comes with additional setting options for third party plugins such as FES. These options are visible by default unless you unchecked them to remove the options.', 'kiwi'),                     
				'options'  => array(
					'1' => 'FrontEnd Submission',
					'2' => 'Wishlist',
					'3' => 'Wallet'
				),
				'default' => array(
					'1' => '1', 
					'2' => '1', 
					'3' => '1',
				)
			),


			array(
				'id'        => 'info-wp-theme-plugin-list-vc',
				'type'      => 'info',
				'desc'      => esc_html__('FES :: Vendor page template', 'kiwi'),
				'required'  => array('mp-extension-list','contains',array('1' => '1')),
			),

			
			array(
				'id'        => 'marketplace-fes-gridcolumns',
				'type'      => 'select',
				'title'     => esc_html__('Grid columns', 'kiwi'),
				'options'   => array(
					'1' => '2 columns', 
					'2' => '3 columns', 
					'3' => '4 columns'
				),
				'default'   => '1',
				'required'  => array('mp-extension-list','contains',array('1' => '1')),
			),
			
			
			array(
				'id'        => 'marketplace-fes-postcount',
				'type'      => 'text',
				'title'     => esc_html__('Item number', 'kiwi'),
				'subtitle'  => esc_html__('Number of items per page.', 'kiwi'),
				'default'   => '10',
				'required'  => array('mp-extension-list','contains',array('1' => '1')),
			),	
			
			
			array(
				'id'        => 'info-metabox-options',
				'type'      => 'info',
				'desc'  	=> wp_kses(__('Extra options :: NON-FES<br><br>For those who don\'t use FES, but still want the extra options in the backend, we\'ve created these extra settings for you. You can enable or disable these features depending on what you use this theme for.<br><br><small>Note: Disabling these metaboxes, makes them no longer visible on the page, but it does <strong>not</strong> remove the data from your database!</small>', 'kiwi'), $wpkses),
				'required'  => array('mp-extension-list','contains',array('1' => '0')),
			),
			
			array(
				'id'        => 'metabox-themes-enable',
				'type'      => 'checkbox',
				'title'     => esc_html__('Themes, Plugins, Graphics', 'kiwi'),
				'desc'      => esc_html__('If checked this will add an extra File Information metabox, a widget and three extra taxonomies (framework, plugin, file format). ', 'kiwi'),
				'default'   => '1',
				'required'  => array('mp-extension-list','contains',array('1' => '0')),
			),

			array(
				'id'        => 'metabox-images-enable',
				'type'      => 'checkbox',
				'title'     => esc_html__('Image uploader', 'kiwi'),
				'desc'      => esc_html__('If checked this will add an extra Item Images metabox.', 'kiwi'),
				'default'   => '1',
				'required'  => array('mp-extension-list','contains',array('1' => '0')),
			), 
		

	 )
	) ;	
	
	
	$this->sections[] = array(
        'title' => esc_html__( 'Marketplace', 'kiwi' ),
        'id'    => 'marketplace',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-fire',
		'fields'     => array(

					array(
                        'id'        => 'info-marketplace-userdashboard',
                        'type'      => 'info',
                        'desc'      => esc_html__('User Dashboard :: Announcement', 'kiwi')
                    ),
					
					array(
                        'id'        => 'marketplace-announcement-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable announcement', 'kiwi'),
						'desc'  => esc_html__('This message will appear inside the user dashboard once logged in.', 'kiwi'),
                        'default'   => '0',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),	
					
					array(
                        'id'        => 'marketplace-announcement',
                        'type'      => 'editor',
                        'title'     => esc_html__('Message', 'kiwi'),
                        'validate'  => 'html', 
						'default'	=> esc_html__('This is the user/buyer dashboard. Add welcome text or any other information that is applicable to your users/buyers.', 'kiwi'),
						'required' 	=> array('marketplace-announcement-enable','=','1'),
				  ),
					
					
		
           
					array(
                        'id'        => 'info-marketplace-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Page title bar :: All Rights Reserved', 'kiwi')
                    ),

					array(
                        'id'        => 'pagetitle-copyright-enableprefix',
                        'type'      => 'switch',
                        'title'     => esc_html__('Pagetitle Prefix', 'kiwi'),
                       'subtitle'  => esc_html__('&copy; All Rights Reserved [author]', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),	

					array(
                        'id'        => 'pagetitle-copyright-margin',
                        'type'      => 'text',
                        'title'     => esc_html__('Pagetitle Prefix margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '10px 0px 0px 0px',
                    ),
					
					
					array(
                        'id'        => 'marketplace-excerpt-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Excerpt', 'kiwi')
                    ),
					
					array(
                        'id'        => 'marketplace-enableexcerpt',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                       'desc'  		=> wp_kses(__('These settings will only take effect if you use <em>list view</em> as a <em>layout option</em>.<br>(Marketplace &raquo; Sidebar &raquo; Archive pages :: Layout option)<br>Or if you use EDD\'s build in [download] shortcode.', 'kiwi'), $wpkses),
						'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),	
				
				array(
                        'id'        => 'marketplace-excerptlength',
                        'type'      => 'text',
                        'title'     => esc_html__('Excerpt length', 'kiwi'),
                        'subtitle'  => esc_html__('(by words)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '20',
                    ),
					
				array(
                        'id'        => 'marketplace-excerptending',
                        'type'      => 'text',
                        'title'     => esc_html__('Excerpt ending', 'kiwi'),
                        'default'   => '...',
                    ),

				array(
                        'id'        => 'marketplace-readmore',
                        'type'      => 'text',
                        'title'     => esc_html__('Read more text', 'kiwi'),
                        'subtitle'  => wp_kses(__('Customize the <strong>read more</strong> text.', 'kiwi'), $wpkses),
                        'default'   => 'more &raquo;',
                    ),	
        )
    ) ;
	
	
	$this->sections[] = array(
        'title'      => esc_html__( 'Page :: Item description', 'kiwi' ),
        'id'         => 'marketplaceitemdesc',
        'subsection' => true,
		'fields'     => array(
		
					array(
                        'id'        => 'marketplace-enable-comments',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable comments', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide comments on an item description page.', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'info-marketplace-description-design',
                        'type'      => 'info',
                        'desc'      => esc_html__('Item description :: Design', 'kiwi')
                    ),
		
					array(
                        'id'        => 'marketplace-enable-design',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Design', 'kiwi'),
                        'subtitle'  => esc_html__('Select design.', 'kiwi'),
                        'desc'      => esc_html__('With or without social media buttons on the left.', 'kiwi'),
                        'options'   => array(
                            '1' => array('alt' => 'With Social Media',   'img' => get_template_directory_uri() . '/framework/theme-options/images/designone.png'),
                            '2' => array('alt' => 'Without Social Media',    'img' => get_template_directory_uri() . '/framework/theme-options/images/designtwo.png'),
                        ), 
                        'default' => '1'
                    ),	
		
					array(
                        'id'        => 'marketplace-enable-design-width',
                        'type'      => 'switch',
                        'title'     => esc_html__('Full width featured image', 'kiwi'),
						'desc'  => esc_html__('If a design without Social Media Buttons is used, enable this option to forcefully adjust the width of the featured image to the column width.', 'kiwi'),
                        'default'   => 'off',
                        'on'        => 'Yes',
                        'off'       => 'No',
						'required' => array('marketplace-enable-design','=','2'),
                    ),
					
					array(
                        'id'        => 'mp-lightbox',
                        'type'      => 'text',
                        'title'     => esc_html__('Lightbox', 'kiwi'),
                        'subtitle'  => esc_html__('Preview slider', 'kiwi'),
                        'desc'      => wp_kses(__('E.g.: data-rel="lightbox" or rel="lightbox" <br><small>Read the documentation of the lightbox plugin you\'re using in order to add the right attribute to these links.</small>', 'kiwi'), $wpkses),
                    ),					
					
		
					array(
                        'id'        => 'info-marketplace-description-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Item description :: License', 'kiwi')
                    ),
					
					array(
                        'id'        => 'marketplace-disclaimer-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable custom license text', 'kiwi'),
						'desc' 		=> wp_kses(__('This will be placed inside the <em>Marketplace :: Purchase block</em> widget.', 'kiwi'), $wpkses),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),	
					
					
					array(
                        'id'        => 'marketplace-disclaimer',
                        'type'      => 'editor',
                        'title'     => esc_html__('Custom license text', 'kiwi'), 
						'validate'  => 'html',		
						'default'	=> __('Use, by you or one client, in a single end product which end users are <strong>not</strong> charged for. The total price includes the item price and a small buyer fee. <a href="#">Detailed explanation <i class="fa fa-angle-double-right"></i></em></a>', 'kiwi'),
						'required' => array('marketplace-disclaimer-enable','=','1'),
					),
					
					
					array(
                        'id'        => 'info-marketplace-description-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Item description :: Price', 'kiwi')
                    ),
					
					array(
                        'id'        => 'marketplace-free-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Change price display if price is $0.00', 'kiwi'),
						'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),	
					
					
					array(
                        'id'        => 'marketplace-free-customtext',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom text', 'kiwi'),
                        'desc'      => esc_html__('Custom text in place of $0.00.', 'kiwi'),
                        'default'	=> esc_html__('Free file', 'kiwi'),
						
                    ),
					
					array(
                        'id'        => 'marketplace-decimal-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Disable decimal from all prices.', 'kiwi'),
						'desc'  	=> esc_html__('Only prices with two zeros after the decimal will be trimmed.', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),
					
					
					array(
                        'id'        => 'info-marketplace-audio',
                        'type'      => 'info',
                        'desc'      => esc_html__('Item description :: Audio Settings', 'kiwi')
                    ),
					array(
                        'id'        => 'marketplace-audio-featuredimage',
                        'type'      => 'text',
                        'title'     => esc_html__('Hide featured image?', 'kiwi'),
						'desc'      => esc_html__('Hide featured image if audio playlist exceeds X number of uploaded tracks.', 'kiwi'),
                        'default'   => '4',
                    ),
					
					
					array(
                        'id'        => 'info-marketplace-slideroption',
                        'type'      => 'info',
                        'desc'      => esc_html__('Item description :: Slider settings', 'kiwi')
                    ),
					array(
                        'id'        => 'marketplace-slider-slideshow',
                        'type'      => 'text',
                        'title'     => esc_html__('Slides to show', 'kiwi'),
                        'desc'      => esc_html__('X number of slides to show at a time. (numeric only)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '4',
                    ),
					
					array(
                        'id'        => 'marketplace-slider-slidescroll',
                        'type'      => 'text',
                        'title'     => esc_html__('Slides to scroll', 'kiwi'),
                        'desc'      => esc_html__('X number of slides to scroll at a time. (numeric only)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '1',
                    ),
					
					array(
                        'id'        => 'marketplace-slider-slidespeed',
                        'type'      => 'text',
                        'title'     => esc_html__('Slide speed', 'kiwi'),
                        'desc'      => esc_html__('In milliseconds. (numeric only)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '1000',
                    ),
					
					array(
                        'id'        => 'marketplace-slider-autoplay',
                        'type'      => 'select',
                        'title'     => esc_html__('Autoplay slider', 'kiwi'),
                        'options'   => array(
                            'true' => 'Yes', 
                            'false' => 'No'
                        ),
                        'default'   => 'false'
                    ),
					
					array(
                        'id'        => 'marketplace-slider-autospeed',
                        'type'      => 'text',
                        'title'     => esc_html__('Autoplay speed', 'kiwi'),
                       'desc'      => esc_html__('In milliseconds. (numeric only)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '2000',
                    ),
					
					
					array(
                        'id'        => 'marketplace-slider-infinite',
                        'type'      => 'select',
                        'title'     => esc_html__('Infinite loop sliding', 'kiwi'),
                        'options'   => array(
                            'true' => 'Yes', 
                            'false' => 'No'
                        ),
                        'default'   => 'true'
                    ),
					
					array(
                        'id'        => 'marketplace-slider-thumb-width',
                        'type'      => 'text',
                        'title'     => esc_html__('Thumbnail width', 'kiwi'),
                        'desc'      => esc_html__('In pixels. (numeric only)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '250',
                    ),
					
					
					array(
                        'id'        => 'marketplace-slider-thumb-height',
                        'type'      => 'text',
                        'title'     => esc_html__('Thumbnail height', 'kiwi'),
                        'desc'      => esc_html__('In pixels. (numeric only)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '100',
                    ),
					
					
					array(
                        'id'        => 'info-marketplace-description-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Item description :: Other', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'marketplace-hidepurchasebutton',
                        'type'      => 'switch',
                        'title'     => esc_html__('Purchase button option', 'kiwi'),
                        'desc'  	=> wp_kses(__('Disable the automatic output of the purchase button under item description.<br><small><strong>Note:</strong> This will override any setting, regardless of the setting of each item separately.</small>', 'kiwi'), $wpkses),
                        'default'   => '0',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),
					
					array(
                        'id'        => 'marketplace-container-itemdesc',
                        'type'      => 'switch',
                        'title'     => esc_html__('Full width container', 'kiwi'),
                        'desc'  => esc_html__('Removes the \'container\' and \'row\' classes.', 'kiwi'),
                        'default'   => '0',
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),
						
        )
    ) ;
	
	
	
		$this->sections[] = array(
        'title'      => esc_html__( 'Sidebar', 'kiwi' ),
        'id'         => 'marketplacesidebar',
        'subsection' => true,
		'fields'     => array(
		
					array(
                        'id'        => 'info-marketplace-sidebar-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Page :: Item description', 'kiwi')
                    ),	
					
					array(
                        'id'        => 'marketplace-sidebar-itemdesc-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                     //   'subtitle'  => esc_html__('No validation can be done on this field type', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-itemdesc-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                       // 'subtitle'  => esc_html__('No validation can be done on this field type', 'kiwi'),
                       // 'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        
                         //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-itemdesc-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
						'default'  => 'marketplace-sidebar-three',
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-itemdesc-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
							'default'  => 'marketplace-sidebar-two',
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-itemdesc-sidebarlayout',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select the position of the sidebar.', 'kiwi'),
                    //    'desc'      => esc_html__('This uses some of the built in images, you can use them for layout options.', 'kiwi'),
                        
                        //Must provide key => value(array:title|img) pairs for radio options
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					
					
					
					
					
					array(
                        'id'        => 'info-marketplace-sidebar-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Archive pages (Category,Tags,Search)', 'kiwi')
                    ),	
					
					array(
                        'id'        => 'marketplace-sidebar-archive-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable sidebars', 'kiwi'),
                     //   'subtitle'  => esc_html__('No validation can be done on this field type', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-archive-number',
                        'type'      => 'radio',
                        'title'     => esc_html__('Number of sidebars', 'kiwi'),
                       // 'subtitle'  => esc_html__('No validation can be done on this field type', 'kiwi'),
                       // 'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        
                         //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => '1', 
                            '2' => '2',
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-archive-sidebarone',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 1', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
						'default'  => 'marketplace-sidebar',
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-archive-sidebartwo',
                        'type'      => 'select',
                        'data'      => 'sidebar',
                        'title'     => esc_html__('Select sidebar 2', 'kiwi'),
                        'subtitle'  => esc_html__('If applicable.', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
							'default'  => 'marketplace-sidebar-two',
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-archive-sidebarlayout',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'kiwi'),
                        'subtitle'  => esc_html__('Select the position of the sidebar.', 'kiwi'),
                    //    'desc'      => esc_html__('This uses some of the built in images, you can use them for layout options.', 'kiwi'),
                        
                        //Must provide key => value(array:title|img) pairs for radio options
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar right',   'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarone.png'),
                            '2' => array('alt' => 'Sidebar left',    'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebartwo.png'),
                            '3' => array('alt' => 'Content middle',  'img' => get_template_directory_uri() . '/framework/theme-options/images/sidebarthree.png'),
                        ), 
                        'default' => '1'
                    ),
					
					
					
					array(
                        'id'        => 'info-marketplace-layout',
                        'type'      => 'info',
                        'desc'      => esc_html__('Archive pages :: Layout option', 'kiwi')
                    ),
				
                    array(
                        'id'        => 'marketplace-sidebar-archive-layout',
                        'type'      => 'radio',
                        'title'     => esc_html__('Layout', 'kiwi'),
                     //   'subtitle'  => esc_html__('No validation can be done on this field type', 'kiwi'),
                     //   'desc'      => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
                        
                         //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'List view', 
                            '2' => 'Grid view', 
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-archive-gridcolumns',
                        'type'      => 'select',
                        'title'     => esc_html__('Grid columns', 'kiwi'),
                    //  'subtitle'  => esc_html__('This field only works if grid is selected.', 'kiwi'),
                        'desc'      => wp_kses(__('This field only works if <em>grid view</em> is selected.', 'kiwi'), $wpkses),
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => '2 columns', 
                            '2' => '3 columns', 
                            '3' => '4 columns'
                        ),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'marketplace-sidebar-archive-griddesign',
                        'type'      => 'select',
                        'title'     => esc_html__('Grid design', 'kiwi'),
                    //  'subtitle'  => esc_html__('This field only works if grid is selected.', 'kiwi'),
                        'desc'      => wp_kses(__('This field only works if <em>grid view</em> is selected.', 'kiwi'), $wpkses),
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => 'Style 1', 
                            '2' => 'Style 2',
							'3' => 'Style 3'
                        ),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'marketplace-sidebar-archive-postcount',
                        'type'      => 'text',
                        'title'     => esc_html__('Item number', 'kiwi'),
                        'subtitle'  => esc_html__('Number of items per page.', 'kiwi'),
                      //  'desc'      => esc_html__('Use this field to downsize your logo if applicable.', 'kiwi'),
                      //  'validate'  => 'numeric',
                        'default'   => '10',
                    ),
					
					
					
					array(
                        'id'        => 'marketplace-enable-equalheights',
                        'type'      => 'switch',
                        'title'     => esc_html__('Equal heights', 'kiwi'),
						'desc'  	=> esc_html__('Enable equal heights for all products inside a grid view.', 'kiwi'),
                        'default'   => 'off',
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),
					
					
						
        )
    ) ;
	

	
	
	$this->sections[] = array(
        'title'      => esc_html__( 'User Login Redirect', 'kiwi' ),
        'id'         => 'marketplaceloginredirect',
        'subsection' => true,
      //  'desc'       => esc_html__( 'Unfortunately, this section is temporarily disabled. ', 'kiwi' ) . '',
        'fields'     => array(
		
				array(
                        'id'        => 'info-marketplace-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Redirect :: User roles', 'kiwi')
                    ),	


				array(
                        'id'        => 'marketplace-enable-redirectroles',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable redirect', 'kiwi'),
                       'desc'  => esc_html__('This will enable the option to redirect a user to a custom page based on their role after logging in.', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),

				 array(
						'id' => 'marketplace-redirect-frontendvendor',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Frontend Vendor', 'kiwi' ),
						'desc' => wp_kses(__('Select page for Frontend Vendor.<br><small>Leave empty if you want to redirect vendors to the vendor dashboard page.</small>', 'kiwi'), $wpkses),
						'required'  => array('mp-extension-list','contains',array('1' => '1')),

						),

				array(
						'id' => 'marketplace-redirect-suspendedvendor',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Suspended Vendor', 'kiwi' ),
						'desc' => esc_html__( 'Select page for Suspended Vendor', 'kiwi' ),
						'required'  => array('mp-extension-list','contains',array('1' => '1')),
				),

				array(
						'id' => 'marketplace-redirect-pendingvendor',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Pending Vendor', 'kiwi' ),
						'required'  => array('mp-extension-list','contains',array('1' => '1')),
						'desc' => esc_html__( 'Select page for Pending Vendor', 'kiwi' ),
						),	

				array(
					'id' => 'marketplace-redirect-shopvendor',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Shop Vendor', 'kiwi' ),
				//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
					'desc' => esc_html__( 'Select page for Shop Vendor', 'kiwi' ),
					'required'  => array('mp-extension-list','contains',array('1' => '1')),	

					),
					
				array(
					'id' => 'marketplace-redirect-shopworker',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Shop Worker', 'kiwi' ),
				//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
					'desc' => esc_html__( 'Select page for Shop Worker', 'kiwi' ),
					),

				array(
					'id' => 'marketplace-redirect-shopaccountant',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Shop Accountant', 'kiwi' ),
				//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
					'desc' => esc_html__( 'Select page for Shop Accountant', 'kiwi' ),
					),	

				array(
					'id' => 'marketplace-redirect-shopmanager',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Shop Manager', 'kiwi' ),
				//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
					'desc' => esc_html__( 'Select page for Shop Manager', 'kiwi' ),
					),
					
					
					array(
					'id' => 'marketplace-redirect-subscriber',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Subscriber', 'kiwi' ),
				//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
					'desc' => esc_html__( 'Select page for Subscriber (or buyer)', 'kiwi' ),
					),
					
				
					
	
	
	 )
    ) ;
	
	$this->sections[] = array(
        'title'      => esc_html__( 'Login/Register links', 'kiwi' ),
        'id'         => 'marketplacemenulinks',
        'subsection' => true,
      'fields'     => array(
				
					array(
                        'id'        => 'info-marketplace-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Menu :: Login/Register links', 'kiwi')
                    ),	
					
						
				array(
                        'id'        => 'marketplace-enablelinks-topbar',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable links', 'kiwi'),
                       'desc'  => esc_html__('This will add register/login links in the topbar menu.', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),

				 array(
						'id' => 'marketplace-enablelinks-register',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Select registration page', 'kiwi' ),
					//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
						'desc' => wp_kses(__('You may wish to select a page that uses one of the following shortcodes:<br><span style="color:grey">[fes_registration_form] [fes_login_registration_form] [edd_register]</span><br><small>Leave blank to use WP built-in registration link.</small>', 'kiwi'), $wpkses),
						
						),

				array(
						'id' => 'marketplace-enablelinks-login',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Select login page', 'kiwi' ),
					//	'subtitle' => esc_html__( 'No validation can be done on this field type', 'kiwi' ),
						'desc' => wp_kses(__('You may wish to select a page that uses one of the following shortcodes:<br><span style="color:grey">[edd_login] [fes_login_registration_form]  [fes_login_form]</span><br><small>Leave blank to use WP built-in login link.</small>', 'kiwi'), $wpkses),
						),

				array(
						'id' => 'marketplace-enablelinks-dashboard',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Select dashboard page', 'kiwi' ),
						'subtitle' => esc_html__( 'For vendors', 'kiwi' ),
						'desc' => wp_kses(__('You may wish to select a page that uses the following shortcode:<br><span style="color:grey">[fes_vendor_dashboard]</span><br><small>Only works if FES plugin is installed and activated.</small>', 'kiwi'), $wpkses),
						'required'  => array('mp-extension-list','contains',array('1' => '1')),
						),	
						
				array(
						'id' => 'marketplace-enablelinks-dashboard-subscriber',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Select dashboard page', 'kiwi' ),
						'subtitle' => esc_html__( 'For buyers', 'kiwi' ),
						'desc' => wp_kses(__('You may wish to select a page that uses the following shortcode:<br><span style="color:grey">[edd_customer_dashboard]</span>', 'kiwi'), $wpkses),
						
				),

				array(
					'id'        => 'marketplace-enablelinks-username',
					'type'      => 'switch',
					'title'     => esc_html__('Enable Username', 'kiwi'),
					'desc'  	=> esc_html__('This will add "Hello, [username]"', 'kiwi'),
					'default'   => '2',
					'on'        => 'Enable',
					'off'       => 'Disable',
				),
				
				
				
			/* 	array(
					'id'        => 'info-marketplace-lostpassword',
					'type'      => 'info',
					'desc'      => esc_html__('Lost Password / Reset Password pages', 'kiwi')
				),	
					
					
				array(
					'id' => 'marketplace-link-lost-password',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Select Lost Password page', 'kiwi' ),
					//'subtitle' => esc_html__( 'For buyers', 'kiwi' ),
					'desc' => wp_kses(__('You may wish to select a page that uses the following shortcode:<br><span style="color:grey">[edd_lost_password]</span>', 'kiwi'), $wpkses),
					
				),
				
				array(
					'id' => 'marketplace-link-reset-password',
					'type' => 'select',
					'data' => 'pages',
					'title' => esc_html__( 'Select Password Reset page', 'kiwi' ),
					//'subtitle' => esc_html__( 'For buyers', 'kiwi' ),
					'desc' => wp_kses(__('You may wish to select a page that uses the following shortcode:<br><span style="color:grey">[edd_reset_password]</span>', 'kiwi'), $wpkses),	
				),  */
					
					
					
					
					
					
					
		
		
	 )
    );
	
	
	$this->sections[] = array(
        'title'      => esc_html__( 'User role menu', 'kiwi' ),
        'id'         => 'marketplacemenulinks',
        'subsection' => true,
		'fields'     => array(
		
		
					array(
                        'id'        => 'marketplace-enable-userroles',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable menu based on user roles', 'kiwi'),
                        'desc'  => esc_html__('Display different topbar navigation menus based on different users roles.', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),
					
					array(
                        'id'        => 'marketplace-empty-userroles',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blank menus', 'kiwi'),
                        'desc'  	=> wp_kses(__('Inside these user role menus <em>Registration/Dashboard/Logout</em> links are added by default due to the settings in the <em>Login/Register links</em> tab. By enabling this option, they will be removed.', 'kiwi'), $wpkses),
						'default'   => '1',
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),
					
					
					array(
						'id' => 'marketplace-userrole-wallet',
						'type' => 'select',
						'data' => 'pages',
						'title' => esc_html__( 'Select deposit page', 'kiwi' ),
						'desc' 	=> wp_kses(__('You may wish to select a page that uses the following shortcode:<br><span style="color:grey">[edd_deposit]</span><br><small>Only works if the EDD Wallet extension is installed and activated.</small>', 'kiwi'), $wpkses),
						'required'  => array('mp-extension-list','contains',array('3' => '1')),	
						),
						
					
					array(
                        'id'        => 'marketplace-position-topbar-icon',
                        'type'      => 'switch',
                        'title'     => esc_html__('Icon position', 'kiwi'),
                        'desc' 		=> esc_html__('Choose which side these icons should be added inside the topbar menu.', 'kiwi'),
                        'default'   => '0',
                        'on'        => 'Right',
                        'off'       => 'Left',
                    ),					
		
					array(
                        'id'        => 'info-marketplace-userrole-vendorlogged',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar Menu :: Logged in vendors', 'kiwi'),
						'required'  => array('mp-extension-list','contains',array('1' => '1')),
                    ),	
					
					array(
						'id'      => 'marketplace-userrole-blocks-vendorlogged',
						'type'    => 'sorter',
						'title'   => 'Menu Layout Manager',
						'desc'    => 'Organize the layout of your menu.',
						'options' => array(
							'enabled'  => array(								
								'cart'    			=> 'Cart',
								'dashboard'			=> 'Dashboard',								
								'purchases'			=> 'Purchases',
								'username'			=> 'Username',
								'wishlist'			=> 'Wishlist',
																
							),
							'disabled' => array(
								'searching'    		=> 'Full Screen Search',
							)
						),
						'required'  => array('mp-extension-list','contains',array('1' => '1')),
					),
					
					array(
                        'id'        => 'info-marketplace-userrole-userlogged',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar Menu :: Logged in users', 'kiwi')
                    ),
					
					
					array(
						'id'      => 'marketplace-userrole-blocks-buyerlogged',
						'type'    => 'sorter',
						'title'   => 'Menu Layout Manager',
						'desc'    => 'Organize the layout of your menu.',
						'options' => array(
							'enabled'  => array(								
								'cart'    		=> 'Cart',
								'purchases'		=> 'Purchases',
								'username'		=> 'Username',
								'wishlist'		=> 'Wishlist',
																
							),
							'disabled' => array(
								'searching'    	=> 'Full Screen Search',
							)
						),
					),
					
					array(
                        'id'        => 'info-marketplace-userrole-nonlogged',
                        'type'      => 'info',
                        'desc'      => esc_html__('Topbar Menu :: Non-logged users', 'kiwi')
                    ),
					
					array(
						'id'      => 'marketplace-userrole-blocks-nonlogged',
						'type'    => 'sorter',
						'title'   => 'Menu Layout Manager',
						'desc'    => 'Organize the layout of your menu.',
						'options' => array(
							'enabled'  => array(								
								'cart'    		=> 'Cart',
								'login'    		=> 'Login',
								'register'    	=> 'Registration',
										
							),
							'disabled' => array(
								'searching'    	=> 'Full Screen Search',
							)
						),
					),
					
					array(
                        'id'        => 'info-marketplace-userrole-dropdown-settings',
                        'type'      => 'info',
                        'desc'      => esc_html__('Dropdown menu :: Settings', 'kiwi')
                    ),	
					
					array(
                        'id'        => 'marketplace-userrole-purchase-limit',
                        'type'      => 'text',
                        'title'     => esc_html__('Purchases', 'kiwi'),
                    //    'subtitle'  => esc_html__('This must be numeric.', 'kiwi'),
                        'desc'      => esc_html__('Max number of downloads to show inside the purchase dropdown menu. ', 'kiwi'),
                    //  'validate'  => 'no_special_chars',
                        'default'   => '6',
                    ),	
					
					array(
                        'id'        => 'marketplace-userrole-wishlist-limit',
                        'type'      => 'text',
                        'title'     => esc_html__('Wishlist', 'kiwi'),
                        'desc'      => esc_html__('Max number of wishlists to show inside the wishlist dropdown menu. ', 'kiwi'),
                        'default'   => '6',
						'required'  => array('mp-extension-list','contains',array('2' => '1')),
                    ),
					
					array(
                        'id'        => 'info-marketplace-userrole-dropdown-mainnav',
                        'type'      => 'info',
                        'desc'      => esc_html__('Others', 'kiwi')
                    ),
					
					array(
                        'id'        => 'marketplace-enable-userroles-main',
                        'type'      => 'switch',
                        'title'     => esc_html__('Main navigation', 'kiwi'),
                        'desc'  => esc_html__('Enable these options above inside the main navigation.', 'kiwi'),
                        'default'   => '0',
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),
					
				
		
		
	 )
    );
	
	$this->sections[] = array(
        'title'      => esc_html__( 'Notification bar', 'kiwi' ),
        'id'         => 'marketplacenotificationbar',
        'subsection' => true,
      'fields'     => array(
					
					array(
                        'id'        => 'notificationbar-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable notification bar', 'kiwi'),
						'desc'  	=> wp_kses(__('This will be placed on top of every page.<br><br> A cookie will be created to save the user\'s preferences. This cookie will automatically expire in 14 days.<br><br>If this option is disabled and the user visit your website again, the cookie will be removed before the expiration date.', 'kiwi'), $wpkses),
						'default'   => '0',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),	
					
					array(
                        'id'        => 'info-marketplace-notificationbar',
                        'type'      => 'info',
                        'desc'      => esc_html__('Notification bar :: Options', 'kiwi'),
						'required' => array('notificationbar-enable','=','1'),
                    ),
					
					array(
                        'id'        => 'notificationbar-customtext',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Notification bar custom text', 'kiwi'),
						'default'  =>  __('This marketplace is a demo. No actual themes or any other products are being sold here.<br>All names, images, prices and descriptions are used solely for exemplary purposes only.', 'kiwi'),	
						'desc'      => esc_html__('Best used inside "Text" view rather than "Visual" view.', 'kiwi'),
						'required' => array('notificationbar-enable','=','1'),
					),
					
					array(
                        'id'        => 'notificationbar-customtext-margin',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom text padding', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '15px 15px 15px 15px',
						'required' => array('notificationbar-enable','=','1'),
                    ),	
					
					
					array(
                        'id'        => 'info-notificationbar-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Notification bar :: Typography', 'kiwi'),
						'required' => array('notificationbar-enable','=','1'),
                    ),
					
					
					array(
                        'id'        => 'notificationbar-typography',
                        'type'      => 'typography',
                        'title'     => esc_html__('Font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify font properties.', 'kiwi'),
						'output'   	=> array('.notificationbar'),
                        'google'    => true,
					//	'line-height'    => false,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#ffffff',
                            'font-size'     => '16px',
                            'font-family'   => 'Roboto',
                            'font-weight'   => 'Normal',
							'line-height'   => 'Normal',
							'text-align'   =>  'center',
                        ),
						'required' => array('notificationbar-enable','=','1'),
                    ),
					

					array(
                        'id'        => 'info-notificationbar-five',
                        'type'      => 'info',
                        'desc'      => esc_html__('Notification bar :: Styling', 'kiwi'),
						'required' => array('notificationbar-enable','=','1'),
                    ),
					
					

					array(
                        'id'        => 'notificationbar-styling',
                        'type'      => 'background',
                        'output'    => array('.notificationbar'),
                        'title'     => esc_html__('Background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background.', 'kiwi'),
						'default'  => array(
											'background-color' => '#00a1cb',
											),
						'preview'	=> true,
						'required' => array('notificationbar-enable','=','1'),				
                    ),
					
					 array(
                        'id'        => 'notificationbar-enableborder',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable borders', 'kiwi'),
                        'default'   => '0',
						'required' => array('notificationbar-enable','=','1'),
                    ),
					
					array(
                        'id'       => 'notificationbar-topborder',
						'type'     => 'border',
						'title'    => esc_html__('Top border', 'kiwi'),
						//'subtitle' => esc_html__('Select top border color.', 'kiwi'),
						'output'   => array('.topbar'),
						// 'desc'     => esc_html__('This is the description field, again good for additional info.', 'kiwi'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => true,
						'bottom'   => false,
						//'style'    => false,
						'default'  => array(
							//'border-color'  => '#e4e4e4',
							//'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0'
						),
						'required' => array( 
									array('notificationbar-enableborder','=','1'), 
									array('notificationbar-enable','=','1'),										
								),							
                    ),
					
					array(
                        'id'       => 'notificationbar-bottomborder',
						'type'     => 'border',
						'title'    => esc_html__('Bottom border', 'kiwi'),
						'output'   => array('.topbar'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => false,
						'bottom'   => true,
						//'style'    => false,
						'default'  => array(
						//'border-color'  => '#e4e4e4',
						//	'border-style'  => 'solid',
							'border-top'    => '0',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0'
						),
						'required' => array( 
									array('notificationbar-enableborder','=','1'), 
									array('notificationbar-enable','=','1'),										
								),						
                    ),
						
        )
    ) ;
	 

   $this->sections[] = array(
        'title' => esc_html__( 'Pages', 'kiwi' ),
        'id'    => 'pages',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-adjust-alt',
		'fields'     => array(	
            array(
                        'id'        => 'page-comment-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable comments on pages', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide comments on pages.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'page-pagetitle-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title on pages', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide title on pages.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					
					array(
                        'id'        => 'page-pagetitle-archive-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title on archive pages', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide title on archive pages.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'page-pagetitle-category-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title on category archive pages', 'kiwi'),
                       'desc'      => esc_html__('Uncheck to hide title on category pages.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'page-pagetitle-tag-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title on tag archive pages', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide title on tag pages.', 'kiwi'),
                        'default'   => '0'
                    ),

					array(
                        'id'        => 'page-pagetitle-index-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title on index page.', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide title on index page.', 'kiwi'),
                        'default'   => '0'
                    ),	

					array(
                        'id'        => 'page-pagetitle-author-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable page title on author archive pages', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide title on archive pages.', 'kiwi'),
                        'default'   => '0'
                    ),	
        )
    ) ;



$this->sections[] = array(
        'title' => esc_html__( 'Blog', 'kiwi' ),
        'id'    => 'blog',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-check',
		'fields'     => array(	
            array(
                        'id'        => 'blog-editpostlink-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Edit post link', 'kiwi'),
                        'desc' 		=> esc_html__('Enable/disable the "Edit" link on the front end when logged in.', 'kiwi'),
                        'default'   => '2',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),
					
				array(
                        'id'        => 'blog-editpostlink-enable-single',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Disable edit link', 'kiwi'),
                        'desc'      => esc_html__('Check to hide edit link on single post and pages.', 'kiwi'),
                        'default'   => '1'
                    ),

				array(
                        'id'        => 'blog-postpreview-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Post Preview', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide next/previous post preview on single post.', 'kiwi'),
                        'default'   => '1'
                    ),

				array(
                        'id'        => 'blog-authorbio-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Author\'s bio', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide author\'s bio on single post.', 'kiwi'),
                        'default'   => '1'
                    ),		
					
				array(
                        'id'        => 'post-hidetitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Post title', 'kiwi'),
                        'desc'      => esc_html__('Uncheck to hide post title on single post.', 'kiwi'),
                        'default'   => '1'
                    ),		
				
				array(
                        'id'        => 'info-blog-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Excerpt', 'kiwi')
                    ),
				
				array(
                        'id'        => 'blog-excerptlength',
                        'type'      => 'text',
                        'title'     => esc_html__('Excerpt length', 'kiwi'),
                        'subtitle'  => esc_html__('(by words)', 'kiwi'),
                        'validate'  => 'numeric',
                        'default'   => '20',
                    ),
					
				array(
                        'id'        => 'blog-excerptending',
                        'type'      => 'text',
                        'title'     => esc_html__('Excerpt ending', 'kiwi'),
                        'default'   => '...',
                    ),

				array(
                        'id'        => 'blog-readmore',
                        'type'      => 'text',
                        'title'     => esc_html__('Read more text', 'kiwi'),
                        'subtitle'  => wp_kses(__('Customize the <strong>read more</strong> text.', 'kiwi'), $wpkses),
                        'default'   => 'View full post &raquo;',
                    ),	
				
				
				array(
                        'id'        => 'info-blog-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Layout option', 'kiwi')
                    ),
				
                    array(
                        'id'        => 'postlayout-bloglayout',
                        'type'      => 'radio',
                        'title'     => esc_html__('Blog layout', 'kiwi'),
                        'options'   => array(
                            '1' => 'List view', 
                            '2' => 'Grid view', 
                        ),
                        'default'   => '2'
                    ),
					
					array(
                        'id'        => 'postlayout-liststyle',
                        'type'      => 'select',
                        'title'     => esc_html__('List style', 'kiwi'),
                       // 'subtitle'  => wp_kses(__('This field only works if <em>list</em> is selected.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Style 1', 
                        ),
                        'default'   => '1',
						'required' => array('postlayout-bloglayout','=','1'),	

                    ),
					
					array(
                        'id'        => 'postlayout-gridcolumns',
                        'type'      => 'select',
                        'title'     => esc_html__('Grid columns', 'kiwi'),
                       // 'subtitle'  => wp_kses(__('This field only works if <em>grid</em> is selected.', 'kiwi'), $wpkses),
						'options'   => array(
                            '1' => '2 columns', 
                            '2' => '3 columns', 
                            '3' => '4 columns'
                        ),
                        'default'   => '2',
						'required' => array('postlayout-bloglayout','=','2'),
                    ),
					
					
					
					array(
                        'id'        => 'postfomat-shadow',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable box shadow', 'kiwi'),
						'desc'      => wp_kses(__('This field only works if <em>grid view</em> is selected.', 'kiwi'), $wpkses),
                        'default'   => '1',
						'required' => array('postlayout-bloglayout','=','2'),
                    ),
        ),
    ) ;

		$this->sections[] = array(
        'title' => esc_html__( 'Post format', 'kiwi' ),
        'id'    => 'postformat',
        'icon'  => 'el el-screenshot',
		'fields'     => array(	
            array(
                        'id'        => 'info-postformat-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Standard', 'kiwi')
                    ),

				array(
                        'id'        => 'postformat-standardtitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-standarddate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-standardmeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),	
					
					array(
                        'id'        => 'postformat-standardexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-standardcontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '0'
                    ),
					
				
				array(
                        'id'        => 'info-postformat-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Aside', 'kiwi')
                    ),
					
				array(
                        'id'        => 'postformat-asidetitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
				array(
                        'id'        => 'postformat-asidedate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '0'
                    ),	
					
				array(
                        'id'        => 'postformat-asidemeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '0'
                    ),


					
				array(
                        'id'        => 'info-postformat-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Audio', 'kiwi')
                    ),	
					
					
				array(
                        'id'        => 'postformat-audiotitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-audiodate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-audiometa',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),	
					
					array(
                        'id'        => 'postformat-audioexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-audiocontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '0'
                    ),
					
				array(
                        'id'        => 'info-postformat-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Chat', 'kiwi')
                    ),
					
				array(
                        'id'        => 'postformat-chattitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-chatdate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-chatmeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-chatexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '0'
                    ),
					
					
					array(
                        'id'        => 'postformat-chatcontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '1'
                    ),

				array(
                        'id'        => 'info-postformat-five',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Gallery', 'kiwi')
                    ),
				
				
				array(
                        'id'        => 'postformat-gallerytitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-gallerydate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-gallerymeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-galleryexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-gallerycontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '0'
                    ),
				
				
				
				array(
                        'id'        => 'info-postformat-six',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Image', 'kiwi')
                    ),
					
				array(
                        'id'        => 'postformat-imagetitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-imagedate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-imagemeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),	
					
					array(
                        'id'        => 'postformat-imageexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-imagecontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '0'
                    ),

					
				array(
                        'id'        => 'info-postformat-seven',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Link', 'kiwi')
                    ),	
					
					array(
                        'id'        => 'postformat-linkbgcolor',
                        'type'      => 'background',
                        'output'    => array('.format-link .link-header'),
                        'title'     => esc_html__('Background color', 'kiwi'),
						'desc' 		=> 'If no featured image is selected, this bgcolor will be used instead.',
						'default'  => array(
											'background-color' => '#2a2c33',
										),						
                        'background-repeat'  => 'false',
						'background-attachment'  => 'false',
						'background-position'  => 'false',
						'background-image'  => 'false',
						'background-size'  => 'false',
						'preview'  => 'false',
						
                    ),

					array(
                        'id'        => 'postformat-linktitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-linkdate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-linkmeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-linkexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-linkcontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '0'
                    ),
					
				array(
                        'id'        => 'info-postformat-eight',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Status', 'kiwi')
                    ),
					
					
				array(
                        'id'        => 'postformat-statusmeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '0'
                    ),	
					
				array(
                        'id'        => 'postformat-statusdate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),	
					
					
				
				array(
                        'id'        => 'info-postformat-nine',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Video', 'kiwi')
                    ),
				
				array(
                        'id'        => 'postformat-videotitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-videodate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-videometa',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-videoexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'postformat-videocontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable full content', 'kiwi'),
                        'default'   => '0'
                    ),
					
				
				array(
                        'id'        => 'info-postformat-ten',
                        'type'      => 'info',
                        'desc'      => esc_html__('Post format :: Quote', 'kiwi')
                    ),
				
                    array(
                        'id'        => 'postformat-quotebgcolor',
                        'type'      => 'background',
                        'output'    => array('.format-quote .featured-image'),
                        'title'     => esc_html__('Background color', 'kiwi'),
						'desc' 		=> 'If no featured image is selected, this bgcolor will be used instead.',
						'default'  => array(
											'background-color' => '#fa5252',
										),						
                        'background-repeat'  => 'false',
						'background-attachment'  => 'false',
						'background-position'  => 'false',
						'background-image'  => 'false',
						'background-size'  => 'false',
						'preview'  => 'false',
						
                    ),
					
					array(
                        'id'        => 'postformat-quotetitle',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post title', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-quotedate',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-quotedateblockquote',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable date format inside blockquote', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'postformat-quotemeta',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-quotemetablockquote',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable post meta data inside blockquote', 'kiwi'),
                        'default'   => '0'
                    ),
					
					
					
					array(
                        'id'        => 'postformat-quoteexcerpt',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable excerpt', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'postformat-quotecontent',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable content', 'kiwi'),
                        'default'   => '0'
                    ),


        )
    ) ;

	$this->sections[] = array(
        'title' => esc_html__( 'Typography', 'kiwi' ),
        'id'    => 'typography',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-font',
		'fields'     => array(	
            array(
                        'id'        => 'typography-body',
                        'type'      => 'typography',
                        'title'     => esc_html__('Body font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the body font properties.', 'kiwi'),
                        'google'    => true,
						'output'   	=> array('body'),
					//	'line-height'    => false,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#333',
                            'font-size'     => '14px',
                            'font-family'   => 'Arial,Helvetica,sans-serif',
                            'font-weight'   => 'Normal',
							//'line-height'   => 'Normal',
                        ),
                    ),					
					
					
					
					array(
                        'id'        => 'typography-header',
                        'type'      => 'typography',
                        'title'     => esc_html__('Header font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the header font properties.', 'kiwi'),
						'output'   	=> array('.mainnav'),
                        'google'    => true,
					//	'line-height'    => false,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#333',
                            'font-size'     => '14px',
                            'font-family'   => 'sans-serif',
                            'font-weight'   => 'Normal',
							'line-height'   => 'Normal',
                        ),
                    ),
					
					
					array(
                        'id'        => 'typography-navigation',
                        'type'      => 'typography',
                        'title'     => esc_html__('Navigation font family', 'kiwi'),
						'output'    => array('.kiwi-nav li a'),
                        'subtitle'  => esc_html__('Specify the navigation font properties.', 'kiwi'),
                        'google'    => true,
						'text-transform'    => true,
						'line-height'    => false,
                        'default'   => array(
                            'color'         => '#888',
                            'font-size'     => '13px',
                            'font-family'   => 'Open sans,sans-serif',
							'text-transform' => 'inherit',
							'font-weight' => '700'
                        ),
                    ),
					
					
					
					array(
                        'id'        => 'typography-footer',
                        'type'      => 'typography',
                        'title'     => esc_html__('Footer font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the footer font properties.', 'kiwi'),
						'output'   	=> array('.footer'),
                        'google'    => true,
					//	'line-height'    => false,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#909090',
                            'font-size'     => '14px',
                            'font-family'   => 'Arial,Helvetica,sans-serif',
                            'font-weight'   => 'Normal',
						//	'line-height'   => 'Normal',
                        ),
                    ),
					
					
					
					
					array(
                        'id'        => 'section-headings-one',
                        'type'      => 'section',
                        'title'     => esc_html__('Headings', 'kiwi'),
                     //   'subtitle'  => esc_html__('With the "section" field you can create indent option sections.', 'kiwi'),
                        'indent'    => true, // Indent all options below until the next 'section' option is set.                     
                    ),
					
					array(
                        'id'        => 'typography-headings',
                        'type'      => 'typography',
                        'title'     => esc_html__('Headings font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the headings font properties for body, topbar, and header.', 'kiwi'),
						'output'   	=> array('h1,h2,h3,h4,h5,h6'),
                        'google'    => true,
						'subsets'   => false,
						'font-size'   => false,
						'line-height'   => false,
                        'default'   => array(
                            'color'         => '#333',
                            'font-family'   => 'Roboto',
                            'font-weight'   => 'Normal'
                        ),
                    ),
					
					
					
					array(
                        'id'        => 'typography-headings-footer',
                        'type'      => 'typography',
                        'title'     => esc_html__('Headings font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify the headings font properties for footer.', 'kiwi'),
						'output'   		 => array('.footer h1,.footer h2,.footer h3,.footer h4,.footer h5,.footer h6'),
                        'google'    => true,
						'subsets'   => false,
						'font-size'   => false,
						'line-height'   => false,
                        'default'   => array(
                            'color'         => '#909090',
                            'font-family'   => 'Roboto Condensed',
                            'font-weight'   => 'Normal'
                        ),
                    ),
					
					array(
                        'id'            => 'typography-headings-h1',
                        'type'          => 'slider',
                        'title'         => esc_html__('H1 font size', 'kiwi'),
                        'subtitle'      => esc_html__('In pixels, default is 36', 'kiwi'),
                     //   'desc'          => esc_html__('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'kiwi'),
						'output'   		 => array('h1,.h1'),
                        'default'       => 36,
                        'min'           => 0,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'
                    ),
					
					array(
                        'id'            => 'typography-headings-h2',
                        'type'          => 'slider',
                        'title'         => esc_html__('H2 font size', 'kiwi'),
                        'subtitle'      => esc_html__('In pixels, default is 30', 'kiwi'),
                     //   'desc'          => esc_html__('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'kiwi'),
						'output'   		 => array('h2,.h2'),
                        'default'       => 30,
                        'min'           => 0,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'
                    ),
					
					array(
                        'id'            => 'typography-headings-h3',
                        'type'          => 'slider',
                        'title'         => esc_html__('H3 font size', 'kiwi'),
                        'subtitle'      => esc_html__('In pixels, default is 24', 'kiwi'),
                     //   'desc'          => esc_html__('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'kiwi'),
						'output'   		 => array('h3,.h3'),
                        'default'       => 24,
                        'min'           => 0,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'
                    ),
					
					array(
                        'id'            => 'typography-headings-h4',
                        'type'          => 'slider',
                        'title'         => esc_html__('H4 font size', 'kiwi'),
                        'subtitle'      => esc_html__('In pixels, default is 18', 'kiwi'),
                     //   'desc'          => esc_html__('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'kiwi'),
						'output'   		 => array('h4,.h4'),
                        'default'       => 18,
                        'min'           => 0,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'
                    ),
					
					array(
                        'id'            => 'typography-headings-h5',
                        'type'          => 'slider',
                        'title'         => esc_html__('H5 font size', 'kiwi'),
                        'subtitle'      => esc_html__('In pixels, default is 14', 'kiwi'),
                     //   'desc'          => esc_html__('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'kiwi'),
						'output'   		 => array('h5,.h5'),
                        'default'       => 14,
                        'min'           => 0,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'
                    ),
					
					array(
                        'id'            => 'typography-headings-h6',
                        'type'          => 'slider',
                        'title'         => esc_html__('H6 font size', 'kiwi'),
                        'subtitle'      => esc_html__('In pixels, default is 12', 'kiwi'),
                     //   'desc'          => esc_html__('Slider description. Min: 0, max: 300, step: 5, default value: 75', 'kiwi'),
						'output'   		 => array('h6,.h6'),
                        'default'       => 12,
                        'min'           => 0,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'
                    ),
					
                    array(
                        'id'        => 'section-headings-one-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('section-media-checkbox', "=", 1),
                    ),
        ),
    ) ;
	
	
	$this->sections[] = array(
        'title' => esc_html__( 'Styling', 'kiwi' ),
        'id'    => 'styling',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-tint',
		'fields'     => array(	
					array(
                        'id'        => 'info-styling-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Background', 'kiwi')
                    ),

					array(
                        'id'        => 'styling-body',
                        'type'      => 'background',
                        'output'    => array('body'),
                        'title'     => esc_html__('Body background color', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select a background color for the <strong>full width</strong> layout.', 'kiwi'), $wpkses),
						 'default'  => array(
											'background-color' => '#eeeeee',
											),
						'preview'	=> true,
						'required' => array('settings-sitelayout','=','1'),	
                    ),
										
					 array(
                        'id'        => 'styling-body-boxed',
                        'type'      => 'background',
                        'output'    => array('body.boxed'),
                        'title'     => esc_html__('Body Background color', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select a background color or image for the <strong>boxed</strong> layout.', 'kiwi'), $wpkses),
                        'default'  => array(
											'background-color' => '#ffffff',
											),
						'preview'	=> true,
						'required' => array('settings-sitelayout','=','2'),	
                    ),	
					
					array(
                        'id'        => 'info-styling-header-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Header', 'kiwi')
                    ),
					
					array(
                        'id'        => 'styling-mainnav-rowone',
                        'type'      => 'background',
                        'output'    => array('.mainnav .header-row-one'),
                        'title'     => esc_html__('Header background color', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select a background color for the header\'s <strong>first</strong> row.', 'kiwi'), $wpkses),
						'default'  => array('background-color' => '#ffffff',),
						'preview'	=> true					
                    ),
					
					array(
                        'id'        => 'styling-mainnav-rowtwo',
                        'type'      => 'background',
                        'output'    => array('.mainnav .header-row-two'),
                        'title'     => esc_html__('Header background color', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select a background color for the header\'s <strong>second</strong> row.', 'kiwi'), $wpkses),
						'default'  => array('background-color' => '#ffffff',),
						'preview'	=> true					
                    ),
					
					
					 
					array(
                        'id'        => 'info-styling-footer-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Header', 'kiwi')
                    ),
					array(
                        'id'        => 'styling-footer',
                        'type'      => 'background',
                        'output'    => array('.footer'),
                        'title'     => esc_html__('Footer background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color.', 'kiwi'),
                        'default'  => array('background-color' => '#1b1b1b',),
                        'preview'	=> true
                    ),
					
					array(
                        'id'        => 'styling-copyright',
                        'type'      => 'background',
                        'output'    => array('.copyright'),
                        'title'     => esc_html__('Copyright background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color.', 'kiwi'),
						'default'  => array('background-color' => '#1b1b1b',),
                        'preview'	=> true
                    ),
					
					
					array(
                        'id'        => 'info-styling-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Links', 'kiwi')
                    ),
					
					
					
					array(
                        'id'        => 'styling-links',
                        'type'      => 'link_color',
                        'title'     => esc_html__('Links Color Option', 'kiwi'),
                        'visited'   => true, 
                        'default'   => array(
                            'regular'   => '#18a9c4',                            
                            'active'    => '#18a9c4',
							'visited'   => '#18a9c4',
							'hover'     => '#fa5252',
                        ),
						
                    ),
					
					array(
                        'id'        => 'styling-links-footer',
                        'type'      => 'link_color',
                        'title'     => esc_html__('Links Color Option', 'kiwi'),
                        'subtitle'  => esc_html__('Footer area', 'kiwi'),
                        'visited'   => true, 
                        'default'   => array(
                            'regular'   => '#bababa',                            
                            'active'    => '#bababa',
							'visited'   => '#bababa',
							'hover'     => '#fa5252',
                        ),
						'output'    => array('.footer a, .footer a:visited, .footer .widget-mk-kiwi h3 a,.footer .widget_recent_entries ul li .post-widget-text a'), // An array of CSS selectors
                    ),
					
					
					array(
                        'id'        => 'styling-linktransition',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable link transition', 'kiwi'),
                        'desc'  	=> esc_html__('Enable this if you want a smooth fade in/out effect.', 'kiwi'),
                        'default'   => '1'
                    ),
					
					
					array(
                        'id'        => 'info-styling-menu',
                        'type'      => 'info',
                        'desc'      => esc_html__('Menu', 'kiwi')
                    ),
					
					array(
                        'id'       => 'styling-menu-dropdown-bg',
						'type'     => 'color',
						'title'    => esc_html__('Dropdown color', 'kiwi'), 
						'desc' 		=> esc_html__('Select a background color for dropdown links.', 'kiwi'),
						'default'  => '#ffffff',
						'validate' => 'color',
                    ),
					
					array(
                        'id'       => 'styling-menu-dropdown-text',
						'type'     => 'color',
						'title'    => esc_html__('Text color', 'kiwi'), 
						//'desc' => esc_html__('Select a background color for dropdown links upon mouse-hover.', 'kiwi'),
						'default'  => '#888888',
						'validate' => 'color',
                    ),
					
					array(
                        'id'       => 'styling-menu-dropdown-bg-hover',
						'type'     => 'color',
						'title'    => esc_html__('Dropdown hover background color', 'kiwi'), 
						'desc' => esc_html__('Select a background color for dropdown links upon mouse-hover.', 'kiwi'),
						'default'  => '#f5f5f5',
						'validate' => 'color',
                    ),
					
					array(
                        'id'       => 'styling-menu-dropdown-text-hover',
						'type'     => 'color',
						'title'    => esc_html__('Text color upon hover', 'kiwi'), 
						//'desc' => esc_html__('Select a background color for dropdown links upon mouse-hover.', 'kiwi'),
						'default'  => '#18a9c4',
						'validate' => 'color',
                    ),
					
					
					array(
                        'id'        => 'info-styling-three',
                        'type'      => 'info',
                        'desc'      => esc_html__('Page template :: Blog', 'kiwi')
                    ),
					
					
					array(
                        'id'        => 'styling-template-blog',
                        'type'      => 'background',
                        'output'    => array('body.page-template-page-blog'),
                        'title'     => esc_html__('Body Background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color or image for the blog page template.', 'kiwi'),
                        'default'  => array('background-color' => '#ffffff',),
						'preview'	=> true
                    ), 
					
					array(
                        'id'        => 'info-styling-four',
                        'type'      => 'info',
                        'desc'      => esc_html__('Search form', 'kiwi')
                    ),
					
					array(
                        'id'        => 'styling-searchform',
                        'type'      => 'background',
                        'output'    => array('#search-form,.search-form button'),
                        'title'     => esc_html__('Search Background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color or image for the search form.', 'kiwi'),
                        'default'  => array('background-color' => '#e5e5e5',),
						'preview'	=> true
                    ), 
					
					
					array(
                        'id'        => 'typography-searchform',
                        'type'      => 'typography',
                        'title'     => esc_html__('Search form font family', 'kiwi'),
                        'subtitle'  => esc_html__('Specify Search form font properties.', 'kiwi'),
						'output'   	=> array('.form-control,.search-form input[placeholder]'),
                        'google'    => true,
						'subsets'   => false,
                        'default'   => array(
                            'color'         => '#666666',
                            'font-size'     => '12px',
                            'font-family'   => 'Arial,Helvetica,sans-serif',
                            'font-weight'   => '700',
						//	'line-height'   => 'Normal',
                        ),
                    ),
					
					array(
                        'id'        => 'info-marketplace-description-styling',
                        'type'      => 'info',
                        'desc'      => esc_html__('Demo button :: Item description page', 'kiwi')
                    ),
					
					array(
                        'id'        => 'styling-demo-button',
                        'type'      => 'background',
                        'output'    => array('.mp-social-buttons .demo a, .widget_marketplace_item_demolink a'),
                        'title'     => esc_html__('Demo background color', 'kiwi'),
                        'subtitle'  => esc_html__('Select a background color for the demo button.', 'kiwi'),
                        'default'  	=> array('background-color' => '#86bd3e',),
                        'preview'	=> true
                    ),
					
					array(
                        'id'       	=> 'styling-demo-button-hover',
						'type'     	=> 'color',
						'output'    => array('.mp-social-buttons .demo:hover, .widget_marketplace_item_demolink a:hover'),
						'title'   	=> esc_html__('Demo hover background color', 'kiwi'), 
						'desc' 		=> esc_html__('Select a hover background color for the demo button.', 'kiwi'),
						'default'  	=> '#18a9c4',
						'validate' 	=> 'color',
                    ),
            
        )
    ) ;

	$this->sections[] = array(
        'title' => esc_html__( 'Footer', 'kiwi' ),
        'id'    => 'footer',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-screenshot',
		'fields'     => array(	
					
					array(
                        'id'        => 'footer-enable',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable footer widgets', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'footer-columns',
                        'type'      => 'select',
                        'title'     => esc_html__('Footer columns', 'kiwi'),
                        'subtitle'  => esc_html__('Select the number of columns to display in the footer.', 'kiwi'),
                        'options'   => array(
                            '1' => '1 column', 
                            '2' => '2 columns', 
                            '3' => '3 columns',
							'4' => '4 columns', 
                            '5' => '5 columns', 
                            '6' => '6 columns',
							'7' => 'Theme default (4)'
                        ),
                        'default'   => '7'
                    ),
					
					array(
                        'id'        => 'footer-padding',
                        'type'      => 'text',
                        'title'     => esc_html__('Footer margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '60px 0px 20px 0px',
                    ),	

					array(
                        'id'        => 'info-footer-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('Copyright', 'kiwi')
                    ),	
					
					array(
                        'id'        => 'footer-copyright-enable',
                        'type'      => 'switch',
                        'title'     => esc_html__('Copyright', 'kiwi'),
                       'subtitle'   => esc_html__('Enable to display the copyright bar.', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
                    ),
					
					array(
                        'id'        => 'footer-copyright-enableprefix',
                        'type'      => 'switch',
                        'title'     => esc_html__('Copyright Prefix', 'kiwi'),
                        'subtitle'  => esc_html__('&copy; [current-year] [site-title]', 'kiwi'),
                        'default'   => '1',
                        'on'        => 'Enable',
                        'off'       => 'Disable',
						'required' => array('footer-copyright-enable','=','1'),	
                    ),
					array(
                        'id'        => 'footer-copyright-message',
                        'type'      => 'editor',
						'validate'  => 'html',
                        'title'     => esc_html__('Custom copyright message', 'kiwi'),
						'default'   => esc_html__(' | All Rights Reserved.', 'kiwi'),
						'required'  => array('footer-copyright-enable','=','1'),
                    ),
					
					
					array(
                        'id'        => 'footer-copyright-message-margin',
                        'type'      => 'text',
                        'title'     => esc_html__('Copyright message margin', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '20px 10px 10px 0px',
						'required'  => array('footer-copyright-enable','=','1'),
                    ),
					
					
					
					 array(
                        'id'        => 'footer-copyright-enableborder',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable copyright border', 'kiwi'),
                        'default'   => '1',
						'required' => array('footer-copyright-enable','=','1'),
                    ),
					
					array(
                        'id'       => 'footer-copyright-topborder',
						'type'     => 'border',
						'title'    => esc_html__('Copyright top border', 'kiwi'),
						'subtitle' => esc_html__('Select top border color.', 'kiwi'),
						'output'   => array('.copyright .copyright-border'),
						'all'      => false,
						'left'     => false,
						'right'    => false,
						'top'      => true,
						'bottom'   => false,
						'default'  => array(
							'border-color'  => '#424242',
							'border-style'  => 'double',
							'border-top'    => '4px',
							'border-right'  => '0',
							'border-bottom' => '0',
							'border-left'   => '0'
						),
						'required' => array( 
							array('footer-copyright-enableborder',"=",'1'),
							array('footer-copyright-enableborder','=','1'),										
						),	
						
                    ),
					
					
					array(
                        'id'        => 'info-footer-two',
                        'type'      => 'info',
                        'desc'      => esc_html__('Footer :: Right block', 'kiwi')
                    ),					
					
					array(
                        'id'        => 'footer-contentright',
                        'type'      => 'select',
                        'title'     => esc_html__('Select Option', 'kiwi'),
                        'subtitle'  => wp_kses(__('Select which content to display in the <strong>right</strong> footer block.', 'kiwi'), $wpkses),
                        'options'   => array(
                            '1' => 'Footer navigation', 
                            '2' => 'Social media', 
                            '3' => 'Custom text', 
                            '4' => 'Leave empty', 
                            '5' => 'Marketplace stats'
                        ),
                        'default'   => '2'
                    ),
					
					array(
                        'id'        => 'footer-contentright-customtext',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Footer custom text', 'kiwi'),
                        'subtitle'  => esc_html__('Right block.', 'kiwi'),
                        'desc'      => esc_html__('This field only works when you selected "custom text".', 'kiwi'),
                        'placeholder'  => esc_html__('HTML is allowed in here.', 'kiwi'),
						'required'  => array('footer-contentright', "=",'3'),
                    ),
					
					array(
                        'id'        => 'footer-contentright-customtextmargin',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom margin', 'kiwi'),
                        'subtitle'  => esc_html__('Right block.', 'kiwi'),
                        'desc'      => esc_html__('top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi'),
                        'default'   => '20px 0px 10px 0px',
                    ),
        )
    ) ;
	
		$this->sections[] = array(
        'title' => esc_html__( 'Social Media', 'kiwi' ),
        'id'    => 'socialmedia',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-group',
		'fields'     => array(	
            array(
                        'id'        => 'socialmedia-facebook',
                        'type'      => 'text',
                        'title'     => esc_html__('Facebook', 'kiwi'),
                        'validate'  => 'url',
						'default'   => esc_html__('https://www.facebook.com/', 'kiwi'),
							'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-twitter',
                        'type'      => 'text',
                        'title'     => esc_html__('Twitter', 'kiwi'),
                        'validate'  => 'url',
                        'default'   => esc_html__('https://twitter.com/', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
							
                        )
                    ),
					array(
                        'id'        => 'socialmedia-google',
                        'type'      => 'text',
                        'title'     => esc_html__('Google+', 'kiwi'),
                        'validate'  => 'url',
						'default'   => esc_html__('https://plus.google.com/', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-pinterest',
                        'type'      => 'text',
                        'title'     => esc_html__('Pinterest', 'kiwi'),
                        'validate'  => 'url',
						'default'   => esc_html__('https://www.pinterest.com/', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-linkedin',
                        'type'      => 'text',
                        'title'     => esc_html__('LinkedIn', 'kiwi'), 
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-tumblr',
                        'type'      => 'text',
                        'title'     => esc_html__('Tumblr', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-dribbble',
                        'type'      => 'text',
                        'title'     => esc_html__('Dribbble', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-vimeo',
                        'type'      => 'text',
                        'title'     => esc_html__('Vimeo', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => 'Please enter a valid <strong>URL</strong> in this field.'
                        )
                    ),
					array(
                        'id'        => 'socialmedia-youtube',
                        'type'      => 'text',
                        'title'     => esc_html__('Youtube', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-rss',
                        'type'      => 'text',
                        'title'     => esc_html__('RSS', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => 'Please enter a valid <strong>URL</strong> in this field.'
                        )
                    ),
					array(
                        'id'        => 'socialmedia-behance',
                        'type'      => 'text',
                        'title'     => esc_html__('Behance', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-flickr',
                        'type'      => 'text',
                        'title'     => esc_html__('Flickr', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-spotify',
                        'type'      => 'text',
                        'title'     => esc_html__('Spotify', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-instagram',
                        'type'      => 'text',
                        'title'     => esc_html__('Instagram', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-github',
                        'type'      => 'text',
                        'title'     => esc_html__('GitHub', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-stackexchange',
                        'type'      => 'text',
                        'title'     => esc_html__('StackExchange', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-soundcloud',
                        'type'      => 'text',
                        'title'     => esc_html__('SoundCloud', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					array(
                        'id'        => 'socialmedia-vk',
                        'type'      => 'text',
                        'title'     => esc_html__('VK', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
					
					array(
                        'id'        => 'socialmedia-skype',
                        'type'      => 'text',
                        'title'     => esc_html__('Skype', 'kiwi'),
                        'validate'  => 'url',
                     'placeholder'   => esc_html__('http://', 'kiwi'),
                        'text_hint' => array(
                            'title'     => '',
                            'content'   => wp_kses(__('Please enter a valid <strong>URL</strong> in this field.', 'kiwi'), $wpkses),
                        )
                    ),
        )
    ) ;



$this->sections[] = array(
        'title' => esc_html__( 'Miscellaneous', 'kiwi' ),
        'id'    => 'miscellaneous',
        'desc'  => esc_html__( '', 'kiwi' ),
        'icon'  => 'el el-braille',
		'fields'     => array(	
            array(
                        'id'        => 'miscellaneous-scrolltotop',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable scroll to top button', 'kiwi'),
                        'subtitle'  => esc_html__('', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'miscellaneous-ubermenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('UberMenu', 'kiwi'),
                        'desc'      => esc_html__('This option adds UberMenu support without editing any code.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'miscellaneous-yoastbreadcrumb',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Yoast Breadcrumb', 'kiwi'),
                        'desc'      => esc_html__('Check to enable Yoast Breadcrumbs instead.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'info-miscellaneous-vc',
                        'type'      => 'info',
                        'desc'      => esc_html__('Visual Composer', 'kiwi')
                    ),
										
					array(
                        'id'        => 'miscellaneous-vcedit',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Disable edit link VC', 'kiwi'),
                        'desc'      => esc_html__('Check to hide visual composer edit link on pages and posts.', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'miscellaneous-vcedit-admin',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Disable admin link VC', 'kiwi'),
                        'desc'      => esc_html__('Check to hide "Edit with Visual Composer" from WordPress Admin Bar', 'kiwi'),
                        'default'   => '1'
                    ),
					
					array(
                        'id'        => 'miscellaneous-vc-rtl-support',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Enable VC RTL support', 'kiwi'),
                        'desc'      => esc_html__('Check to enable RTL support within VC and your theme.', 'kiwi'),
                        'default'   => '0'
                    ),
					
					array(
                        'id'        => 'info-miscellaneous-one',
                        'type'      => 'info',
                        'desc'      => esc_html__('KIWI comes packaged with multiple jQuery scripts that you may optionally use within your theme or wish to disable to prevent possible conflicts with newly installed plugins that are not part of this theme.', 'kiwi')
                    ),
					
					array(
                        'id'        => 'script-masonry',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Masonry', 'kiwi'),
						'desc'      => esc_html__('Check to disable Masonry script.', 'kiwi'),
                        'subtitle'  => esc_html__('', 'kiwi'),
                        'default'   => '0'
                    ),
					
					
					array(
                        'id'        => 'script-bsmenu',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Bootstrap menu', 'kiwi'),
						'desc'      => esc_html__('Check to disable Bootstrap smart-menu script.', 'kiwi'),
                        'subtitle'  => esc_html__('', 'kiwi'),
                        'default'   => '0'
                    ),
										
					array(
                        'id'        => 'info-miscellaneous-seo',
                        'type'      => 'info',
                        'desc'      => esc_html__('SEO :: Performance', 'kiwi')
                    ),
					
					array(
                        'id'        => 'gtmetric-static',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Static resources', 'kiwi'),
						'desc'      => esc_html__('If enabled, it will remove query strings from static resources', 'kiwi'),
                        'default'   => '0'
                    ),
					
					
					array(
                        'id'        => 'gtmetric-deferparsing',
                        'type'      => 'checkbox',
                        'title'     => esc_html__('Defer parsing', 'kiwi'),
						'desc'      => esc_html__('If enabled, it will defer parsing of JavaScript', 'kiwi'),
                        'default'   => '0'
                    ),
					

        ),
   ) ;

 $this->sections[] = array(
        'title' => esc_html__( 'Custom CSS', 'kiwi' ),
        'id'    => 'themecss',
        'desc'  => esc_html__( 'Advanced CSS Customizations', 'kiwi' ),
        'icon'  => 'el el-css',
		'fields'     => array(	
            array(
                        'id'        => 'mp-editor-css',
                        'type'      => 'ace_editor',
                        'title'     => esc_html__('CSS Code', 'kiwi'),
                        'subtitle'  => esc_html__('Paste your CSS code here.', 'kiwi'),
                        'mode'      => 'css',
                        'theme'     => 'monokai',
                        'desc'      => 'Any custom CSS entered here will override the theme CSS. In some cases, the !important tag may be needed.',
                    ),            
        )
    ) ;


   $this->sections[] = array(
        'icon'            => 'el el-list-alt',
        'title'           => esc_html__( 'Customizer Only', 'kiwi' ),
        'desc'            => esc_html__( '<p class="description">This Section should be visible only in Customizer</p>', 'kiwi' ),
        'customizer_only' => true,
        'fields'          => array(
            array(
                'id'              => 'opt-customizer-only',
                'type'            => 'select',
                'title'           => esc_html__( 'Customizer Only Option', 'kiwi' ),
                'subtitle'        => esc_html__( 'The subtitle is NOT visible in customizer', 'kiwi' ),
                'desc'            => esc_html__( 'The field desc is NOT visible in customizer.', 'kiwi' ),
                'customizer_only' => true,
                //Must provide key => value pairs for select options
                'options'         => array(
                    '1' => 'Opt 1',
                    '2' => 'Opt 2',
                    '3' => 'Opt 3'
                ),
                'default'         => '2'
            ),
        )
    ) ;	
	
				
            }

            public function setHelpTabs() {

                // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-1',
                    'title'   => esc_html__( 'Theme Information 1', 'kiwi' ),
                    'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'kiwi' )
                );

                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-2',
                    'title'   => esc_html__( 'Theme Information 2', 'kiwi' ),
                    'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'kiwi' )
                );

                // Set the help sidebar
                $this->args['help_sidebar'] = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'kiwi' );
            }

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'           => 'kiwi_theme_option',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'       => $theme->get( 'Name' ),
                    // Name that appears at the top of your panel
                    'display_version'    => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'          => 'submenu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'     => true,
                    // Show the sections below the admin menu item or not
                     'menu_title'        => esc_html__( 'Theme Options', 'kiwi' ),
					'page_title'         => esc_html__( 'Theme Options', 'kiwi' ),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'     	=> '',
                    // Must be defined to add google fonts to the typography module

					'compiler'           => true,
                    'async_typography'   => true,
                    // Use a asynchronous font on the front end or font string
                   
				   // Show the panel pages on the admin bar
					'admin_bar_icon'       => 'dashicons-portfolio',
					// Choose an icon for the admin bar menu
					'admin_bar_priority'   => 50,
					// Choose an priority for the admin bar menu
					'global_variable'      => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'           => false,
                    // Show the time the page took to load, etc
                    'customizer'         => true,
                    // Enable basic customizer support

                    // OPTIONAL -> Give you extra features
                    'page_priority'      => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'        => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'   => 'manage_options',
                    // Permissions needed to access the options panel.
                    'menu_icon'          => '',
                    // Specify a custom URL to an icon
                    'last_tab'           => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'          => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'          => '_options',
                    // Page slug used to denote the panel
                    'save_defaults'      => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'       => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'       => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export' => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'     => 60 * MINUTE_IN_SECONDS,
                    'output'             => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'         => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                     'footer_credit'     => ' ',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'           => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

                    // HINTS
                    'hints'              => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );


                // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
                /* $this->args['share_icons'][] = array(
                    'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                    'title' => 'Visit us on GitHub',
                    'icon'  => 'el el-github'
                    //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                    'title' => 'Like us on Facebook',
                    'icon'  => 'el el-facebook'
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'http://twitter.com/reduxframework',
                    'title' => 'Follow us on Twitter',
                    'icon'  => 'el el-twitter'
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'http://www.linkedin.com/company/redux-framework',
                    'title' => 'Find us on LinkedIn',
                    'icon'  => 'el el-linkedin'
                ); */

                // Panel Intro text -> before the form
                if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                    if ( ! empty( $this->args['global_variable'] ) ) {
                        $v = $this->args['global_variable'];
                    } else {
                        $v = str_replace( '-', '_', $this->args['opt_name'] );
                    }
                    $this->args['intro_text'] = sprintf( esc_html__( '', 'kiwi' ), $v );
                } else {
                    $this->args['intro_text'] = esc_html__( '', 'kiwi' );
                }

                // Add content after the form.
                $this->args['footer_text'] = esc_html__( '', 'kiwi' );
            }

        }

        global $kiwiConfig;
        $kiwiConfig = new Kiwi_Framework_Config();
    }


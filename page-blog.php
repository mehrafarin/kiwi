<?php
/*
Template Name: Blog Posts
*/

get_header(); 

	global $kiwi_theme_option;
 			
	$grid_columns = $kiwi_theme_option['postlayout-gridcolumns']; 
	
	if ($grid_columns == '1') { 
		$variable_width = ' two-columns'; 
	} elseif ($grid_columns == '2') { 
		$variable_width = ' three-columns'; 
	} elseif ($grid_columns == '3') { 
		$variable_width = ' four-columns'; 
	} else {
		$variable_width = '';
	}				
?>


<?php if ( $kiwi_theme_option['sidebar-blog-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-blog-enable'] == '0') { ?>

		<div class="container" itemscope itemtype="http://schema.org/Blog">
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12 fullwidth">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '1' && $kiwi_theme_option['sidebar-blog-number'] == '1'
				&& $kiwi_theme_option['sidebar-blog-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 template-blog">				
			<?php } ?>
							
					<?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '1' && $kiwi_theme_option['sidebar-blog-number'] == '1' 
					&& $kiwi_theme_option['sidebar-blog-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 template-blog" style="float:right!important">				
					<?php } ?>
								
			<?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '1' && $kiwi_theme_option['sidebar-blog-number'] == '2' 
			&& $kiwi_theme_option['sidebar-blog-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 template-blog half">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-blog-enable'] == '1' && $kiwi_theme_option['sidebar-blog-number'] == '2' 
						&& $kiwi_theme_option['sidebar-blog-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6 half" style="float:right!important">
						<?php } ?>
			
			
			
		<?php if ( have_posts() ) : ?>
			<div class="template-page">
				
			<?php if ( $kiwi_theme_option['page-pagetitle-enable'] == '1' ) { ?>
				<h3 class="index"><?php echo wp_get_document_title(); ?></h3>
			<?php } ?>
			
			</div>
			
			<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '2' ) { ?>
				<div class="grid-masonry<?php echo esc_attr( $variable_width ); ?> index-layout category">	
			<?php } ?>
			
			
			<?php 
				$postcount = $kiwi_theme_option['sidebar-blog-postcount'];
				$wp_query = new WP_Query(); 
				$wp_query->query('posts_per_page='.$postcount.'' . '&paged='.$paged);
				
				while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
				
				<?php get_template_part( 'content', get_post_format() ); ?>
				
			<?php endwhile; ?>
			
			<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '2' ) { ?>
				</div>	
			<?php } ?>
			
			<div class="clear"></div>
			<?php kiwi_content_nav( 'nav-below' ); ?> 

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>

			</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-blog-enable'] > '0' && $kiwi_theme_option['sidebar-blog-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-blog-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-blog-enable'] > '0' && $kiwi_theme_option['sidebar-blog-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-blog-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-blog-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>


<!-- -->
<?php if ( $kiwi_theme_option['sidebar-blog-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-blog-enable'] > '0' && $kiwi_theme_option['sidebar-blog-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 blog sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-blog-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 template-blog half">
			
		<?php if ( have_posts() ) : ?>
			<div class="template-page">						
							
			<?php if ( $kiwi_theme_option['page-pagetitle-enable'] == '1' ) { ?>
				<h3 class="index"><?php echo wp_get_document_title(); ?></h3>
			<?php } ?>
			
			</div>
			
			<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '2' ) { ?>
				<div class="grid-masonry<?php echo esc_attr( $variable_width ); ?> index-layout category">	
			<?php } ?>

			<?php 
				$wp_query = new WP_Query(); 
				$wp_query->query('showposts=5' . '&paged='.$paged);
				
				while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
				
				<?php get_template_part( 'content', get_post_format() ); ?>
				
			<?php endwhile; ?>
			
			<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '2' ) { ?>
				</div>	
			<?php } ?>
			
			<div class="clear"></div>
			<?php kiwi_content_nav( 'nav-below' ); ?> 
			
		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>
			
	</div>
				 
	
	<div class="col-sm-12 col-md-3 blog sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-blog-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	
<?php } ?>

<?php get_footer(); ?>
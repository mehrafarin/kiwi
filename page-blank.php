<?php
/* Template Name: Blank page */

get_header();

global $kiwi_theme_option;

?>
						
<div class="page-blank">

		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>				
			<?php the_content(); ?>	
		<?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
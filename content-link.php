<?php 

	global $kiwi_theme_option, $post; 
 
	$background 	= '';
	$postcategories = get_the_category(); 
	
	if (has_post_thumbnail( $post->ID ) ):
		$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$background = 'background:url('. esc_url( $image_attributes[0] ).')'; 
	else:
		$image_attributes = array( ' ' ); 
		$background = 'opacity:1!important';
	endif;
	
?>

<?php if ( !is_single() ) { ?>
	<article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php } ?>

<?php if ( is_single() ) { ?>
	<div itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php } ?>
	
	<div class="link-header">		
		<div class="featured-image" style="<?php echo esc_attr( $background ); ?>"></div>
		<div class="entry-content">
		
			<?php 
				$heading = get_post_meta( get_the_ID(), '_cmb2_linkheading', true );
				$link = get_post_meta( get_the_ID(), '_cmb2_linkurl', true );
				$subheading = get_post_meta( get_the_ID(), '_cmb2_linksubheading', true );
				
				if( ! empty( $heading ) ) {
					echo "<h3>" . esc_html( $heading ). " </h3>";
				} 
				
				if( ! empty( $link ) ) {
					echo "<a href=" . esc_url( $link ) . ">". esc_url( $link ) ."</a>";
				} 
				
				if( ! empty( $subheading ) ) {
					echo "<div>" . esc_html( $subheading ). " </div>";
				} 
			?>
		</div>	
	</div>
	
	<div class="link-content">
						
			<?php if ( is_single() ) { ?>
				<div class="meta-category-single">
				<span class="pull-left"><?php kiwi_entry_meta(); ?></span>
				
				<?php if ( $kiwi_theme_option['blog-editpostlink-enable-single'] == '0' && is_single() ) { ?>
					<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="pull-right comment-edit-link">', '</span>' ); ?>
				<?php } ?>
				
				<div class="clear"></div>
				</div>
			<?php } ?>
			
			
			<?php if ( $kiwi_theme_option['postformat-linktitle'] == '1' && !is_single() ) { ?>
			
				<?php if ( !empty($postcategories)) { ?>
					<span class="meta-category pull-left"><i class="fa fa-tag"></i> <?php the_category(', '); ?> </span>
				<?php } ?>
				
				<?php if ( $kiwi_theme_option['blog-editpostlink-enable'] == '1') { ?>
					<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="grid edit-link pull-right">', '</span>' ); ?>
				<?php } ?>
				
				<div class="clear"></div>
				
				<div class="post-title">
					<h3 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
				</div>
				
			<?php } ?>
			
			
				<?php if ( $kiwi_theme_option['postformat-linktitle'] == '1' && is_single() ) { ?>		
					<h3 class="entry-title" itemprop="headline"><?php the_title(); ?></h3>			
				<?php } ?>
				
				<?php if ( $kiwi_theme_option['postformat-linktitle'] == '0' && !is_single() ) { ?>		
					<div class="extra-space"></div>			
				<?php } ?>

				
			<?php if ( $kiwi_theme_option['postformat-linkexcerpt'] == '1' && !is_single() ) { ?>
				<div class="grid-excerpt" itemprop="description"><?php the_excerpt(); ?></div>
			<?php } ?>				 
		 
			<?php if ( $kiwi_theme_option['postformat-linkcontent'] == '1' && !is_single() ) { ?>
				<div itemprop="articleBody"><?php the_content(); ?></div>
			<?php } ?>
			
			<?php if ( is_single() ) : ?>	
				<div itemprop="articleBody"><?php the_content(); ?></div>
			<?php endif; ?>
						
			<?php if ( $kiwi_theme_option['postformat-linkdate'] >= '1' && !is_single() || $kiwi_theme_option['postformat-linkmeta'] >= '1' && !is_single() ) { ?>
			<div class="kiwi-postformat meta-data">						
					<div class="pull-left">
						<?php if ( $kiwi_theme_option['postformat-linkdate'] == '1' && !is_single() ) { ?>			
							<span itemprop="datePublished"><span itemprop="datePublished"><?php echo esc_attr( get_the_date( get_option('date_format') ) ); ?></span></span>
						<?php } ?>
					</div>	
				
					<div class="pull-right">
						<?php if ( $kiwi_theme_option['postformat-linkmeta'] == '1' && !is_single() ) { ?>
							<a href="<?php comments_link(); ?>" class="meta-commentlink"><span class="comments"><i class="fa fa-comment"></i> <?php comments_number( '0', '1', '%' ); ?></span></a>
							
						<?php } ?>
					</div>			
			<div class="clear"></div>
			</div>	
			<?php } ?>
			
			
			<?php if ( is_single() ) { ?>
				<?php kiwi_singlebottom_meta(); ?>
			<?php } ?>
					
	</div>
	
<?php if ( !is_single() ) { ?>
	</article>
<?php } ?>

<?php if ( is_single() ) { ?>
	</div>
<?php } ?>	

<?php if ( $kiwi_theme_option['blog-authorbio-enable'] == '1' && is_single() || is_multi_author() && is_single() ) : ?>
			<?php if ( get_the_author_meta( 'description' )  ) : ?>
				<?php get_template_part( 'templates/author', 'bio' ); ?>
			<?php endif; ?>
<?php endif; ?>	
<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarlayout'] <= '2'  || $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] == '0') { ?>

			
		<?php if ( $kiwi_theme_option['marketplace-container-itemdesc'] == '1' ) {
			echo '<div class="marketplace"><div>';
			} else {
				echo '<div class="container marketplace mp-vc-items"';				
				echo esc_attr( kiwi_rtl() );
				echo '>';				
				echo '<div class="row" role="main">';
			} 
		?>	
		
			<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] == '0' ) { ?>				
				<?php if ( $kiwi_theme_option['marketplace-container-itemdesc'] == '1' ) {
						echo '<div>';
					} else {
						echo '<div class="col-sm-12 col-md-12">';
					} 
				?>				
			<?php } ?>
			
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '1'
				&& $kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 marketplace-layout">				
			<?php } ?>
			
				
					<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '1' 
					&& $kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 marketplace-layout" style="float:right!important">				
					<?php } ?>
					
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '2' 
			&& $kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 marketplace-layout">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '2' 
						&& $kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6" style="float:right!important">
						<?php } ?>
						
			
			<!-- begin loop -->
			<?php if ( have_posts() ) :  while (have_posts()) : the_post(); ?>			
				<?php get_template_part( 'templates/content', 'marketplace' ); ?>			
			<?php endwhile; endif; ?>
			
							</div>		

			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-itemdesc-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>



<!-- -->
<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarlayout'] == '3' ) { ?>
		
	<?php if ( $kiwi_theme_option['marketplace-sidebar-itemdesc-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-itemdesc-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-itemdesc-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 marketplace-layout">
			<!-- begin loop -->
			<?php if ( have_posts() ) :  while (have_posts()) : the_post(); ?>			
				<?php get_template_part( 'templates/content', 'marketplace' ); ?>			
			<?php endwhile; endif; ?>
	</div>
	
	
	<div class="col-sm-12 col-md-3 sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-itemdesc-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	

<?php } ?>

<?php get_footer(); ?>
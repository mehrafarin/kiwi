<?php 

$force_search_results = get_post_meta( get_the_ID(), '_cmb2_mpsearchstring', true );

if ( class_exists( 'Easy_Digital_Downloads' ) && !empty( $force_search_results ) || 
class_exists( 'Easy_Digital_Downloads' ) && is_singular( 'download' ) || 
class_exists( 'Easy_Digital_Downloads' ) && is_tax( 'download_category' ) || 
class_exists( 'Easy_Digital_Downloads' ) && edd_is_checkout() || 
class_exists( 'Easy_Digital_Downloads' ) && is_tax( 'download_tag' ) || 
class_exists( 'Easy_Digital_Downloads' ) && is_post_type_archive( 'download' ) || 
class_exists( 'Easy_Digital_Downloads' ) && is_page_template('page-marketplace.php') || 
class_exists( 'Easy_Digital_Downloads' ) && is_page_template('page-themes.php') ) : ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
	<div class="form-group">
		<input type="hidden" value="download" name="post_type" id="post_type" />
		<input type="text" name="s" id="search-form" class="form-control" value="<?php echo get_search_query() ?>" placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'kiwi' ); ?>">
		<button type="submit" class="btn btn-default" value="Submit"><i class="fa fa-search"></i></button>
	</div>
	<div class="clear"></div>
</form>

<?php else : ?>
	
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
	<div class="form-group">
		<input type="hidden" name="post_types" value="page,post" />
		<input type="text" name="s" id="search-form" class="form-control" value="<?php echo get_search_query() ?>" placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'kiwi' ); ?>">
		<button type="submit" class="btn btn-default" value="Submit"><i class="fa fa-search"></i></button>
	</div>
	<div class="clear"></div>
</form>

<?php endif; ?>
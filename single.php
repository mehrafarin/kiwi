<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-posts-sidebarlayout'] <= '2'  || $kiwi_theme_option['sidebar-posts-enable'] == '0') { ?>

		<div class="container" itemscope itemtype="http://schema.org/Blog">
			<div class="row" role="main"<?php esc_attr( kiwi_rtl() ); ?>>
		
			<?php if ( $kiwi_theme_option['sidebar-posts-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-posts-enable'] == '1' && $kiwi_theme_option['sidebar-posts-number'] == '1'
				&& $kiwi_theme_option['sidebar-posts-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 post-layout">				
			<?php } ?>
						
				
					<?php if ( $kiwi_theme_option['sidebar-posts-enable'] == '1' && $kiwi_theme_option['sidebar-posts-number'] == '1' 
					&& $kiwi_theme_option['sidebar-posts-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 post-layout" style="float:right!important">				
					<?php } ?>
					
			
			<?php if ( $kiwi_theme_option['sidebar-posts-enable'] == '1' && $kiwi_theme_option['sidebar-posts-number'] == '2' 
			&& $kiwi_theme_option['sidebar-posts-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 post-layout">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-posts-enable'] == '1' && $kiwi_theme_option['sidebar-posts-number'] == '2' 
						&& $kiwi_theme_option['sidebar-posts-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6" style="float:right!important">
						<?php } ?>
			
			
			<!-- begin loop -->
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				
					<?php if ( $kiwi_theme_option['blog-postpreview-enable'] == '1' ) { ?>		
						<?php get_template_part( 'templates/single', 'preview' ); ?>
					<?php } ?>

				<?php comments_template( '', true ); ?>

			<?php endwhile; ?>

							</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-posts-enable'] > '0' && $kiwi_theme_option['sidebar-posts-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-posts-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-posts-enable'] > '0' && $kiwi_theme_option['sidebar-posts-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-posts-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-posts-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>



<!-- -->
<?php if ( $kiwi_theme_option['sidebar-posts-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-posts-enable'] > '0' && $kiwi_theme_option['sidebar-posts-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 blog sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-posts-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 post-layout">
			<!-- begin loop -->
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				
					<?php if ( $kiwi_theme_option['blog-postpreview-enable'] == '1' ) { ?>		
						<?php get_template_part( 'templates/single', 'preview' ); ?>
					<?php } ?>

				<?php comments_template( '', true ); ?>

			<?php endwhile; ?>
	</div>
	
	
	<div class="col-sm-12 col-md-3 blog sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-posts-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	

<?php } ?>

<?php get_footer(); ?>
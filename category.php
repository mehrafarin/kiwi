<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-archive-enable'] == '0') { ?>

		<div class="container" itemscope itemtype="http://schema.org/Blog">
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12 fullwidth">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1'
				&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 category-layout">				
			<?php } ?>
							
					<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1' 
					&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 category-layout" style="float:right!important">				
					<?php } ?>
								
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
			&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 category-layout half">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
						&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6 half" style="float:right!important">
						<?php } ?>
						
						
				<?php if ( $kiwi_theme_option['page-pagetitle-category-enable'] == '1' ) { ?>
				<div class="category-page">
					<h3><?php printf( esc_html__( 'Category Archives: %s', 'kiwi' ), '<span>' . esc_html( single_cat_title( '', false ) ). '</span>' ); ?></h3>
				
				<?php if ( category_description() ) : ?>				
					<div class="sep"><?php esc_html_e( 'Category description', 'kiwi' ); ?></div>
					<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
				</div>				
				<?php } ?>
				
			
			<?php get_template_part( 'templates/layout', 'settings' ); ?>

							</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>



<!-- -->
<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 blog sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	
	<div class="col-sm-12 col-md-6 category-layout">
					
			<div class="category-page">				
				
				<?php if ( $kiwi_theme_option['page-pagetitle-category-enable'] == '1' ) { ?>
					<h3><?php printf( esc_html__( 'Category Archives: %s', 'kiwi' ), '<span>' . esc_html( single_cat_title( '', false ) ). '</span>' ); ?></h3>
				<?php } ?>
				
				
			<?php if ( category_description() ) : ?>				
				<div class="sep"><?php esc_html_e( 'Category description', 'kiwi' ); ?></div>
				<div class="archive-meta"><?php echo category_description(); ?></div>
			<?php endif; ?>
			</div>
			
			<?php get_template_part( 'templates/layout', 'settings' ); ?>
			
	</div>
				 
	
	<div class="col-sm-12 col-md-3 blog sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	

<?php } ?>


<?php get_footer(); ?>
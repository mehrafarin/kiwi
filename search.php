<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-search-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-search-enable'] == '0') { ?>

		<div class="container">
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['sidebar-search-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-search-enable'] == '1' && $kiwi_theme_option['sidebar-search-number'] == '1'
				&& $kiwi_theme_option['sidebar-search-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 search-layout">				
			<?php } ?>
			
			
				
					<?php if ( $kiwi_theme_option['sidebar-search-enable'] == '1' && $kiwi_theme_option['sidebar-search-number'] == '1' 
					&& $kiwi_theme_option['sidebar-search-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 search-layout" style="float:right!important">				
					<?php } ?>
					
			
			<?php if ( $kiwi_theme_option['sidebar-search-enable'] == '1' && $kiwi_theme_option['sidebar-search-number'] == '2' 
			&& $kiwi_theme_option['sidebar-search-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 search-layout">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-search-enable'] == '1' && $kiwi_theme_option['sidebar-search-number'] == '2' 
						&& $kiwi_theme_option['sidebar-search-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6" style="float:right!important">
						<?php } ?>
						
					<?php if ( $kiwi_theme_option['page-pagetitle-archive-enable'] == '1' ) { ?>	
						<div class="page-search">
							<h3 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'kiwi' ), '<span>' . esc_html( get_search_query( false ) ) . '</span>' ); ?></h3>
						</div>
					<?php } ?>
					
					
					
					<?php get_template_part( 'templates/layout', 'settings' ); ?>

			</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-search-enable'] > '0' && $kiwi_theme_option['sidebar-search-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-search-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>

			<?php if ( $kiwi_theme_option['sidebar-search-enable'] > '0' && $kiwi_theme_option['sidebar-search-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-search-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-search-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>

<!-- -->
<?php if ( $kiwi_theme_option['sidebar-search-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-search-enable'] > '0' && $kiwi_theme_option['sidebar-search-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 sidebar-layout blog">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-search-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	
	<div class="col-sm-12 col-md-6 search-layout">

			<?php if ( $kiwi_theme_option['page-pagetitle-archive-enable'] == '1' ) { ?>	
					<div class="page-search">
						<h3 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'kiwi' ), '<span>' . esc_html( get_search_query( false ) ) . '</span>' ); ?></h3>
					</div>
			<?php } ?>

			<?php get_template_part( 'templates/layout', 'settings' ); ?>
			
	</div>	
	
	<div class="col-sm-12 col-md-3 sidebar-layout blog">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-search-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	
<?php } ?>

<?php get_footer(); ?>
<?php get_header(); 

global $kiwi_theme_option; 

?>

<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-archive-enable'] == '0') { ?>

		<div class="container" itemscope itemtype="http://schema.org/Blog">
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12 fullwidth">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1'
				&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 category-layout">				
			<?php } ?>
							
					<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1' 
					&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 category-layout" style="float:right!important">				
					<?php } ?>
								
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
			&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 category-layout half">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
						&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6 half" style="float:right!important">
						<?php } ?>
			
			
			
		<?php get_template_part( 'templates/layout', 'settings' ); ?>

			</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>



<!-- -->
<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 blog sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	
	<div class="col-sm-12 col-md-6 category-layout">
					
			<div class="author-page">
				
				<?php if ( $kiwi_theme_option['page-pagetitle-author-enable'] == '1' ) { ?>
					<h3 class="archive-title entry-title">
					<?php printf( esc_html__( 'Author Archives: %s', 'kiwi' ), '<span class="vcard author post-author"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?></h3>
				<?php } ?>
			
				<?php rewind_posts(); ?>
				
				<?php if ( get_the_author_meta( 'description' ) ) : ?>			
					<?php get_template_part( 'templates/author', 'bio' ); ?>			
				<?php endif; ?>
				
				<?php get_template_part( 'templates/layout', 'settings' ); ?>
			
			</div>
	
	
	<div class="col-sm-12 col-md-3 blog sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	
<?php } ?>

<?php get_footer(); ?>
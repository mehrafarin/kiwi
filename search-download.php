<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] <= '2'  || $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '0') { ?>

		<div class="container marketplace mp-vc-items"<?php esc_attr( kiwi_rtl() ); ?>>
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '1'
				&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 marketplace-layout">				
			<?php } ?>
			
				
					<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '1' 
					&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 marketplace-layout" style="float:right!important">				
					<?php } ?>
					
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2' 
			&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 marketplace-layout">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2' 
						&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6" style="float:right!important">
						<?php } ?>			
			
							<!-- begin loop -->							
							<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '1' ) { ?>
									<?php get_template_part( 'templates/marketplace', 'archiveslist' ); ?>
							<?php } ?>
							
							<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '2' ) { ?>
									<?php get_template_part( 'templates/marketplace', 'archivesgrid' ); ?>
							<?php } ?>
							
							
							</div>		
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>



<!-- -->
<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] == '3' ) { ?>

	<div class="container marketplace mp-vc-items"<?php esc_attr( kiwi_rtl() ); ?>>
			<div class="row" role="main">
		
	<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 marketplace-layout">
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '1' ) { ?>
					<?php get_template_part( 'templates/marketplace', 'archiveslist' ); ?>
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '2' ) { ?>
					<?php get_template_part( 'templates/marketplace', 'archivesgrid' ); ?>
			<?php } ?>
							
	</div>
	
	
	<div class="col-sm-12 col-md-3 sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	
	</div>
</div>
<?php } ?>

<?php get_footer(); ?>
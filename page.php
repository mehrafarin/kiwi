<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-pages-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-pages-enable'] == '0') { ?>

		<div class="container">
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['sidebar-pages-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12 fullwidth">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-pages-enable'] == '1' && $kiwi_theme_option['sidebar-pages-number'] == '1'
				&& $kiwi_theme_option['sidebar-pages-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 page-layout">				
			<?php } ?>
			
			
				
					<?php if ( $kiwi_theme_option['sidebar-pages-enable'] == '1' && $kiwi_theme_option['sidebar-pages-number'] == '1' 
					&& $kiwi_theme_option['sidebar-pages-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 page-layout" style="float:right!important">				
					<?php } ?>
					
			
			<?php if ( $kiwi_theme_option['sidebar-pages-enable'] == '1' && $kiwi_theme_option['sidebar-pages-number'] == '2' 
			&& $kiwi_theme_option['sidebar-pages-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 page-layout half">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-pages-enable'] == '1' && $kiwi_theme_option['sidebar-pages-number'] == '2' 
						&& $kiwi_theme_option['sidebar-pages-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6 half" style="float:right!important">
						<?php } ?>
			
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'templates/content', 'page' ); ?>
				
				<?php if ( $kiwi_theme_option['page-comment-enable'] == '1') { ?>
					<?php comments_template( '', true ); ?>
				<?php } ?>	
				
			<?php endwhile; ?>

							</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-pages-enable'] > '0' && $kiwi_theme_option['sidebar-pages-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-pages-enable'] > '0' && $kiwi_theme_option['sidebar-pages-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>


<!-- -->
<?php if ( $kiwi_theme_option['sidebar-pages-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-pages-enable'] > '0' && $kiwi_theme_option['sidebar-pages-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 sidebar-layout blog">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 page-layout half">
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'templates/content', 'page' ); ?>		
				
				<?php if ( $kiwi_theme_option['page-comment-enable'] == '1') { ?>
					<?php comments_template( '', true ); ?>
				<?php } ?>	
				
			<?php endwhile; ?>
			
	</div>
	
	
	<div class="col-sm-12 col-md-3 sidebar-layout blog">	
		<div class="sidebar">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebartwo'])):
				endif; ?>
		</div>
	</div>
	<?php } ?>
	

<?php } ?>

<?php get_footer(); ?>
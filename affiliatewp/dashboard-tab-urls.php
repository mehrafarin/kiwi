<div id="affwp-affiliate-dashboard-url-generator" class="affwp-tab-content">

	<?php if ( 'id' == affwp_get_referral_format() ) : ?>
		<p><?php printf( __( 'Your affiliate ID is: <strong>%s</strong>', 'kiwi' ), affwp_get_affiliate_id() ); ?></p>
	<?php elseif ( 'username' == affwp_get_referral_format() ) : ?>
		<p><?php printf( __( 'Your affiliate username is: <strong>%s</strong>', 'kiwi' ), affwp_get_affiliate_username() ); ?></p>
	<?php endif; ?>
	
	<p><?php printf( __( 'Your referral URL is: <strong>%s</strong>', 'kiwi' ), esc_url( urldecode( affwp_get_affiliate_referral_url() ) ) ); ?></p>
	

	<br />
		<div class="edd_widget_title">
			<h3><?php esc_html_e( 'Referral URL Generator', 'kiwi' ); ?></h3>
		</div>
	
	
	
		<p><?php esc_html_e( 'Enter any URL from this website in the form below to generate a referral link!', 'kiwi' ); ?></p>
	<br />	
	
	
	
	
	
	
	<form id="affwp-generate-ref-url" class="mp-affwp-form form-horizontal" method="get" action="#affwp-generate-ref-url">
		
		<div class="form-group affwp-wrap affwp-base-url-wrap">
			<label for="affwp-url" class="col-sm-4 control-label"><?php esc_html_e( 'Page URL', 'kiwi' ); ?></label>
			<div class="col-sm-4">
				<input type="text" name="url" id="affwp-url" value="<?php echo esc_url( urldecode( affwp_get_affiliate_base_url() ) ); ?>" />
			</div>
		</div>
		
		<div class="form-group affwp-wrap affwp-campaign-wrap">
			<label for="affwp-campaign" class="col-sm-4 control-label"><?php esc_html_e( 'Campaign Name (optional)', 'kiwi' ); ?></label>
			<div class="col-sm-4">
				<input type="text" name="url" id="affwp-url" value="<?php echo esc_url( urldecode( affwp_get_affiliate_base_url() ) ); ?>" />
			</div>
		</div>
				

		<div class="form-group affwp-wrapaffwp-wrap affwp-referral-url-wrap" <?php if ( ! isset( $_GET['url'] ) ) { echo 'style="display:none;"'; } ?>>
			<label for="affwp-referral-url" class="col-sm-4 control-label"><?php esc_html_e( 'Referral URL', 'kiwi' ); ?></label>
			<div class="col-sm-4">
				<input type="text" id="affwp-referral-url" value="<?php echo esc_url( urldecode( affwp_get_affiliate_referral_url() ) ); ?>" />
				<div class="description"><?php esc_html_e( 'Copy your referral link and share it with your friends on social media, on your blog, in forums, anywhere!', 'kiwi' ); ?></div>
			</div>	
		</div>

		
		<div class="form-group affwp-referral-url-submit-wrap">
		<div class="col-sm-6">
			<input type="hidden" class="affwp-affiliate-id" value="<?php echo esc_attr( urldecode( affwp_get_referral_format_value() ) ); ?>" />
			<input type="hidden" class="affwp-referral-var" value="<?php echo esc_attr( affiliate_wp()->tracking->get_referral_var() ); ?>" />
			<input type="submit" class="button" value="<?php esc_html_e( 'Generate URL', 'kiwi' ); ?>" />
		</div>
	  </div>
  
  
		
	</form>
</div>

﻿<?php global $kiwi_theme_option;

$active_tab = ! empty( $_GET['tab'] ) ? sanitize_text_field( $_GET['tab'] ) : 'urls'; 

if ( class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor() ) {		
	$url_id_vendor = $kiwi_theme_option['marketplace-enablelinks-dashboard'];
	$url = get_page_link ($url_id_vendor);		
} elseif ( class_exists( 'EDD_Customer_Dashboard' ) ) {		
	$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
	$url = get_page_link ($url_id);
} else {
	$url = home_url();		
}

?>

<div class="container-fluid mp-fes-vendor-menu">

<div class="container align-center"> 
<div class="row">

<nav class="fes-vendor-menu">
	<ul id="affwp-affiliate-dashboard-tabs">
	
	
			<li class="user-dashboard">
				<i class="icon icon-home icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'task', '', $url ) ); ?>"><?php esc_html_e( 'Main Dashboard', 'kiwi' ); ?></a>
			</li>
			
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'urls' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-url icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'urls', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Affiliate URLs', 'kiwi' ); ?></a>
			</li>
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'stats' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-stats icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'stats', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Statistics', 'kiwi' ); ?></a>
			</li>
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'graphs' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-graphs icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'graphs', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Graphs', 'kiwi' ); ?></a>
			</li>
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'referrals' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-referral icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'referrals', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Referrals', 'kiwi' ); ?></a>
			</li>
			
			<?php if ( class_exists( 'AffiliateWP_Order_Details_For_Affiliates' ) ) { ?>
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'order-details' ? ' active' : ''; ?>">
				<i class="icon icon-gift icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'order-details' ) ); ?>"><?php esc_html_e( 'Order Details', 'kiwi' ); ?></a>
			</li>	
			<?php } ?>
			
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'visits' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-visits icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'visits', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Visits', 'kiwi' ); ?></a>
			</li>
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'creatives' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-creatives icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'creatives', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Creatives', 'kiwi' ); ?></a>
			</li>
			<li class="affwp-affiliate-dashboard-tab<?php echo $active_tab == 'settings' ? ' active' : ''; ?>">
				<i class="icon icon-affiliate-settings icon-white"></i>
				<a href="<?php echo esc_url( add_query_arg( 'tab', 'settings', get_permalink( affiliate_wp()->settings->get( 'affiliates_page' ) ) ) ); ?>"><?php esc_html_e( 'Settings', 'kiwi' ); ?></a>
			</li>
						
			<?php do_action( 'affwp_affiliate_dashboard_tabs', affwp_get_affiliate_id(), $active_tab ); ?>
		</ul> 
</nav>


</div>
</div>

</div>

<div class="container mp-full-dashboard">


<h3><?php esc_html_e( 'Affiliate area', 'kiwi' ); ?></h3><br />

<div id="affwp-affiliate-dashboard">

	<?php if ( 'pending' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>
		<p class="affwp-notice"><?php esc_html_e( 'Your affiliate account is pending approval', 'kiwi' ); ?></p>
	<?php elseif ( 'inactive' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>

		<p class="affwp-notice"><?php esc_html_e( 'Your affiliate account is not active', 'kiwi' ); ?></p>

	<?php elseif ( 'rejected' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>
		<p class="affwp-notice"><?php esc_html_e( 'Your affiliate account request has been rejected', 'kiwi' ); ?></p>
	<?php endif; ?>

	<?php if ( 'active' == affwp_get_affiliate_status( affwp_get_affiliate_id() ) ) : ?>

		<?php do_action( 'affwp_affiliate_dashboard_top', affwp_get_affiliate_id() ); ?>

		<?php if ( ! empty( $_GET['affwp_notice'] ) && 'profile-updated' == $_GET['affwp_notice'] ) : ?>
			<p class="affwp-notice"><?php esc_html_e( 'Your affiliate profile has been updated', 'kiwi' ); ?></p>
		<?php endif; ?>

		<?php do_action( 'affwp_affiliate_dashboard_notices', affwp_get_affiliate_id() ); ?>

		<?php affiliate_wp()->templates->get_template_part( 'dashboard-tab', $active_tab ); ?>

		<?php do_action( 'affwp_affiliate_dashboard_bottom', affwp_get_affiliate_id() ); ?>

	<?php endif; ?>
	
</div>

</div>
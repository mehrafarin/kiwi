<?php
global $affwp_login_redirect;
affiliate_wp()->login->print_errors();
?>

<form id="affwp-login-form" class="affwp-form" action="" method="post">
	<?php do_action( 'affwp_affiliate_login_form_top' ); ?>

	<fieldset>
		
		<div class="edd_widget_title">
			<h3><?php esc_html_e( 'Log into Your Account', 'kiwi' ); ?></h3>
		</div>



		<?php do_action( 'affwp_login_fields_before' ); ?>

		<p>
			<label for="affwp-login-user-login"><?php esc_html_e( 'Username', 'kiwi' ); ?></label>
			<input id="affwp-login-user-login" class="required" type="text" name="affwp_user_login" title="<?php esc_attr_e( 'Username', 'kiwi' ); ?>" />
		</p>

		<p>
			<label for="affwp-login-user-pass"><?php esc_html_e( 'Password', 'kiwi' ); ?></label>
			<input id="affwp-login-user-pass" class="password required" type="password" name="affwp_user_pass" />
		</p>

		<p>
			<label class="affwp-user-remember" for="affwp-user-remember">
				<input id="affwp-user-remember" type="checkbox" name="affwp_user_remember" value="1" /> <?php esc_html_e( 'Remember Me', 'kiwi' ); ?>
			</label>
		</p>

		<p>
			<input type="hidden" name="affwp_redirect" value="<?php echo esc_url( $affwp_login_redirect ); ?>"/>
			<input type="hidden" name="affwp_login_nonce" value="<?php echo wp_create_nonce( 'affwp-login-nonce' ); ?>" />
			<input type="hidden" name="affwp_action" value="user_login" />
			<input type="submit" class="button" value="<?php esc_attr_e( 'Login', 'kiwi' ); ?>" />
		</p>

		<p class="affwp-lost-password">
			<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost Password?', 'kiwi' ); ?></a>
		</p>

		<?php do_action( 'affwp_login_fields_after' ); ?>
	</fieldset>

	<?php do_action( 'affwp_affiliate_login_form_bottom' ); ?>
</form>

</div>

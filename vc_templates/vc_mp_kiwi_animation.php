<?php


// Parent Element
function VCExtendAddonClassAnimation() {
	vc_map( 
		array(
			"icon" 					  	=> 'icon-kiwi-animation',
		    'name'                    	=> esc_html__( 'Kiwi :: Animation' , 'kiwi' ),
		    'base'                    	=> 'kiwi_animation',
		    'description'             	=> esc_html__( 'Create animation effects.', 'kiwi' ),
		    'as_parent'               	=> array('except' => 'kiwi_animation'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		    'content_element'        	 => true,
		    'show_settings_on_create' 	=> true,
		    "js_view" 					=> 'VcColumnView',
			"admin_enqueue_css" => array(get_template_directory_uri().'/framework/theme-options/vc-addons/kiwi-vc-addon.css'),
		    "category" => esc_html__('Theme :: Kiwi', 'kiwi'),
			"params" => array(
			  array(        
				'type' => 'dropdown',        
				'heading' => esc_html__( 'Select Animation effect', 'kiwi' ),        
				'param_name' => 'mp_animation_effect',  
				'value' => array(
							 esc_html__( 'Select', 'kiwi' ) => '',
							 esc_html__( 'fadeIn', 'kiwi' ) => 'fadeIn',
							 esc_html__( 'fadeInDown', 'kiwi' ) => 'fadeInDown',
							 esc_html__( 'fadeInDownBig', 'kiwi' ) => 'fadeInDownBig',
							 esc_html__( 'fadeInLeft', 'kiwi' ) => 'fadeInLeft',
							 esc_html__( 'fadeInLeftBig', 'kiwi' ) => 'fadeInLeftBig',							
							 esc_html__( 'fadeInRight', 'kiwi' ) => 'fadeInRight' ,
							 esc_html__( 'fadeInRightBig', 'kiwi' ) => 'fadeInRightBig',
							 esc_html__( 'fadeInUp', 'kiwi' ) => 'fadeInUp',
							 esc_html__( 'fadeInUpBig', 'kiwi' ) => 'fadeInUpBig' ,
							 esc_html__( 'fadeOut', 'kiwi' ) => 'fadeOut',
							 esc_html__( 'fadeOutDown', 'kiwi' ) => 'fadeOutDown',							
							 esc_html__( 'fadeOutDownBig', 'kiwi' ) => 'fadeOutDownBig' ,
							 esc_html__( 'fadeOutLeft', 'kiwi' ) => 'fadeOutLeft',
							 esc_html__( 'fadeOutLeftBig', 'kiwi' ) => 'fadeOutLeftBig',
							 esc_html__( 'fadeOutRight', 'kiwi' ) => 'fadeOutRight' ,
							 esc_html__( 'fadeOutRightBig', 'kiwi' ) => 'fadeOutRightBig',
							 esc_html__( 'fadeOutUp', 'kiwi' ) => 'fadeOutUp',
							 esc_html__( 'fadeOutUpBig', 'kiwi' ) => 'fadeOutUpBig',							
							 esc_html__( 'flipInX', 'kiwi' ) => 'flipInX',
							 esc_html__( 'flipInY', 'kiwi' ) => 'flipInY',
							 esc_html__( 'flipOutX', 'kiwi' ) => 'flipOutX' ,
							 esc_html__( 'flipOutY', 'kiwi' ) => 'flipOutY',							
							 esc_html__( 'lightSpeedIn', 'kiwi' ) => 'lightSpeedIn',
							 esc_html__( 'lightSpeedOut', 'kiwi' ) => 'lightSpeedOut' ,
							 esc_html__( 'rotateIn', 'kiwi' ) => 'rotateIn',
							 esc_html__( 'rotateInDownLeft', 'kiwi' ) => 'rotateInDownLeft',							
							 esc_html__( 'rotateInDownRight', 'kiwi' ) => 'rotateInDownRight' ,
							 esc_html__( 'rotateInUpLeft', 'kiwi' ) => 'rotateInUpLeft',
							 esc_html__( 'rotateInUpRight', 'kiwi' ) => 'rotateInUpRight',
							 esc_html__( 'rotateOut', 'kiwi' ) => 'rotateOut' ,
							 esc_html__( 'rotateOutDownLeft', 'kiwi' ) => 'rotateOutDownLeft',
							 esc_html__( 'rotateOutDownRight', 'kiwi' ) => 'rotateOutDownRight',
							 esc_html__( 'rotateOutUpLeft', 'kiwi' ) => 'rotateOutUpLeft',
							 esc_html__( 'rotateOutUpRight', 'kiwi' ) => 'rotateOutUpRight',
							 esc_html__( 'bounceIn', 'kiwi' ) => 'bounceIn',
							 esc_html__( 'bounceInDown', 'kiwi' ) => 'bounceInDown',
							 esc_html__( 'bounceInLeft', 'kiwi' ) => 'bounceInLeft',
							 esc_html__( 'bounceInRight', 'kiwi' ) => 'bounceInRight',
							 esc_html__( 'bounceInUp', 'kiwi' ) => 'bounceInUp',
							 esc_html__( 'bounceOut', 'kiwi' ) => 'bounceOut',
							 esc_html__( 'bounceOutDown', 'kiwi' ) => 'bounceOutDown',
							 esc_html__( 'bounceOutLeft', 'kiwi' ) => 'bounceOutLeft',
							 esc_html__( 'bounceOutRight', 'kiwi' ) => 'bounceOutRight',
							 esc_html__( 'bounceOutUp', 'kiwi' ) => 'bounceOutUp',
							 esc_html__( 'zoomIn', 'kiwi' ) => 'zoomIn',
							 esc_html__( 'zoomInDown', 'kiwi' ) => 'zoomInDown',
							 esc_html__( 'zoomInLeft', 'kiwi' ) => 'zoomInLeft',
							 esc_html__( 'zoomInRight', 'kiwi' ) => 'zoomInRight',
							 esc_html__( 'zoomInUp', 'kiwi' ) => 'zoomInUp',
							 esc_html__( 'zoomOut', 'kiwi' ) => 'zoomOut',
							 esc_html__( 'zoomOutDown', 'kiwi' ) => 'zoomOutDown',
							 esc_html__( 'zoomOutLeft', 'kiwi' ) => 'zoomOutLeft',
							 esc_html__( 'zoomOutRight', 'kiwi' ) => 'zoomOutRight',
							 esc_html__( 'zoomOutUp', 'kiwi' ) => 'zoomOutUp',
							 esc_html__( 'slideInDown', 'kiwi' ) => 'slideInDown',
							 esc_html__( 'slideInLeft', 'kiwi' ) => 'slideInLeft',
							 esc_html__( 'slideInRight', 'kiwi' ) => 'slideInRight',
							 esc_html__( 'slideInUp', 'kiwi' ) => 'slideInUp',
							 esc_html__( 'slideOutDown', 'kiwi' ) => 'slideOutDown',
							 esc_html__( 'slideOutLeft', 'kiwi' ) => 'slideOutLeft',
							 esc_html__( 'slideOutRight', 'kiwi' ) => 'slideOutRight',
							 esc_html__( 'slideOutUp', 'kiwi' ) => 'slideOutUp',
							 esc_html__( 'hinge', 'kiwi' ) => 'hinge',
							 esc_html__( 'rollIn', 'kiwi' ) => 'rollIn',
							 esc_html__( 'rollOut', 'kiwi' ) => 'rollOut',							
							 esc_html__( 'bounce', 'kiwi' ) => 'bounce',
							 esc_html__( 'flash', 'kiwi' ) => 'flash',
							 esc_html__( 'pulse', 'kiwi' ) => 'pulse',
							 esc_html__( 'rubberBand', 'kiwi' ) => 'rubberBand',
							 esc_html__( 'shake', 'kiwi' ) => 'shake',
							 esc_html__( 'swing', 'kiwi' ) => 'swing',
							 esc_html__( 'tada', 'kiwi' ) => 'tada',
							 esc_html__( 'wobble', 'kiwi' ) => 'wobble',
							 esc_html__( 'jello', 'kiwi' ) => 'jello',  							
					),
				'description' => ' ',
				'edit_field_class' => 'vc_col-sm-12 vc_column',
			),
			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Animation Duration', 'kiwi' ),
				'param_name' => 'mp_animation_duration',
				'description' => 'Decides the speed of the animation. (numeric only)',
				'value' => '', // default value		
				'edit_field_class' => 'vc_col-sm-6 vc_column',	
			),
			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Animation Delay', 'kiwi' ),
				'param_name' => 'mp_animation_delay',
				'description' => 'Delays the animation effect for x seconds. (numeric only)',
				'value' => '', // default value		
				'edit_field_class' => 'vc_col-sm-6 vc_column',	
			),
			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Offset', 'kiwi' ),
				'param_name' => 'mp_animation_viewport',
				'description' => 'Distance to start the animation from viewpoint (numeric only)',
				'value' => '', // default value		
				'edit_field_class' => 'vc_col-sm-6 vc_column',	
			),
			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Repeat', 'kiwi' ),
				'param_name' => 'mp_animation_iteration',
				'description' => 'Number of times the animation is repeated. (numeric only)',
				'value' => '', // default value		
				'edit_field_class' => 'vc_col-sm-6 vc_column',	
			),
			
            ),
		) 
	);
}
add_action( 'vc_before_init', 'VCExtendAddonClassAnimation' );


/**
 * The Shortcode
 */
function ebor_skills_shortcode( $atts, $content = null ) {
	
		extract( shortcode_atts( array(
			'output' => '',
			'mp_animation_effect' => '',
			'mp_animation_duration' => '',
			'mp_animation_delay' => '',
			'mp_animation_iteration' => '',
			'mp_animation_viewport' => '',
		), $atts ) );
	
		
		$output .= '<div class="wow '. esc_attr( $mp_animation_effect ).'" data-wow-duration="'.esc_attr( $mp_animation_duration).'s" data-wow-delay="'.esc_attr( $mp_animation_delay).'s" data-wow-offset="'.esc_attr( $mp_animation_viewport ).'" data-wow-iteration="'.esc_attr( $mp_animation_iteration ).'">'; 
        $output .= do_shortcode($content);
		$output .= '</div>';	
	
	return $output;
}
add_shortcode( 'kiwi_animation', 'ebor_skills_shortcode' );


// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_kiwi_animation extends WPBakeryShortCodesContainer {

    }
}



?>
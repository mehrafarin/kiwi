<?php
/*
This example/starter plugin can be used to speed up Visual Composer plugins creation process.
More information can be found here: http://kb.wpbakery.com/index.php?title=Category:Visual_Composer
*/

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCExtendAddonClassButton {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrateWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'kiwibutton', array( $this, 'renderMykiwibutton' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'loadCssAndJs' ) );
    }
 
    public function integrateWithVC() {
        // Check if Visual Composer is installed
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            // Display notice that Visual Compser is required
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }
 
        /*
        Add your Visual Composer logic here.
        Lets call vc_map function to "register" our custom shortcode within Visual Composer interface.

        More info: http://kb.wpbakery.com/index.php?title=Vc_map
        */
        vc_map( array(
            "name" => esc_html__("Kiwi :: Button", 'kiwi'),
            "description" => esc_html__("Create buttons.", 'kiwi'),
            "base" => "kiwibutton",
           // "class" => "",
           // "controls" => "full",		
            "icon" => "icon-kiwi-buttons",
			"category" => esc_html__('Theme :: Kiwi', 'kiwi'),
            'admin_enqueue_css' => array(get_template_directory_uri().'/framework/theme-options/vc-addons/kiwi-vc-addon.css'),
			"params" => array(
                array(
                  "type" => "textarea",
                  "holder" => "div",
                  "class" => "",
                  "heading" => esc_html__("Heading", 'kiwi'),
                  "param_name" => "block_textarea",
                 // "value" => esc_html__("Default params value", 'kiwi'),
                  "description" => esc_html__("Text that will be placed inside the button.", 'kiwi')
				),
				array(
				  'type' => 'vc_link',
				  'heading' => esc_html__( 'Link url', 'kiwi' ),
				  'param_name' => 'kiwi_button_url',
				  'value' => '',
				  //'description' => esc_html__( 'Append filter to grid.', 'kiwi' ),
				),
			
			// Design Settings
			
				array(        
					'type' => 'colorpicker',        
					'heading' => "Text color",        
					'param_name' => 'kiwi_button_fontcolor',        
					'value' => '',        
					'description' => esc_html__( " ", "kiwi" ) ,
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-4',		
				),
				
				array(        
					'type' => 'colorpicker',        
					'heading' => "Background color",        
					'param_name' => 'kiwi_button_bgcolor',        
					'value' => '',        
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-4 vc_column',		
				),
				
				 array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Make text bold?', 'kiwi' ),
					'param_name' => 'kiwi_button_textbold',
					//'description' => esc_html__( 'Allow items to be repeated in infinite loop (carousel).', 'kiwi' ),
					'value' => array( esc_html__( 'Yes, please.', 'kiwi' ) => 'yes' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-4',	
				), 
			
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Font size', 'kiwi' ),
					'param_name' => 'kiwi_button_fontsize',
					'value' => '',  
					'description' => esc_html__( 'Font size in px. (e.g., 25px)', 'kiwi' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
				
				array(        
					'type' => 'textfield',
					'heading' => esc_html__( 'Font family', 'kiwi' ),
					'param_name' => 'kiwi_button_fontfamily',
					'value' => '',
					'description' => esc_html__( 'Enter the name of the font you wish to use (e.g., Lato, Roboto, or Raleway)', 'kiwi' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6',
				),
				
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Line height', 'kiwi' ),
					'param_name' => 'kiwi_button_lineheight',
					'value' => '',  
					'description' => esc_html__( 'E.g.: 22px', 'kiwi' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',	
				),

				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Letter-spacing', 'kiwi' ),
					'param_name' => 'kiwi_button_spacing',
					'value' => '',
					'description' => esc_html__( 'Increases or decreases the space between characters in a text; e.g., 1px.', 'kiwi' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),		
				array(        
					'type' => 'dropdown',        
					'heading' => esc_html__( 'Font style', 'kiwi' ),        
					'param_name' => 'kiwi_button_fontstyle',        
					'value' => array( "normal", "italic" ),        
					'description' => esc_html__( " ", "kiwi" ) ,
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',	
				),		
				array(        
					'type' => 'dropdown',        
					'heading' => esc_html__( 'Text transform', 'kiwi' ),        
					'param_name' => 'kiwi_button_transform',        
					'value' => array( "normal", "uppercase", "lowercase" ),        
					'description' => esc_html__( " ", "kiwi" ) ,
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',	
				),	

				array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Button text alignment', 'kiwi' ),
						'param_name' => 'kiwi_button_alignment',
						'value'       => array(
							esc_html__( 'Select', 'kiwi' ) => '',
							esc_html__( 'Left', 'kiwi' ) => 'alignment-left',
							esc_html__( 'Right', 'kiwi' ) => 'alignment-right',
							esc_html__( 'center', 'kiwi' ) => 'alignment-center',				
							),			
						//'description' => 'Layout options for navigation.',	
						//'edit_field_class' => 'vc_col-sm-12 vc_column',
						'group' => esc_html__( 'Typography', 'kiwi' ),
						'edit_field_class' => 'vc_col-sm-6 vc_column',	
				),
			
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Button padding', 'kiwi' ),
					'param_name' => 'kiwi_button_padding',
					'value' => '', // default value
					//'param_holder_class' => 'vc_not-for-custom',
					'description' => esc_html__( 'E.g. 5px 10px 5px 10px', 'kiwi' ),
					//'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',
					'group' => esc_html__( 'Typography', 'kiwi' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
			
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Enable border?', 'kiwi' ),
					'param_name' => 'kiwi_button_border',
					//'description' => esc_html__( 'Allow items to be repeated in infinite loop (carousel).', 'kiwi' ),
					'value' => array( esc_html__( 'Yes, please.', 'kiwi' ) => 'yes' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',	
				),
				
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Enable rounded border?', 'kiwi' ),
					'param_name' => 'kiwi_button_rounded_border',
					//'description' => esc_html__( 'Allow items to be repeated in infinite loop (carousel).', 'kiwi' ),
					'value' => array( esc_html__( 'Yes, please.', 'kiwi' ) => 'yes' ),
					'group' => esc_html__( 'Typography', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',	
				),
			
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Border style', 'kiwi' ),
					'param_name' => 'kiwi_button_border_style',
					'value' => '', // default value
					//'param_holder_class' => 'vc_not-for-custom',
					'description' => esc_html__( 'E.g. border: 1px solid #000000', 'kiwi' ),
					//'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',
					'group' => esc_html__( 'Typography', 'kiwi' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'dependency' => Array('element' => 'kiwi_button_border', 'value' => 'yes' ),
				),
				
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Border radius', 'kiwi' ),
					'param_name' => 'kiwi_button_border_radius',
					'value' => '', // default value
					//'param_holder_class' => 'vc_not-for-custom',
					'description' => esc_html__( 'E.g. 5px', 'kiwi' ),
					//'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',
					'group' => esc_html__( 'Typography', 'kiwi' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'dependency' => Array('element' => 'kiwi_button_rounded_border', 'value' => 'yes' ),
				),
				
				array(        
					'type' => 'colorpicker',        
					'heading' => "Text color",        
					'param_name' => 'kiwi_button_hover_fontcolor',        
					'value' => '',        
					//'description' => esc_html__( "On hover.", "kiwi" ) ,
					'group' => esc_html__( 'Hover', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-12',		
				),
			
				
				array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Background transitions', 'kiwi' ),
						'param_name' => 'kiwi_button_bghover_effect',
						'value'       => array(
							esc_html__( 'Select', 'kiwi' ) => '',
							esc_html__( 'Fade', 'kiwi' ) => 'button-bg-effect-fade',
							//esc_html__( 'Back Pulse', 'kiwi' ) => 'button-bg-effect-pulse',
							esc_html__( 'Sweep To Right', 'kiwi' ) => 'button-bg-effect-sweep-right',
							esc_html__( 'Sweep To Left', 'kiwi' ) => 'button-bg-effect-sweep-left',
							esc_html__( 'Sweep To Bottom', 'kiwi' ) => 'button-bg-effect-sweep-bottom',
							esc_html__( 'Sweep To Top', 'kiwi' ) => 'button-bg-effect-sweep-top',
							//esc_html__( 'Bounce To Right', 'kiwi' ) => 'button-bg-effect-bounce-right',
							//esc_html__( 'Bounce To Left', 'kiwi' ) => 'button-bg-effect-bounce-left',
							//esc_html__( 'Bounce To Bottom', 'kiwi' ) => 'button-bg-effect-bounce-bottom',
							//esc_html__( 'Bounce To Top', 'kiwi' ) => 'button-bg-effect-bounce-top',							
							//esc_html__( 'Radial Out', 'kiwi' ) => 'button-bg-effect-radialout',
							//esc_html__( 'Radial In', 'kiwi' ) => 'button-bg-effect-radialin',
							esc_html__( 'Rectangle In', 'kiwi' ) => 'button-bg-effect-rectanglein',
							//esc_html__( 'Rectangle Out', 'kiwi' ) => 'button-bg-effect-rectanglout',
							//esc_html__( 'Shutter In Horizontal', 'kiwi' ) => 'button-bg-effect-shutter-in-hor',
							//esc_html__( 'Shutter Out Horizontal', 'kiwi' ) => 'button-bg-effect-shutter-out-hor',
							//esc_html__( 'Shutter In Vertical', 'kiwi' ) => 'button-bg-effect-shutter-in-ver',
							//esc_html__( 'Shutter Out Vertical', 'kiwi' ) => 'button-bg-effect-shutter-out-ver',
							),			
						//'description' => 'Layout options for navigation.',	
						//'edit_field_class' => 'vc_col-sm-12 vc_column',
						'group' => esc_html__( 'Hover', 'kiwi' ),
						'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',	
				),
				
				/* array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Border transitions', 'kiwi' ),
						'param_name' => 'kiwi_button_border_hover_effect',
						'value'       => array(
							esc_html__( 'Select', 'kiwi' ) => '',
							esc_html__( 'Fade', 'kiwi' ) => 'bg-effect-fade',
							esc_html__( 'Hollow', 'kiwi' ) => 'bg-effect-hollow',
							esc_html__( 'Trim', 'kiwi' ) => 'bg-effect-trim',
							esc_html__( 'Ripple Out', 'kiwi' ) => 'bg-effect-rippleout',
							esc_html__( 'Ripple In', 'kiwi' ) => 'bg-effect-ripplein',							
							esc_html__( 'Outline Out', 'kiwi' ) => 'bg-effect-outlineout',
							esc_html__( 'Outline In', 'kiwi' ) => 'bg-effect-outlinein',
							esc_html__( 'Round Corners', 'kiwi' ) => 'bg-effect-roundcorners',
							esc_html__( 'Underline From Left', 'kiwi' ) => 'bg-effect-underline-left',
							esc_html__( 'Underline From Center', 'kiwi' ) => 'bg-effect-underline-center',							
							esc_html__( 'Underline From Right', 'kiwi' ) => 'bg-effect-right',
							esc_html__( 'Reveal', 'kiwi' ) => 'bg-effect-reveal',
							esc_html__( 'Underline Reveal', 'kiwi' ) => 'bg-effect-underline-reveal',
							esc_html__( 'Overline Reveal', 'kiwi' ) => 'bg-effect-overline-reveal',
							esc_html__( 'Overline From Left', 'kiwi' ) => 'bg-effect-overline-left',
							esc_html__( 'Overline From Center', 'kiwi' ) => 'bg-effect-overline-center',
							esc_html__( 'Overline From Right', 'kiwi' ) => 'bg-effect-overline-right',
							),			
						//'description' => 'Layout options for navigation.',	
						//'edit_field_class' => 'vc_col-sm-12 vc_column',
						'group' => esc_html__( 'Hover', 'kiwi' ),
						'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',	
				), */
				
				
				
				array(        
					'type' => 'colorpicker',        
					'heading' => "Background color",        
					'param_name' => 'kiwi_button_hover_bgcolor',        
					'value' => '',        
					'description' => esc_html__( "On hover.", "kiwi" ) ,
					'group' => esc_html__( 'Hover', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column vc_column-with-padding',		
				),
			
				array(        
					'type' => 'colorpicker',        
					'heading' => "Border color",        
					'param_name' => 'kiwi_button_hover_border',        
					'value' => '',        
					'description' => esc_html__( "On hover.", "kiwi" ) ,
					'group' => esc_html__( 'Hover', 'kiwi' ),	
					'edit_field_class' => 'vc_col-sm-6 vc_column',		
				),
             
            )
        ) );
    }
    
    /*
    Shortcode logic how it should be rendered
    */
    public function renderMykiwibutton( $atts, $content = null ) {
      extract( shortcode_atts( array(
        'output' => '',
        'block_textarea' => '',
        'kiwi_button_url' => '',
        'kiwi_button_fontcolor' => '',
        'kiwi_button_bgcolor' => '',
        'kiwi_button_textbold' => '',
        'kiwi_button_fontsize' => '',
        'kiwi_button_fontfamily' => '',
        'kiwi_button_lineheight' => '',
        'kiwi_button_spacing' => '',
        'kiwi_button_fontstyle' => '',
        'kiwi_button_transform' => '',
        'kiwi_button_alignment' => '',
        'kiwi_button_padding' => '',
        'kiwi_button_border' => '',
        'kiwi_button_rounded_border' => '',
        'kiwi_button_border_style' => '',
        'kiwi_button_border_radius' => '',
        'kiwi_button_hover_fontcolor' => '',
        'kiwi_button_bghover_effect' => '',
      //  'kiwi_button_border_hover_effect' => '',
        'kiwi_button_hover_bgcolor' => '',
        'kiwi_button_hover_border' => '',
      ), $atts ) );
      $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
    
		$uniqid = uniqid('kiwi-button-');
	 
		if( isset( $kiwi_button_textbold ) && !empty ($kiwi_button_textbold)  ) {
			$value_button_bold = 'font-weight: bold;';
		} else {
			$value_button_bold = '';
		}
	 

		/* if( $kiwi_button_border_hover_effect == 'bg-effect-fade'   ) {
			$value_bgbr_effect = 'hvr-border-fade';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-hollow'   ) {
			$value_bgbr_effect = 'hvr-hollow';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-trim'   ) {
			$value_bgbr_effect = 'hvr-trim';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-rippleout'   ) {
			$value_bgbr_effect = 'hvr-ripple-out';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-ripplein'   ) {
			$value_bgbr_effect = 'hvr-ripple-in';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-outlineout'   ) {
			$value_bgbr_effect = 'hvr-outline-out';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-outlinein'   ) {
			$value_bgbr_effect = 'hvr-outline-in';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-roundcorners'   ) {
			$value_bgbr_effect = 'hvr-round-corners';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-underline-left'   ) {
			$value_bgbr_effect = 'hvr-underline-from-left';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-underline-center'   ) {
			$value_bgbr_effect = 'hvr-underline-from-center';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-right'   ) {
			$value_bgbr_effect = 'hvr-underline-from-right';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-reveal'   ) {
			$value_bgbr_effect = 'hvr-reveal';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-underline-reveal'   ) {
			$value_bgbr_effect = 'hvr-underline-reveal';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-overline-reveal'   ) {
			$value_bgbr_effect = 'hvr-overline-reveal';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-overline-left'   ) {
			$value_bgbr_effect = 'hvr-overline-from-left';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-overline-center'   ) {
			$value_bgbr_effect = 'hvr-overline-from-center';
		} elseif( $kiwi_button_border_hover_effect == 'bg-effect-overline-right'   ) {
			$value_bgbr_effect = 'hvr-overline-from-right';
		} elseif( $kiwi_button_border_hover_effect == '' ) {
			$value_bgbr_effect = '';
		} else {
			$value_bgbr_effect = '';
		} */
	 
	 
	 
		if( $kiwi_button_bghover_effect == 'button-bg-effect-fade'   ) {
			$value_bg_effect = 'hvr-fade';
		}  elseif( $kiwi_button_bghover_effect == 'button-bg-effect-sweep-right'   ) {
			$value_bg_effect = 'mp-button-ease hvr-sweep-to-right';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-sweep-left'   ) {
			$value_bg_effect = 'mp-button-ease hvr-sweep-to-left';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-sweep-bottom'   ) {
			$value_bg_effect = 'mp-button-ease hvr-sweep-to-bottom';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-sweep-top'   ) {
			$value_bg_effect = 'mp-button-ease hvr-sweep-to-top';			
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-rectanglein'   ) {
			$value_bg_effect = 'mp-button-ease hvr-rectangle-in';
		} elseif( $kiwi_button_bghover_effect == '' ) {
			$value_bg_effect = '';
		} else {
			$value_bg_effect = '';
		}
		
		/* elseif( $kiwi_button_bghover_effect == 'button-bg-effect-pulse'   ) {
			$value_bg_effect = 'hvr-back-pulse';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-bounce-right'   ) {
			$value_bg_effect = 'hvr-bounce-to-right';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-bounce-left'   ) {
			$value_bg_effect = 'hvr-bounce-to-left';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-bounce-bottom'   ) {
			$value_bg_effect = 'hvr-bounce-to-bottom';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-bounce-top'   ) {
			$value_bg_effect = 'hvr-bounce-to-top';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-radialout'   ) {
			$value_bg_effect = 'hvr-radial-out';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-radialin'   ) {
			$value_bg_effect = 'hvr-radial-in';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-rectanglout'   ) {
			$value_bg_effect = 'hvr-rectangle-out';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-shutter-in-hor'   ) {
			$value_bg_effect = 'hvr-shutter-in-horizontal';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-shutter-out-hor'   ) {
			$value_bg_effect = 'hvr-shutter-out-horizontal';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-shutter-in-ver'   ) {
			$value_bg_effect = 'hvr-shutter-in-vertical';
		} elseif( $kiwi_button_bghover_effect == 'button-bg-effect-shutter-out-ver'   ) {
			$value_bg_effect = 'hvr-shutter-out-vertical';
		} */ 
	 
		
		$css_button_hovereffects = '<style>
		
		.mp-button-ease, .hvr-fade {transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out; -webkit-transition:all .3s ease-in-out;}
		#'.$uniqid.' .hvr-sweep-to-right {box-shadow: inset 0 0 0 0 '.$kiwi_button_bgcolor.';}	
		#'.$uniqid.' .hvr-sweep-to-right:hover {box-shadow:250px 0 0 0 '.$kiwi_button_hover_bgcolor.' inset}
		#'.$uniqid.' .hvr-sweep-to-left {box-shadow: inset 0 0 0 0 '.$kiwi_button_bgcolor.';}
		#'.$uniqid.' .hvr-sweep-to-left:hover  {box-shadow:-250px 0 0 0 '.$kiwi_button_hover_bgcolor.' inset;}
		#'.$uniqid.' .hvr-sweep-to-bottom {box-shadow: inset 0 0 0 0 '.$kiwi_button_bgcolor.';}
		#'.$uniqid.' .hvr-sweep-to-bottom:hover  {box-shadow:0 100px 0 0 '.$kiwi_button_hover_bgcolor.' inset}
		#'.$uniqid.' .hvr-sweep-to-top {box-shadow: inset 0 0 0 0 '.$kiwi_button_bgcolor.';}
		#'.$uniqid.' .hvr-sweep-to-top:hover  {box-shadow:0 -100px 0 0 '.$kiwi_button_hover_bgcolor.' inset}			
		#'.$uniqid.' .hvr-rectangle-in {box-shadow: inset 0 0 0 0 '.$kiwi_button_bgcolor.';}
		#'.$uniqid.' .hvr-rectangle-in:hover  {box-shadow: 0 0 0 100px '.$kiwi_button_hover_bgcolor.' inset ;}
		#'.$uniqid.' .hvr-rectangle-out {box-shadow: inset 0 0 0 0 '.$kiwi_button_bgcolor.';}
		#'.$uniqid.' .hvr-rectangle-out:hover  {box-shadow: 0 0 0 100px '.$kiwi_button_hover_bgcolor.' inset;}
			</style>';
		
		 $css_button_style = '<style>
		#'.$uniqid.' a {
			color:'.$kiwi_button_fontcolor.';
			background:'.$kiwi_button_bgcolor.';
			'.$value_button_bold.'
			font-size:'.$kiwi_button_fontsize.';
			font-family:'.$kiwi_button_fontfamily.';
			line-height:'.$kiwi_button_lineheight.';
			letter-spacing:'.$kiwi_button_spacing.';
			font-style:'.$kiwi_button_fontstyle.';
			text-transform:'.$kiwi_button_transform.';
			padding:'.$kiwi_button_padding.';
			border:'.$kiwi_button_border_style.';
			border-radius:'.$kiwi_button_border_radius.';-webkit-border-radius:'.$kiwi_button_border_radius.';-moz-border-radius:'.$kiwi_button_border_radius.';
		}	
		#'.$uniqid.' a:hover {
			color:'.$kiwi_button_hover_fontcolor.';
			background:'.$kiwi_button_hover_bgcolor.';
			border-color:'.$kiwi_button_hover_border.';
		}
		
			</style>';
			
		$output .= $css_button_style;
		$output .= $css_button_hovereffects;	
	 
        $link = '';
		if($kiwi_button_url != '' ){
				$href = vc_build_link($kiwi_button_url);
				$link = $href['url'];
				
				if(isset($href['target'])){
					$target = 'target="'.$href['target'].'"';
				}
				
			$output .= '<div id="'. esc_attr( $uniqid ).'" class="mp-vc-button '.esc_attr( $kiwi_button_alignment ).'"><a class="'. esc_attr( $value_bg_effect ).'" '.$target.' href="'. esc_url( $link ).'">'. esc_textarea( $block_textarea ) .'</a></div>';
							
			} else {
				
				$output .= '';
		}	
	  
	  
      return $output;
    }

    /*
    Load plugin css and javascript files which you may need on front end of your site
    */
    public function loadCssAndJs() {
     // wp_register_style( 'vc_extend_style', plugins_url('assets/vc_extend.css', __FILE__) );
    // wp_enqueue_style( 'hover-css', get_template_directory_uri() . '/css/hover.css' );

      // If you need any javascript files on front end, here is how you can load them.
      //wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
    }

    /*
    Show notice if your plugin is activated but Visual Composer is not
    */
    public function showVcVersionNotice() {
        $plugin_data = get_plugin_data(__FILE__);
        echo '
        <div class="updated">
          <p>'.sprintf(esc_html__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'kiwi'), $plugin_data['Name']).'</p>
        </div>';
    }
}
// Finally initialize code
new VCExtendAddonClassButton();
<?php 
/* Template Name: FES :: Vendor */  

get_header();

global $kiwi_theme_option; 

$author = get_user_by( 'slug', get_query_var( 'vendor' ) );

if ( !$author ) {
	$url = $_SERVER['REQUEST_URI'];
	$page_id = EDD_FES()->helper->get_option( 'fes-vendor-page', false );
	$page = get_page( $page_id );
	$page_name = ! empty( $page->post_name ) ? $page->post_name : EDD_FES()->vendors->get_vendor_constant_name( false, false );
	$permalink = '/'. trailingslashit( $page_name ) ;
	
	if ( !empty ( $_SERVER['REQUEST_URI'] ) && !empty( $url ) ){
		$string = str_replace( $permalink, '', $url );
		$author = get_user_by( 'slug', untrailingslashit ( $string ) );
	}
}

if ( ! $author ) {
	$author = get_current_user_id();
} 

 $img_icon_portfolio 		= get_template_directory_uri() . '/framework/theme-options/images/mp-icon-products-black.png';
 $img_icon_sales 			= get_template_directory_uri() . '/framework/theme-options/images/mp-icon-earnings-black.png'; 
 $portfolio 				= EDD_FES()->vendors->get_all_products_count($author->ID, 'publish'); 
 
 $website_url 				= $author->user_url; 
 
 $vendor_standard 			= esc_html__( 'Vendor Page', 'kiwi' );
 $vendor_store 				= get_the_author_meta( 'store_name', $author->ID );		
 $vendor_subtitle 			= get_the_author_meta( 'subtitle_store', $author->ID );
 
 $vendor_custom_image 		= get_the_author_meta( 'store_image_upload', $author->ID  );
 
	if ( !empty( $vendor_custom_image ) ) { 
		$table 	= ' mp-table';
		$cell 	= ' mp-table-cell'; 
	} else {
		$table 	= '';
		$cell 	= '';
	}
 
 
    $products = EDD_FES()->vendors->get_all_products($author->ID);
	$sales = 0;
	$earnings = 0;

	if (!empty($products)) { 		
		foreach ($products as $product) : 			 
			$sales+= $product['sales']; 		
		endforeach; 	 
	} 
?>
	
<div class="container-fluid mp-fes-fullwidth marketplace"<?php esc_attr( kiwi_rtl() ); ?>>	
	<div class="row">

		<div class="container-fluid mp-fes-vendor-menu vendor-page">
			<div class="container align-center<?php echo esc_attr( $table ); ?>"> 
			<div class="row<?php echo esc_attr( $cell ); ?>">	
				
					<?php if ( class_exists( 'EDD_Front_End_Submissions' ) ) { 
		
							if ( !empty( $vendor_store ) ) { 
								echo '<h2>'. esc_html( $vendor_store ).'</h2>';
							}	
							
							if (!empty( $vendor_subtitle ) ) { 	
								echo '<h5 class="mp-vendor-subtitle">'. esc_html( $vendor_subtitle ).'</h5>';
							}
							
						} else {
								echo '<h3>'. esc_html( $vendor_standard ).'</h3>';
						}
	
					?>						

			</div>
			</div>
		</div>
			
		
		
<div class="container mp-full-dashboard mp-vendor-page"> 
	<div class="row">
	  <div class="col-md-6 col-sm-12 mp-vendor-avatar">
	  
		<div class="author-avatar">
			<?php echo get_avatar( $author->ID, 100 ); ?>
		</div>
			<h3 class="author-name"><?php echo esc_attr( $author->display_name ); ?> <?php if ( class_exists( 'EDD_Reviews' ) ) { mp_vendor_review_feedback_avarage( $author->ID ); } ?></h3>
			<div class="vendor-desc"><?php echo esc_attr( $author->description ); ?></div>
	  
	  </div>
	  
	  <div class="clearfix visible-xs"></div>
	  <div class="col-md-6 col-sm-12 mp-vendor-stats">
	  
		<div class="row">
		
			<div class="col-md-3 col-sm-3 col-xs-6 align-center">
						<img src="<?php echo esc_url( $img_icon_portfolio ); ?>"><br />
						<div class="text-subline"><?php echo sprintf( _n( '%s item', '%s items', $portfolio, 'kiwi' ), $portfolio ); ?></span></div>

						
						
			</div>
		  
			<div class="col-md-3 col-sm-3 col-xs-6 align-center">  
						<img src="<?php echo esc_url( $img_icon_sales ); ?>"><br />
						<div class="text-subline"><?php echo sprintf( _n( '%s sale', '%s sales', $sales, 'kiwi' ), $sales ); ?></span>
			</div>		
						
		</div>
		  
		  <div class="col-md-6 col-sm-6 col-xs-12 mp-link-buttons">
			<a href="#" class="mp-link mp-vendor-send-message" data-toggle="modal" data-target="#myModal"> <span><?php esc_html_e( 'Contact', 'kiwi' ); ?></span></a>
			<a href="<?php echo esc_url( $website_url ); ?>" class="mp-link mp-vendor-website" target="_blank"> <span><?php esc_html_e( 'Website', 'kiwi' ); ?></span></a>
		  </div>
		  
		</div>
	  
	  </div>
	</div>	
		
	<?php do_action( 'kiwi_hook_author_before_portfolio' ); ?>
	
<div class="mp-vendor-portfolio mp-vc-items">			
								
	<h4><?php esc_html_e( 'Portfolio', 'kiwi' ); ?></h4>
					
		<?php if ( has_filter( 'the_content', 'edd_coming_soon_single_download' ) )
				remove_filter( 'the_content', 'edd_coming_soon_single_download' );
		?>
				
		<?php
		
			$store_page_setting = (is_front_page() && is_page_template('page-marketplace.php') ? 'page' : 'paged' );
			$current_page = get_query_var( $store_page_setting );
			$per_page = $kiwi_theme_option['marketplace-fes-postcount'];
			$offset = $current_page > 0 ? $per_page * ( $current_page-1 ) : 0;
			$product_args = array(
				'post_type' 	 => 'download',
				'posts_per_page' => $per_page,
				'post_status'    => 'publish',
				'offset'		 => $offset,
				'author' 		 => $author->ID
			);
			$products = new WP_Query( $product_args );			
						
			$grid_columns = $kiwi_theme_option['marketplace-fes-gridcolumns']; 
	
				if ($grid_columns == '1') { 
					$variable_width = ' two-columns'; 
				} elseif ($grid_columns == '2') { 
					$variable_width = ' three-columns'; 
				} elseif ($grid_columns == '3') { 
					$variable_width = ' four-columns'; 
				} else {
					$variable_width = '';
				}				
		?>
			
			<?php $have_results = $products->have_posts();					
				if (!empty( $have_results) && $products->have_posts()) : $i = 1; ?>
	
		<div class="grid-view<?php echo esc_attr( $variable_width ); ?>">
		
		<ul class="layout-sorting">	
			
			<?php while ($products->have_posts()) : $products->the_post(); 
		
		
		
		echo '<li>';
		
		if ( $kiwi_theme_option['marketplace-enable-equalheights'] == '1' ) { 
			echo '<div class="mp-equal-heights">';
		} 
		 
		
		echo  '<div class="iso-thumbnail">';		
		
			if( function_exists('mp_audio_video_featured')){ mp_audio_video_featured(); } 
		 
		echo  '<div class="imageoverlay"></div>';
		
		
		if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) {					
			$coming_soon_cs_active = edd_coming_soon_is_active( get_the_ID() );					
			$coming_soon_custom_text = get_post_meta( get_the_ID(), 'edd_coming_soon_text', true );
			$coming_soon_custom_text = !empty ( $coming_soon_custom_text ) ? $coming_soon_custom_text : apply_filters( 'edd_cs_coming_soon_text', esc_html__( 'Coming Soon', 'kiwi' ) );				
		} 
				
		if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && edd_coming_soon_is_active() ) {
			$variable_class = '<span class="cart mp-comingsoon-class"><i class="fa fa-clock-o"></i></span>';			
		} else {
			$link = edd_get_purchase_link( array( 'textafter' => '<i class="fa fa-sign-out"></i>','text' => '<i class="fa fa-cart-arrow-down"></i>','price' => '0','download_id' => get_the_ID() ) );
			$variable_class = '<span class="cart">'. $link.'</span>';
		}
		
		
		echo  '<div class="mp-button styleblock"><span class="viewmore"><a href="'. get_permalink().'"><i class="fa fa-eye"></i></a></span>';
		echo  $variable_class;
		echo  '</div>';
		
		echo  '</div>';
		
		echo  '<div class="iso-desc div-flex">';		
		
		echo  '<div class="left-column align-left truncate one-line">';
				
		echo  '<h4><a href="'. get_permalink().'">'.get_the_title().'</a></h4>';		
		echo  '</div>';
		
		
		echo  '<div class="right-column align-right">';
		
			if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && $coming_soon_cs_active ) {			
				
			$price = apply_filters( 'edd_coming_soon_display_text', '<p><strong>' . $coming_soon_custom_text . '</strong></p>' );
		
		echo  sprintf( '<span class="item-price soon">%s</span>', $price );
		
		} else {
			
			remove_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_get_custom_status_text', 30, 2 );
			
			$price = get_post_meta( get_the_ID(), 'edd_price', true );
					
					if ( $price == 0 && $kiwi_theme_option['marketplace-free-enable'] == '1' && !edd_has_variable_prices( get_the_ID() ) ) {
						echo '<span class="price free">';
						$price = $kiwi_theme_option['marketplace-free-customtext'];
						echo '</span>';
					} elseif ( edd_has_variable_prices( get_the_ID() ) ) {
						 $price = edd_price_range( get_the_ID() );
					} else {
						$price = edd_price( get_the_ID(), false );
					}	
					
				echo  sprintf( '<span class="item-price">%s</span>', $price );	
			
			}
				
		echo  '</div>';
		echo  '<div class="clear"></div>';
		echo  '</div>';
		
		echo  '<div class="mp_author">';
		
		$sales = edd_get_download_sales_stats( get_the_ID() );
		
			echo  '<span class="byline"><span class="author vcard">';
				echo sprintf( _n( '%s sale', '%s sales', $sales, 'kiwi' ), $sales );				
			echo  '</span></span>';
				
				
		if ( class_exists( 'EDD_Reviews' ) ) {		
			echo  '<div class="right-column align-right reviews-stylethree">';			
				mp_extension_review_function_core();			
			echo  '</div>'; 
		}	
			
		echo  '</div>';			
		echo  '<div class="clear"></div>';
		
		if ( $kiwi_theme_option['marketplace-enable-equalheights'] == '1' ) { 
			echo '</div>';
		} 
				
		echo '</li>';
		
			endwhile; 
		
		echo '</ul>';
		
		echo  '<div class="clear"></div>';
		echo  '</div>';	
		
		?>
				
				<div class="pagination kiwi-pagination">
					<?php 					
						$big = 999999999; 			
						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, $current_page ),
							'total' => $products->max_num_pages
						) );
					?>
				</div>
				
			<?php else : ?>		
				<p class="center"><?php esc_html_e( 'Sorry, no items have been uploaded.' , 'kiwi' ); ?></p>		
			<?php endif; ?>		
				
</div>			

	
</div>

	</div>
</div>	
	

<!-- -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="table">
        <div class="table-cell">
		  <div class="modal-dialog-center" role="document">
			<div class="modal-content">
			  
			  <div class="modal-body">
				<?php echo do_shortcode( '[fes_vendor_contact_form id="' . $author->ID . '"]' ); ?>
			  </div>
			
			</div>
		  </div>
		 </div>
	 </div>	 
</div>
<!-- -->			
			
<?php get_footer(); ?>
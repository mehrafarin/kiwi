<?php global $kiwi_theme_option; ?>

<?php if ( is_page_template( 'page-blank.php' ) ) : ?>
	<!-- no footer output -->
<?php else: ?>
				</div>
</div>


<?php 
	
$footer_reg_widget = $kiwi_theme_option['footer-columns']; 
		
if($footer_reg_widget == '1'){ $variable_width_one =''; $variable_width_two =''; $variable_width_three =''; $variable_width_four =''; $variable_width = 'col-xs-12 col-sm-12 col-md-12 col-lg-12'; } 
	elseif($footer_reg_widget == '2'){ $variable_width_one =''; $variable_width_two =''; $variable_width_three =''; $variable_width_four =''; $variable_width = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';} 
	elseif($footer_reg_widget == '3'){ $variable_width_one =''; $variable_width_two =''; $variable_width_three =''; $variable_width_four =''; $variable_width = 'col-xs-12 col-sm-6 col-md-4 col-lg-4'; } 
	elseif($footer_reg_widget == '4'){ $variable_width_one =''; $variable_width_two =''; $variable_width_three =''; $variable_width_four =''; $variable_width = 'col-xs-12 col-sm-6 col-md-3 col-lg-3'; } 
	elseif($footer_reg_widget == '5'){ $variable_width_one =''; $variable_width_two =''; $variable_width_three =''; $variable_width_four =''; $variable_width = 'col-xs-12 col-sm-6 col-md-5 col-lg-5'; } 
	elseif($footer_reg_widget == '6'){ $variable_width_one =''; $variable_width_two =''; $variable_width_three =''; $variable_width_four =''; $variable_width = 'col-xs-12 col-sm-6 col-md-2 col-lg-2'; } 
	elseif($footer_reg_widget == '7'){ $variable_width_one = ' col-xs-12 col-sm-6 col-md-4'; $variable_width_two = ' col-xs-12 col-sm-6 col-md-2'; $variable_width_three = ' col-xs-12 col-sm-6 col-md-2'; $variable_width_four = ' col-xs-12 col-sm-6 col-md-4'; $variable_width = '';} 
	else { $variable_width = ''; $variable_width_one = ''; $variable_width_two = ''; $variable_width_three = '';	$variable_width_four = '';}	
					
?>

<?php 
$searching = '';
if( $kiwi_theme_option['header-expandable-search'] == '1' || $kiwi_theme_option['header-expandable-search-topbar'] == '1' || isset ($kiwi_theme_option['marketplace-userrole-blocks-vendorlogged']['enabled']['searching'] )|| isset ($kiwi_theme_option['marketplace-userrole-blocks-buyerlogged']['enabled']['searching'] ) || isset ($kiwi_theme_option['marketplace-userrole-blocks-nonlogged']['enabled']['searching'] ) ){ ?>
	<?php if ( is_rtl() ) {
		echo '<div class="overlay overlay-data" dir="rtl">';
	} else {
		echo '<div class="overlay overlay-data">';
	} ?>
	
		<div type="button" class="overlay-close"></div>
			<div class="site-search">
				<div class="site-search-inner">
					<?php the_widget( 'WP_Widget_Search', 'title=' ); ?>
				</div>
			</div>
        </div>
<?php } ?>

<?php do_action( 'kiwi_footer_before' ); ?>
<div class="footer-combined"<?php esc_attr( kiwi_rtl() ); ?>>

<?php if ( $kiwi_theme_option['footer-enable'] == '1' ) { ?>
<div class="footer">
	<div class="container">
	
		<?php do_action( 'kiwi_footer_start' ); ?>
		
		<div class="row">	
					
				<?php if($footer_reg_widget >= '1'): ?>
					<div class="<?php echo esc_attr( $variable_width_one ); ?><?php echo esc_attr( $variable_width ); ?>">			
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 1')):
							endif; ?>					
					</div>
					
				<?php endif; ?>
				
				<?php if($footer_reg_widget >= '2'): ?>
					<div class="<?php echo esc_attr( $variable_width_two ); ?><?php echo esc_attr( $variable_width ); ?>">			
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 2')):
							endif; ?>					
					</div>
					
				<?php endif; ?>
				
				<?php if($footer_reg_widget >= '3'): ?>
					<div class="<?php echo esc_attr( $variable_width_three ); ?><?php echo esc_attr( $variable_width ); ?>">			
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 3')):
							endif; ?>					
					</div>
					
				<?php endif; ?>
				
				<?php if($footer_reg_widget >= '4'): ?>
					<div class="<?php echo esc_attr( $variable_width_four ); ?><?php echo esc_attr( $variable_width ); ?>">			
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 4')):
								endif; ?>					
					</div>
					
				<?php endif; ?>
					
						
				
				<?php if($footer_reg_widget >= '5' && $footer_reg_widget != '7'): ?>
					<div class="<?php echo esc_attr( $variable_width ); ?>">		
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 5')):
								endif; ?>					
					</div>
					
				<?php endif; ?>
						
				
				<?php if($footer_reg_widget >= '6' && $footer_reg_widget != '7'): ?>
					<div class="<?php echo esc_attr( $variable_width ); ?>">			
						<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 6')):
								endif; ?>				
					</div>
					
				<?php endif; ?>
		
		</div>
		<?php do_action( 'kiwi_footer_end' ); ?>
	</div>
	
</div>
<?php } ?>


<?php if ( $kiwi_theme_option['footer-copyright-enable'] == '1' ) { ?>
<div class="copyright">
	<div class="container">
		
		<div class="row">	
		
		<?php if($kiwi_theme_option['footer-copyright-enableborder'] == '1') { ?>
			<div class="copyright-border"></div>
		<?php } ?>
		
		<?php do_action( 'kiwi_copyright_start' ); ?>
		
		<div class="copyright-message">
		
			<?php if ( $kiwi_theme_option['footer-copyright-enableprefix'] == '1' ) { ?>
				<?php 				
					$blogname = date('Y') . ' ' . get_bloginfo('name');				
					printf( esc_attr__( '&copy; %s', 'kiwi' ), esc_html( $blogname ) );
				?>
			<?php } ?>				
						
			<?php echo '' . $kiwi_theme_option['footer-copyright-message']; ?>
				
		</div>	
		
		<!-- -->	
			
		<?php if ( $kiwi_theme_option['footer-contentright'] == '1' ) { ?>	
			<?php if ( has_nav_menu( 'top_navigation' ) ) { ?>
				<div class="pull-right" style="margin:<?php echo '' . $kiwi_theme_option['footer-contentright-customtextmargin'];	?>">			 
				<?php if ( has_nav_menu( 'footer_navigation' ) ) { ?> 	
					<?php wp_nav_menu(array('theme_location' => 'footer_navigation',  'container' => false,  'menu_class' => 'footer-menu', 'walker' => new wp_bootstrap_navwalker())); ?>
				<?php } ?>	
				</div>
			<?php } ?>		
		<?php } ?>		
		
		
		<?php if ( $kiwi_theme_option['footer-contentright'] == '2' ) { ?>				 
				<span class="pull-right" style="margin:<?php echo $kiwi_theme_option['footer-contentright-customtextmargin']; ?>">				
					<?php do_action('kiwi_social_media'); ?>			
				</span>
		<?php } ?>
		
		
		<?php if ( $kiwi_theme_option['footer-contentright'] == '3' ) { ?>	
				<span class="pull-right" style="margin:<?php echo '' . $kiwi_theme_option['footer-contentright-customtextmargin'];	?>">			 
					<?php echo '' . $kiwi_theme_option['footer-contentright-customtext'];	?>
				</span>	
		<?php } ?>
		
		
		<?php if ( $kiwi_theme_option['footer-contentright'] == '4' ) { ?>	
		<?php } ?>
		
		
		
		<?php if ( $kiwi_theme_option['footer-contentright'] == '5'  ) { 		
			if ( class_exists( 'Easy_Digital_Downloads' ) ) {
			echo '<span class="pull-right edd-stats" style="margin:' . $kiwi_theme_option['footer-contentright-customtextmargin'].'">';		
				
				if( function_exists('kiwi_marketplace_stats')){ kiwi_marketplace_stats(); }
				
			echo '</span>';	
			} 		
		} ?>
		
			
		</div>	
			<?php do_action( 'kiwi_copyright_end' ); ?>
	</div>
	
</div>
<?php } ?>
		
</div>


	<?php if ( $kiwi_theme_option['miscellaneous-scrolltotop'] == '1' ) { ?>
		<div id="backtotop"><i class="fa fa-arrow-up"></i></div>
	<?php } ?>

<?php if(!empty($kiwi_theme_option['settings-sitewidth']) && $kiwi_theme_option['settings-sitelayout'] == '1' ) { ?>
	</div>		
<?php } ?>
	<!--[if lte IE 8]>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
	<![endif]-->
	
	
			<?php endif; ?>	
			

<?php if(!empty($kiwi_theme_option['settings-bodytag'])) echo $kiwi_theme_option['settings-bodytag']; ?>
<?php if(!empty($kiwi_theme_option['settings-googletracking'])) echo $kiwi_theme_option['settings-googletracking']; ?>

<?php get_template_part( 'templates/kiwi', 'scripts' ); ?>

<?php wp_footer(); ?>

<?php do_action( 'kiwi_body_end' ); ?>

</body>
</html>
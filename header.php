<?php global $kiwi_theme_option; ?>

<!doctype html>

	<?php if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) { ?>
			<html>	
	<?php } else { ?>
			<html <?php language_attributes(); ?>>
	<?php } ?>	

<head itemscope itemtype="http://schema.org/WebSite">
<title itemprop='name'><?php echo wp_get_document_title(); ?></title>
<!-----TAKEXPERT start changelog--->
<!--<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/fonts/wpyar-fonts/fonts.css" />-->
<!-----TAKEXPERT end changelog--->
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta charset="<?php esc_html( bloginfo( 'charset' ) ); ?>" />

<?php if ( $kiwi_theme_option['settings-pageloader-enable'] == '1' ) { ?>			
	<script type="text/javascript" src="<?php echo( get_template_directory_uri() . '/js/pace.min.js'); ?>"></script>
	<link rel="stylesheet" id="pageloader-css" href="<?php echo( esc_url( get_template_directory_uri() . '/css/pageloader.css') ); ?>" type="text/css" media="all">	
<?php } ?>

<?php wp_head(); ?>

<?php get_template_part( 'templates/head', 'styles' ); ?>

<?php if(!empty($kiwi_theme_option['settings-favicon'])) { ?>
	<link rel="shortcut icon" href="<?php echo esc_url( $kiwi_theme_option['settings-favicon']['url'] ); ?>" />
<?php } ?>

<?php if(!empty($kiwi_theme_option['settings-headtag'])) echo $kiwi_theme_option['settings-headtag']; ?>

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php echo( get_template_directory_uri() . '/css/ie.css'); ?>" />
<![endif]-->
</head>

	<?php if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) { ?>			
		<body <?php body_class('rtl'); ?>>			
	<?php } else { ?>
		<body <?php body_class(); ?>>		
	<?php } ?>

<?php do_action( 'kiwi_body_start' ); ?>
				
		<?php if ( is_page_template( 'page-blank.php' ) ) : ?>
			<!-- no header output -->
		<?php else: ?>	
				
<?php do_action( 'kiwi_pgcontainer_bedore' ); ?>

<?php if(!empty($kiwi_theme_option['settings-sitewidth']) && $kiwi_theme_option['settings-sitelayout'] == '1' ) { ?>	
		<div class="page-container">	
<?php } ?>


<?php if(!empty($kiwi_theme_option['settings-sitewidth']) && $kiwi_theme_option['settings-sitelayout'] == '2' && !is_page_template( 'page-fullwidth.php' ) ) { ?>
	<div class="page-container boxed">
<?php } ?>


<?php do_action( 'kiwi_combined_before' ); ?>

<div class="mp-menu-combined"<?php esc_attr( kiwi_rtl() ); ?>>

<?php do_action( 'kiwi_combined_start' ); ?>

	<?php do_action( 'kiwi_topbar_before' ); ?>

		<?php if ( $kiwi_theme_option['topbar-enable'] == '1' && $kiwi_theme_option['topbar-position'] == '1' ) { ?>
			<?php get_template_part( 'templates/topbar', 'main' ); ?>		
		<?php } ?>
		
	<?php do_action( 'kiwi_topbar_after' ); ?>

			
	<?php do_action( 'kiwi_header_before' ); ?>

		<?php if ( $kiwi_theme_option['header-layoutstyle'] <= '2' ) { ?>
			<?php get_template_part( 'templates/header', 'main' ); ?>	
		<?php } ?>

	<?php do_action( 'kiwi_header_after' ); ?>


	<?php do_action( 'kiwi_topbar_two_before' ); ?>

		<?php if ( $kiwi_theme_option['topbar-enable'] == '1' && $kiwi_theme_option['topbar-position'] == '2' ) { ?>	
			<?php get_template_part( 'templates/topbar', 'main' ); ?>			
		<?php } ?>
		
	<?php do_action( 'kiwi_topbar_two_after' ); ?>

<?php do_action( 'kiwi_combined_end' ); ?>

</div>

<?php do_action( 'kiwi_combined_after' ); ?>


<?php do_action( 'kiwi_pagetitle_before' ); ?>

	
		<?php if ( $kiwi_theme_option['pagetitle-enable'] == '1' && ! is_page_template( 'page-fesdashboard.php' ) && ! is_page_template( 'page-vendor.php' ) ) { ?>
			<?php get_template_part( 'templates/pagetitle', 'main' ); ?>
		<?php } ?>

		
<?php do_action( 'kiwi_pagetitle_after' ); ?>

		
	<?php if ( is_page_template( 'page-fullwidth.php' ) ) {
			echo '<div class="container page-fullwidth">';
		} elseif( is_page_template( 'page-fesdashboard.php' ) || is_page_template( 'page-vendor.php' ) ) {
			echo '<div class="container-fluid mp-fes-fullwidth"';
			echo esc_attr( kiwi_rtl() );
			echo '>';
			} elseif( $kiwi_theme_option['marketplace-container-itemdesc'] == '1' && is_singular( 'download' ) ) {
			echo '<div class="kiwi-content mp-itemdescription">';
		} elseif( $kiwi_theme_option['marketplace-container-itemdesc'] == '0' && is_singular( 'download' ) ) {
			echo '<div class="container kiwi-content mp-itemdescription">';
		} else {
			echo '<div class="container kiwi-content">';
		} 
	?>		
	
	<?php if ( $kiwi_theme_option['marketplace-container-itemdesc'] == '1' && is_singular( 'download' ) ) {
			echo '<div>';
		} else {
			echo '<div class="row">';
		} 
	?>	
	
	<?php do_action( 'kiwi_pagecontent_before' ); ?>
		
			<?php endif; ?>

<?php 

	global $kiwi_theme_option, $post; 

	$featured 	= get_post_thumbnail_id();
	$background = '';	
	
	if (empty($featured)) {
		$fimage = ' no-featuredimage';
	} else {
		$fimage = '';
	} 				 
 
	if (has_post_thumbnail( $post->ID ) ):
		$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$background = 'url('. esc_url( $image_attributes[0] ).')'; 
	else:
		$image_attributes = array( ' ' ); 
		$background = '#12b0a5';
	endif;
	
?>	
			
<?php if ( !is_single() ) { ?>
	<article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php } ?>

<?php if ( is_single() ) { ?>
	<div itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php } ?>

	<div class="status<?php echo esc_attr( $fimage ); ?>">

		<div class="entry-content">
			<div class="featured-block">
		
			<div class="imageoverlay"></div>
			<div class="featured-image" style="background:<?php echo esc_attr( $background ); ?>"></div>
		
				<div class="entry">
					<div class="avatar"><?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?></div>
					
						<span class="author"><?php the_author_link(); ?></span>	
						
						<?php if ( $kiwi_theme_option['postformat-statusdate'] == '1' && !is_single() ) { ?>
							<span itemprop="datePublished"><span itemprop="datePublished"><?php echo esc_attr( get_the_date( get_option('date_format') ) ); ?></span></span>
						<?php } ?>
						
						<?php if ( $kiwi_theme_option['blog-editpostlink-enable'] == '1' && !is_single() ) { ?>
							<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="edit-link">', '</span>' ); ?>
						<?php } ?>
					<div class="clear"></div>				
				</div>
		
				<div class="wrap">    
					<div class="arrow"></div>
				</div>
		
			</div>
		
			<?php if ( !is_single() ) { ?> 
				<div class="content-message">
					<div itemprop="articleBody">
						<?php the_content(); ?>
					</div>
				</div>
			<?php } ?>
					
		</div>	
						
			<?php if ( is_single() ) { ?>
				<?php kiwi_singlebottom_meta(); ?>
			<?php } ?>
						
	</div>

	
<?php if ( !is_single() ) { ?>
	</article>
<?php } ?>

<?php if ( is_single() ) { ?>
	</div>
<?php } ?>	

<?php if ( $kiwi_theme_option['blog-authorbio-enable'] == '1' && is_single() || is_multi_author() && is_single() ) : ?>
			<?php if ( get_the_author_meta( 'description' )  ) : ?>
				<?php get_template_part( 'templates/author', 'bio' ); ?>
			<?php endif; ?>
<?php endif; ?>	
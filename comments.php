<?php
if ( post_password_required() )
	return;

if (empty_content($post->post_content)) { 
	$div_class = ' no-mp-content';
} else {
	$div_class = '';
}
?>

<?php if ( ! comments_open() && get_comments_number() ) { ?>

	<!--<p class="nocomments"><?php esc_html_e( 'Comments are closed.' , 'kiwi' ); ?></p>-->
	
<?php } else { ?>

	<div class="marketplace-commentform<?php echo esc_attr( $div_class ); ?>">
		<div id="comments" class="comments-area">

			<?php if ( have_comments() ) : ?>
			
					<?php if ( is_singular( 'download' ) ) { ?>
						<h3 class="mp-edd-comment"><span><?php esc_html_e( 'Comments' , 'kiwi' ); ?><span></h3>
					<?php } else { ?>
						<h2 class="comments-title"><?php printf( _n( 'One comment', '%1$s comments', get_comments_number(), 'kiwi' ), esc_html( number_format_i18n( get_comments_number() ) ), '<span>' . get_the_title() . '</span>' ); ?></h2>
					<?php } ?>
			
				
				<ol class="commentlist none">
					<?php if ( is_singular( 'download' ) ) {
						wp_list_comments( array( 'callback' => 'kiwi_comment_marketplace', 'style' => 'ol' ) );
					} else {
						wp_list_comments( array( 'callback' => 'kiwi_comment', 'style' => 'ol' ) );
					}
					?>
				</ol>

				<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>			
					 <nav id="comment-nav-below" class="navigation kiwi-pagination" role="navigation">					
						<?php paginate_comments_links(); ?> 					
					</nav> 				
				<?php endif; ?>
							
			<?php endif; ?>

			<?php comment_form(); ?>

		</div>
	</div>	

<?php } ?>
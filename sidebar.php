<?php if ( is_active_sidebar( 'post-sidebar' ) ) : ?>
	<div class="sidebar">
		<?php dynamic_sidebar( 'post-sidebar' ); ?>
	</div>
<?php endif; ?>
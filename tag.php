<?php get_header(); ?>

<?php global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-archive-enable'] == '0') { ?>

		<div class="container">
			<div class="row" role="main"<?php esc_attr( kiwi_rtl() ); ?>>
		
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1'
				&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 archive-layout">				
			<?php } ?>					
				
					<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1' 
					&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 archive-layout" style="float:right!important">				
					<?php } ?>
								
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
			&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 archive-layout">
			<?php } ?>
			
					<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
					&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-6" style="float:right!important">
					<?php } ?>
			
			
			
			<div class="archive-page">
				<?php if ( $kiwi_theme_option['page-pagetitle-tag-enable'] == '1' ) { ?>
				<h3><?php printf( esc_html__( 'Tag Archives: %s', 'kiwi' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h3>
				<?php } ?>
			</div>
			
			<?php get_template_part( 'templates/layout', 'settings' ); ?>

					</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif; ?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif; ?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 sidebar-layout blog">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
							endif; ?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>


<!-- -->
<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 sidebar-layout blog">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 archive-layout">			
			<?php get_template_part( 'templates/layout', 'settings' ); ?>			
	</div>
	
	
	<div class="col-sm-12 col-md-3 sidebar-layout blog">	
		<div class="sidebar">
			<?php 
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
				endif; ?>
		</div>
	</div>
	<?php } ?>
	
<?php } ?>

<?php get_footer(); ?>
<?php get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-archive-enable'] == '0') { ?>

		<div class="container" itemscope itemtype="http://schema.org/Blog">
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1'
				&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 category-layout">				
			<?php } ?>
							
					<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '1' 
					&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 category-layout" style="float:right!important">				
					<?php } ?>
								
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
			&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 category-layout">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['sidebar-archive-enable'] == '1' && $kiwi_theme_option['sidebar-archive-number'] == '2' 
						&& $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6" style="float:right!important">
						<?php } ?>			
						
				<?php if ( $kiwi_theme_option['page-pagetitle-index-enable'] == '1' ) { ?>
					<h3 class="index"><?php echo wp_get_document_title(); ?></h3>
				<?php } ?>
							
				<?php get_template_part( 'templates/layout', 'settings' ); ?>				

					</div>		

			
			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 blog sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>

<!-- -->
<?php if ( $kiwi_theme_option['sidebar-archive-sidebarlayout'] == '3') { ?>
		
	<?php if ( $kiwi_theme_option['sidebar-archive-enable'] > '0' && $kiwi_theme_option['sidebar-archive-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 blog sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 category-layout">
			
		<?php if ( $kiwi_theme_option['page-pagetitle-index-enable'] == '1' ) { ?>
				<h3 class="index"><?php echo wp_get_document_title(); ?></h3>
				<?php } ?>
							
		<?php get_template_part( 'templates/layout', 'settings' ); ?>
			
	</div>
	
	
	<div class="col-sm-12 col-md-3 blog sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-archive-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	
<?php } ?>

<?php get_footer(); ?>
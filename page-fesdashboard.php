<?php
/*
Template Name: FES :: Dashboard
*/

if ( ! is_user_logged_in() ) {	
	wp_redirect( home_url() ); exit;
} else {	
	get_header(); 
	
?>	
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>				
			<?php the_content(); ?>	
		<?php endwhile; 
		endif; ?>
		
	</div>	
		
<?php get_footer(); 
} ?>
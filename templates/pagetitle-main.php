<?php global $kiwi_theme_option; ?>
	
	<?php if ( $kiwi_theme_option['pagetitle-showon'] ['1']) { ?>	
		<?php if ( is_single() ) : ?>			
		<?php get_template_part( 'templates/pagetitle', 'content' ); ?>				
		<?php endif; ?>	
	<?php } ?>
	
	<?php if ( $kiwi_theme_option['pagetitle-showon'] ['2']) { ?>	
		<?php if ( is_page() ) : ?>			
		<?php get_template_part( 'templates/pagetitle', 'content' ); ?>			
		<?php endif; ?>	
	<?php } ?>
	
	<?php if ( $kiwi_theme_option['pagetitle-showon'] ['1'] && $kiwi_theme_option['pagetitle-showon'] ['2'] ) { ?>	
		<?php if ( is_single() && is_page() ) : ?>			
		<?php get_template_part( 'templates/pagetitle', 'content' ); ?>				
		<?php endif; ?>	
	<?php } ?>
	
	<?php if ( $kiwi_theme_option['pagetitle-showon'] ['3']) { ?>			
		<?php get_template_part( 'templates/pagetitle', 'content' ); ?>			
	<?php } ?>
	
	<?php if ( $kiwi_theme_option['pagetitle-showon'] ['4']) { ?>	
		<?php if ( !is_front_page() && !is_home() ) : ?>			
		<?php get_template_part( 'templates/pagetitle', 'content' ); ?>			
		<?php endif; ?>	
	<?php } ?>
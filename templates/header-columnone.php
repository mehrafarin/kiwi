<?php global $kiwi_theme_option; 

if ( is_rtl() ) {
	$rtl = ' sm-rtl';
} else {
	$rtl = '';
}

if ( $kiwi_theme_option['marketplace-enable-userroles-main'] == '1' ) {
		$extra_class = ' topbar-features';
	} else {
		$extra_class = '';
	}

?>

		<?php if ( $kiwi_theme_option['header-columnone'] == '1'  ) { ?>	
		
			<?php if ( $kiwi_theme_option['settings-headerlogo-enable'] == '1' ) { ?>
				<div itemscope itemtype="http://schema.org/Organization">
					<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="logo" src="<?php echo esc_url( $kiwi_theme_option['settings-headerlogo']['url'] ); ?>" height="<?php echo '' . $kiwi_theme_option['settings-logoheight']; ?>" style="margin:<?php echo '' . $kiwi_theme_option['settings-logomargin']; ?>" class="main-logo" alt=""></a>
				</div>
			<?php } ?>			
				
			<?php if ( $kiwi_theme_option['settings-headerlogo-enable'] == '0' ) { ?>	
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo esc_html( bloginfo( 'name' ) ); ?></a></h1>
				<h2 class="site-description"><?php echo esc_html( bloginfo( 'description' ) ); ?></h2>	
			<?php } ?> 
			
		<?php } ?>
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '2'  ) { ?>
			<?php if ( has_nav_menu( 'main_navigation' ) ) { ?> 
				<?php kiwi_bootstrap_menu() ?>
					<?php wp_nav_menu(array('theme_location' => 'main_navigation', 'depth' => 0, 'container' => false,  'menu_class' => 'kiwi-nav'. esc_attr( $extra_class ).' nav navbar-nav'. esc_attr( $rtl ) .'', 'walker' => new wp_bootstrap_navwalker() )); ?>		
				<?php kiwi_bootstrap_menu_end() ?>		
			<?php } ?>
		<?php } ?>
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '3'  ) { ?>
			<?php if ( has_nav_menu( 'header_nav_first' ) ) { ?> 
				<?php kiwi_bootstrap_menu() ?>
					<?php wp_nav_menu(array('theme_location' => 'header_nav_first',  'container' => false,  'menu_class' => 'kiwi-nav header-nav-first nav navbar-nav'. esc_attr( $rtl ) .'', 'walker' => new wp_bootstrap_navwalker())); ?>				
				<?php kiwi_bootstrap_menu_end() ?>
			<?php } ?>
		<?php } ?>
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '4'  ) { ?>
			<?php if ( has_nav_menu( 'header_nav_second' ) ) { ?> 
				<?php kiwi_bootstrap_menu() ?>
					<?php wp_nav_menu(array('theme_location' => 'header_nav_second',  'container' => false,  'menu_class' => 'kiwi-nav header-nav-second nav navbar-nav'. esc_attr( $rtl ) .'', 'walker' => new wp_bootstrap_navwalker())); ?>		
				<?php kiwi_bootstrap_menu_end() ?>
			<?php } ?>
		<?php } ?>
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '5'  ) { ?>				
				<?php do_action('kiwi_social_media'); ?>     
		<?php } ?>
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '6'  ) { ?>				
				<?php get_search_form(); ?>     
		<?php } ?>		
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '7'  ) { ?>				
				<?php echo '' . $kiwi_theme_option['header-columnoneeditor']; ?>     
		<?php } ?>
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '8'  ) { ?>								
		<?php } ?>	
		
		<?php if ( $kiwi_theme_option['header-columnone'] == '9'  ) { ?>			
		
		<?php if ( class_exists( 'Easy_Digital_Downloads' ) ) { ?>		
			<?php if( function_exists('kiwi_marketplace_stats')){ kiwi_marketplace_stats(); } ?> 
		<?php } ?>	
		
		<?php } ?>
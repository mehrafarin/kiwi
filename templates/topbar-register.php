<?php global $kiwi_theme_option;

	$url_id = $kiwi_theme_option['marketplace-enablelinks-register'];
	$buyer_link = get_page_link ($url_id);

if ( isset($kiwi_theme_option['marketplace-enablelinks-register']) && !empty($kiwi_theme_option['marketplace-enablelinks-register']) ) {	
	$register = '<li class="mp-register"><a href="'. esc_url( $buyer_link ) .'">'. esc_html__( 'Register', 'kiwi' ).'</a></li>';
} else {
	$register = '<li class="mp-register"><a href="' . esc_url( wp_registration_url() ) . '">'. esc_html__( 'Register', 'kiwi' ).'</a></li>';
}
	echo $register;

?>
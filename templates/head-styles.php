<?php global $kiwi_theme_option; ?>

<style>
.mainnav.stuckMenu.isStuck {<?php echo 'background:' . $kiwi_theme_option['styling-mainnav-rowone']['background-color'] . '!important;'; ?>}
	
<?php 
	$limit = $kiwi_theme_option['marketplace-userrole-purchase-limit'] + 2;
	echo '.dropdown-menu.purchases li.edd_download_thumbnail:nth-child(n+' . $limit . ') {display:none}'; 
?>

	
<?php echo '' . $kiwi_theme_option['mp-editor-css']; ?>
	<?php  if ( class_exists( 'Easy_Digital_Downloads' ) && edd_has_variable_prices(  get_the_ID() ) ) { ?>
		.edd_download_purchase_form .edd_download_quantity_wrapper .edd-item-quantity {width:100%}
		.edd_download_purchase_form .edd_download_quantity_wrapper {float:none;width:100%;margin-bottom: 20px;} 
	<?php } ?>
	
	<?php  
		$first = $kiwi_theme_option['settings-sitewidth'];
		$second = 30;
		$sum_total = $first - $second;		
	?>
		@media (min-width:1200px) {.container.mp-full-dashboard{<?php echo 'width:' . $sum_total . 'px!important;'; ?> } }
		
	<?php if ( $kiwi_theme_option['styling-linktransition'] == '1' ) { ?>	
		.socialmedia li a i, button, a, .topbar .socialmedia li a i, .search-form button:before,.copyright .socialmedia a .fa, 
		#backtotop, .marketplace-tags li, .edd-submit, input.edd-submit[type="submit"], .wpmenucart-contents,  
		a.edd-wl-action.edd-wl-button span, a.edd-wl-action.edd-wl-button, .mp-fes-fullwidth .fes-vendor-menu ul li, .widget_edd_product_details .edd-wl-button:hover i, .widget_edd_product_details .edd-wl-button i,
		.widget_edd_product_details a.edd-wl-action i, .widget_edd_product_details a.edd-wl-action span, ul.menu-filtre li,
		.customer-dashboard-menu ul li, .mp-social-buttons .demo, .tour-style1 .vc_tta-tabs-list li, 
		.slider-view .nav-arrow, .topbar li.menu-item-has-children ul li.menu-item a,  .dcjq-count.ver, .edd-currency-button-reset,
		.topbar-features li li a, .topbar-features .pro-wishlist li.menu-item .edd-wl-item-count,.mp-license-status-deactivate,
		.fes-el  div.edd-bk-builder i
		{transition:all .3s ease-in-out!important;-moz-transition:all .3s ease-in-out!important; -webkit-transition:all .3s ease-in-out!important;}				
	<?php } ?>	

	<?php if(!empty($kiwi_theme_option['header-stickymargin'])) { ?>
		.mainnav.stuckMenu.isStuck {<?php echo 'padding:' . $kiwi_theme_option['header-stickymargin'] . '!important;'; ?>} 
	<?php } ?>
	
	<?php if ( $kiwi_theme_option['marketplace-enable-design-width'] == '1' && $kiwi_theme_option['marketplace-enable-design'] == '2' ) { ?>
		.product-description.row.designtwo .small-responsive {width:100%!important}
	<?php } ?>
		
	<?php if(!empty($kiwi_theme_option['styling-links-footer']['hover'])) { ?>
		.footer .footer-widget ul li a:hover .dcjq-count.ver {<?php echo 'color:' . $kiwi_theme_option['styling-links-footer']['hover'] . ''; ?>}
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['topbar-customtext-rightmargin']) && $kiwi_theme_option['topbar-contentright'] == '1') { ?>
		.pull-right.topmenu {<?php echo 'padding:' . $kiwi_theme_option['topbar-customtext-rightmargin'] . '!important;'; ?>} 
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['topbar-customtext-leftmargin'])) { ?>
		.topbar .pull-left {<?php echo 'margin:' . $kiwi_theme_option['topbar-customtext-leftmargin'] . '!important;'; ?>} 
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['styling-demo-button-hover'])) { ?>
		.widget_marketplace_item_demolink a:hover {<?php echo 'background:' . $kiwi_theme_option['styling-demo-button-hover'] . '!important;'; ?>} 
	<?php } ?>		
	
	<?php if(!empty($kiwi_theme_option['styling-menu-dropdown-bg'])) { ?>
		.kiwi-nav > li > ul, .kiwi-nav > li > ul ul, .kiwi-nav > li > ul ul ul, .kiwi-nav > li > ul ul ul ul {<?php echo 'background:' . $kiwi_theme_option['styling-menu-dropdown-bg'] . '!important;'; ?>} 
	<?php } ?>
	<?php if(!empty($kiwi_theme_option['styling-menu-dropdown-text'])) { ?>
		.kiwi-nav > li > ul li a {<?php echo 'color:' . $kiwi_theme_option['styling-menu-dropdown-text'] . '!important;'; ?>} 
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['styling-menu-dropdown-bg-hover'])) { ?>
		.kiwi-nav > li li:hover, .kiwi-nav > li li li:hover, .kiwi-nav > li li li li:hover {<?php echo 'background:' . $kiwi_theme_option['styling-menu-dropdown-bg-hover'] . '!important;'; ?>} 
	<?php } ?>
	<?php if(!empty($kiwi_theme_option['styling-menu-dropdown-text-hover'])) { ?>
		.kiwi-nav > li > ul li a:hover {<?php echo 'color:' . $kiwi_theme_option['styling-menu-dropdown-text-hover'] . '!important;'; ?>} 
	<?php } ?>
			
	<?php if(!empty($kiwi_theme_option['settings-sitewidth']) && $kiwi_theme_option['settings-sitelayout'] <= '2' ) { ?>
		@media (min-width:1200px) {
			.page-container {width:100%!important;margin:0 auto; }	
			.page-container .container{<?php echo 'width:' . $kiwi_theme_option['settings-sitewidth'] . 'px;'; ?> }	
		}
		
		@media (min-width:960px)  {
			.page-container.boxed{<?php echo 'width:' . $kiwi_theme_option['settings-sitewidth'] . 'px!important;'; ?>
			<?php echo 'margin:' . $kiwi_theme_option['settings-boxedmargin'] . ';'; ?>}			
		}
	<?php } ?>	
	
	<?php if ( $kiwi_theme_option['header-columnonecentermenu'] == '1' || $kiwi_theme_option['header-columntwocentermenu'] == '1' || $kiwi_theme_option['header-columnthreecentermenu'] == '1' || $kiwi_theme_option['header-columnfourcentermenu'] == '1' ||	$kiwi_theme_option['header-secondrowcolumnonecentermenu'] == '1' || $kiwi_theme_option['header-secondrowcolumntwocentermenu'] == '1' || $kiwi_theme_option['header-secondrowcolumnthreecentermenu'] == '1' || $kiwi_theme_option['header-secondrowcolumnfourcentermenu'] == '1' ) { ?>
		ul.kiwi-nav, div.kiwi-nav > ul { display: table; margin: 0 auto;}
	<?php } ?>
	
	<?php if ( $kiwi_theme_option['header-layoutstyle'] >= '1'  ) { ?>	
		.mainnav {<?php echo 'padding:' . $kiwi_theme_option['header-heightcontrol'] . ';'; ?>}
	<?php } ?>

	<?php if(!empty($kiwi_theme_option['header-columnone']) && $kiwi_theme_option['header-columnone'] <= '9') { ?>
		.column-one {
			<?php echo 'float:' . $kiwi_theme_option['header-columnonefloat'] . '!important;'; ?> 
			<?php echo 'width:' . $kiwi_theme_option['header-columnonewidth'] . ';'; ?>
			<?php echo 'margin:' . $kiwi_theme_option['header-columnonemargin'] . ';'; ?>
			<?php echo 'text-align:' . $kiwi_theme_option['header-columnonealign'] . ';'; ?>
		}
	<?php } ?>

	<?php if(!empty($kiwi_theme_option['header-columntwo']) && $kiwi_theme_option['header-columntwo'] <= '9') { ?>
		.column-two {
			<?php echo 'float:' . $kiwi_theme_option['header-columntwofloat'] . '!important;'; ?>  
			<?php echo 'width:' . $kiwi_theme_option['header-columntwowidth'] . ';'; ?> 
			<?php echo 'margin:' . $kiwi_theme_option['header-columntwomargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-columntwoalign'] . ';'; ?>
		}
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['header-columnthree']) && $kiwi_theme_option['header-columnthree'] <= '9') { ?>		
		.column-three {
			<?php echo 'float:' . $kiwi_theme_option['header-columnthreefloat'] . '!important;'; ?> 
			<?php echo 'width:' . $kiwi_theme_option['header-columnthreewidth'] . ';'; ?> 
			<?php echo 'margin:' . $kiwi_theme_option['header-columnthreemargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-columnthreealign'] . ';'; ?>
		}	 
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['header-columnfour']) && $kiwi_theme_option['header-columnfour'] <= '9') { ?>
		.column-four {
			<?php echo 'float:' . $kiwi_theme_option['header-columnfourfloat'] . '!important;'; ?>
			<?php echo 'width:' . $kiwi_theme_option['header-columnfourwidth'] . ';'; ?> 
			<?php echo 'margin:' . $kiwi_theme_option['header-columnfourmargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-columnfouralign'] . ';'; ?>
		}	 
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['header-secondrowcolumnone']) && $kiwi_theme_option['header-secondrowcolumnone'] <= '9') { ?>
		.column-one-row {
			<?php echo 'float:' . $kiwi_theme_option['header-secondrowcolumnonefloat'] . '!important;'; ?>			
			<?php echo 'width:' . $kiwi_theme_option['header-secondrowcolumnonewidth'] . ';'; ?> 
			<?php echo 'margin:' . $kiwi_theme_option['header-secondrowcolumnonemargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-secondrowcolumnonealign'] . ';'; ?>
		}		
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['header-secondrowcolumntwo']) && $kiwi_theme_option['header-secondrowcolumntwo'] <= '9') { ?>
		.column-two-row {
			<?php echo 'float:' . $kiwi_theme_option['header-secondrowcolumntwofloat'] . '!important;'; ?>
			<?php echo 'width:' . $kiwi_theme_option['header-secondrowcolumntwowidth'] . ';'; ?>  
			<?php echo 'margin:' . $kiwi_theme_option['header-secondrowcolumntwomargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-secondrowcolumntwoalign'] . ';'; ?>
		}
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['header-secondrowcolumnthree']) && $kiwi_theme_option['header-secondrowcolumnthree'] <= '9') { ?>		
		.column-three-row {
			<?php echo 'float:' . $kiwi_theme_option['header-secondrowcolumnthreefloat'] . '!important;'; ?>
			<?php echo 'width:' . $kiwi_theme_option['header-secondrowcolumnthreewidth'] . ';'; ?> 
			<?php echo 'margin:' . $kiwi_theme_option['header-secondrowcolumnthreemargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-secondrowcolumnthreealign'] . ';'; ?>
		}	 
	<?php } ?>
	
	<?php if(!empty($kiwi_theme_option['header-secondrowcolumnfour']) && $kiwi_theme_option['header-secondrowcolumnfour'] <= '9') { ?>
		.column-four-row {
			<?php echo 'float:' . $kiwi_theme_option['header-secondrowcolumnfourfloat'] . '!important;'; ?>
			<?php echo 'width:' . $kiwi_theme_option['header-secondrowcolumnfourwidth'] . ';'; ?> 
			<?php echo 'margin:' . $kiwi_theme_option['header-secondrowcolumnfourmargin'] . ';'; ?> 
			<?php echo 'text-align:' . $kiwi_theme_option['header-secondrowcolumnfouralign'] . ';'; ?>
		}	 
	<?php } ?>
	
	<?php if($kiwi_theme_option['miscellaneous-vcedit'] == '1') { ?>
		.kiwi-content .vc_inline-link {display:none}
	<?php } ?>
	
	<?php if($kiwi_theme_option['blog-editpostlink-enable-single'] == '1') { ?>
		.kiwi-content .post-edit-link {display:none}
	<?php } ?>
	
	
	<?php if(!empty($kiwi_theme_option['settings-contentmargin'])) { ?>
	.container.kiwi-content {<?php echo 'margin:' . $kiwi_theme_option['settings-contentmargin'] . '!important;'; ?>}
	<?php } ?>
	
	.topbar{<?php echo 'margin:' . $kiwi_theme_option['topbar-custommargin'] . ';'; ?>}
		
	.topbar {  
		<?php if($kiwi_theme_option['topbar-enableborder'] == '1'): ?>
			<?php echo 'border-top:'. $kiwi_theme_option['topbar-topborder']['border-top'] . ' ' . $kiwi_theme_option['topbar-topborder']['border-style'] . ' ' . $kiwi_theme_option['topbar-topborder']['border-color'] . ';'; ?>
			<?php echo 'border-bottom:'. $kiwi_theme_option['topbar-bottomborder']['border-bottom'] . ' ' . $kiwi_theme_option['topbar-bottomborder']['border-style'] . ' ' . $kiwi_theme_option['topbar-bottomborder']['border-color'] . ';'; ?>
		<?php else: ?>
		border:0!important;
		<?php endif ?>
	}
	
	.copyright .copyright-border {
		<?php if($kiwi_theme_option['footer-copyright-enableborder'] == '1'): ?>
			<?php echo 'border-top:'. $kiwi_theme_option['footer-copyright-topborder']['border-top'] . ' ' . $kiwi_theme_option['footer-copyright-topborder']['border-style'] . ' ' . $kiwi_theme_option['footer-copyright-topborder']['border-color'] . ';'; ?>
		<?php else: ?>
			border:0!important;
		<?php endif ?>	
	}
	
	.copyright-message {
		<?php echo 'margin:' . $kiwi_theme_option['footer-copyright-message-margin'] . ';'; ?> 
	}
	
	.footer {
		<?php echo 'padding:' . $kiwi_theme_option['footer-padding'] . ';'; ?> 
	}
	
	.pagetitle {
		<?php echo 'border-top:'. $kiwi_theme_option['pagetitle-topborder']['border-top'] . ' ' . $kiwi_theme_option['pagetitle-topborder']['border-style'] . ' ' . $kiwi_theme_option['pagetitle-topborder']['border-color'] . ';'; ?>
		<?php echo 'border-bottom:'. $kiwi_theme_option['pagetitle-bottomborder']['border-bottom'] . ' ' . $kiwi_theme_option['pagetitle-bottomborder']['border-style'] . ' ' . $kiwi_theme_option['pagetitle-bottomborder']['border-color'] . ';'; ?>
		<?php echo 'margin:' . $kiwi_theme_option['pagetitle-margin'] . ';'; ?> 
		<?php echo 'padding:' . $kiwi_theme_option['pagetitle-padding'] . ';'; ?> 
		<?php echo 'text-align:' . $kiwi_theme_option['pagetitle-align'] . ';'; ?>
	}
	
	.pagetitle a {<?php echo 'color:' . $kiwi_theme_option['pagetitle-typography']['color'] . ';'; ?>}
	
	.search-form button {<?php echo 'color:' . $kiwi_theme_option['typography-searchform']['color'] . ';'; ?>}
	.search-form .form-control::-moz-placeholder{<?php echo 'color:' . $kiwi_theme_option['typography-searchform']['color'] . '!important;'; ?>}
	.search-form .form-control:-ms-input-placeholder{<?php echo 'color:' . $kiwi_theme_option['typography-searchform']['color'] . '!important;'; ?>}
	.search-form .form-control::-webkit-input-placeholder{<?php echo 'color:' . $kiwi_theme_option['typography-searchform']['color'] . '!important;'; ?>}
 
	<?php if ( $kiwi_theme_option['header-secondrowborder'] == '1' ) { ?>	
		.mainnav .kiwi-border{<?php echo 'border-bottom:'. $kiwi_theme_option['header-secondrow-border']['border-bottom'] . ' ' . $kiwi_theme_option['header-secondrow-border']['border-style'] . ' ' . $kiwi_theme_option['header-secondrow-border']['border-color'] . ';'; ?>}
	<?php } ?>
	
	<?php if($kiwi_theme_option['postfomat-shadow'] == '1') { ?>
		.grid-masonry article{-webkit-box-shadow: 0px 2px 5px -1px rgba(218, 218, 218, 1); -moz-box-shadow: 0px 2px 5px -1px rgba(218, 218, 218, 1); box-shadow: 0px 2px 5px -1px rgba(218, 218, 218, 1);}
	<?php } ?>	
	
	<?php if($kiwi_theme_option['boxed-shadow'] == '1') { ?>
		.page-container.boxed{-webkit-box-shadow: 0px 2px 5px -1px rgba(218, 218, 218, 1); -moz-box-shadow: 0px 2px 5px -1px rgba(218, 218, 218, 1); box-shadow: 0px 2px 5px -1px rgba(218, 218, 218, 1);}	
	<?php } ?>	
	
	.breadcrumbs {<?php echo 'margin:' . $kiwi_theme_option['pagetitle-breadcrumbs-margin'] . ';'; ?> }
	
	<?php if ( $kiwi_theme_option['pagetitle-copyright-enableprefix'] == '1' ) { ?>
		.author-copyright {<?php echo 'margin:' . $kiwi_theme_option['pagetitle-copyright-margin'] . '!important;'; ?> }
	<?php } ?>	
	
	<?php if ( $kiwi_theme_option['notificationbar-enable'] == '1' ) { ?>		
		.notificationbar {			
			<?php echo 'background-color: ' . $kiwi_theme_option['notificationbar-styling']['background-color'] . ';'; ?>		
			<?php if( !empty ($kiwi_theme_option['notificationbar-styling']['background-image'])  ) { ?>
				<?php echo 'background-image: ' . $kiwi_theme_option['notificationbar-styling']['background-image'] . ';'; ?>
			<?php } ?>	
			<?php if( !empty ($kiwi_theme_option['notificationbar-styling']['background-repeat'])  ) { ?>
				<?php echo 'background-repeat: ' . $kiwi_theme_option['notificationbar-styling']['background-repeat'] . ';'; ?>
			<?php } ?>			
			<?php if( !empty ($kiwi_theme_option['notificationbar-styling']['background-position'])  ) { ?>
				<?php echo 'background-position: ' . $kiwi_theme_option['notificationbar-styling']['background-position'] . ';'; ?>
			<?php } ?>	
			<?php if( !empty ($kiwi_theme_option['notificationbar-styling']['background-size'])  ) { ?>
				<?php echo 'background-size: ' . $kiwi_theme_option['notificationbar-styling']['background-size'] . ';'; ?>
			<?php } ?>	
			<?php if( !empty ($kiwi_theme_option['notificationbar-styling']['background-attachment'])  ) { ?>
				<?php echo 'background-attachment: ' . $kiwi_theme_option['notificationbar-styling']['background-attachment'] . ';'; ?>
			<?php } ?>				
			<?php echo 'padding:' . $kiwi_theme_option['notificationbar-customtext-margin'] . ';'; ?>			
				<?php if ( $kiwi_theme_option['notificationbar-enableborder'] == '1' ) { ?>
					<?php echo 'border-top:'. $kiwi_theme_option['notificationbar-topborder']['border-top'] . ' ' . $kiwi_theme_option['notificationbar-topborder']['border-style'] . ' ' . $kiwi_theme_option['notificationbar-topborder']['border-color'] . ';'; ?>
					<?php echo 'border-bottom:'. $kiwi_theme_option['notificationbar-bottomborder']['border-bottom'] . ' ' . $kiwi_theme_option['notificationbar-bottomborder']['border-style'] . ' ' . $kiwi_theme_option['notificationbar-bottomborder']['border-color'] . ';'; ?>
				<?php } ?>
		}
	<?php } ?>	
	
	<?php if ( function_exists ( 'easy_share_buttons' )) { echo '.product-description .btn-group-vertical.social .btn-default {border-top:0}'; } ?>
		<?php if (class_exists('EDD_Download_Info') && empty( $download_demo_link ) || class_exists( 'EDD_Front_End_Submissions' ) && empty( $fes_demo_url ) ) {
			echo '@media (min-width:980px){
					.marketplace .essb_links.essb_template_grey-blocks-retina .essb_links_list li:last-child{border-bottom:1px solid #e8e8e8!important}
				}';
		} else {
			'@media (min-width:980px){
					.marketplace .essb_links.essb_template_grey-blocks-retina .essb_links_list li:nth-child(n+2){border-bottom:0px!important}
				}';
		}
		?>
	
	<?php
	
	$transparency = get_post_meta( get_the_ID(), '_cmb2_header_transparency', true );
	$menucolor = get_post_meta( get_the_ID(), '_cmb2_header_menu_color', true );
	$headeroverlap = get_post_meta( get_the_ID(), '_cmb2_header_overlap', true );
	$headerhide = get_post_meta( get_the_ID(), '_cmb2_header_hide', true );
	$topbarhide = get_post_meta( get_the_ID(), '_cmb2_header_topbarhide', true );
	$contentmargin = get_post_meta( get_the_ID(), '_cmb2_ptcontent', true );
	
	$pagetitlemargin = get_post_meta( get_the_ID(), '_cmb2_ptmargin', true );
		
	if ( $pagetitlemargin ) {
		echo '.pagetitle {margin:'.$pagetitlemargin.'!important}';
	}
		
	if ( !is_singular( 'download' ) && !empty( $transparency ) && !isset($_GET['s']) ) {
		echo '.mainnav .header-row-two, .mainnav .header-row-one {background:none!important;}';
	}

	if ( !is_singular( 'download' ) && !empty( $menucolor ) && !isset($_GET['s']) ) {
			echo '@media (min-width:767px) {
				.kiwi-nav li a {color:#fff!important;}
				.kiwi-nav li a.menu-item-has-children:hover {background:none!important}
				
				.nav .open>a:hover{background:none!important;}

				.navbar-collapse .dropdown-menu > li > a {color:#888!important;}
				.navbar-collapse .dropdown-menu > li > a:hover {color:#18a9c4!important;}
			}';
		}		
	
	if (  !is_singular('download') && !empty( $headeroverlap ) && !isset($_GET['s']) ) {
		echo '@media (min-width:767px) {
				.mainnav {position:absolute!important;}	
			}';
	}

	if ( !is_singular( 'download' ) && !empty( $headerhide ) && !isset($_GET['s']) ) {
			echo '.pagetitle {display:none;}';
		}
		
	if ( !is_singular( 'download' ) && !empty( $topbarhide ) && !isset($_GET['s']) ) {
			echo '.topbar {display:none;}';
		}
			
	if( !is_singular( 'download' ) && ! empty( $contentmargin ) && !isset($_GET['s']) ) {
			echo ".container.kiwi-content {margin:" . $contentmargin . "!important}";
		} 	
	?>
	
<?php 	
	if (class_exists( 'Easy_Digital_Downloads' ) && class_exists( 'EDD_Front_End_Submissions' ) ){ 

	$author = get_query_var( 'vendor' );
	$author = get_user_by( 'slug', $author );

	if ( !$author ) {
		$url = $_SERVER['REQUEST_URI'];
		$page_id = EDD_FES()->helper->get_option( 'fes-vendor-page', false );
		$page = get_page( $page_id );
		$page_name = ! empty( $page->post_name ) ? $page->post_name : EDD_FES()->vendors->get_vendor_constant_name( false, false );
		$permalink = '/'. trailingslashit( $page_name ) ;
		
		if ( !empty ( $_SERVER['REQUEST_URI'] ) && !empty( $url ) ){
			$string = str_replace( $permalink, '', $url );
			$author = get_user_by( 'slug', untrailingslashit ( $string ) );
		}
	}

	if ( ! $author ) {
		$author = get_current_user_id();
	} 
	
	$vendor_custom_image = !empty($author->ID) ? get_the_author_meta('store_image_upload', $author->ID) : false;
	
	if ( !empty( $vendor_custom_image ) ) { 		
		foreach($vendor_custom_image as $key=>$val){
			$media_url = wp_get_attachment_image_src( $val, 'full' ); 			
			echo " .vendor-page {background:url(" . $media_url[0]. ") 50% 50% no-repeat;  background-position:top center; /* background-size:cover;  */ margin:0; height:" . $media_url[2]. "px; }";
		}
	}
	 	
} 
?>
</style>
<?php
	global $kiwi_theme_option;
	
	$current_user = wp_get_current_user();
	
	$vendorname = '<span class="name">'.$current_user->display_name.'</span>';
	
	$cart_quantity = edd_get_cart_quantity();
	$display       = $cart_quantity > 0 ? '' : ' style="display:none;"';
?>

<li class="menu-item menu-item-has-children pro-cart"><a href="#"><i class="icon icon-cart"></i> 
<span class="edd-cart-number-of-items"<?php echo esc_attr( $display ); ?>><span class="edd-cart-quantity cart-quantity"><?php echo esc_attr( $cart_quantity ); ?></span></span></a>
	<?php get_template_part( 'templates/topbar', 'widgetcart' ); ?>	
</li> 
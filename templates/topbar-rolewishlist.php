<?php if ( class_exists( 'EDD_Wish_Lists' ) ) { ?>					
	<li class="menu-item menu-item-has-children pro-wishlist"><a href="#"><i class="icon icon-wishlist"></i></a>
		<ul class="dropdown-menu">
			<li class="heading"><?php esc_html_e( 'Favorites', 'kiwi' ); ?></li>
				<?php get_template_part( 'templates/topbar', 'rolewishlistview' ); ?>			
		</ul>
	</li>
<?php } ?>
<?php global $kiwi_theme_option; ?>
	
<li>

<?php if ( $kiwi_theme_option['marketplace-enable-equalheights'] == '1' ) { ?>
	<div class="mp-equal-heights">
<?php } ?>
			

			
	<div class="iso-thumbnail">		
		<?php if( function_exists('mp_audio_video_featured')){ mp_audio_video_featured(); } ?>		
	<div class="imageoverlay"></div>		
		<?php if( function_exists('mp_compatibility_rvi_cm')){ mp_compatibility_rvi_cm(); } ?>	
	</div>
	
	<div class="iso-desc archive-style-one">
		<div class="truncate one-line">
			<h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		</div>		
	
		<?php
		
			global $post;
			
			$name 			= get_the_author_meta('display_name', $post->post_author);
			$vendor_name	= get_the_author_meta( 'user_login' );
			$avatar			= get_avatar( get_the_author_meta( 'ID' ), 25 );
			
			if ( class_exists( 'EDD_Front_End_Submissions' ) ) {	
				$constant = EDD_FES()->helper->get_option( 'fes-vendor-constant', '' );
				$constant = ( isset( $constant ) && $constant != '' ) ? $constant : esc_html__( 'vendor', 'kiwi' );
				$constant = apply_filters( 'fes_vendor_constant_singular_lowercase', $constant );
			}
			
			echo '<div class="left-column mp_author">';
			
			if ( class_exists( 'EDD_Front_End_Submissions' ) && !is_page_template( 'page-vendor.php' ) ) { 
					echo '<span class="byline"><span class="author vcard">'.$avatar.'';
					echo '<a rel="author" href="'. esc_url(site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name) ).'">'. esc_html( $name ).'</a>'; 
					echo '</span></span>';
				} else {					
					echo '<span class="byline"><span class="author vcard">'.$avatar.'';
					echo esc_html( $name ); 
					echo '</span></span>';
			}
			
			echo '</div>';
			
		if ( class_exists( 'EDD_Reviews' ) ) {	
			echo '<div class="right-column reviews align-right">';			
			mp_extension_review_function_core();			
			echo '</div>';
		}	
			
			echo '<div class="clear"></div>';
		?>
			
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '1' ) { ?>
				<?php echo marketplace_cat_terms(); ?>
				
				<div class="excerpt"><?php the_excerpt(); ?></div>
			<?php } ?>
					
	</div>
	
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '1' ) { ?>
				<div class="clear"></div>
			<?php } ?>
			
<?php if ( $kiwi_theme_option['marketplace-enable-equalheights'] == '1' ) { ?>
	</div>
<?php } ?>		
				
</li>
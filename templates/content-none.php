<?php
/** The template for displaying a "No posts found" message **/
?>

<div class="content-no-post">

<header class="page-header content-none">
	<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'kiwi' ); ?></h1>
</header>

<div class="page-content content-none">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'kiwi' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'kiwi' ); ?></p>
	<?php get_search_form(); ?>

	<?php else : ?>

	<p><?php esc_html_e( 'It seems we can\'t find what you\'re looking for. Perhaps searching can help.', 'kiwi' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</div>

</div>
<li class="menu-item menu-item-has-children pro-downloads"><a href="#"><i class="icon icon-purchase"></i></a>
	<ul class="dropdown-menu purchases">
		<li class="heading"><?php esc_html_e( 'My Purchases', 'kiwi' ); ?></li>
		<?php get_template_part( 'templates/topbar', 'widgetpurchases' ); ?>
	</ul>
</li>
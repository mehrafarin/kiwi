<?php global $kiwi_theme_option;

	$url_id = $kiwi_theme_option['marketplace-enablelinks-login'];
	$buyer_link = get_page_link ($url_id);

if ( isset($kiwi_theme_option['marketplace-enablelinks-login']) && !empty($kiwi_theme_option['marketplace-enablelinks-login']) ) {	
	$login = '<li class="mp-login"><a href="'. esc_url( $buyer_link ) .'">'. esc_html__( 'Log in', 'kiwi' ).'</a></li>';
} else {
	$login = '<li class="mp-login"><a href="' . esc_url( wp_login_url() ). '">'. esc_html__( 'Log in', 'kiwi' ).'</a></li>';
}
	echo $login;

?>
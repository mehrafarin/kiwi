<?php global $kiwi_theme_option; ?>	

<div class="pagetitle">
	
	<div class="container">
		<div class="row">		
		
			<?php if ( $kiwi_theme_option['pagetitle-breadcrumbs-enable'] == '1' && $kiwi_theme_option['pagetitle-breadcrumbs-position'] == '1' ) { ?>
				
				<?php if ( $kiwi_theme_option['miscellaneous-yoastbreadcrumb'] == '0' && function_exists('ft_custom_breadcrumbs')) {
						echo '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';
						echo ft_custom_breadcrumbs();
						echo '</div>';	
					}  ?>
				
				<?php if ( $kiwi_theme_option['miscellaneous-yoastbreadcrumb'] == '1' && function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="breadcrumbs">','</div>');
					}  ?>
				 
			<?php } ?>			
			
	<?php if ( is_tax( 'download_category' ) || is_tax( 'download_tag' )  ) : ?>
		<h4><?php single_cat_title('Currently browsing :: '); ?></h4>
	<?php elseif ( is_post_type_archive( 'download' ) ): ?>
		<h4><?php esc_html_e( 'All items', 'kiwi' ); ?></h4>
	<?php elseif ( is_front_page() ): ?>
		<h4><?php echo esc_html( bloginfo('description') ); ?></h4>
	<?php elseif ( is_singular( 'download' ) ): ?>
		<h4><?php echo wp_get_document_title(); ?></h4>
	<?php else : ?>	
		<h4><?php echo wp_get_document_title(); ?></h4>
	<?php endif; ?>		
		
			<?php if ( $kiwi_theme_option['pagetitle-breadcrumbs-enable'] == '1' && $kiwi_theme_option['pagetitle-breadcrumbs-position'] == '2' ) { ?>			
				
				<?php if ( $kiwi_theme_option['miscellaneous-yoastbreadcrumb'] == '0' && function_exists('ft_custom_breadcrumbs')) {
						echo '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';
						echo ft_custom_breadcrumbs();
						echo '</div>';	
					}  ?>
				
				<?php if ( $kiwi_theme_option['miscellaneous-yoastbreadcrumb'] == '1' && function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="breadcrumbs">','</div>');
					}  ?>
				
			<?php } ?>
			
			
			<?php if ( $kiwi_theme_option['pagetitle-copyright-enableprefix'] == '1' && is_singular( 'download' )  ) { ?>

				<?php 
					global $post; 
					$author_id = $post->post_author;
				?>			
				
				<div class="breadcrumbs author-copyright">				
					<?php esc_html_e( '&copy; All Rights Reserved', 'kiwi' ); ?>
						<span><?php the_author_meta( 'display_name', $author_id ); ?></span>
				</div>				
			<?php } ?>
			
		</div>
	</div>
	
</div>
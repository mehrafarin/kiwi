<?php global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['settings-pageloader-enable'] == '1' ) { ?>			
	<script> 
		jQuery(function ($) {	
			$(window).load(function() {
				$(".pace").fadeOut(1000);
			});
		});
	</script>
<?php } ?>

<?php if ( $kiwi_theme_option['notificationbar-enable'] == '1' && !isset($_COOKIE['mp_notification_viewed'])) { 
	
	$message = $kiwi_theme_option['notificationbar-customtext'];

	$script = "<script type='text/javascript'>
	jQuery( document ).ready( function($) {         
		$(document).ready(function() {	
			$.notifyBar({
				html: '$message', 
				cssClass: 'notificationbar', 
				close: true, 
				waitingForClose: true, 
				closeOnClick: false,
				animationSpeed: 'slow',
			  });
			  
			$( '.notify-bar-close' ).click(function() {
				Cookies.set('mp_notification_viewed', 'yes', { expires: 14 });
			});	 
			  
		});
	}
 );
</script>";

echo $script;

} ?>


<?php if ( $kiwi_theme_option['notificationbar-enable'] == '0' && isset($_COOKIE['mp_notification_viewed'])) { 

$delete_cookie = "<script type='text/javascript'>
jQuery( document ).ready( function($) {         
	$(document).ready(function() {	
			Cookies.remove('mp_notification_viewed');			
	});
}
 );
</script>";

echo $delete_cookie;
		
} ?>










<?php if ( $kiwi_theme_option['header-sticky'] == '1'  ) { ?>	
 <script type="text/javascript">                
	jQuery(function ($) {                
		 $(window).load(function() {                    
		 $('.mainnav').stickUp();                 
	 });               
 });              

jQuery(window).load(function(){
  if (jQuery(window).width() <= 768){	
	   $( ".mainnav" ).removeClass( "isStuck" );
  }
});
</script> 

<?php } ?>

<script>              
 jQuery(function() {
	jQuery('.kiwi-nav, .top-menu').smartmenus({
		keepInViewport: true,
		mainMenuSubOffsetX: 0,
		subMenusSubOffsetY: 0,
		subIndicatorsText:''
	});
}); 
 
 



// Truncate titles
	jQuery(function ($) {                
		 $(window).load(function() {                   
			$('.mp-vc-items .truncate.one-line h4 a, .widget-mk-kiwi h3 a').trunk8({
			  lines: 1,
			  fill: '...'
			}); 

			$('.mp-vc-items .truncate.two-lines h4 a').trunk8({
			  lines: 2,
			  fill: '...'
			});	
			
			$('.mp-vc-items .truncate.three-lines h4 a').trunk8({
			  lines: 3,
			  fill: '...'
			});
			
			$('.mp-vc-items .truncate.four-lines h4 a').trunk8({
			  lines: 4,
			  fill: '...'
			});
	 });               
 });  
</script>

<?php 
global $kiwi_theme_option;

$slideshow = $kiwi_theme_option['marketplace-slider-slideshow'];
$slidescroll = $kiwi_theme_option['marketplace-slider-slidescroll'];
$autoplay = $kiwi_theme_option['marketplace-slider-autoplay'];
$slidespeed = $kiwi_theme_option['marketplace-slider-slidespeed'];
$slide_in = $kiwi_theme_option['marketplace-slider-infinite'];
$autospeed = $kiwi_theme_option['marketplace-slider-autospeed'];


if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
	$slide_direction = 'true';
} else {
	$slide_direction = 'false';
}

$script_flex_edd = "<script type='text/javascript'>
			jQuery( document ).ready( function($) {         
				$(document).ready(function() {				  			  
				   $('.flex-edd-images ul.slides').slick({
					  infinite: $slide_in,
					  slidesToShow: $slideshow,
					  slidesToScroll: $slidescroll,
					  autoplay: $autoplay,
					  rtl: $slide_direction,
					  autoplaySpeed: $autospeed,
					  speed: $slidespeed,
					  prevArrow: $('.flex-edd-images .flex-direction-nav .flex-prev'),
					  nextArrow: $('.flex-edd-images .flex-direction-nav .flex-next'),
					  
					  responsive: [
						{
						  breakpoint: 768,
						  settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
						  }
						},
						{
						  breakpoint: 480,
						  settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						  }
						}
					  ],
					  
					}); 
			});
		}
);
</script>";

	echo $script_flex_edd;

?>











<?php 

global $kiwi_theme_option;

if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
	$masonry = 'false'; 
} else {
	$masonry = 'true';
}

$script_masonry = "<script type='text/javascript'>
			jQuery( document ).ready( function($) {         
				$(document).ready(function() {				  			  
				  $('.grid-masonry').masonry({ 
					itemSelector: 'article',
					singleMode: true,
					originLeft: $masonry,	  
				}); 
			});
		}
);
</script>";

	echo $script_masonry;

?>



				






<!-- Slick Slider -->
<?php

if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
	$slide_direction = 'true';
} else {
	$slide_direction = 'false';
}

$script_slick_slider = "<script type='text/javascript'>
			jQuery( document ).ready( function($) {         
				$(document).ready(function() {				  			  
				  $('.kiwi-slider-settings .slick-slider').slick({
			  dots: false,
			  adaptiveHeight: true,			  
				  infinite: true,
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  autoplay: true,
				  rtl: $slide_direction,
				  autoplaySpeed: 4000,
				  speed: 1000,
				  prevArrow: $('.post .flex-direction-nav .flex-prev'),
				  nextArrow: $('.post .flex-direction-nav .flex-next'),
				  
				  responsive: [
					{
					  breakpoint: 768,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					  }
					},
					{
					  breakpoint: 480,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					  }
					}
				  ],
			  
			}); 
			});
		}
);
</script>";

  echo $script_slick_slider;

?>

<?php 
$searching = '';
if( $kiwi_theme_option['header-expandable-search'] == '1' || $kiwi_theme_option['header-expandable-search-topbar'] == '1' || isset ($kiwi_theme_option['marketplace-userrole-blocks-vendorlogged']['enabled']['searching'] )|| isset ($kiwi_theme_option['marketplace-userrole-blocks-buyerlogged']['enabled']['searching'] ) || isset ($kiwi_theme_option['marketplace-userrole-blocks-nonlogged']['enabled']['searching'] ) ){ ?>			
<script> 
  jQuery(function ($) {	
	$(window).load(function() {		 
		$( "#overlay-menu" ).click(function() {
            $( ".overlay" ).addClass('overlay-open');
        });
        $( ".overlay-close" ).click(function() {
            $( ".overlay" ).removeClass('overlay-open');
        });
	});
});
</script> 
<?php } 
<?php 
	global $kiwi_theme_option;
			
	$grid_columns = $kiwi_theme_option['marketplace-sidebar-archive-gridcolumns']; 
	
	if ($grid_columns == '1') { 
		$variable_width = ' two-columns'; 
	} elseif ($grid_columns == '2') { 
		$variable_width = ' three-columns'; 
	} elseif ($grid_columns == '3') { 
		$variable_width = ' four-columns'; 
	} else {
		$variable_width = ' one-columns';
	}				
?>
				
						
<?php if ( have_posts() ) : ?>

	<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '2' ) { ?>
		<div class="grid-view archive-style-two<?php echo esc_attr( $variable_width ); ?>">
	<?php } elseif ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '3' ) { ?>
		<div class="grid-view archive-style-three<?php echo esc_attr( $variable_width ); ?>">
	<?php } else { ?>	
		<div class="grid-view<?php echo esc_attr( $variable_width ); ?>">
	<?php } ?>			
	
		<ul class="layout-sorting">
		
			<?php while ( have_posts() ) : the_post(); ?>

			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '2' ) { 
				get_template_part( 'templates/marketplace', 'archivescontentdesigntwo' );					
			 } elseif ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '3' ) { 
				get_template_part( 'templates/marketplace', 'archivescontentdesignthree' );					
			 } else {
				get_template_part( 'templates/marketplace', 'archivescontent' ); 
			 } ?>
				
			<?php endwhile; ?>

		</ul>
		
	</div>	

	<div class="clear"></div>

		<?php kiwi_content_nav() ?>		 

<?php else : ?>
	<?php get_template_part( 'templates/content', 'none' ); ?>
<?php endif; ?>

<?php global $kiwi_theme_option;
	
	$current_user = wp_get_current_user();
	
	$author_id = $current_user->ID;
    $registered = date_i18n( get_option('date_format'), strtotime( get_the_author_meta( 'user_registered', $author_id ) ) );
	
		//if ( ! $user_id ) {
			$user_id = get_current_user_id();
		//}

		$customers = EDD()->customers->get_customers(
			array( 
				'number'  => -1,
				'user_id' => $user_id
			)
		);

		$purchase_values = array();

		if ( $customers ) {
			foreach ( $customers as $customer ) {
				$purchase_values[] = $customer->purchase_count;
			}
		}
		
		$total_sales_user = array_sum( $purchase_values );		
		
		remove_filter( 'edd_usd_currency_filter_before', 'mp_format_currency', 30, 3 );
		remove_filter( 'edd_usd_currency_filter_before', 'mp_format_currency_commissions' ); 	

?>
	
<h3><?php esc_html_e( 'Dashboard', 'kiwi' ); ?></h3>


<?php if ( $kiwi_theme_option['marketplace-announcement-enable'] == '1' ) { ?>
	<div class="edd-user-announcements">
		<?php echo '' . $kiwi_theme_option['marketplace-announcement'] . ''; ?> 
	</div>
<?php } ?>


<div id="fes-vendor-store-link">
	<span><?php printf( esc_html__( 'You\'ve been a member since: %s.', 'kiwi' ), esc_attr( $registered ) ); ?></span>
</div>


<div class="mp-account-overview-wrapper">
	<h4><?php esc_html_e( 'Account overview', 'kiwi' ); ?></h4>

<div class="mp-account-overview">
<div class="row">

	<div class="col-md-6 col-sm-6 stats-finance">		
		<div class="row mp-stats-finance">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h5><?php esc_html_e( 'Total spend', 'kiwi' ); ?></h5>
					<div class="result text-success"><?php echo wp_kses( mp_total_spent(), wp_kses_allowed_html( 'post' ) ); ?></div>
					<div class="result text-subline"><?php esc_html_e( 'since joining', 'kiwi' ); ?></div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-6 mp-second">
					<h5><?php esc_html_e( 'Total transactions', 'kiwi' ); ?></h5>
					<div class="result text-due"><?php echo wp_kses( $total_sales_user, wp_kses_allowed_html( 'post' ) ); ?></div>
					<div class="result text-subline"><?php esc_html_e( 'since joining', 'kiwi' ); ?></div>			
				</div>	
		</div>
	</div>
	
</div>
</div>

</div>
<?php
	if (class_exists( 'EDD_Front_End_Submissions' ) ){
		$url = get_permalink( EDD_FES()->helper->get_option( 'fes-vendor-dashboard-page', get_permalink() ) );
	} else {
		$url = '';
	}
?>

<li class="menu-item menu-item-has-children pro-dashboard"><a href="#"><i class="icon icon-dashboard"></i></a>	
	<ul class="dropdown-menu pro-submenu">
		<li class="heading"><?php esc_html_e( 'Quick Overview', 'kiwi' ); ?></li>		
		<li class="menu-item dashboard"><a href="<?php echo esc_url( add_query_arg( 'task', 'dashboard', $url ) ); ?>"><i class="icon icon-dashboardvendor"></i><?php esc_html_e( 'Dashboard', 'kiwi' ); ?></a></li>
		<li class="menu-item new-item"><a href="<?php echo esc_url( add_query_arg( 'task', 'new-product', $url ) ); ?>"><i class="icon icon-upload"></i><?php esc_html_e( 'Upload', 'kiwi' ); ?></a></li>
	<?php if ( EDD_FES()->integrations->is_commissions_active() ) { ?>
		<li class="menu-item earnings"><a href="<?php echo esc_url( add_query_arg( 'task', 'earnings', $url ) ); ?>"><i class="icon icon-earnings"></i><?php esc_html_e( 'Earnings', 'kiwi' ); ?></a></li>
	<?php } else { ?>
		<li class="menu-item purchases"><a href="<?php echo esc_url( add_query_arg( 'task', 'download', $url ) ); ?>"><i class="icon icon-purchases"></i><?php esc_html_e( 'Purchases', 'kiwi' ); ?></a></li>
	<?php } ?>
		<li class="menu-item my-items"><a href="<?php echo esc_url( add_query_arg( 'task', 'products', $url ) ); ?>"><i class="icon icon-myitems"></i><?php esc_html_e( 'My items', 'kiwi' ); ?></a></li>
	</ul>
</li>
<div class="post-preview-nav">

<div class="edd_widget_title">
	<h3><?php esc_html_e( 'Post preview', 'kiwi' ); ?></h3>
</div>



<?php 
	global $post;
	$prev_post = get_previous_post();
	
	if (!empty( $prev_post )) {
		$args = array(
			'posts_per_page' => 1,
			'include' => $prev_post->ID
		);
		$prev_post = get_posts($args);
		foreach ($prev_post as $post) {
			setup_postdata($post);
	
	 $background = '';
	 if (has_post_thumbnail( $post->ID ) ):
		$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$background = 'background:url('. esc_url( $image_attributes[0] ).')'; 
	else:
		$image_attributes = array( ' ' ); 
		$background = 'background:#333';
	endif;	
?>



<div class="post-previous">
	<div class="imageoverlay"></div>
	<div class="featured-image" style="<?php echo esc_attr( $background ); ?>"></div>
	
	<div class="info">
		<h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		
		<div class="hovertexts"><p><?php $content = get_the_content(); echo wp_trim_words( $content , '20' ); ?></p> 	
		<a class="previous" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Previous Article', 'kiwi' ); ?></a>
		</div>
			
	</div>	
</div>

<?php
		wp_reset_postdata();
	} //end foreach
} // end if ?>
  
  
  
<?php  
	global $post;  
	$next_post = get_next_post();

	if (!empty( $next_post )) {
	$args = array(
		'posts_per_page' => 1,
		'include' => $next_post->ID
	);
	$next_post = get_posts($args);
	foreach ($next_post as $post) {
		setup_postdata($post);
		
	 $background = '';
	 if (has_post_thumbnail( $post->ID ) ):
		$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$background = 'background:url('. esc_url( $image_attributes[0] ).')';  
	else:
		$image_attributes = array( ' ' ); 
		$background = 'background:#333';
	endif;			
		
?>

<div class="post-next">
	<div class="imageoverlay"></div>
	<div class="featured-image" style="<?php echo esc_attr( $background ); ?>"></div>
	
	<div class="info">
		<h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<div class="hovertexts">
			<p><?php $content = get_the_content(); echo wp_trim_words( $content , '20' ); ?></p>	 
			<a class="previous" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Next Article', 'kiwi' ); ?></a>
		</div>
	</div>
	
</div>
<?php wp_reset_postdata();
} //end foreach
} // end if
?>
<div class="clear"></div>

</div>
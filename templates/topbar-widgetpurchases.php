<?php
 global $kiwi_theme_option;
 
$purchases = edd_get_users_purchases( get_current_user_id(), 20, true, 'complete' );

	$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
	$buyer_link = get_page_link ($url_id);


	if ( class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor() ) {
		$vendor_dashboard = get_permalink( EDD_FES()->helper->get_option( 'fes-vendor-dashboard-page', get_permalink() ) );	
		$show_vendorpage = '<li class="menu-item purchases"><a href="'. esc_url( add_query_arg( 'task', 'download', $vendor_dashboard ) ).'">'. esc_html__( 'View All', 'kiwi' ).'</a></li>';
	} else {
		$show_vendorpage = '<li class="menu-item purchases"><a href="'. esc_url( add_query_arg( 'task', 'download', $buyer_link ) ).'">'. esc_html__( 'View All', 'kiwi' ).'</a></li>';
	}
	
	
	
if ( $purchases ) :
	 foreach ( $purchases as $payment ) :
	 
		$downloads      = edd_get_payment_meta_cart_details( $payment->ID, true );
		$purchase_data  = edd_get_payment_meta( $payment->ID );
		$email          = edd_get_payment_user_email( $payment->ID );

		if ( $downloads ) :		
		
		$limit = array_slice($downloads, 0, $kiwi_theme_option['marketplace-userrole-purchase-limit']);
			
			$i = 0;		
			foreach ( $limit as $download ) :
			$i++;	
			
				// Skip over Bundles. Products included with a bundle will be displayed individually
				if ( edd_is_bundled_product( $download['id'] ) )
					continue; 
				
					$price_id 		= edd_get_cart_item_price_id( $download );
					$download_files = edd_get_download_files( $download['id'], $price_id );
					$name           = get_the_title( $download['id'] );

					// Retrieve and append the price option name
					if ( ! empty( $price_id ) ) {
						$name .= ' - ' . edd_get_price_option_name( $download['id'], $price_id, $payment->ID );
					}

					do_action( 'edd_download_history_row_start', $payment->ID, $download['id'] );
					
						
		if ( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( $download['id'] ) ) {	
			
			echo '<li class="edd_download_thumbnail '.$i.'">';
				
		 // ****************
			if ( class_exists( 'EDD_Front_End_Submissions' ) ) { 
				$vendor_thumbnail = get_post_meta( $download['id'], 'upload_item_thumbnail', true );		
			}

						if ( class_exists( 'EDD_Front_End_Submissions' ) && !empty( $vendor_thumbnail ) ) { 
							foreach($vendor_thumbnail as $key=>$val){
								$url = wp_get_attachment_image_src( $val, 'full' ); 
								$post_thumbnail = '<img src="'. esc_url( $url[0] ).'" width="'.$url[1].'" height="'.$url[2].'">';
							}
						} else {
							$post_thumbnail = get_the_post_thumbnail( $download['id'], apply_filters( 'edd_widgets_kiwipack_thumbnail_size', array( 80,80 ) ) );
						}	
		// ****************
		
				echo '<a href="'.get_permalink( $download['id'] ).'">'; 
					echo $post_thumbnail;
				echo '</a>';
				
			} 
			
			echo '</li>';
			
			endforeach; // End foreach $downloads
				
			
			endif; // End if $purchases
			endforeach;
			
			echo '<li class="clear"></li>';
			echo $show_vendorpage;
			
		
		?>
	
<?php else : ?>
	<p class="edd-no-downloads"><?php esc_html_e( 'You haven\'t purchased any items.', 'kiwi' ); ?></p>
<?php endif; ?>
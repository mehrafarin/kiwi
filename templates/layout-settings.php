<?php global $kiwi_theme_option; ?>

<?php 			
	$grid_columns = $kiwi_theme_option['postlayout-gridcolumns']; 
	
	if ($grid_columns == '1') { 
		$variable_width = ' two-columns'; 
	} elseif ($grid_columns == '2') { 
		$variable_width = ' three-columns'; 
	} elseif ($grid_columns == '3') { 
		$variable_width = ' four-columns'; 
	} else {
		$variable_width = '';
	} 				
?>

<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '2' ) { ?>
		
		<?php if ( have_posts() ) : ?>
		
		<div class="grid-masonry<?php echo esc_attr( $variable_width ); ?> index-layout category">	
		
		<?php while ( have_posts() ) : the_post();			
			get_template_part( 'content', get_post_format() );
		  endwhile; ?>	
		  
		</div>
		
		<div class="clear"></div>
				
		<?php kiwi_content_nav( 'nav-below' ); ?> 

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>
		
<?php } ?>
			
			
			
			
<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '1' && $kiwi_theme_option['postlayout-liststyle'] == '1') { ?>
				
		<?php if ( have_posts() ) : ?>
		
		<?php while ( have_posts() ) : the_post();			
			get_template_part( 'content', get_post_format() );
		  endwhile; ?>				
	
		<div class="clear"></div>
		
		<?php kiwi_content_nav( 'nav-below' ); ?> 

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>
		
<?php } ?>
			
			
			
<!-- <?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '1' && $kiwi_theme_option['postlayout-liststyle'] == '2' ) { ?>
				
		list style 2
		
<?php } ?>	

			
<?php if ( $kiwi_theme_option['postlayout-bloglayout'] == '1' && $kiwi_theme_option['postlayout-liststyle'] == '3' ) { ?>
				
		list style 3
		
<?php } ?>	 -->
		
		
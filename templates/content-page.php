<?php global $kiwi_theme_option; 

/* The template used for displaying page content in page.php */

?>
		
	<?php if ( !is_page_template( 'page-fullwidth.php' ) && has_post_thumbnail() ) : ?>
		<div class="singlepage-thumbs">
			<?php the_post_thumbnail(); ?>
		</div>
	<?php else : ?>			
			<?php the_post_thumbnail(); ?>	
	<?php endif; ?>
	
	
	<?php if ( $kiwi_theme_option['page-pagetitle-enable'] == '1'  ) { ?>
		<h3 class="entry-title"><?php the_title(); ?></h3>
	<?php } ?>			

	<div class="entry-content kiwi-page">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kiwi' ), 'after' => '</div>' ) ); ?>
	</div>
	
	<footer class="entry-meta">
		<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="edit-link">', '</span>' ); ?>
	</footer>
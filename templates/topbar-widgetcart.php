<?php
	$cart_items    = edd_get_cart_contents();
	$cart_quantity = edd_get_cart_quantity();
	$display       = $cart_quantity > 0 ? '' : ' style="display:none;"';
?>

<ul class="dropdown-menu edd-cart">
	<li class="heading"><?php esc_html_e( 'Shopping cart', 'kiwi' ); ?></li>

<?php if( $cart_items ) : ?>

	<?php foreach( $cart_items as $key => $item ) : ?>
		<?php echo edd_get_cart_item_template( $key, $item, false ); ?>
	<?php endforeach; ?>
	
	<?php edd_get_template_part( 'widget', 'cart-checkout' ); ?>

<?php else : ?>
	<?php edd_get_template_part( 'widget', 'cart-empty' ); ?>
<?php endif; ?>
</ul>
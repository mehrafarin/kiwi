<?php global $kiwi_theme_option; ?>

	<?php if ( have_posts() ) : ?>

				<div class="listview">				
					<ul class="layout-sorting">
					
				<?php while ( have_posts() ) : the_post(); ?>
								
					<?php get_template_part( 'templates/marketplace', 'archivescontent' ); ?>
					
		<?php endwhile; ?>
		
			</ul>
		</div>	
		
		<div class="clear"></div>		
		
		<?php kiwi_content_nav() ?>		

<?php else : ?>
	<?php get_template_part( 'templates/content', 'none' ); ?>
<?php endif; ?>
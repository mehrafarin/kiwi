<?php 

	global $kiwi_theme_option;
	
	$public 	= edd_wl_get_query( 'public' ); // get the public lists		
		
	$url_id = $kiwi_theme_option['marketplace-enablelinks-dashboard-subscriber'];
	$buyer_link = get_page_link ($url_id);
	
	if ( class_exists( 'EDD_Front_End_Submissions' ) && EDD_FES()->vendors->vendor_is_vendor() ) {
		$url = get_permalink( EDD_FES()->helper->get_option( 'fes-vendor-dashboard-page', get_permalink() ) );
		$show_vendorpage = '<li class="menu-item view-all"><a href="'. esc_url( add_query_arg( 'task', 'wishlist', $url ) ).'">'. esc_html__( 'View All', 'kiwi' ).'</a></li>';
	} else {
		$show_vendorpage = '<li class="menu-item view-all"><a href="'. esc_url( add_query_arg( 'task', 'wishlist', $buyer_link ) ) .'">'. esc_html__( 'View All', 'kiwi' ).'</a></li>';
	}
?>

<?php 
if ( $public ) : ?>
	
	<?php 
	
	$limit = array_slice($public, 0, $kiwi_theme_option['marketplace-userrole-wishlist-limit'] );
		
	foreach ( $limit as $id ) : ?>
		<li class="menu-item">
			<a href="<?php echo esc_url( edd_wl_get_wish_list_view_uri( $id ) ); ?>" title="<?php echo esc_attr( the_title_attribute( array('post' => $id ) ) ); ?>">
			<?php echo get_the_title( $id ); ?></a> <span class="edd-wl-item-count"><?php echo esc_attr( edd_wl_get_item_count( $id ) ); ?></span>
		</li>

		<?php endforeach; 
		
		echo $show_vendorpage;
	
		?>

<?php else : ?>
	<p class="empty"><?php esc_html_e( 'You have no wishlists.', 'kiwi' ); ?></p>
<?php endif; ?>
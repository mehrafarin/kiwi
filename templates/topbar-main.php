<?php global $kiwi_theme_option; 

if ( $kiwi_theme_option['marketplace-enable-userroles'] == '1' ) {
	
	if( class_exists( 'Easy_Digital_Downloads' ) && class_exists( 'EDD_Front_End_Submissions' )  ) {
		if( is_user_logged_in() && !EDD_FES()->vendors->vendor_is_vendor() && has_nav_menu( 'logged_navigation' ) ) {
			 $location = 'logged_navigation';
		} elseif ( is_user_logged_in() && EDD_FES()->vendors->vendor_is_vendor() && has_nav_menu( 'vendor_navigation' ) ) {
			 $location = 'vendor_navigation';
		} elseif ( !is_user_logged_in() && has_nav_menu( 'nonlogged_navigation' ) ) {
			 $location = 'nonlogged_navigation';
		} else {
			 $location = 'top_navigation';
		}
	} else {
		if( is_user_logged_in() && has_nav_menu( 'logged_navigation' ) ) {
			 $location = 'logged_navigation';
		} elseif ( !is_user_logged_in() && has_nav_menu( 'nonlogged_navigation' ) ) {
			 $location = 'nonlogged_navigation';
		} else {	
			
			if( has_nav_menu( 'top_navigation' ) ) {
				$location = 'top_navigation';
			}
		}
	}
	
} else {
	if( has_nav_menu( 'top_navigation' ) ) {
		$location = 'top_navigation';
	}
}


if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) { 
    $rtl_class = 'top-menu sm-rtl';
} else {
    $rtl_class = 'top-menu sm';
}

?>


					



<div class="topbar topbar-features">
	<div class="container">
		<div class="row">									
			
			<?php if ( $kiwi_theme_option['topbar-contentleft'] == '1' ) { ?>
				<div class="pull-left topmenu">
				<?php if ( has_nav_menu( 'top_navigation' ) || has_nav_menu( 'nonlogged_navigation' ) || has_nav_menu( 'logged_navigation' ) || has_nav_menu( 'vendor_navigation' ) ) { ?> 
					<?php wp_nav_menu(array('theme_location' => $location, 'container' => false, 'menu_class' => $rtl_class, 'walker' => new wp_bootstrap_navwalker())); ?>
				<?php } ?>	
				</div>
			<?php } ?>				
							
			<?php if ( $kiwi_theme_option['topbar-contentleft'] == '2' ) { ?>				 
			<div class="pull-left"> 				
				<?php do_action('kiwi_social_media'); ?>									
			</div>				
			<?php } ?>
							
			<?php if ( $kiwi_theme_option['topbar-contentleft'] == '3' ) { ?>
				<div class="pull-left customtext" style="margin:<?php echo '' . $kiwi_theme_option['topbar-customtext-leftmargin']; ?>">
					<?php echo '' . $kiwi_theme_option['topbar-customtext-left'];	?>
				</div>
			<?php } ?>
							
			<?php if ( $kiwi_theme_option['topbar-contentleft'] == '4' ) { ?>					
			<?php } ?>
			
			
			
			<?php if ( $kiwi_theme_option['topbar-contentleft'] == '5' ) { ?>				 
			<div class="pull-left topbar-logo"> 					

				<?php if ( $kiwi_theme_option['settings-headerlogo-enable'] == '1' ) { ?>	
					<div itemscope itemtype="http://schema.org/Organization">
						<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="logo" src="<?php echo esc_url( $kiwi_theme_option['settings-headerlogo']['url'] ); ?>" height="<?php echo '' . $kiwi_theme_option['settings-logoheight']; ?>" style="margin:<?php echo '' . $kiwi_theme_option['settings-logomargin']; ?>" class="main-logo" alt=""></a>
					</div>
				<?php } ?>			
					
				<?php if ( $kiwi_theme_option['settings-headerlogo-enable'] == '0' ) { ?>	
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo esc_html( bloginfo( 'name' ) ); ?></a></h1>
				<h2 class="site-description"><?php echo esc_html( bloginfo( 'description' ) ); ?></h2>	
			<?php } ?> 
				
			</div>				
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['topbar-contentleft'] == '6' ) { ?>
				<div class="pull-left customtext" style="margin:<?php echo '' . $kiwi_theme_option['topbar-customtext-leftmargin']; ?>"><?php if( function_exists('kiwi_marketplace_stats')){ kiwi_marketplace_stats(); } ?></div>					
			<?php } ?>
			
			<!-- -->
			
			<?php if ( $kiwi_theme_option['topbar-contentright'] == '1' ) { ?>
				<div class="pull-right topmenu">
				<?php if ( has_nav_menu( 'top_navigation' ) || has_nav_menu( 'nonlogged_navigation' ) || has_nav_menu( 'logged_navigation' ) || has_nav_menu( 'vendor_navigation' ) ) { ?>
					<?php wp_nav_menu(array('theme_location' => $location,  'container' => false, 'menu_class' => $rtl_class, 'walker' => new wp_bootstrap_navwalker() )); ?>
				<?php } ?>	
				</div>
			<?php } ?>
									
			<?php if ( $kiwi_theme_option['topbar-contentright'] == '2' ) { ?>				 
			<div class="pull-right"> 									
				<?php do_action('kiwi_social_media'); ?>				
			</div>
			<?php } ?>
									
			<?php if ( $kiwi_theme_option['topbar-contentright'] == '3' ) { ?>
				<div class="pull-right customtext" style="padding:<?php echo '' . $kiwi_theme_option['topbar-customtext-rightmargin']; ?>">
					<?php echo '' . $kiwi_theme_option['topbar-customtext-right'];	?>
				</div>
			<?php } ?>
							
			<?php if ( $kiwi_theme_option['topbar-contentright'] == '4' ) { ?>					
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['topbar-contentright'] == '5' ) { ?>				 
			<div class="pull-right topbar-logo"> 					

				<?php if ( $kiwi_theme_option['settings-headerlogo-enable'] == '1' ) { ?>	
					<div itemscope itemtype="http://schema.org/Organization">
						<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="logo" src="<?php echo esc_url( $kiwi_theme_option['settings-headerlogo']['url'] ); ?>" height="<?php echo '' . $kiwi_theme_option['settings-logoheight']; ?>" style="margin:<?php echo '' . $kiwi_theme_option['settings-logomargin']; ?>" class="main-logo" alt=""></a>
					</div>
				<?php } ?>			
					
				<?php if ( $kiwi_theme_option['settings-headerlogo-enable'] == '0' ) { ?>	
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo esc_html( bloginfo( 'name' ) ); ?></a></h1>
				<h2 class="site-description"><?php echo esc_html( bloginfo( 'description' ) ); ?></h2>	
			<?php } ?> 
				
			</div>				
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['topbar-contentright'] == '6' ) { ?>
				<div class="pull-right customtext" style="margin:<?php echo '' . $kiwi_theme_option['topbar-customtext-leftmargin']; ?>"><?php if( function_exists('kiwi_marketplace_stats')){ kiwi_marketplace_stats(); } ?></div>					
			<?php } ?>
			
		</div>			
	</div>
</div>
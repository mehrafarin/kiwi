<div class="author-info">

    <div class="container-xs-height">
        <div class="col-xs-height col-top author-avatar">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
		</div>
       
	   <div class="col-xs-height col-top author-right">
	   
		<?php if ( is_author() ) { ?>
			<h3><span class="author post-author"><?php esc_html_e( 'About', 'kiwi' ); ?></span> <?php echo get_the_author(); ?></h3>
		<?php } ?>
	   
	   <?php if ( !is_author() ) { ?>
		<h3><span class="author post-author"><?php esc_html_e( 'Written by', 'kiwi' ); ?></span> <?php echo get_the_author(); ?></h3>
	   <?php } ?>
	  
		<div class="author-bio"><?php the_author_meta( 'description' ); ?></div>
			
			<?php if ( !is_author() ) { ?>
			<div>
				<a class="author-link post-author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
				<?php printf( esc_html__( 'View all posts by %s', 'kiwi' ), get_the_author() ); ?>
				</a>
			</div>
			<?php } ?>
			
		</div>
    </div>
	
	<div class="clear"></div>
	
</div>
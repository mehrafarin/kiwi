<?php
/*
Template Name: Sidebar Left 1
*/

get_header(); 

global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['sidebar-pages-sidebarlayout'] <= '2' || $kiwi_theme_option['sidebar-pages-enable'] == '0') { ?>
	<div class="container">
		<div class="row" role="main">
		
			<div class="col-sm-12 col-md-8 page-layout">	
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'templates/content', 'page' ); ?>
					
					<?php if ( $kiwi_theme_option['page-comment-enable'] == '1') { ?>
						<?php comments_template( '', true ); ?>
					<?php } ?>	
					
				<?php endwhile; ?>
			</div>		
		
			<div class="col-sm-12 col-md-4 sidebar-layout blog">	
				<div class="sidebar">
					<?php
						if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebarone'])):
						endif;
					?>
				</div>
			</div>
					
		</div>
	</div>
<?php } ?>

<?php get_footer(); ?>
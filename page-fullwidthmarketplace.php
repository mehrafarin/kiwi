<?php
/*
Template Name: Full width :: Marketplace
*/

get_header(); 

global $kiwi_theme_option; ?>

<div class="container marketplace mp-vc-items"<?php esc_attr( kiwi_rtl() ); ?>>
	<div class="row" role="main">	
		
		<div class="container">
				
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>				
			<?php the_content(); ?>	
		<?php endwhile; endif; ?>
		
&nbsp;
			</div>
	</div>
</div>			

<?php get_footer(); ?>
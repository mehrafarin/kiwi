<?php 
	global $kiwi_theme_option, $post; 

	$featured_link = '';
	$featured = get_post_thumbnail_id();
	
	$postcategories = get_the_category(); 
	
	if (has_post_thumbnail( $post->ID ) ):
		$image_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$featured_link = $image_full[0];  
	else:
		$image_attributes = array( ' ' ); 
		$featured_link = ' ';
	endif;
 					
	if (empty($featured)) {
		$fimage = ' no-featuredimage';
	} else {
		$fimage = '';
	} 
	
?>


<?php if ( !is_single() ) { ?>	
	<article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php } ?>

<?php if ( is_single() ) { ?>
	<div itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class('kiwi-single-pt'); ?>>
<?php } ?>
			
		<header class="entry-header">			
			<div class="featured-image">
				
				<?php if ( is_single() && has_post_thumbnail() ) { ?>
					<div class="image-overlay">
						<a href="<?php echo esc_url( $featured_link ); ?>"><span class="image-overlay-inside"></span></a>
					</div>
				<?php } ?>
			
				 <?php if ( ! post_password_required() && ! is_attachment() ) :
						echo '<span itemprop="image">';
							the_post_thumbnail();
						echo '</span>';
					endif; ?> 			
			</div>	
			
				<?php if ( is_sticky() && is_home() && ! is_paged() ) { ?>
					<div class="sticky-post">
						<div class="ribbon"><i class="fa fa-thumb-tack"></i></div>
					</div>
				<?php } ?>			
		</header>
			
			<div class="post-standard<?php echo esc_attr( $fimage ); ?>">	
				
				<?php if ( is_single() ) { ?>
					<div class="meta-category-single">
					<span class="pull-left"><?php kiwi_entry_meta(); ?></span>
					
					<?php if ( $kiwi_theme_option['blog-editpostlink-enable-single'] == '0' && is_single() ) { ?>
						<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="pull-right comment-edit-link">', '</span>' ); ?>
					<?php } ?>
					
					<div class="clear"></div>
					</div>
				<?php } ?>
							
				<?php if ( $kiwi_theme_option['postformat-standardtitle'] == '1' && !is_single() ) { ?>
					
						<?php if ( !empty($postcategories)) { ?> 						
							<span class="meta-category pull-left"><i class="fa fa-tag"></i> <?php the_category(', '); ?> </span>
						<?php } ?> 
				
						
						
						<?php if ( $kiwi_theme_option['blog-editpostlink-enable'] == '1') { ?>
							<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="grid edit-link pull-right">', '</span>' ); ?>
						<?php } ?>
						
						<div class="clear"></div>
				
				
					<div class="post-title">
						<h3 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
					</div>
					<?php } ?>
				
					<?php if ( $kiwi_theme_option['postformat-standardtitle'] == '1' && is_single() ) { ?>		
						<h3 class="entry-title" itemprop="headline"><?php the_title(); ?></h3>			
					<?php } ?>	

							<?php if ( $kiwi_theme_option['postformat-standardtitle'] == '0' && !is_single() ) { ?>		
								<div class="extra-space"></div>			
							<?php } ?>
				
				<?php if ( $kiwi_theme_option['postformat-standardexcerpt'] == '1' && !is_single() || is_search() ) { ?>
					<div class="grid-excerpt" itemprop="description"><?php the_excerpt(); ?></div>
				<?php } ?>
				 
				 
					<?php if ( $kiwi_theme_option['postformat-standardcontent'] == '1' && !is_single() ) { ?>
						<div itemprop="articleBody">
							<?php the_content(); ?>
						</div>		
					<?php } ?>					
					
					<?php if ( is_single() ) : ?>
						 <div itemprop="articleBody">		
							<?php the_content(); ?>
						 </div>	
					<?php endif; ?>	
						
						
					<?php if ( $kiwi_theme_option['postformat-standarddate'] >= '1' && !is_single() || $kiwi_theme_option['postformat-standardmeta'] >= '1' && !is_single() ) { ?>
					<div class="kiwi-postformat meta-data">						
							<div class="pull-left">
								<?php if ( $kiwi_theme_option['postformat-standarddate'] == '1' && !is_single() ) { ?>			
									<span itemprop="datePublished"><?php echo esc_html( get_the_date( get_option('date_format') ) ); ?></span>
								<?php } ?>
							</div>	
						
							<div class="pull-right">
								<?php if ( $kiwi_theme_option['postformat-standardmeta'] == '1' && !is_single() ) { ?>
									<a href="<?php comments_link(); ?>" class="meta-commentlink"><span class="comments"><i class="fa fa-comment"></i> <?php comments_number( '0', '1', '%' ); ?></span></a>
									
								<?php } ?>
							</div>			
					<div class="clear"></div>
					</div>	
					<?php } ?>						
										
						<?php if ( is_single() ) { ?>
							<?php kiwi_singlebottom_meta(); ?>
						<?php } ?>
				
			</div>			

		
<?php if ( !is_single() ) { ?>
	</article>
<?php } ?>

<?php if ( is_single() ) { ?>
	</div>
<?php } ?>

<?php if ( $kiwi_theme_option['blog-authorbio-enable'] == '1' && is_single() || is_multi_author() && is_single() ) : ?>
			<?php if ( get_the_author_meta( 'description' )  ) : ?>
				<?php get_template_part( 'templates/author', 'bio' ); ?>
			<?php endif; ?>
<?php endif; ?>	
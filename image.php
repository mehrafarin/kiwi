<?php 

get_header(); 
 
$metadata = wp_get_attachment_metadata(); 

$attachments = array_values( get_children( array( 'post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) ) );
foreach ( $attachments as $k => $attachment ) :
	if ( $attachment->ID == $post->ID )
		break;
endforeach;

if ( count( $attachments ) > 1 ) :
	$k++;
	if ( isset( $attachments[ $k ] ) ) :		
		$next_attachment_url = get_attachment_link( $attachments[ $k ]->ID );
	else :
		$next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID );
	endif;
	else :
		$next_attachment_url = wp_get_attachment_url();
	endif;
?>

	<div id="primary" class="site-image">
		<div id="content" role="main">
				
	<div class="col-sm-12 col-md-9 entry-attachment">

	<?php while ( have_posts() ) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?>>					

			<a href="<?php echo esc_url( wp_get_attachment_url() ); ?>" title="<?php the_title_attribute(); ?>" rel="attachment">
			<?php $attachment_size = apply_filters( 'kiwi_attachment_size', 'full' );
				echo wp_get_attachment_image( $post->ID, $attachment_size ); ?>
			</a>
			
			<div class="image-content">
				<h3 class="entry-title"><?php the_title(); ?></h3>	

				<?php if ( ! empty( $post->post_excerpt ) ) : ?>
				<div class="entry-caption">
					<div class="sep"><?php esc_html_e( 'Image caption', 'kiwi' ); ?></div>
					<?php the_excerpt(); ?>
				</div>
				<?php endif; ?>
				
				
				<?php if ( ! empty( $post->post_content ) ) : ?>
						<div class="entry-description">
							<div class="sep"><?php esc_html_e( 'Image description', 'kiwi' ); ?></div>
							<?php the_content(); ?>
						</div>
				<?php else : ?>
						<div class="entry-description">
							<div class="sep"><?php esc_html_e( 'Image description', 'kiwi' ); ?></div>
							<?php esc_html_e( 'Image description is not available.', 'kiwi' ); ?>
						</div>
				<?php endif; ?>
									
			</div>
		
			</div>			
			
	</div>
				
	<div class="col-sm-12 col-md-3 sidebar">
	
		<div class="filestats">					
			<div class="pull-left">	<h3><?php esc_html_e( 'File statistics', 'kiwi' ); ?></h3> </div>								
			<div class="pull-right"> <?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="edit-link">', '</span>' ); ?></div>
			
			<div class="clear"></div>
									
			<div class="data-info">											
				
			<?php esc_html_e( 'Uploaded on:', 'kiwi' ); ?> <span class="post-date updated"><?php echo esc_html( get_the_date() ); ?></span><br />
			<?php esc_html_e( 'Dimensions:', 'kiwi' ); ?> <span><?php echo esc_attr( $metadata['width'] ); ?> <?php esc_html_e( '&times;', 'kiwi' ); ?> <?php echo esc_attr( $metadata['height'] ); ?></span><br />
			<?php esc_html_e( 'File size:', 'kiwi' ); ?> <span>
			
					<?php
						$docsize = @filesize( get_attached_file( $attachment->ID ) );
							if (FALSE === $docsize)
						$docsize = 0;
								else
						$docsize = size_format($docsize, 2);
								echo esc_html( $docsize );  
						?>
					</span><br />
					
			<?php esc_html_e( 'Full size:', 'kiwi' ); ?> <span><a href="<?php echo esc_url( wp_get_attachment_url() ); ?>"><?php esc_html_e( 'download', 'kiwi' ); ?></a></span><br />
			</div>
		</div>
		
		<div class="btn-group image-navigation">
			<?php previous_image_link( false, '<span class="previous-image">' . esc_html__( 'Previous Image', 'kiwi' ) . '</span>' ); ?>
			<?php next_image_link( false, '<span class="previous-image">' . esc_html__( 'Next Image', 'kiwi' ) . '</span>' ); ?>
		</div>	
							
	</div>
	
	<div class="clear"></div>

	<div class="col-sm-12 col-md-9">
		<?php comments_template(); ?>
	</div>
				
	<?php endwhile; ?>

		</div>
	</div>

<?php get_footer(); ?>
<div class="container-fluid mp-fes-vendor-menu">
	<div class="container align-center"> 
		<div class="row">
			<?php mp_license_menu(); ?>
		</div>
	</div>
</div>

<div class="container mp-full-dashboard">
<div id="fes-vendor-dashboard">

<h3><?php esc_html_e( 'Author Review', 'kiwi' ); ?></h3>

<div class="mp-vendor-review-edit-dashboard"> 

<?php
/**
 * This template is used to render the Vendor Feedback page
 */

global $edd_options;

$payment_key    = urldecode( $_GET['payment_key'] );
$edd_receipt_id = edd_get_purchase_id_by_key( $payment_key );
$customer_id    = edd_get_payment_user_id( $edd_receipt_id );
$payment        = get_post( $edd_receipt_id );
?>

<?php if ( isset( $_GET['vendor_feedback_submitted'] ) ) { ?>
	<p class="edd-reviews-vendor-feedback-submitted"><strong><?php _e( 'Your feedback has been successfully submitted.', 'kiwi' ); ?></strong></p>
<?php } ?>

<?php if ( empty( $payment ) ) { ?>
	<div class="edd_errors edd-alert edd-alert-error">
		<?php _e( 'The specified receipt ID appears to be invalid', 'kiwi' ); ?>
	</div>
<?php } ?>


<?php do_action( 'mp_hook_before_review_table' ); ?>	

<?php
$meta   = edd_get_payment_meta( $payment->ID );
$cart   = edd_get_payment_meta_cart_details( $payment->ID, true );
$user   = edd_get_payment_meta_user_info( $payment->ID );
$email  = edd_get_payment_user_email( $payment->ID );
$status = edd_get_payment_status( $payment, true );

if ( $cart ) { ?>
	
	<div class="table-responsive">
	<table id="edd_vendor_feedback_" class="mp-edd-vendor-feedback">
		<thead>
			<tr class="vendor-feedback-row">
				<?php do_action( 'edd_vendor_feedback_header_start' ); ?>				
					<th  colspan="2" class="edd_download_download_name"><?php esc_html_e( 'Product', 'kiwi' ); ?></th>				
					<th class="edd_download_download_name"><?php esc_html_e( 'Author review', 'kiwi' ); ?></th>			
				<?php do_action( 'edd_vendor_feedback_header_end' ); ?>
			</tr>
		</thead>
	
	
	<?php
	foreach ( $cart as $key => $item ) {
		if ( ! apply_filters( 'edd_user_can_view_receipt_item', true, $item ) ) {
			continue; // Skip this item if can't view it
		}

		$post = get_post( $item['id'] );
		$author = get_userdata( $post->post_author );

		if ( ! EDD_FES()->vendors->user_is_vendor( $post->post_author ) ) {
			continue; // Skip if the user for this download isn't a vendor
		}

		if ( empty( $item['in_bundle'] ) ) { 
		
		$heading = isset( $edd_options['edd_reviews_vendor_feedback_form_heading'] ) ? $edd_options['edd_reviews_vendor_feedback_form_heading'] : __( 'Author rating', 'kiwi' );
				
		?>
			<tr class="edd-reviews-vendor-feedback-item">
			
			
			<td class="fes-product-list-td"><?php
				if ( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( $post->ID ) ) {
					echo get_the_post_thumbnail( $post->ID, 'full' );
				} ?></td>
			
			<td class="fes-product-list-td mp-product-info">
				<h3><a href="<?php echo get_permalink( $item['id'] ); ?>"><?php echo esc_html( $item['name'] ); ?></a></h3>
				<div class="mp-product-author"><span><?php _e( 'Author:', 'kiwi' ); ?></span> <?php echo $author->display_name; ?></p>
				
				<div class="mp-purchase-date"><span><?php esc_html_e( 'Purchase date:', 'kiwi' ); ?></span> <?php echo date_i18n( get_option( 'date_format' ), strtotime( $meta['date'] ) ); ?></div>
				<div class="mp-invoice-link"><a href="<?php echo esc_url( add_query_arg( 'payment_key', $payment_key, edd_get_success_page_uri() ) ); ?>"><?php esc_html_e( 'View invoice', 'kiwi' ); ?></a></p>
				
			</td>
			
			
			<td class="fes-product-list-td">
						
				<form action="<?php echo esc_url( add_query_arg( 'vendor_feedback_submitted', true ) ); ?>" method="post" name="<?php echo edd_reviews()->vendor_feedback_form_args( 'name_form' ) ?>" id="<?php echo edd_reviews()->vendor_feedback_form_args( 'id_form' ) ?>" class="<?php echo edd_reviews()->vendor_feedback_form_args( 'class_form' ) ?>">

					<?php wp_nonce_field( 'edd-review-vendor-feedback_' . $post->ID ); ?>
					<input type="hidden" name="download_id" value="<?php echo $post->ID; ?>" />
					<input type="hidden" name="edd_action" value="process_vendor_review" />

					<div class="edd-reviews-vendor-feedback-item-wrap">
						<p class="edd-reviews-vendor-feedback-rating">
							<label for="edd-reviews-rating"><?php _e( 'Author Rating', 'kiwi' ) ?></label>
							<?php edd_reviews()->render_star_rating_html(); ?>
						</p>

						<p class="edd-reviews-vendor-feedback-item-as-described">
							<label class="mp-fill-fes-feedback" for="edd-reviews-item-as-described"><?php echo sprintf( __( 'Was item description accurate?', 'kiwi' ), ucwords( edd_get_label_singular() ) ); ?></label>
							<label>
								<input type="radio" name="edd-reviews-item-as-described" value="1" />
								<span><?php _e( 'Yes', 'kiwi' ); ?></span>
							</label>

							<label>
								<input type="radio" name="edd-reviews-item-as-described" value="0" />
								<span><?php _e( 'No' ,'kiwi' ); ?></span>
							</label>
						</p>

						<p class="edd-reviews-vendor-feedback-comment">
							<div class="mp-fill-fes-feedback"><?php _e( 'Feedback', 'kiwi' ); ?></div>
							<textarea id="edd-reviews-vendor-feedback-comments" name="edd-reviews-vendor-feedback-comments" cols="55" rows="5" aria-required="true" required="required"></textarea>
						</p>

						<p class="edd-reviews-vendor-feedback-form-submit">
							<input type="submit" class="edd-reviews-vendor-feedback-form-submit" id="edd-reviews-vendor-feedback-form-submit" name="edd-reviews-vendor-feedback-form-submit" value="<?php _e( 'Submit Feedback', 'kiwi' ) ?>" />
						</p>
					</div>
				</form>
			</td>
			
		<?php
		} // end if
		echo '</tr>';
	} // end foreach ?>
	
	</table>
	</div>
	
	<?php do_action( 'mp_hook_after_review_table' ); ?>

	<?php
} // end if ?>


</div>

</div>
</div>
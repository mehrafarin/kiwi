<?php
/* This template is used to display the Checkout page when items are in the cart */ 

global $post; ?>
<table id="edd_checkout_cart" <?php if ( ! edd_is_ajax_disabled() ) { echo 'class="ajaxed"'; } ?>>
	<thead>
		<tr>
			<?php do_action( 'edd_checkout_table_header_first' ); ?>
			<th><?php esc_html_e( 'Item Name', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Item Price', 'kiwi' ); ?></th>
			<th colspan="2"><?php esc_html_e( 'Quantity', 'kiwi' ); ?></th>			
			<?php do_action( 'edd_checkout_table_header_last' ); ?>
		</tr>
	</thead>
	<tbody>
		<?php $cart_items = edd_get_cart_contents(); ?>
		<?php do_action( 'edd_cart_items_before' ); ?>
		<?php if ( $cart_items ) : ?>
			<?php foreach ( $cart_items as $key => $item ) : ?>
				<tr class="edd_cart_item" id="edd_cart_item_<?php echo esc_attr( $key ) . '_' . esc_attr( $item['id'] ); ?>" data-download-id="<?php echo esc_attr( $item['id'] ); ?>">
					<?php do_action( 'edd_checkout_table_body_first', $item ); ?>
					<td class="edd_cart_item_name">
						<?php
						
							if ( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail( $item['id'] ) ) {
								
							// ****************
							if ( class_exists( 'EDD_Front_End_Submissions' ) ) { 
								$vendor_thumbnail = get_post_meta( $item['id'], 'upload_item_thumbnail', true );		
							}

										if ( class_exists( 'EDD_Front_End_Submissions' ) && !empty( $vendor_thumbnail ) ) { 
											foreach($vendor_thumbnail as $keys=>$val){
												$url = wp_get_attachment_image_src( $val, 'full' ); 
												$post_thumbnail = '<img src="'. esc_url( $url[0] ).'" width="'.$url[1].'" height="'.$url[2].'">';
											}
										} else {
											$post_thumbnail = get_the_post_thumbnail( $item['id'], apply_filters( 'edd_checkout_image_size', array( 80,80 ) ) );
										}	
							// ****************	
										
										echo '<div class="edd_cart_item_image"><a href="' . get_permalink( $item['id'] ) . '">';
											echo $post_thumbnail;
										echo '</a></div>';
									}
							
							$item_title = get_the_title( $item['id'] );
							$variable_option = edd_get_cart_item_price_name( $item );
							if ( ! empty( $item['options'] ) && edd_has_variable_prices( $item['id'] ) ) {								
								$variable_option = ' <span class="variable-option">(' . edd_get_cart_item_price_name( $item ) .')</span>';
							}
							echo '<span class="edd_checkout_cart_item_title"><a href="' . get_permalink( $item['id'] ) . '">' . esc_html( $item_title ) . '</a></span>';
							echo $variable_option;
						?>
					</td>
					<td class="edd_cart_item_price">
						<?php echo edd_cart_item_price( $item['id'], $item['options'] );
							do_action( 'edd_checkout_cart_item_price_after', $item );
						?>
					</td>	
					<td class="edd_cart_actions">
						<?php if( edd_item_quantities_enabled() ) : ?>
							<input type="number" min="1" step="1" name="edd-cart-download-<?php echo esc_attr( $key ); ?>-quantity" data-key="<?php echo esc_attr( $key ); ?>" class="edd-input edd-item-quantity" value="<?php echo esc_attr( edd_get_cart_item_quantity( $item['id'], $item['options'] ) ); ?>"/>
							<input type="hidden" name="edd-cart-downloads[]" value="<?php echo esc_attr( $item['id'] ); ?>"/>
							<input type="hidden" name="edd-cart-download-<?php echo esc_attr( $key ); ?>-options" value="<?php echo esc_attr( serialize( $item['options'] ) ); ?>"/>
						<?php endif; ?>
						<?php do_action( 'edd_cart_actions', $item, $key ); ?>
						<a class="edd_cart_remove_item_btn" href="<?php echo esc_url( edd_remove_item_url( $key ) ); ?>"><?php esc_html_e( 'Remove', 'kiwi' ); ?></a>
					</td>
					<?php do_action( 'edd_checkout_table_body_last', $item ); ?>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php do_action( 'edd_cart_items_middle' ); ?>
		<!-- Show any cart fees, both positive and negative fees -->
		<?php if( edd_cart_has_fees() ) : ?>
			<?php foreach( edd_get_cart_fees() as $fee_id => $fee ) : ?>
				<tr class="edd_cart_fee" id="edd_cart_fee_<?php echo esc_attr( $fee_id ); ?>">
				<?php do_action( 'edd_cart_fee_rows_before', $fee_id, $fee ); ?>
					<td class="edd_cart_fee_label"><?php echo esc_html( $fee['label'] ); ?></td>
					<td class="edd_cart_fee_amount"><?php echo esc_html( edd_currency_filter( edd_format_amount( $fee['amount'] ) ) ); ?></td>
					<td>
						<?php if( ! empty( $fee['type'] ) && 'item' == $fee['type'] ) : ?>
							<a href="<?php echo esc_url( edd_remove_cart_fee_url( $fee_id ) ); ?>"><?php esc_html_e( 'Remove', 'kiwi' ); ?></a>
						<?php endif; ?>
					</td>
					<?php do_action( 'edd_cart_fee_rows_after', $fee_id, $fee ); ?>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>

		<?php do_action( 'edd_cart_items_after' ); ?>
	</tbody>
	<tfoot>

		<?php if( has_action( 'edd_cart_footer_buttons' ) ) : ?>
			<tr class="edd_cart_footer_row<?php if ( edd_is_cart_saving_disabled() ) { echo ' edd-no-js'; } ?>">
				<th colspan="<?php echo esc_attr( edd_checkout_cart_columns() ); ?>">
					<?php do_action( 'edd_cart_footer_buttons' ); ?>
				</th>
			</tr>
		<?php endif; ?>

		<?php if( edd_use_taxes() ) : ?>
			<tr class="edd_cart_footer_row edd_cart_subtotal_row"<?php if ( ! edd_is_cart_taxed() ) echo ' style="display:none;"'; ?>>
				<?php do_action( 'edd_checkout_table_subtotal_first' ); ?>
				<th colspan="<?php echo esc_attr( edd_checkout_cart_columns() ); ?>" class="edd_cart_subtotal">
					<?php esc_html_e( 'Subtotal: ', 'kiwi' ); ?><span class="edd_cart_subtotal_amount"><?php echo esc_html( edd_cart_subtotal() ); ?></span>
				</th>
				<?php do_action( 'edd_checkout_table_subtotal_last' ); ?>
			</tr>
		<?php endif; ?>
		
		<tr class="edd_cart_footer_row edd_cart_discount_row" <?php if( ! edd_cart_has_discounts() )  echo ' style="display:none;"'; ?>>
			<?php do_action( 'edd_checkout_table_discount_first' ); ?>
			<th colspan="<?php echo edd_checkout_cart_columns(); ?>" class="edd_cart_discount">
				<?php edd_cart_discounts_html(); ?>
			</th>
			<?php do_action( 'edd_checkout_table_discount_last' ); ?>
		</tr>

		<?php if( edd_use_taxes() ) : ?>
			<tr class="edd_cart_footer_row edd_cart_tax_row"<?php if( ! edd_is_cart_taxed() ) echo ' style="display:none;"'; ?>>
				<?php do_action( 'edd_checkout_table_tax_first' ); ?>
				<th colspan="<?php echo edd_checkout_cart_columns(); ?>" class="edd_cart_tax">
					<?php esc_html_e( 'Tax: ', 'kiwi' ); ?><span class="edd_cart_tax_amount" data-tax="<?php echo esc_attr( edd_get_cart_tax( false ) ); ?>"><?php echo esc_html( edd_cart_tax() ); ?></span>
				</th>
				<?php do_action( 'edd_checkout_table_tax_last' ); ?>
			</tr>

		<?php endif; ?>

		<tr class="edd_cart_footer_row">
			<?php do_action( 'edd_checkout_table_footer_first' ); ?>
			<th colspan="<?php echo esc_attr( edd_checkout_cart_columns() ); ?>" class="edd_cart_total"><?php esc_html_e( 'Total', 'kiwi' ); ?>: <span class="edd_cart_amount" data-subtotal="<?php echo esc_attr( edd_get_cart_total() ); ?>" data-total="<?php echo esc_attr( edd_get_cart_total() ); ?>"><?php echo esc_html( edd_cart_total() ); ?></span></th>
			<?php do_action( 'edd_checkout_table_footer_last' ); ?>
		</tr>
	</tfoot>
</table>
<?php 
/*Load Scripts for password reset page*/
wp_enqueue_script( 'zxcvbn-async' );
wp_enqueue_script( 'user-profile' );
wp_enqueue_script( 'password-strength-meter' );
wp_enqueue_script( 'user-suggest' );

/* $rp_key = isset($_POST['key']) ? $_POST['key'] : '';
$rp_login = isset($_POST['login']) ? $_POST['login'] : '';
$pass1 = isset($_POST['pass1']) ? $_POST['pass1'] : '';
$pass2 = isset($_POST['pass2']) ? $_POST['pass2'] : ''; */
?>

<div class="edd_widget_title"><h3><?php _e('Custom Reset Password','kiwi'); ?></h3></div>

<div class="mp-custom-form-edd">
<form name="resetpassform" id="resetpassform" action="<?php echo esc_url( network_site_url( 'wp-login.php?action=resetpass', 'login_post' ) ); ?>" method="post" autocomplete="off">
	
	
	<input type="hidden" id="user_login" value="<?php echo esc_attr( $rp_login ); ?>" autocomplete="off" />

	<div class="user-pass1-wrap">
		<p>
			<label for="pass1"><?php _e( 'New password','kiwi' ) ?></label>
		</p>

		<div class="wp-pwd">
			<span class="password-input-wrapper">
				<input type="password" data-reveal="1" data-pw="<?php echo esc_attr( wp_generate_password( 16 ) ); ?>" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" aria-describedby="pass-strength-result" />
			</span>
			<div id="pass-strength-result" class="hide-if-no-js" aria-live="polite"><?php _e( 'Strength indicator','kiwi' ); ?></div>
		</div>
	</div>

	<p class="user-pass2-wrap">
		<label for="pass2"><?php _e( 'Confirm new password','kiwi' ) ?></label><br />
		<input type="password" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" />
	</p>
	
	<div class="login-error"></div>

	<p class="mp-indicator-hint"><?php echo wp_get_password_hint(); ?></p>
	<br class="clear" />

	<?php
	/**
	 * Fires following the 'Strength indicator' meter in the user password reset form.
	 *
	 * @since 3.9.0
	 *
	 * @param WP_User $user User object of the user whose password is being reset.
	 */
	do_action( 'resetpass_form', $user );
	?>
	<input type="hidden" name="rp_key" value="<?php echo esc_attr( $rp_key ); ?>" />
	<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Reset Password','kiwi'); ?>" /></p>
</form>
</div>
<?php
/**
 * Edit Wish List template
*/

$list_id   = edd_wl_get_list_id();
$wish_list = get_post( $list_id );
$content   = $wish_list->post_content;
$title     = get_the_title( $list_id );
$privacy   = get_post_status( $list_id );

?>

<div class="container-fluid mp-fes-vendor-menu">

	<div class="container align-center"> 
		<div class="row">
			<?php mp_license_menu(); ?>
		</div>
	</div>

</div>

<div class="container mp-full-dashboard">
<div id="fes-vendor-dashboard">

<h3><?php esc_html_e( 'Edit wishlist', 'kiwi' ); ?></h3>

<div class="mp-wishlist-edit-dashboard"> 

<form action="<?php echo esc_url( add_query_arg( 'updated', true ) ); ?>" class="wish-list-form" method="post">
	<p>
		<label for="list-title"><?php esc_html_e( 'Title', 'kiwi' ); ?> <span class="required">*</span></label>
		<input type="text" name="list-title" id="list-title" value="<?php echo esc_attr( $title ); ?>">
	</p>
	<p>
		<label for="list-description"><?php esc_html_e( 'Description', 'kiwi' ); ?></label>
		<textarea name="list-description" id="list-description" rows="2" cols="30"><?php echo esc_attr( $content ); ?></textarea>
	</p>
	<p>
		<select name="privacy">
			<option value="private" <?php selected( $privacy, 'private' ); ?>><?php esc_html_e( 'Private - only viewable by you', 'kiwi' ); ?></option>
			<option value="publish" <?php selected( $privacy, 'publish' ); ?>><?php esc_html_e( 'Public - viewable by anyone', 'kiwi' ); ?></option>
		</select>
	</p>
	<p>
		<input type="submit" value="<?php esc_html_e( 'Update', 'kiwi' ); ?>" class="button">
	</p>

	<input type="hidden" name="submitted" id="submitted" value="true">

	<?php wp_nonce_field( 'list_nonce', 'list_nonce_field' ); ?>
</form>

<?php echo edd_wl_delete_list_link(); ?>


</div>

</div>
</div>
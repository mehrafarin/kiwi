<?php

$color = edd_get_option( 'checkout_color', 'gray' );
$color = ( $color == 'inherit' ) ? '' : $color;

if ( is_user_logged_in() ) :
	$license_keys = edd_software_licensing()->get_license_keys_of_user();
	?>
	<script type="text/javascript">jQuery(document).ready(function($){ $(".edd_sl_show_key").on("click",function(e){e.preventDefault(),$(this).parent().find(".edd_sl_license_key").on("click",function(){$(this).select()}).fadeToggle("fast")}); });</script>
	<?php do_action( 'edd_sl_license_keys_before' ); ?>
	
	<div class="table-responsive">
	<table id="edd_sl_license_keys" class="edd_sl_table mp-license-table">
		<thead>
			<tr class="edd_sl_license_row">
				<?php do_action('edd_sl_license_keys_header_before'); ?>
				<th class="edd_sl_item"><?php echo esc_html_e( 'Item', 'kiwi' ); ?></th>
				<th class="edd_sl_details"><?php echo esc_html_e( 'License Details', 'kiwi' ); ?></th>
				<th class="edd_sl_purchase"><?php echo esc_html_e( 'Purchase', 'kiwi' ); ?></th>
				<?php do_action('edd_sl_license_keys_header_after'); ?>
			</tr>
		</thead>
		<?php if ( $license_keys ) : ?>
			<?php foreach ( $license_keys as $license ) : ?>
				<?php $payment_id = edd_software_licensing()->get_payment_id( $license->ID ); ?>
				<tr class="edd_sl_license_row">
					<?php do_action( 'edd_sl_license_keys_row_start', $license->ID ); ?>
					<td>
						<div class="edd_sl_item_name edd_purchase_id">
						
						<?php echo edd_software_licensing()->get_download_name( $license->ID ); ?>		
							
							<?php if( $price_id = edd_software_licensing()->get_price_id( $license->ID ) ) : ?>
								<span class="edd_sl_key_sep"><?php esc_html_e( ' &ndash; ', 'kiwi' ); ?></span>
								<span class="edd_sl_key_price_option"><?php echo edd_get_price_option_name( edd_software_licensing()->get_download_id( $license->ID ), $price_id ); ?></span>
							<?php endif; ?>
						</div>
						<input type="text" readonly="readonly" class="edd_sl_license_key" value="<?php echo esc_attr( edd_software_licensing()->get_license_key( $license->ID ) ); ?>" />
					</td>
					<td class="mp-edd-license-info">
						<span class="edd_sl_status_label"><?php esc_html_e( 'Status: ', 'kiwi' ); ?></span>
						<span class="edd_sl_license_status edd-sl-<?php echo edd_software_licensing()->get_license_status( $license->ID ); ?>">
							<?php echo edd_software_licensing()->get_license_status( $license->ID ); ?>
						</span>
						<div class="edd_sl_item_expiration">
							<span class="edd_sl_expiries_label"><?php 'expired' === edd_software_licensing()->get_license_status( $license->ID ) ? esc_html_e( 'Expired: ', 'kiwi' ) : esc_html_e( 'Expires: ', 'kiwi' ); ?></span>
							<?php if( edd_software_licensing()->is_lifetime_license( $license->ID ) ) : ?>
								<?php esc_html_e( 'Never', 'kiwi' ); ?>
							<?php else: ?>
								<?php echo esc_attr( date_i18n( get_option('date_format'), edd_software_licensing()->get_license_expiration( $license->ID ) ) ); ?>								
							<?php endif; ?>
						</div>
						<span class="edd_sl_limit_label"><?php esc_html_e( 'Activations: ', 'kiwi' ); ?></span>
						<span class="edd_sl_limit_used"><?php echo esc_attr( edd_software_licensing()->get_site_count( $license->ID ) ); ?></span>
						<span class="edd_sl_limit_sep"><?php esc_html_e( ' &frasl; ', 'kiwi' ); ?></span>
						<span class="edd_sl_limit_max"><?php echo esc_attr( edd_software_licensing()->license_limit( $license->ID ) ); ?></span>
						<?php if( ! edd_software_licensing()->force_increase() ) : ?>
						
						
							<br/><a href="<?php echo esc_url( add_query_arg( array( 'license_id' => $license->ID, 'action' => 'manage_licenses', 'payment_id' => $payment_id ) ) ); ?>">
								
							
							<?php esc_html_e( 'Manage Sites', 'kiwi' ); ?></a>
						<?php endif; ?>
						<?php if( edd_sl_license_has_upgrades( $license->ID ) && 'expired' !== edd_software_licensing()->get_license_status( $license->ID ) ) : ?>
							<span class="edd_sl_limit_sep"><?php esc_html_e( ' &ndash; ', 'kiwi' ); ?></span>
							<a href="<?php echo esc_url( add_query_arg( array( 'view' => 'upgrades', 'license_id' => $license->ID, 'action' => 'manage_licenses', 'payment_id' => $payment_id ) ) ); ?>"><?php esc_html_e( 'View Upgrades', 'kiwi' ); ?></a>
						<?php elseif ( edd_sl_license_has_upgrades( $license->ID ) && 'expired' == edd_software_licensing()->get_license_status( $license->ID ) ) : ?>
							<span class="edd_sl_limit_sep"><?php esc_html_e( ' &ndash; ', 'kiwi' ); ?></span>
							<span class="edd_sl_no_upgrades"><?php esc_html_e( 'Renew to upgrade', 'kiwi' ); ?></span>
						<?php endif; ?>
						<?php if( edd_sl_renewals_allowed() ) : ?>
							<?php if( 'expired' === edd_software_licensing()->get_license_status( $license->ID ) ) : ?><span class="edd_sl_key_sep"><?php esc_html_e( ' &ndash; ', 'kiwi' ); ?></span>
								<a href="<?php echo edd_software_licensing()->get_renewal_url( $license->ID ); ?>" title="<?php esc_attr_e( 'Renew license', 'kiwi' ); ?>"><?php esc_html_e( 'Renew license', 'kiwi' ); ?></a>
							<?php elseif( ! edd_software_licensing()->is_lifetime_license( $license->ID ) ) : ?><span class="edd_sl_key_sep"><?php esc_html_e( ' &ndash; ', 'kiwi' ); ?></span>
								<a href="<?php echo edd_software_licensing()->get_renewal_url( $license->ID ); ?>" title="<?php esc_attr_e( 'Extend license', 'kiwi' ); ?>"><?php esc_html_e( 'Extend license', 'kiwi' ); ?></a>
							<?php endif; ?>
						<?php endif; ?>
					</td>
					<td>
						<a href="<?php echo esc_url( edd_get_success_page_url( '?payment_key=' . edd_get_payment_key( $payment_id ) ) ); ?>" title="<?php esc_attr_e( 'View Purchase Record', 'kiwi' ); ?>"><?php esc_html_e( '#', 'kiwi' ); ?><?php echo esc_html( edd_get_payment_number( $payment_id ) ); ?></a>
					</td>
					<?php do_action( 'edd_sl_license_keys_row_end', $license->ID ); ?>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr class="edd_sl_license_row">
				<td colspan="4"><?php esc_html_e( 'You currently have no licenses', 'kiwi' ); ?></td>
			</tr>
		<?php endif; ?>
	</table>
	</div>
	<?php do_action( 'edd_sl_license_keys_after' ); ?>
<?php else : ?>
	<p class="edd-alert edd-alert-warn">
		<?php esc_html_e( 'You must be logged in to view license keys.', 'kiwi' ); ?>
	</p>
	<?php echo edd_login_form(); ?>
<?php endif; ?>

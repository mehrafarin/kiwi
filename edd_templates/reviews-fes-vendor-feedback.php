<h3><?php esc_html_e( 'Customer Feedback', 'kiwi' ); ?> <?php if ( class_exists( 'EDD_Reviews' ) ) { mp_vendor_review_feedback_avarage( get_current_user_id() ); } ?></h3>

<?php do_action( 'mp_hook_customer_feedback_before' ); ?>

<?php
/**
 * This template is used to render each vendor feedback item
 */

$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$feedback = edd_reviews()->fes->get_vendor_feedback( $page );

if ( ! empty ( $feedback ) ) {
	echo '<ul class="mp-review-author-feedback">';
	foreach ( $feedback as $item ) {
		$rating = get_comment_meta( $item->comment_ID, 'edd_rating', true );
		$described = get_comment_meta( $item->comment_ID, 'edd_item_as_described', true );
		$post = get_post( $item->comment_post_ID );
		?>
	<li>
	
		<div class="mp-review-meta">
		<span class="mp-edd-review-meta-rating">						
			<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="star-rating">
				<div class="edd_reviews_rating_box" role="img" aria-label="<?php echo $rating . ' ' . __( 'stars', 'kiwi' ); ?>">
					<div class="edd_star_rating" style="width: <?php echo ( 19 * $rating ); ?>px"></div>
				</div>
				<div style="display:none" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
					<meta itemprop="worstRating" content="1" />
					<span itemprop="ratingValue"><?php echo $rating; ?></span>
					<span itemprop="bestRating">5</span>
				</div>
			</div>						
		</span>
		<span class="mp-review-date"><?php comment_date( get_option('date_format'), $item->comment_ID ); ?>, <a href="mailto:<?php echo comment_author_email( $item->comment_ID ); ?>"><?php comment_author( $item->comment_ID ); ?></a></span>
		</div>
		<div class="mp-review-meta-title"><a href="<?php the_permalink( $item->comment_post_ID ) ?>"><?php echo get_the_title( $item->comment_post_ID ); ?></a></div>
		<div class="mp-feedback-meta mp-review-meta-described"><span><?php _e( 'Item as Described:', 'kiwi' ); ?></span> <?php ($described == 0) ? _e( 'No', 'kiwi' ) : _e( 'Yes', 'kiwi' ); ?></div>
		<div class="mp-feedback-meta mp-review-meta-feedback"><span><?php _e( 'Feedback:', 'kiwi' ); ?></span> <?php comment_text( $item->comment_ID ); ?></div>
		
	</li>
	<?php
	} // end foreach
	echo '</ul>';
} else { ?>
	<p><?php _e( 'No feedback found.', 'kiwi' ); ?></p>
<?php
} // end if

edd_reviews()->fes->pagination(); ?>

<?php do_action( 'mp_hook_customer_feedback_after' ); ?>
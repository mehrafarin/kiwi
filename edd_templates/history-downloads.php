<?php if( ! empty( $_GET['edd-verify-success'] ) ) : ?>
<p class="edd-account-verified edd_success">
	<?php esc_html_e( 'Your account has been successfully verified!', 'kiwi' ); ?>
</p>
<?php
endif;
/* This template is used to display the download history of the current user. */
$purchases = edd_get_users_purchases( get_current_user_id(), 20, true, 'any' );
if ( $purchases ) :
	do_action( 'edd_before_download_history' ); 	
	?>
	<div class="table-responsive">
	<table id="edd_user_history" class="edd_history mp-purchases">
		<thead>
			<tr class="edd_download_history_row">
				<?php do_action( 'edd_download_history_header_start' ); ?>
				
				<th class="edd_download_download_image"><?php esc_html_e( 'Image', 'kiwi' ); ?></th>
				
				<th class="edd_download_download_name"><?php esc_html_e( 'Item name', 'kiwi' ); ?></th>
				
			<?php if ( class_exists( 'EDD_Reviews' ) ) { ?>	
				<th class="edd_download_download_ratings"><?php esc_html_e( 'Ratings', 'kiwi' ); ?></th>
			<?php } ?>	
				
				
				<?php if ( ! edd_no_redownload() ) : ?>
					<th class="edd_download_download_files"><?php esc_html_e( 'Files', 'kiwi' ); ?></th>
				<?php endif; //End if no redownload?>
				<?php do_action( 'edd_download_history_header_end' ); ?>
			</tr>
		</thead>
		<?php foreach ( $purchases as $payment ) :
			$downloads      = edd_get_payment_meta_cart_details( $payment->ID, true );
			$purchase_data  = edd_get_payment_meta( $payment->ID );
			$email          = edd_get_payment_user_email( $payment->ID );

			if ( $downloads ) :
				foreach ( $downloads as $download ) :

					// Skip over Bundles. Products included with a bundle will be displayed individually
					if ( edd_is_bundled_product( $download['id'] ) )
						continue; ?>

					<tr class="edd_download_history_row">
						<?php
						$price_id 		= edd_get_cart_item_price_id( $download );
						$download_files = edd_get_download_files( $download['id'], $price_id );
						$name           = get_the_title( $download['id'] );

						// Retrieve and append the price option name
						if ( ! empty( $price_id ) ) {
							$name .= ' - ' . edd_get_price_option_name( $download['id'], $price_id, $payment->ID );
						}

						do_action( 'edd_download_history_row_start', $payment->ID, $download['id'] );
						?>
						
						<td class="edd_download_download mp-thumbnail"><a href="<?php the_permalink($download['id']); ?>"><?php echo get_the_post_thumbnail( $download['id'], 'full'); ?></a></td>
						<td class="edd_download_download_name"><a href="<?php the_permalink($download['id']); ?>"><?php echo esc_html( $name ); ?></a>
						
					<?php 
											
					$itemversion = get_post_meta( $download['id'], '_cmb2_marketplace_item_version', true );
					$itemversion_fes = get_post_meta( $download['id'], 'item_version', true );
					
					if (class_exists( 'EDD_Front_End_Submissions' ) && !empty( $itemversion_fes ) ){ 
						echo '<div class="mp-current-version">';
						echo '<span>'.esc_html__( 'Current version: ', 'kiwi' ).'</span>';
						echo esc_html( $itemversion_fes );	
					} elseif ( !class_exists( 'EDD_Front_End_Submissions' ) && !empty( $itemversion ) ){
						echo '<div class="mp-current-version">';
						echo '<span>'.esc_html__( 'Current version: ', 'kiwi' ).'</span>';
						echo esc_html( $itemversion );
					} else {
						echo '';
					}
						
						echo '</div>';
					?>
						</td>
						
						

<?php if ( class_exists( 'EDD_Reviews' ) ) {
	
		echo '<td class="edd_download_download mp-ratings">';
	
		$args = array(
			'post_id' => $download['id'],
			'count' => true, 
			'meta_key' => 'edd_rating',
		);
		$comments = get_comments($args);

		$rating_count = 0;
		$total_rating = 0;
		/* */
		
		$reviews = get_comments(
			apply_filters(
				'widget_edd_per_product_reviews_args',
				array(
					'status' => 'approve',
					'post_status' => 'publish',
					'post_type' => 'download',
					'post_id' => $download['id'],
				)
			)
		);
	
		if ( $reviews ) {		

			foreach ( (array) $reviews as $review ) {				
				$total_rating = $total_rating + get_comment_meta( $review->comment_ID, 'edd_rating', true );
				$rating_count = $rating_count + 1;
			}
			
		if($total_rating != 0) {	
			$average = $total_rating / $comments;
			$number = sprintf('%0.2f', $average);
		} else {
			$number = 0;
		}
		
			echo  '<div class="edd_reviews_rating_box" role="img" aria-label="'. $number . ' ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo  '<div class="edd_star_rating custom" style="float: none; width: ' . ( 19 * $number ) . 'px;"></div>';
			echo  '</div>';
			
		} else {
			
			echo  '<div class="edd_reviews_rating_box" role="img" aria-label="0' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo  '<div class="edd_star_rating custom" style="float: none; width:0;"></div>';
			echo  '</div>';
		}	
	} 
	
			echo '</td>';
?>
						
						<?php if ( ! edd_no_redownload() ) : ?>
							<td class="edd_download_download_files">
								<?php

								if ( edd_is_payment_complete( $payment->ID ) ) :

									if ( $download_files ) :

										foreach ( $download_files as $filekey => $file ) :

											$download_url = edd_get_download_file_url( $purchase_data['key'], $email, $filekey, $download['id'], $price_id );
											
											?>

											<div class="edd_download_file">
												<a href="<?php echo esc_url( $download_url ); ?>" class="edd_download_file_link">
													<?php echo esc_html__( 'Download', 'kiwi' ); ?>
												</a>
											</div>

											<?php do_action( 'edd_download_history_files', $filekey, $file, $id, $payment->ID, $purchase_data );
										endforeach;

									else :
										esc_html_e( 'No downloadable files found.', 'kiwi' );
									endif; // End if payment complete

								else : ?>
									<span class="edd_download_payment_status">
										<?php printf( esc_html__( 'Payment status is %s', 'kiwi' ), edd_get_payment_status( $payment, true) ); ?>
									</span>
									<?php
								endif; // End if $download_files
								?>
							</td>
						<?php endif; // End if ! edd_no_redownload()

						do_action( 'edd_download_history_row_end', $payment->ID, $download['id'] );
						
						?>
					</tr>
					<?php
				endforeach; // End foreach $downloads
			endif; // End if $downloads
		endforeach;
		?>
	</table>
	</div>
	
	<div id="edd_download_history_pagination" class="edd_pagination navigation">
		<?php
		$big = 999999;
		echo paginate_links( array(
			'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'  => '?paged=%#%',
			'current' => max( 1, get_query_var( 'paged' ) ),
			'total'   => ceil( edd_count_purchases_of_customer() / 20 ) // 20 items per page
		) );
		?>
	</div>
	
	<?php do_action( 'edd_after_download_history' ); ?>
<?php else : ?>
	<p class="edd-no-downloads"><?php esc_html_e( 'You have not purchased any downloads', 'kiwi' ); ?></p>
<?php endif; ?>
<?php 
	$redirect_to 	= apply_filters( 'lostpassword_redirect', $lostpassword_redirect );
	$user_login 	= isset($_POST['user_login']) ? wp_unslash($_POST['user_login']) : '';
?>

<div class="edd_widget_title"><h3><?php _e('Custom Lost Password','kiwi'); ?></h3></div>

<div class="mp-form-description"><?php _e( 'Enter your email address and we\'ll send you a link you can use to pick a new password.','kiwi'); ?></div>

<div id="message"></div>

	<div class="mp-custom-form-edd">
	<form name="lostpasswordform" id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
		<p>
			<label for="user_login" ><?php _e('Username or Email','kiwi') ?></label>
			<input type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr($user_login); ?>" size="20" />
		</p>
		<?php
		/**
		 * Fires inside the lostpassword form tags, before the hidden fields.
		 *
		 * @since 2.1.0
		 */
		do_action( 'lostpassword_form' ); ?>
		<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />
		<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Get New Password','kiwi'); ?>" /></p>
	</form>
	</div>
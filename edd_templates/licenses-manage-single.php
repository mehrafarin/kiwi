<?php

$payment_id  = absint( $_GET['payment_id' ] );
$license_id  = absint( $_GET['license_id' ] );
$download_id = absint( edd_software_licensing()->get_download_id( $license_id ) );
$user_id     = edd_get_payment_user_id( $payment_id );

if( ! current_user_can( 'edit_shop_payments' ) && $user_id != get_current_user_id() ) {
	return;
}

$color = edd_get_option( 'checkout_color', 'gray' );
$color = ( $color == 'inherit' ) ? '' : $color;

// Retrieve all sites for the specified license
$sites = edd_software_licensing()->get_sites( $license_id );
?>


	<div class="container-fluid mp-fes-vendor-menu">

		<div class="container align-center"> 
			<div class="row">
				<?php mp_license_menu(); ?>
			</div>
		</div>

	</div>

	<div class="container mp-full-dashboard">
	<div id="fes-vendor-dashboard">


<?php 
 	
	$x = 0;		
		
	foreach ( (array)$sites as $site ) : 	 
	$x++; endforeach; 	
	
	if ( $x >= '2' || $x = '0' ) {
		$div_number = esc_html__( 'Manage Sites', 'kiwi' );  
	} else {
		$div_number = esc_html__( 'Manage Site', 'kiwi' ); 
	} 

	echo '<h3>'.esc_attr( $div_number).'</h3>';	
?>


<?php edd_sl_show_errors(); ?>
<table id="edd_sl_license_sites" class="edd_sl_table">
	<thead>
		<tr class="edd_sl_license_row">
			<?php do_action('edd_sl_license_sites_header_before'); ?>
			<th class="edd_sl_url"><?php esc_html_e( 'Site URL', 'kiwi' ); ?></th>
			<th class="edd_sl_actions"><?php esc_html_e( 'Actions', 'kiwi' ); ?></th>
			<?php do_action('edd_sl_license_sites_header_after'); ?>
		</tr>
	</thead>
	<?php if ( $sites ) : ?>
		<?php foreach ( $sites as $site ) : ?>
			<tr class="edd_sl_license_row">
				<?php do_action( 'edd_sl_license_sites_row_start', $license_id ); ?>
				<td><a href="<?php esc_html_e( 'http://', 'kiwi' ); ?><?php echo esc_url( $site ); ?>" target="_blank"><?php echo esc_url( $site ); ?></a></td>
				<td><a href="<?php echo wp_nonce_url( add_query_arg( array( 'edd_action' => 'deactivate_site', 'site_url' => $site, 'license' => $license_id ) ), 'edd_deactivate_site_nonce', '_wpnonce' ); ?>"><span class="mp-license-status-deactivate"><?php esc_html_e( 'Deactivate Site', 'kiwi' ); ?></span></a></td>
				<?php do_action( 'edd_sl_license_sites_row_end', $license_id ); ?>
			</tr>
		<?php endforeach; ?>
	<?php else: ?>
		<tr class="edd_sl_license_row">
			<?php do_action( 'edd_sl_license_sites_row_start', $license_id ); ?>
			<td colspan="2"><?php esc_html_e( 'No sites have been activated for this license', 'kiwi' ); ?></td>
			<?php do_action( 'edd_sl_license_sites_row_end', $license_id ); ?>
		</tr>
	<?php endif; ?>
</table>

<?php $status   = edd_software_licensing()->get_license_status( $license_id ); ?>
<?php $at_limit = edd_software_licensing()->is_at_limit( $license_id, $download_id ); ?>

<?php if ( ! $at_limit && ( $status == 'active' || $status == 'inactive' ) ) : ?>
<form method="post" id="edd_sl_license_add_site_form" class="edd_sl_form">
	<div>
		<span><?php esc_html_e( 'Use this form to authorize a new site URL for this license. Enter the full site URL.', 'kiwi' ); ?></span>
		<input type="text" name="site_url" class="edd-input" value="<?php esc_attr_e( 'http://', 'kiwi' ); ?>"/>
		<input type="submit" class="button-primary button" value="<?php esc_attr_e( 'Add Site', 'kiwi' ); ?>"/>
		<input type="hidden" name="license_id" value="<?php echo esc_attr( $license_id ); ?>"/>
		<input type="hidden" name="edd_action" value="insert_site"/>
		<?php wp_nonce_field( 'edd_add_site_nonce', 'edd_add_site_nonce', true ); ?>
	</div>
</form>
<?php endif; ?>



<p><a href="<?php echo esc_url( remove_query_arg( array( 'license_id', 'edd_sl_error', '_wpnonce' ) ) ); ?>" class="edd-manage-license-back edd-submit button <?php echo esc_attr( $color ); ?>"><?php esc_html_e( 'Go back', 'kiwi' ); ?></a></p>

</div>
	</div>	
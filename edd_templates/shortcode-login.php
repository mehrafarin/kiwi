<?php
/* This template is used to display the login form with [edd_login] */
global $edd_login_redirect;
if ( ! is_user_logged_in() ) :

	// Show any error messages after form submission
	edd_print_errors(); ?>
	<form id="edd_login_form" class="edd_form" action="" method="post">
		<fieldset>
			<div class="edd_widget_title"><h3><?php esc_html_e( 'Log into Your Account', 'kiwi' ); ?></h3></div>
			
			<?php do_action( 'edd_login_fields_before' ); ?>
			<p>
				<label for="edd_user_Login"><?php esc_html_e( 'Username', 'kiwi' ); ?></label>
				<input name="edd_user_login" id="edd_user_login" class="required edd-input" type="text" title="<?php esc_attr_e( 'Username', 'kiwi' ); ?>"/>
			</p>
			<p>
				<label for="edd_user_pass"><?php esc_html_e( 'Password', 'kiwi' ); ?></label>
				<input name="edd_user_pass" id="edd_user_pass" class="password required edd-input" type="password"/>
			</p>
			<p>
				<input type="hidden" name="edd_redirect" value="<?php echo esc_url( $edd_login_redirect ); ?>"/>
				<input type="hidden" name="edd_login_nonce" value="<?php echo wp_create_nonce( 'edd-login-nonce' ); ?>"/>
				<input type="hidden" name="edd_action" value="user_login"/>
				<input id="edd_login_submit" type="submit" class="edd_submit" value="<?php esc_attr_e( 'Log In', 'kiwi' ); ?>"/>
			</p>
			<p class="edd-lost-password">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" title="<?php esc_attr_e( 'Lost Password', 'kiwi' ); ?>">
					<?php esc_html_e( 'Lost Password?', 'kiwi' ); ?>
				</a>
			</p>
			<?php do_action( 'edd_login_fields_after' ); ?>
		</fieldset>
	</form>
<?php else : ?>
	<p class="edd-logged-in"><?php esc_html_e( 'You are already logged in', 'kiwi' ); ?></p>
<?php endif; ?>
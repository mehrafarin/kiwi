<?php
global $post;
$suggestion_data = edd_rp_get_suggestions( $post->ID );

if ( is_array( $suggestion_data ) && !empty( $suggestion_data ) ) :
	$suggestions = array_keys( $suggestion_data );

	$suggested_downloads = new WP_Query( array( 'post__in' => $suggestions, 'post_type' => 'download' ) );

	$column = edd_get_option( 'edd_rp_suggestion_count' );
	
	if ( $column == '1' ) {
		$column_class = ' col-1';
		} elseif ( $column == '2' ) {
		$column_class = ' col-2';
	} elseif ( $column == '3' ) {
		$column_class = ' col-3';
	} elseif ( $column == '4' ) {
		$column_class = ' col-4';
	} elseif ( $column == '5' ) {
		$column_class = ' col-5';
	} else {
		$column_class = '';
		}	
	
	if ( $suggested_downloads->have_posts() ) : ?>
			
			<div class="mp-product-description mp-edd-recommended-items">
			
			<div class="edd_widget_title">
				<h3><?php echo sprintf( __( 'Users who purchased %s, also purchased:', 'kiwi' ), get_the_title() ); ?></h3>
			</div>
			
				<div id="edd-rp-items-wrapper" class="edd-rp-single<?php echo $column_class ?>">
					<?php while ( $suggested_downloads->have_posts() ) : ?>
						<?php $suggested_downloads->the_post();	?>
						
						<div class="edd_download">
							
							<?php 
							
							edd_get_template_part( 'shortcode', 'content-image' );						
							edd_get_template_part( 'shortcode', 'content-title' );
							edd_get_template_part( 'shortcode', 'content-price' );
							edd_get_template_part( 'shortcode', 'content-excerpt' );
							
							?>
							
							
						</div>
						
						
					<?php endwhile; ?>
				</div>
			</div>	
	<?php endif; ?>

	<?php wp_reset_postdata(); ?>

<?php endif; ?>

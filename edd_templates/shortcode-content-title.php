<?php global $kiwi_theme_option; ?>

<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '2' || $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '1' ) { ?>

	<div class="iso-desc">

	<div class="truncate one-line">
		<h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	</div>

<?php } else { ?>

	<div class="iso-desc div-flex">
		<div class="left-column align-left truncate one-line">
			<h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		</div>	
		
<?php } ?>	
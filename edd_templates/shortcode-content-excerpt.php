<?php

	global $kiwi_theme_option, $post;

	$name 			= get_the_author_meta('display_name', $post->post_author);
	$vendor_name	= get_the_author_meta( 'user_login' );
	$avatar			= get_avatar( get_the_author_meta( 'ID' ), 25 );

	if ( class_exists( 'EDD_Front_End_Submissions' ) ) {
		$constant = EDD_FES()->helper->get_option( 'fes-vendor-constant', '' );
		$constant = ( isset( $constant ) && $constant != '' ) ? $constant : esc_html__( 'vendor', 'kiwi' );
		$constant = apply_filters( 'fes_vendor_constant_singular_lowercase', $constant );
	}

	if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) ) {
		$coming_soon_cs_active 		= edd_coming_soon_is_active( get_the_ID() );
		$coming_soon_custom_text 	= get_post_meta( get_the_ID(), 'edd_coming_soon_text', true );
		$coming_soon_custom_text 	= !empty ( $coming_soon_custom_text ) ? $coming_soon_custom_text : apply_filters( 'edd_cs_coming_soon_text', esc_html__( 'Coming Soon', 'kiwi' ) );
	}

	$edd_catlist 	= get_the_term_list( $post->ID, 'download_category', '', ', ', '' );


?>



<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '2' ) { ?>

<div class="excerpt"><?php the_excerpt(); ?></div>	

		<?php

			echo  '<div class="custom-columns">';
			echo  '<div class="left-column edd_catlist">';

			if ( !empty( $edd_catlist ) ){
			   echo $edd_catlist;
			}
			echo  '</div>';

			echo  '<div class="right-column align-right">';

				if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && $coming_soon_cs_active ) {

				$price = apply_filters( 'edd_coming_soon_display_text', '<p><strong>' . $coming_soon_custom_text . '</strong></p>' );

					echo sprintf( '<span class="item-price">%s</span>', $price );

			} else {

				remove_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_get_custom_status_text', 30, 2 );

				$price = get_post_meta( get_the_ID(), 'edd_price', true );

					if ( $price == 0 && $kiwi_theme_option['marketplace-free-enable'] == '1' && !edd_has_variable_prices( get_the_ID() ) ) {
						echo '<span class="price free">';
						$price = $kiwi_theme_option['marketplace-free-customtext'];
						echo '</span>';
					} elseif ( edd_has_variable_prices( get_the_ID() ) ) {
						 $price = edd_price_range( get_the_ID() );
					} else {
						$price = edd_price( get_the_ID(), false );
					}

					echo sprintf( '<span class="item-price">%s</span>', $price );

				}


			echo  '</div>';
			echo  '<div class="clear"></div>';
			echo  '</div>';

			echo '<div class="">';
			echo '<div class="left-column mp_author">';

			if ( class_exists( 'EDD_Front_End_Submissions' ) && !is_page_template( 'page-vendor.php' ) ) {
					echo '<span class="byline"><span class="author vcard">'.$avatar.'';
					echo '<a rel="author" href="'. esc_url(site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name) ).'">'. esc_html( $name ).'</a>';
					echo '</span></span>';
				} else {
					echo '<span class="byline"><span class="author vcard">'.$avatar.'';
					echo esc_html( $name );
					echo '</span></span>';
			}

			echo '</div>';

			echo '</div>';

		if ( class_exists( 'EDD_Reviews' ) ) {
			echo '<div class="right-column reviews align-right">';
			 mp_extension_review_function_core();
			echo '</div>';
		}

			echo '<div class="clear"></div>';
			//echo '</div>';

		?>






<?php  } elseif ( $kiwi_theme_option['marketplace-sidebar-archive-griddesign'] == '3' ) { ?>


		<?php

			echo  '<div class="right-column align-right">';

			if ( is_plugin_active( 'edd-coming-soon/edd-coming-soon.php' ) && $coming_soon_cs_active ) {

				$price = apply_filters( 'edd_coming_soon_display_text', '<p><strong>' . $coming_soon_custom_text . '</strong></p>' );
				echo sprintf( '<span class="item-price">%s</span>', $price );

			} else {

				remove_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_get_custom_status_text', 30, 2 );


					$price = get_post_meta( get_the_ID(), 'edd_price', true );

					if ( $price == 0 && $kiwi_theme_option['marketplace-free-enable'] == '1' && !edd_has_variable_prices( get_the_ID() ) ) {
						echo '<span class="price free">';
						$price = $kiwi_theme_option['marketplace-free-customtext'];
						echo '</span>';
					} elseif ( edd_has_variable_prices( get_the_ID() ) ) {
						 $price = edd_price_range( get_the_ID() );
					} else {
						$price = edd_price( get_the_ID(), false );
					}

					echo sprintf( '<span class="item-price">%s</span>', $price );
			}

			echo  '<div class="clear"></div>';
			echo  '</div>';
			echo '</div>';

			echo '<div class="mp_author">';

			if ( class_exists( 'EDD_Front_End_Submissions' ) && !is_page_template( 'page-vendor.php' ) ) {
					echo '<span class="byline"><span class="author vcard">'.esc_html__('by ', 'kiwi').'';
					echo '<a rel="author" href="'.site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name).'">'. esc_html( $name ).'</a>';
					echo '</span></span>';
				} else {
					echo '<span class="byline"><span class="author vcard">'.esc_html__('by ', 'kiwi').'';
					echo esc_html( $name );
					echo '</span></span>';
			}

		if ( class_exists( 'EDD_Reviews' ) ) {
			echo '<div class="right-column reviews align-right reviews-stylethree">';
			 mp_extension_review_function_core();
			echo '</div>';
		}

			echo '<div class="clear"></div>';
			//echo '</div>';

		?>

 <?php } else { ?>

	<?php

			echo '<div class="left-column mp_author">';

			if ( class_exists( 'EDD_Front_End_Submissions' ) && !is_page_template( 'page-vendor.php' ) ) {
					echo '<span class="byline"><span class="author vcard">'.$avatar.'';
					echo '<a rel="author" href="'. esc_url(site_url().'/'.$constant.'/'.str_replace(" ","-", $vendor_name) ).'">'. esc_html( $name ).'</a>';
					echo '</span></span>';
				} else {
					echo '<span class="byline"><span class="author vcard">'.$avatar.'';
					echo esc_html( $name );
					echo '</span></span>';
			}

			echo '</div>';

		if ( class_exists( 'EDD_Reviews' ) ) {
			echo '<div class="right-column reviews align-right">';
			mp_extension_review_function_core();
			echo '</div>';
		}

			echo '<div class="clear"></div>';
		?>


 <?php }

 ?>

</div>

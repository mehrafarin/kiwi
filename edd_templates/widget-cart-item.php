<li class="edd-cart-item">
	<span class="edd-cart-item-quantity">{item_quantity}<?php esc_html_e( 'x', 'kiwi' ); ?></span> 
	<span class="edd-cart-item-title">{item_title}</span><br />
	<span class="edd-cart-item-price">{item_amount}</span>
	<a href="{remove_url}" data-cart-item="{cart_item_id}" data-download-id="{item_id}" data-action="edd_remove_from_cart" class="edd-remove-from-cart"><?php esc_html_e( 'remove', 'kiwi' ); ?></a>
</li>
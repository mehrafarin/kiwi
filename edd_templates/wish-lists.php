<?php
/** Wish List template */

$private 	= edd_wl_get_query( 'private' ); // get the private lists
$public 	= edd_wl_get_query( 'public' ); // get the public lists

echo edd_wl_create_list_link(); // create list 

/** Private lists */

echo '<div class="mp-wishlist-one">';

if ( $private ) : ?>

<div class="edd_widget_title">
	<h3 class="plugin"><?php echo sprintf( esc_html__( 'Private %s', 'kiwi' ), edd_wl_get_label_plural() ); ?></h3>
</div>
	
	<ul class="edd-wish-list">
	<?php foreach ( $private as $id ) : ?>
		<li>
			<span class="edd-wl-item-title">
				<a href="<?php echo esc_url( edd_wl_get_wish_list_view_uri( $id ) ); ?>" title="<?php echo the_title_attribute( array( 'post' => $id ) ); ?>"><?php echo get_the_title( $id ); ?></a>
				<span class="edd-wl-item-count"><?php echo edd_wl_get_item_count( $id ); ?></span>
			</span>

			<?php // edit link
				echo edd_wl_edit_link( $id );
			?>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; // if private lists ?>

<?php 
/**
 * Public lists
*/
if ( $public ) : ?>
	
<div class="edd_widget_title">
	<h3 class="plugin"><?php echo sprintf( esc_html__( 'Public %s', 'kiwi' ), edd_wl_get_label_plural() ); ?></h3>
</div>
	
	<ul class="edd-wish-list">
	<?php foreach ( $public as $id ) : ?>
		<li>
			<span class="edd-wl-item-title">
				<a href="<?php echo esc_url( edd_wl_get_wish_list_view_uri( $id ) ); ?>" title="<?php echo the_title_attribute( array('post' => $id ) ); ?>"><?php echo get_the_title( $id ); ?></a>
				<span class="edd-wl-item-count"><?php echo edd_wl_get_item_count( $id ); ?></span>
			</span>

			<?php // edit link
				echo edd_wl_edit_link( $id );
			?>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; // if public lists ?>

</div>
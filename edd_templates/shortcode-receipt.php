<?php
/* This template is used to display the purchase summary with [edd_receipt] */
global $edd_receipt_args, $kiwi_theme_option;

$payment   = get_post( $edd_receipt_args['id'] );

if( empty( $payment ) ) : ?>

	<div class="edd_errors edd-alert edd-alert-error">
		<?php esc_html_e( 'The specified receipt ID appears to be invalid', 'kiwi' ); ?>
	</div>

<?php
return;
endif;

$meta      = edd_get_payment_meta( $payment->ID );
$cart      = edd_get_payment_meta_cart_details( $payment->ID, true );
$user      = edd_get_payment_meta_user_info( $payment->ID );
$email     = edd_get_payment_user_email( $payment->ID );
$status    = edd_get_payment_status( $payment, true );

	global $current_user;	
	$user_id      = get_current_user_id();	
	$address      = edd_get_customer_address( $user_id );

?>






<div class="row mp-invoice-heading">
  <div class="col-xs-12 col-md-6"><h2 class="mp-invoice-title"><?php esc_html_e( 'Invoice', 'kiwi' ); ?></h2></div>
  <div class="col-xs-12 col-md-6"><?php do_action( 'mp_hook_invoices_pdf_download' ); ?></div>
</div>


<?php if ( !empty( $kiwi_theme_option['mp-invoice-logo']['url'] ) || !empty($kiwi_theme_option['mp-invoice-company-address'] ) ) { ?>
	<div class="row mp-invoice-logo">
		<div class="col-xs-12 col-md-6">	
			<?php if ( !empty( $kiwi_theme_option['mp-invoice-logo']['url'] ) ) { ?>	
				<img class="km-invoice-logo" src="<?php echo esc_url( $kiwi_theme_option['mp-invoice-logo']['url'] ); ?>" alt="">
			<?php } ?>		
		</div>
		<div class="col-xs-12 col-md-6">
			<?php if( !empty($kiwi_theme_option['mp-invoice-company-address'] ) ) { echo $kiwi_theme_option['mp-invoice-company-address']; } ?>
		</div>
	</div>
<?php } ?>	






<div class="kiwi-edd-purchase-receipt">

<div id="mp-receipt-billing" class="row">
  <div class="col-md-6">
  
	<?php if ($current_user->first_name) { echo '<h3 class="mp-invoice-to">'. esc_html__( 'Invoice to', 'kiwi' ).'</h3>'; } ?>
	
	<div class="mp-invoice-address">		
		<?php if ($current_user->first_name) { ?><?php echo esc_html( $current_user->first_name ); ?> <?php echo esc_html( $current_user->last_name ); ?><br /><?php } ?>
		<?php echo esc_attr( $address['line1'] ); ?><br />
		<?php echo esc_attr( $address['line2'] ); ?><br />
		<?php echo esc_attr( $address['city'] ); ?> <?php echo esc_attr( $address['zip'] ); ?><br />
		<?php echo esc_html( $address['country'] ); ?>
	</div>
  
  </div>
  <div class="col-md-6 second-column">
	<div class="row">
	  <div class="col-xs-6 col-md-6">
		<span class="mp-pay-id"><?php esc_html_e( 'Payment ID: ', 'kiwi' ); ?></span><br />		
		<?php if ( $edd_receipt_args['date'] ) : ?> <?php esc_html_e( 'Date: ', 'kiwi' ); ?><?php endif; ?><br />
		<?php esc_html_e( 'Payment Status: ', 'kiwi' ); ?><br />
		<?php if ( $edd_receipt_args['payment_method'] ) : ?><?php esc_html_e( 'Payment Method:', 'kiwi' ); ?><?php endif; ?><br />			
		<?php esc_html_e( 'Payment Key: ', 'kiwi' ); ?>		
	  </div>
	  <div class="col-xs-6 col-md-6">
	  
		<?php if ( filter_var( $edd_receipt_args['payment_id'], FILTER_VALIDATE_BOOLEAN ) ) : ?>
			<?php echo esc_html( edd_get_payment_number( $payment->ID ) ); ?><br />
		<?php endif; ?>
		
		<?php if ( filter_var( $edd_receipt_args['date'], FILTER_VALIDATE_BOOLEAN ) ) : ?>
			<?php echo esc_attr( date_i18n( get_option( 'date_format' ), strtotime( $meta['date'] ) ) ); ?><br />
		<?php endif; ?>
		
		
		<span class="edd_receipt_payment_status <?php echo esc_attr( strtolower( $status ) ); ?>"><?php echo esc_html( $status ); ?></span>						
		
		<?php if ( filter_var( $edd_receipt_args['payment_method'], FILTER_VALIDATE_BOOLEAN ) ) : ?>
			<?php echo esc_html( edd_get_gateway_checkout_label( edd_get_payment_gateway( $payment->ID ) ) ); ?><br />
		<?php endif; ?>	
		
		
			<?php echo esc_html( get_post_meta( $payment->ID, '_edd_payment_purchase_key', true ) ); ?>	
		
	  </div>
	</div>
  </div>
</div>




<?php remove_action( 'edd_payment_receipt_after', 'edd_sl_show_keys_on_receipt', 10, 2 ); ?>

<table id="edd_purchase_receipt">
	<thead>
		<?php do_action( 'edd_payment_receipt_before', $payment, $edd_receipt_args ); ?>		
	</thead>
	<tbody>
		<?php do_action( 'edd_payment_receipt_after', $payment, $edd_receipt_args ); ?>
	</tbody>
</table>



	<h2 class="mp-invoice-summary"><?php echo apply_filters( 'mp_payment_receipt_summary_title', esc_html__( 'Invoice Summary', 'kiwi' ) ); ?></h2>

<?php if ( filter_var( $edd_receipt_args['products'], FILTER_VALIDATE_BOOLEAN ) ) : ?>

	<table id="edd_purchase_receipt_products">
		<thead>
			<th><?php esc_html_e( 'Description', 'kiwi' ); ?></th>
			<?php if ( edd_use_skus() ) { ?>
				<th><?php esc_html_e( 'SKU', 'kiwi' ); ?></th>
			<?php } ?>
			<?php if ( edd_item_quantities_enabled() ) : ?>
				<th><?php esc_html_e( 'Quantity', 'kiwi' ); ?></th>
			<?php endif; ?>
			<th><?php esc_html_e( 'Price', 'kiwi' ); ?></th>
		</thead>

		<tbody>
		<?php if( $cart ) : ?>
			<?php foreach ( $cart as $key => $item ) : ?>

				<?php if( ! apply_filters( 'edd_user_can_view_receipt_item', true, $item ) ) : ?>
					<?php continue; // Skip this item if can't view it ?>
				<?php endif; ?>

				<?php if( empty( $item['in_bundle'] ) ) : ?>
				<tr>
					<td>

						<?php
						$price_id       = edd_get_cart_item_price_id( $item );
						$download_files = edd_get_download_files( $item['id'], $price_id );
						?>

						<div class="edd_purchase_receipt_product_name">
							<?php echo esc_html( $item['name'] ); ?>
							<?php if( edd_has_variable_prices( $item['id'] ) && ! is_null( $price_id ) ) : ?>
							<span class="edd_purchase_receipt_price_name"><?php esc_html_e( ' - ', 'kiwi' ); ?>							
								<?php echo edd_get_price_option_name( $item['id'], $price_id, $payment->ID ); ?>							
							</span>
							
							<?php endif; ?>
						</div>

						<?php if ( $edd_receipt_args['notes'] ) : ?>
							<div class="edd_purchase_receipt_product_notes"><?php echo wpautop( edd_get_product_notes( $item['id'] ) ); ?></div>
						<?php endif; ?>

						<?php
						if( edd_is_payment_complete( $payment->ID ) && edd_receipt_show_download_files( $item['id'], $edd_receipt_args ) ) : ?>
						<ul class="edd_purchase_receipt_files">
							<?php
							if ( ! empty( $download_files ) && is_array( $download_files ) ) :

								foreach ( $download_files as $filekey => $file ) :

									$download_url = edd_get_download_file_url( $meta['key'], $email, $filekey, $item['id'], $price_id );
									?>
									<li class="edd_download_file">
										<a href="<?php echo esc_url( $download_url ); ?>" class="edd_download_file_link"><?php esc_html_e( 'Download' , 'kiwi' ); ?></a>
									</li>
									<?php
									do_action( 'edd_receipt_files', $filekey, $file, $item['id'], $payment->ID, $meta );
								endforeach;

							elseif( edd_is_bundled_product( $item['id'] ) ) :

								$bundled_products = edd_get_bundled_products( $item['id'] );

								foreach( $bundled_products as $bundle_item ) : ?>
									<li class="edd_bundled_product">
										<span class="edd_bundled_product_name"><?php echo get_the_title( $bundle_item ); ?></span>
										<ul class="edd_bundled_product_files">
											<?php
											$download_files = edd_get_download_files( $bundle_item );

											if( $download_files && is_array( $download_files ) ) :

												foreach ( $download_files as $filekey => $file ) :

													$download_url = edd_get_download_file_url( $meta['key'], $email, $filekey, $bundle_item, $price_id ); ?>
													<li class="edd_download_file">
														<a href="<?php echo esc_url( $download_url ); ?>" class="edd_download_file_link"><?php esc_html_e( 'Download' , 'kiwi' ); ?></a>
													</li>
													<?php
													do_action( 'edd_receipt_bundle_files', $filekey, $file, $item['id'], $bundle_item, $payment->ID, $meta );

												endforeach;
											else :
												echo '<li>' . esc_html__( 'No downloadable files found for this bundled item.', 'kiwi' ) . '</li>';
											endif;
											?>
										</ul>
									</li>
									<?php
								endforeach;

							else :
								echo '<li class="mp-edd-no-downloads">' . apply_filters( 'edd_receipt_no_files_found_text', esc_html__( 'No downloadable files found.', 'kiwi' ), $item['id'] ) . '</li>';
							endif; ?>
						</ul>
						<?php endif; ?>

					</td>
					<?php if ( edd_use_skus() ) : ?>
						<td><?php echo esc_html( edd_get_download_sku( $item['id'] ) ); ?></td>
					<?php endif; ?>
					<?php if ( edd_item_quantities_enabled() ) { ?>
						<td><?php echo esc_html( $item['quantity'] ); ?></td>
					<?php } ?>
					<td class="mp-price">
						<?php if( empty( $item['in_bundle'] ) ) : // Only show price when product is not part of a bundle ?>
							<?php echo edd_currency_filter( edd_format_amount( $item[ 'price' ] ) ); ?>
						<?php endif; ?>
					</td>
				</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if ( ( $fees = edd_get_payment_fees( $payment->ID, 'item' ) ) ) : ?>
			<?php foreach( $fees as $fee ) : ?>
				<tr>
					<td class="edd_fee_label"><?php echo esc_html( $fee['label'] ); ?></td>
					<?php if ( edd_item_quantities_enabled() ) : ?>
						<td></td>
					<?php endif; ?>
					<td class="edd_fee_amount"><?php echo wp_kses( edd_currency_filter( edd_format_amount( $fee['amount'] ) ), wp_kses_allowed_html( 'post' ) ); ?></td>	
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		
		
		<?php if ( filter_var( $edd_receipt_args['price'], FILTER_VALIDATE_BOOLEAN ) ) : ?>

			<tr class="mp-subtotal">
					<?php if ( edd_use_skus() ) : ?><td></td><?php endif; ?>
					<?php if ( edd_item_quantities_enabled() ) { ?><td></td><?php } ?>
				<td class="mp-dir"><?php esc_html_e( 'Subtotal', 'kiwi' ); ?></td>
				<td><?php echo wp_kses( edd_payment_subtotal( $payment->ID ), wp_kses_allowed_html( 'post' ) ); ?></td>
			</tr>
			
			<?php if ( $edd_receipt_args['discount'] && isset( $user['discount'] ) && $user['discount'] != 'none' ) : ?>
			<tr class="mp-discount">
					<?php if ( edd_use_skus() ) : ?><td></td><?php endif; ?>
					<?php if ( edd_item_quantities_enabled() ) { ?><td></td><?php } ?>
				<td class="mp-dir"><?php esc_html_e( 'Discount(s):', 'kiwi' ); ?></td>
				<td class="mp-discount-price"><?php echo esc_html( $user['discount'] ); ?></td>
			</tr>
			<?php endif; ?>
			
			
			<?php if ( ( $fees = edd_get_payment_fees( $payment->ID, 'fee' ) ) ) : ?>
			<tr  class="mp-fees">
				<?php if ( edd_use_skus() ) : ?><td></td><?php endif; ?>
				<?php if ( edd_item_quantities_enabled() ) { ?><td></td><?php } ?>
				<td class="mp-dir">
				<?php foreach( $fees as $fee ) : ?>
					<?php if ( !empty ($fee['label']) ) {
							echo esc_html( $fee['label'] ); 
						} else {
							esc_html_e( 'Fees', 'kiwi' );
						}
					?>
				<?php endforeach; ?>				
				</td>
				<td>
					<ul class="edd_receipt_fees">
						<?php foreach( $fees as $fee ) : ?>
							<li><span class="edd_fee_amount"><?php echo esc_html( edd_currency_filter( edd_format_amount( $fee['amount'] ) ) ); ?></span></li>
						<?php endforeach; ?>
					</ul>
				</td>
			</tr>
		<?php endif; ?>
			
			
			<?php if( edd_use_taxes() ) : ?>
			<tr class="mp-vat">
					<?php if ( edd_use_skus() ) : ?><td></td><?php endif; ?>
					<?php if ( edd_item_quantities_enabled() ) { ?><td></td><?php } ?>
				<td class="mp-dir"><?php esc_html_e( 'Tax', 'kiwi' ); ?></td>
				<td><?php echo wp_kses( edd_payment_tax( $payment->ID ), wp_kses_allowed_html( 'post' ) ); ?></td>
			</tr>
			<?php endif; ?>

			<tr class="mp-totalprice">
					<?php if ( edd_use_skus() ) : ?><td></td><?php endif; ?>
					<?php if ( edd_item_quantities_enabled() ) { ?><td></td><?php } ?>
				<td class="mp-dir"><?php esc_html_e( 'Total Price', 'kiwi' ); ?></td>
				<td><?php echo wp_kses( edd_payment_amount( $payment->ID ), wp_kses_allowed_html( 'post' ) ); ?></td>
				
				
				
			</tr>

		<?php endif; ?>
				
		</tbody>
	</table>	
	
	
	<?php do_action( 'edd_payment_receipt_after_table', $payment, $edd_receipt_args ); ?>

	
	<?php if( class_exists( 'EDD_Upload_File' ) && edd_get_option( 'edd_upload_file_location' ) == 'receipt' ) {	
		echo '<div class="mp-ext-edd-file-upload-receipt">';		
			do_action( 'edd_payment_receipt_after_table_upload_field', $payment, $edd_receipt_args );			
		echo '</div>';
	} ?>
	
	<h2 class="mp-invoice-thankyou"><?php esc_html_e( 'Thank you for your purchase.', 'kiwi' ); ?></h2>
	<div class="reachout">
		<?php esc_html_e( 'If you have any questions or concerns about this order, please do not hesitate to contact us.', 'kiwi' ); ?>
	</div>
	
	
	 	
<?php endif; ?>

</div>
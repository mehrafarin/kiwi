<?php
$cart_items 	= edd_get_cart_contents();
$post_ids 		= wp_list_pluck( $cart_items, 'id' );
?>

<?php if ( $cart_items ) : ?>
<?php
	$user_id 			= ( is_user_logged_in() ) ? get_current_user_id() : false;
	$suggestion_data 	= edd_rp_get_multi_suggestions( $post_ids, $user_id );

	if ( is_array( $suggestion_data ) && !empty( $suggestion_data ) ) :
	$suggestions = array_keys( $suggestion_data );
	
	$column = edd_get_option( 'edd_rp_suggestion_count' );
	
	if ( $column == '1' ) {
		$column_class = ' col-1';
	} elseif ( $column == '2' ) {
		$column_class = ' col-2';
	} elseif ( $column == '3' ) {
		$column_class = ' col-3';
	} elseif ( $column == '4' ) {
		$column_class = ' col-4';
	} elseif ( $column == '5' ) {
		$column_class = ' col-5';
	} else {
		$column_class = '';
	}

	$suggested_downloads = new WP_Query( array( 'post__in' => $suggestions, 'post_type' => 'download' ) );

	if ( $suggested_downloads->have_posts() ) :
		$single = __( 'this item', 'kiwi' );
		$plural = __( 'these items', 'kiwi' );
		//$cart_items_text = _n( $single, $plural, count( $post_ids ), 'kiwi' );
		$cart_items_text = _n( 'this item', 'these items', count( $post_ids ), 'kiwi' );
		
		?>
		
		<div class="mp-product-description mp-edd-recommended-items mp-edd-recommended-items-checkout">
			
			<div class="edd_widget_title">
				<h3><?php echo sprintf( __( 'Users who purchased %s, also purchased:', 'kiwi' ), $cart_items_text ); ?></h3>
			</div>
			
			<div id="edd-rp-items-wrapper" class="edd-rp-checkout<?php echo $column_class ?>">
				<?php while ( $suggested_downloads->have_posts() ) : ?>
					
					<?php $suggested_downloads->the_post();	?>
					
					<div class="edd_download">						
						<?php 						
						edd_get_template_part( 'shortcode', 'content-image' );						
						edd_get_template_part( 'shortcode', 'content-title' );
						edd_get_template_part( 'shortcode', 'content-price' );
						edd_get_template_part( 'shortcode', 'content-excerpt' );						
						?>						
					</div>
					
				<?php endwhile; ?>
			</div>
			
		</div>
	<?php endif; ?>

	<?php wp_reset_postdata(); ?>

	<?php endif; ?>

<?php endif; ?>

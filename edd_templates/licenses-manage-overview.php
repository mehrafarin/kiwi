<?php
$payment_id = absint( $_GET['payment_id' ] );
$user_id    = edd_get_payment_user_id( $payment_id );

if( ! current_user_can( 'edit_shop_payments' ) && $user_id != get_current_user_id() ) {
	return;
}

$color = edd_get_option( 'checkout_color', 'gray' );
$color = ( $color == 'inherit' ) ? '' : $color;

?>

<div class="container-fluid mp-fes-vendor-menu">
	<div class="container align-center"> 
		<div class="row">
			<?php mp_license_menu(); ?>
		</div>
	</div>
</div>


<script type="text/javascript">jQuery(document).ready(function($){ $(".edd_sl_show_key").on("click",function(e){e.preventDefault(),$(this).parent().find(".edd_sl_license_key").on("click",function(){$(this).select()}).fadeToggle("fast")}); });</script>

<div class="container mp-full-dashboard">
<div id="fes-vendor-dashboard">


<?php 
 	$edd_sl = edd_software_licensing();
	$keys   = $edd_sl->get_licenses_of_purchase( $payment_id );
	
	$x = 0;		
		
	foreach ( (array)$keys as $license ) : 
	$x++; endforeach; 	
	
	if ( $x >= '2' || $x = '0' ) {
		$div_number = esc_html__( 'Manage Sites', 'kiwi' );  
	} else {
		$div_number = esc_html__( 'Manage Site', 'kiwi' ); 
	} 

	echo '<h3>'.esc_attr( $div_number).'</h3>';	

	do_action( 'mp_edd_license_change' ); 
?>


<?php
// Retrieve all license keys for the specified payment
$edd_sl = edd_software_licensing();
$keys   = $edd_sl->get_licenses_of_purchase( $payment_id );
if ( $keys ) : ?>

<div class="table-responsive">
	<table id="edd_sl_license_keys" class="edd_sl_table">
		<thead>
			<tr class="edd_sl_license_row">
				<?php do_action('edd_sl_license_header_before'); ?>
				<th class="edd_sl_item"><?php esc_html_e( 'Item', 'kiwi' ); ?></th>
				<th class="edd_sl_key"><?php esc_html_e( 'Key', 'kiwi' ); ?></th>
				<th class="edd_sl_status"><?php esc_html_e( 'Status', 'kiwi' ); ?></th>
				<th class="edd_sl_limit"><?php esc_html_e( 'Activations', 'kiwi' ); ?></th>
				<th class="edd_sl_expiration"><?php esc_html_e( 'Expiration', 'kiwi' ); ?></th>
				<?php if( ! $edd_sl->force_increase() ) : ?>
					<th class="edd_sl_sites"><?php esc_html_e( 'Manage Sites', 'kiwi' ); ?></th>
				<?php endif; ?>
				<th class="edd_sl_upgrades"><?php esc_html_e( 'Upgrades', 'kiwi' ); ?></th>
				<?php do_action('edd_sl_license_header_after'); ?>
			</tr>
		</thead>
		<?php foreach ( $keys as $license ) : ?>
			<tr class="edd_sl_license_row">
				<?php do_action( 'edd_sl_license_row_start', $license->ID ); ?>
				<td class="edd_purchase_id">
					<?php
					$download_id = $edd_sl->get_download_id( $license->ID );
					$price_id    = $edd_sl->get_price_id( $license->ID );
					//echo get_the_title( $download_id ); 
					?>
					
					<a href="<?php the_permalink($download_id); ?>"><?php echo get_the_title( $download_id ); ?></a>
					
					<?php if( '' !== $price_id ) : ?>
						<span class="edd_sl_license_price_option"><?php echo edd_get_price_option_name( $download_id, $price_id ); ?></span>
					<?php endif; ?>
				</td>
				<td>
					<span class="view-key-wrapper">
						<a href="#" class="edd_sl_show_key" title="<?php esc_attr_e( 'Click to view license key', 'kiwi' ); ?>"><!--<img src="<?php echo EDD_SL_PLUGIN_URL . '/images/key.png'; ?>"/>--></a>
						<input type="text" readonly="readonly" class="edd_sl_license_key" value="<?php echo esc_attr( $edd_sl->get_license_key( $license->ID ) ); ?>" />
					</span>
				</td>
				
				<td class="edd_sl_license_status edd-sl-<?php echo $edd_sl->get_license_status( $license->ID ); ?>">
					<span class="mp-license-status"><?php echo $edd_sl->license_status( $license->ID ); ?></span>
				</td>
				
				<td><span class="edd_sl_limit_used"><?php echo $edd_sl->get_site_count( $license->ID ); ?></span><span class="edd_sl_limit_sep"><?php esc_html_e( ' &frasl; ', 'kiwi' ); ?></span><span class="edd_sl_limit_max"><?php echo $edd_sl->license_limit( $license->ID ); ?></span></td>
				<td>
				<?php if ( method_exists( $edd_sl, 'is_lifetime_license' ) && $edd_sl->is_lifetime_license( $license->ID ) ) : ?>
					<?php esc_html_e( 'Lifetime', 'kiwi' ); ?>
				<?php else: ?>
					<?php echo esc_attr( date_i18n( get_option('date_format'), $edd_sl->get_license_expiration( $license->ID ) ) ); ?>
				<?php endif; ?>
				</td>
				<?php if( ! $edd_sl->force_increase() ) : ?>
				<td><a href="<?php echo esc_url( add_query_arg( 'license_id', $license->ID ) ); ?>"><?php esc_html_e( 'Manage Sites', 'kiwi' ); ?></a></td>
				<?php endif; ?>
				<td>
				<?php if( edd_sl_license_has_upgrades( $license->ID ) && 'expired' !== $edd_sl->get_license_status( $license->ID ) ) : ?>
					<a href="<?php echo esc_url( add_query_arg( array( 'view' => 'upgrades', 'license_id' => $license->ID ) ) ); ?>"><?php esc_html_e( 'View Upgrades', 'kiwi' ); ?></a>
				<?php elseif ( edd_sl_license_has_upgrades( $license->ID ) && 'expired' == $edd_sl->get_license_status( $license->ID ) ) : ?>
					<span class="edd_sl_no_upgrades"><?php esc_html_e( 'Renew to upgrade', 'kiwi' ); ?></span>
				<?php else : ?>
					<span class="edd_sl_no_upgrades"><?php esc_html_e( 'No upgrades available', 'kiwi' ); ?></span>
				<?php endif; ?>
				</td>
				<?php do_action( 'edd_sl_license_row_end', $license->ID ); ?>
			</tr>
		<?php $x++; endforeach; ?>
	</table>
</div>	
<?php else : ?>
	<p class="edd_sl_no_keys"><?php esc_html_e( 'There are no license keys for this purchase.', 'kiwi' ); ?></p>
<?php endif;?>


	<p><a href="<?php echo esc_url( remove_query_arg( array( 'payment_id', 'edd_sl_error' ) ) ); ?>" class="edd-manage-license-back edd-submit button <?php echo esc_attr( $color ); ?>"><?php esc_html_e( 'Go back', 'kiwi' ); ?></a></p>

</div>
	</div>	

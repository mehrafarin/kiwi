<?php if ( edd_use_taxes() ) : ?>
<li class="cart_item edd-cart-meta edd_subtotal"><?php echo esc_html__( 'Subtotal:', 'kiwi' ). " <span class='subtotal'>" . esc_html( edd_currency_filter( edd_format_amount( edd_get_cart_subtotal() ) ) ); ?></span></li>
<li class="cart_item edd-cart-meta edd_purchase_tax_rate"><?php esc_html_e( 'Estimated Tax:', 'kiwi' ); ?> <?php echo esc_html( edd_currency_filter( edd_format_amount( edd_get_cart_tax() ) ) ); ?></li>
<?php endif; ?>
<li class="cart_item edd-cart-meta edd_checkout edd_total"><?php esc_html_e( 'Total:', 'kiwi' ); ?> <span><?php echo esc_html( edd_currency_filter( edd_format_amount( edd_get_cart_total() ) ) ); ?></span></li>
<li class="cart_item edd_checkout"><a href="<?php echo esc_url( edd_get_checkout_uri() ); ?>"><?php esc_html_e( 'Checkout', 'kiwi' ); ?></a></li>
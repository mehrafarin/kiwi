<?php 

/** Wish List template */

$list_id = edd_wl_get_list_id();
$downloads = edd_wl_get_wish_list( $list_id );
$list = get_post( $list_id );
$title = get_the_title( $list_id );
$privacy = get_post_status( $list_id );

?>


<div class="container-fluid mp-fes-vendor-menu">

	<div class="container align-center"> 
		<div class="row">
			<?php mp_license_menu(); ?>
		</div>
	</div>

</div>

<div class="container mp-full-dashboard">
<div id="fes-vendor-dashboard">

<h3><?php echo esc_html( $title ); ?></h3>

<div class="mp-wishlist-view-dashboard"> 

<?php if ( $list_id && $list->post_content ) : ?>
	<p><?php echo $list->post_content; ?></p>
<?php endif; ?>

<?php if ( $downloads ) : ?>

	

	<div class="table-responsive">
	<table id="mp_edd_wishlist" class="edd-wish-list">
		<thead>
			<tr class="edd_download_history_row">
				<?php do_action( 'edd_download_history_header_start' ); ?>
				
				<th class="edd_download_download_image"><?php esc_html_e( 'Item', 'kiwi' ); ?></th>
				
				<th class="edd_download_download_name"><?php esc_html_e( 'Price', 'kiwi' ); ?></th>
				
			<?php if ( class_exists( 'EDD_Reviews' ) ) { ?>	
				<th class="edd_download_download_ratings"><?php esc_html_e( 'Purchase', 'kiwi' ); ?></th>
			<?php } ?>	
				
				
				<?php if ( ! edd_no_redownload() ) : ?>
					<th class="edd_download_download_files"><?php esc_html_e( 'Remove', 'kiwi' ); ?></th>
				<?php endif; //End if no redownload?>
				<?php do_action( 'edd_download_history_header_end' ); ?>
			</tr>
		</thead>
		
			
			<?php foreach ( $downloads as $key => $item ) : ?>
			<tr class="wl-row">
			<td><?php echo edd_wl_item_title( $item );?></td>
			<td><?php echo edd_wl_item_price( $item['id'], $item['options'] );?></td>
			<td><?php echo edd_wl_item_purchase( $item );?></td>
			<td class="mp-remove-wishlist-item"><?php echo edd_wl_item_remove_link( $item['id'], $key, $list_id ); ?></td>
			</tr>
			<?php endforeach; ?>
		
			
			<tr class="wl-row">
			<td></td>
			<td></td>
			<td><?php echo edd_wl_add_all_to_cart_link( $list_id ); ?></td>
			<td></td>
			</tr>
			
			
			
			
			
		</table>
		</div>
	<?php /** Sharing - only shown for public lists*/
	if ( 'private' !== get_post_status( $list_id ) && apply_filters( 'edd_wl_display_sharing', true ) ) : ?>
		<div class="edd-wl-sharing">
			<h3 class="edd-wl-heading"><?php esc_html_e( 'Share', 'kiwi' ); ?></h3>
			<p><?php echo wp_get_shortlink( $list_id ); // Shortlink to share ?></p>

			<?php
				// Share via email
				echo edd_wl_share_via_email_link();

				// Social sharing services
				echo edd_wl_sharing_services();
			?>
		</div>
	<?php endif; ?>

<?php endif; ?>

<?php echo '<div class="mp-wishlist-edit-settings">' .edd_wl_edit_settings_link( $list_id ).'</div>'; ?>


</div>

</div>
</div>
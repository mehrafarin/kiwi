<?php 
/** Email sharing template */
?>

<p>
    <label for="edd-wl-from-name"><?php esc_html_e( 'Your Name', 'kiwi' ); ?></label>
    <input type="text" placeholder="<?php esc_html_e( 'Your Name', 'kiwi' ); ?>" name="edd_wl_from_name" id="edd-wl-from-name" class="edd-input" data-msg-required="<?php esc_html_e( 'Please enter your name', 'kiwi' ); ?>" data-rule-required="true" />
</p>

<p>
    <label for="edd-wl-from-email"><?php esc_html_e( 'Your Email Address', 'kiwi' ); ?></label>
    <input type="email" placeholder="<?php esc_html_e( 'Your Email address', 'kiwi' ); ?>" name="edd_wl_from_email" id="edd-wl-from-email" class="edd-input" data-rule-required="true" data-rule-email="true" data-msg-required="<?php esc_attr_e( 'Please enter your email address', 'kiwi' ); ?>" data-msg-email="<?php esc_attr_e( 'Please enter a valid email address', 'kiwi' ); ?>" />
</p>

<p>
    <label for="edd-wl-share-emails"><?php esc_html_e( 'Friend\'s Email Address', 'kiwi' ); ?></label>
    <span class="edd-description"><?php esc_html_e( 'To send to multiple email addresses, separate each email with a comma', 'kiwi' ); ?></span>
    <input type="text" placeholder="<?php esc_attr_e( 'Friend\'s Email address', 'kiwi' ); ?>" data-rule-required="true" data-rule-multiemail="true" name="edd_wl_share_emails" id="edd-wl-share-emails" class="edd-input" data-msg-required="<?php esc_attr_e( 'Please enter one or more email addresses', 'kiwi' ); ?>" data-msg-multiemail="<?php esc_attr_e( 'You must enter a valid email, or comma separate multiple emails', 'kiwi' ); ?>" />
</p>

<p>
    <label for="edd-wl-share-message"><?php esc_html_e( 'Your Message', 'kiwi' ); ?></label>
    <span class="edd-description"><?php esc_html_e( 'Optional', 'kiwi' ); ?></span>
    <textarea name="edd_wl_share_message" id="edd-wl-share-message" rows="3" cols="30"></textarea>
</p>
<?php
/* Template Name: Sidebar Left and Right */

get_header(); 

global $kiwi_theme_option; ?>

	<div class="col-sm-12 col-md-3 sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebarone'])):
				endif;
			?>
		</div>
	</div>
	
	<div class="col-sm-12 col-md-6 page-layout half">			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'templates/content', 'page' ); ?>		
				
				<?php if ( $kiwi_theme_option['page-comment-enable'] == '1') { ?>
					<?php comments_template( '', true ); ?>
				<?php } ?>	
				
			<?php endwhile; ?>			
	</div>
		
	<div class="col-sm-12 col-md-3 sidebar-layout">	
		<div class="sidebar">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['sidebar-pages-sidebartwo'])):
				endif; ?>
		</div>
	</div>

<?php get_footer(); ?>
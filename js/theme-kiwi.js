// Masonry
/* jQuery(function ($) { 
 var $container = $('.grid-masonry');
	$container.imagesLoaded( function() {
	$container.masonry({
			itemSelector: 'article',
			singleMode: true,
		});
	});  
}); */

// Scroll to top	
jQuery(function ($) {	
	$(window).scroll(function() {
		if($(this).scrollTop() != 0) {
			$('#backtotop').fadeIn();	
		} else {
			$('#backtotop').fadeOut();
		}
	});
 
	$('#backtotop').click(function() {
		$('body,html').animate({scrollTop:0},800);
	});	
});



// Matchheight js
/* jQuery(function ($) { 	
	$(document).ready(function(){		
		$('.mp-vc-items .mp-equal-heights').matchHeight();
	});
});
 */


jQuery(function ($) { 	
	$(document).ready(function(){	
		$('.edd-reviews-star-rating').off('click').on('click', function() {
			$('.edd-reviews-stars-filled').width( $(this).attr('data-rating') * 21 )
			$('input#edd-reviews-star-rating').val($(this).attr('data-rating'));
		});
	});
});	
<?php 
	global $kiwi_theme_option; 

	$postcategories = get_the_category(); 
?>

<?php if ( !is_single() ) { ?>
	<article itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class('embed-video'); ?>>
<?php } ?>

<?php if ( is_single() ) { ?>
	<div itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting" id="post-<?php the_ID(); ?>" <?php post_class('embed-video'); ?>>
<?php } ?>


	<?php 
		$video = wp_oembed_get( esc_url( get_post_meta( get_the_ID(), '_cmb2_videoembed', true ) ), array( 'width' => 'auto' ) );
		$fallback = wp_mediaelement_fallback( esc_url( get_post_meta( get_the_ID(), '_cmb2_videofile', true ) ) );
		
				
		if( ! empty( $video ) ) {
			echo "<div class='video-container embed' itemprop='video'>" . $video . " </div>";
		} elseif ( ! empty( $fallback ) ) {
			$attr = array(
				'src'      => strip_tags($fallback),
			);			
			echo "<div class='video-container' itemprop='video'>" . wp_video_shortcode( $attr ) . " </div>";						
		} else {
			echo '';
			}	
		
	?>	
	
	<div class="clear"></div>
	
	<div class="video-content">		
	
		<?php if ( is_single() ) { ?>
			<div class="meta-category-single">
			<span class="pull-left"><?php kiwi_entry_meta(); ?></span>
			
			<?php if ( $kiwi_theme_option['blog-editpostlink-enable-single'] == '0' && is_single() ) { ?>
				<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="pull-right comment-edit-link">', '</span>' ); ?>
			<?php } ?>
			
			<div class="clear"></div>
			</div>
		<?php } ?>
						
						
		<?php if ( $kiwi_theme_option['postformat-videotitle'] == '1' && !is_single() ) { ?>

			<?php if ( !empty($postcategories)) { ?>
				<span class="meta-category pull-left"><i class="fa fa-tag"></i> <?php the_category(', '); ?> </span>
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['blog-editpostlink-enable'] == '1') { ?>
				<?php edit_post_link( esc_html__( 'Edit', 'kiwi' ), '<span class="grid edit-link pull-right">', '</span>' ); ?>
			<?php } ?>
			
			<div class="clear"></div>
	
			<div class="post-title">
				<h3 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
			</div>
			
		<?php } ?>
		
			<?php if ( $kiwi_theme_option['postformat-videotitle'] == '1' && is_single() ) { ?>		
					<h3 class="entry-title" itemprop="headline"><?php the_title(); ?></h3>			
			<?php } ?>
			
				<?php if ( $kiwi_theme_option['postformat-videotitle'] == '0' && !is_single() ) { ?>		
					<div class="extra-space"></div>			
				<?php } ?>
				
		
		<?php if ( $kiwi_theme_option['postformat-videoexcerpt'] == '1' && !is_single() ) { ?>
		 <div class="grid-excerpt" itemprop="description"><?php the_excerpt(); ?></div>
		 <?php } ?>
						 
		<?php if ( $kiwi_theme_option['postformat-videocontent'] == '1' && !is_single() ) { ?> 
			<div itemprop="articleBody"><?php the_content(); ?></div>
		<?php } ?>
		
		<?php if ( is_single() ) : ?>	
			<div itemprop="articleBody"><?php the_content(); ?></div>
		<?php endif; ?>
		
			
			<?php if ( $kiwi_theme_option['postformat-videodate'] >= '1' && !is_single() || $kiwi_theme_option['postformat-videometa'] >= '1' && !is_single() ) { ?>
			<div class="kiwi-postformat meta-data">						
					<div class="pull-left">
						<?php if ( $kiwi_theme_option['postformat-videodate'] == '1' && !is_single() ) { ?>			
							<span itemprop="datePublished"><span itemprop="datePublished"><?php echo esc_attr( get_the_date( get_option('date_format') ) ); ?></span></span>
						<?php } ?>
					</div>	
				
					<div class="pull-right">
						<?php if ( $kiwi_theme_option['postformat-videometa'] == '1' && !is_single() ) { ?>
							<a href="<?php comments_link(); ?>" class="meta-commentlink"><span class="comments"><i class="fa fa-comment"></i> <?php comments_number( '0', '1', '%' ); ?></span></a>
							
						<?php } ?>
					</div>			
			<div class="clear"></div>
			</div>	
			<?php } ?>
			
								
			<?php if ( is_single() ) { ?>
				<?php kiwi_singlebottom_meta(); ?>
			<?php } ?>
					
	</div>

<?php if ( !is_single() ) { ?>
	</article>
<?php } ?>

<?php if ( is_single() ) { ?>
	</div>
<?php } ?>	

<?php if ( $kiwi_theme_option['blog-authorbio-enable'] == '1' && is_single() || is_multi_author() && is_single() ) : ?>
			<?php if ( get_the_author_meta( 'description' )  ) : ?>
				<?php get_template_part( 'templates/author', 'bio' ); ?>
			<?php endif; ?>
<?php endif; ?>	
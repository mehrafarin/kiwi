<?php
/* Template Name: Marketplace :: Front */

get_header(); 

	global $kiwi_theme_option;
			
	$grid_columns = $kiwi_theme_option['marketplace-sidebar-archive-gridcolumns']; 
	
	if ($grid_columns == '1') { 
		$variable_width = ' two-columns'; 
	} elseif ($grid_columns == '2') { 
		$variable_width = ' three-columns'; 
	} elseif ($grid_columns == '3') { 
		$variable_width = ' four-columns'; 
	} else {
		$variable_width = ' one-columns';
	}	
 
	$store_page_setting = (is_front_page() && is_page_template('page-marketplace.php') ? 'page' : 'paged' );
	$current_page = get_query_var( $store_page_setting );
	$per_page = $kiwi_theme_option['marketplace-sidebar-archive-postcount'];
	$offset = $current_page > 0 ? $per_page * ( $current_page-1 ) : 0;
	
	$product_args = array(
		'post_type'			=> 'download',
		'posts_per_page'	=> $per_page,
		'offset'			=> $offset
	);
	$products = new WP_Query( $product_args );

?>


<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] <= '2'  || $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '0') { ?>

		<div class="container marketplace mp-vc-items"<?php esc_attr( kiwi_rtl() ); ?>>
			<div class="row" role="main">
		
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '0' ) { ?>
				<div class="col-sm-12 col-md-12">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '1'
				&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-8 marketplace-layout">				
			<?php } ?>
			
				
					<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '1' 
					&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] == '2') { ?>
						<div class="col-sm-12 col-md-8 marketplace-layout" style="float:right!important">				
					<?php } ?>
					
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2' 
			&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] != '2') { ?>
				<div class="col-sm-12 col-md-6 marketplace-layout">
			<?php } ?>
			
						<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] == '1' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2' 
						&& $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] == '2') { ?>
							<div class="col-sm-12 col-md-6" style="float:right!important">
						<?php } ?>			
			
							
					<!-- begin loop -->				
					<?php 
					
					$have_results = $products->have_posts();					
					if ( !empty( $have_results) && $products->have_posts() ) : ?>
					
					<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '1' ) { ?>
							<div class="listview">
					<?php } ?>
					
					<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '2' ) { ?>
							<div class="grid-view<?php echo esc_attr( $variable_width ); ?>">
					<?php } ?>
											
						<ul class="layout-sorting">
		
						<?php while ($products->have_posts()) : $products->the_post(); ?>
							<?php get_template_part( 'templates/marketplace', 'archivescontent' ); ?>	
						<?php endwhile; ?>
					
						</ul>
				</div>	
		
		<div class="clear"></div>
		
		<div class="kiwi-pagination">
					<?php 					
						$big = 999999999; 				
						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, $current_page ),
							'total' => $products->max_num_pages
						) );
					?>
		</div>
		 

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>
									
							</div>		
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '1') { ?>
				<div class="col-sm-12 col-md-4 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>
			

			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2') { ?>
				<div class="col-sm-12 col-md-3 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebarone'])):
							endif;
						?>
					</div>
				</div>

				<div class="col-sm-12 col-md-3 sidebar-layout">	
					<div class="sidebar">
						<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebartwo'])):
							endif;
						?>
					</div>
				</div>
			<?php } ?>


	</div>
</div>
<?php } ?>



<!-- -->
<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-sidebarlayout'] == '3' ) { ?>

	<div class="container marketplace mp-vc-items"<?php esc_attr( kiwi_rtl() ); ?>>
			<div class="row" role="main">
		
	<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-enable'] > '0' && $kiwi_theme_option['marketplace-sidebar-archive-number'] == '2') { ?>
		<div class="col-sm-12 col-md-3 sidebar-layout">	
			<div class="sidebar">
				<?php
					if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebarone'])):
					endif;
				?>
			</div>
		</div>
	

	<div class="col-sm-12 col-md-6 marketplace-layout">
			
			<!-- begin loop -->
			<?php 
				$have_results = $products->have_posts();					
				if ( !empty( $have_results) && $products->have_posts() ) : ?>
							
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '1' ) { ?>
					<div class="listview">
			<?php } ?>
			
			<?php if ( $kiwi_theme_option['marketplace-sidebar-archive-layout'] == '2' ) { ?>
					<div class="grid-view<?php echo esc_attr( $variable_width ); ?>">
			<?php } ?>
												
					<ul class="layout-sorting">
			
						<?php while ($products->have_posts()) : $products->the_post(); ?>

							<?php get_template_part( 'templates/marketplace', 'archivescontent' ); ?>	
										
						<?php endwhile; ?>
		
					</ul>
				</div>	
		
		<div class="clear"></div>
		
		<div class="kiwi-pagination">
			<?php 					
				$big = 999999999;				
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, $current_page ),
					'total' => $products->max_num_pages
				) );
			?>
		</div>
		 
		</div>

		<?php else : ?>
			<?php get_template_part( 'templates/content', 'none' ); ?>
		<?php endif; ?>
	
	
	<div class="col-sm-12 col-md-3 sidebar-layout">	
		<div class="sidebar">
			<?php
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($kiwi_theme_option['marketplace-sidebar-archive-sidebartwo'])):
				endif;
			?>
		</div>
	</div>
	<?php } ?>
	
	</div>
</div>

<?php } ?>

<?php get_footer(); ?>
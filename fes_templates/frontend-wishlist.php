<h3><?php esc_html_e( 'Wishlist', 'kiwi' ); ?></h3>

<?php 
if( class_exists( 'EDD_Wish_Lists' ) ) {
	echo do_shortcode( '[edd_wish_lists]' ); 
}
else{
	echo esc_html__( 'The plugin EDD Wishlist is not installed and/or active.', 'kiwi' );
}
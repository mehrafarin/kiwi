<?php

global $kiwi_theme_option; 

$task       = ! empty( $_GET['task'] ) ? $_GET['task'] : 'dashboard';
$icon_css   = apply_filters( "fes_vendor_dashboard_menu_icon_css", "icon-white" ); //else icon-black/dark
$menu_items = EDD_FES()->dashboard->get_vendor_dashboard_menu();

$x = 0;

$url_id_vendor = $kiwi_theme_option['marketplace-enablelinks-dashboard'];
$vendor_link = get_page_link ($url_id_vendor);

?>

<div class="container-fluid mp-fes-vendor-menu">

<div class="container align-center"> 
<div class="row">

<?php foreach ( $menu_items as $item => $values ) : $values["task"] = isset( $values["task"] ) ? $values["task"] : ''; ?>
<?php $x++; endforeach; ?>
		
<?php if ( $x == '9' || $x == '10' || $x == '11'  ) {
		$div_number = 'mp-dashboard-links-limit'; 
	} elseif ( $x == '12' || $x == '13' || $x == '14' ) {
		$div_number = 'mp-dashboard-links-limit-extra'; 
	} elseif ( $x >= '15' ) {
		$div_number = 'mp-dashboard-links-limit-extra more'; 
	} else {
		$div_number = 'mp-dashboard-links'; 
	} 	
?>


<nav class="fes-vendor-menu">
	<ul class="<?php echo esc_attr( $div_number ); ?>">
		<?php foreach ( $menu_items as $item => $values ) : $values["task"] = isset( $values["task"] ) ? $values["task"] : ''; ?>		
			<li class="<?php if ( $task === $values["task"]  ) { echo "active"; } ?>">				
				<a href='<?php echo esc_url( add_query_arg( 'task', $values["task"], $vendor_link ) ); ?>'>
					<i class="icon icon-<?php echo esc_attr( $values["icon"] ); ?> <?php echo esc_attr( $icon_css ); ?>"></i> <span class="hidden-phone hidden-tablet"><?php echo isset( $values["name"] ) ? $values["name"] : $item; ?></span>
				</a>
			</li>
		<?php 
		endforeach; ?>
		</ul>  
</nav>


</div>
</div>

</div>

<div class="container mp-full-dashboard">

<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active('edd-bookings/edd-bookings.php') ) { ?>

	<?php if ( $task === 'bookings' ) { ?>
		<h3><?php esc_html_e( 'Bookings', 'kiwi' ); ?></h3><br />
	<?php } ?>

	<?php if ( $task === 'bookings-calendar'  ) { ?>
		<h3><?php esc_html_e( 'Bookings - Calendar', 'kiwi' ); ?></h3><br />
	<?php } ?>
	
	<?php if ( $task === 'edit-booking'  ) { ?>
		<h3><?php esc_html_e( 'Bookings - Details', 'kiwi' ); ?></h3><br />
	<?php } ?>

<?php } ?>
<?php global $kiwi_theme_option; ?>

<div class="container-fluid mp-fes-vendor-menu">

	<div class="container align-center"> 
		<div class="row">
			<?php mp_license_menu(); ?>
		</div>
	</div>

</div>

<div class="container mp-full-dashboard">
<div id="fes-vendor-dashboard">

<h3><?php esc_html_e( 'Application successfully sent!', 'kiwi' ); ?></h3>

<?php if ( EDD_FES()->vendors->user_is_status( 'pending' ) ) { ?>
	<p><?php esc_html_e( 'Your application has been submitted and will be reviewed.', 'kiwi' ); ?></p>
<?php } else { 
	$base_url = get_permalink( EDD_FES()->helper->get_option( 'fes-vendor-dashboard-page', get_permalink() ) );	
	wp_redirect( $base_url ); exit;		
} ?>

</div>
</div>
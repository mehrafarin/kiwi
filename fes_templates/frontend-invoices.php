<h3><?php esc_html_e( 'Invoices', 'kiwi' ); ?></h3>
<?php do_action( 'mp_dashboard_invoices_before' ); ?>
<?php echo do_shortcode( '[purchase_history]' ); 
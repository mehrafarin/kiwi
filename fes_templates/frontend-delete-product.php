<?php
$post_id = absint( $_REQUEST['post_id'] );
// check to make sure vendor is author of this download & can delete
if ( !EDD_FES()->vendors->vendor_can_delete_product($post_id) ){
	esc_html_e('Access Denied: You may only delete your own products','kiwi');
} else{ ?>
	<h3 class="fes-headers" id="fes-delete-product-page-title"><?php echo sprintf( esc_html_x( 'Delete %s:', 'FES uppercase singular setting for download', 'kiwi' ), EDD_FES()->helper->get_product_constant_name( $plural = false, $uppercase = true ) ) .  ' ' . esc_html__('#','kiwi'); echo esc_html( $post_id ); ?></h3>
	<div class="mp-delete"><?php printf( esc_html_x( 'Are you sure you want to delete this %s? This action is irreversible.', 'FES lowercase singular setting for vendor', 'kiwi' ), EDD_FES()->helper->get_product_constant_name( $plural = false, $uppercase = false ) ); ?></div>
	<form id="fes-delete-form" action="" method="post">
		<input type="hidden" name="pid" value="<?php echo esc_attr( $post_id ); ?>">
		<?php wp_nonce_field('fes_delete_nonce', 'fes_nonce'); ?>
		<button class="fes-delete button" type="submit"><?php esc_html_e( 'Delete', 'kiwi' ); ?></button>
	</form>
	<?php
}
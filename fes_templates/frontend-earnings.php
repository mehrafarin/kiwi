<?php
if ( EDD_FES()->integrations->is_commissions_active() ) { ?>
	<h3><?php esc_html_e( 'Commissions Overview', 'kiwi' ); ?></h3>
	
	<?php do_action( 'mp_dashboard_earnings_before' ); ?>
	
	<?php 
	if( eddc_user_has_commissions() ) {
		echo do_shortcode('[edd_commissions]'); 
	}
	else{
		echo esc_html__( 'You haven\'t made any sales yet!', 'kiwi' );
	}
} else { ?>
	<h3><?php esc_html_e( 'Commissions Overview', 'kiwi' ); ?></h3> <?php
	esc_html_e( 'There are no earnings available.', 'kiwi' );
}
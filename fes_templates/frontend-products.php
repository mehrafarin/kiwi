<?php global $products; ?>
<h3><?php echo EDD_FES()->helper->get_product_constant_name( $plural = true, $uppercase = true ) ?></h3>

<div class="clear"></div>

<?php do_action( 'mp_dashboard_products_before_table' ); ?>

<?php echo EDD_FES()->dashboard->product_list_status_bar(); ?>

<div class="table-responsive">
<table class="table fes-table table-condensed" id="fes-product-list">
	<thead>
		<tr>
			<th><?php esc_html_e( 'Image', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Name', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Status', 'kiwi' ); ?></th>
			<?php if ( class_exists( 'EDD_Reviews' ) ) { ?>	
				<th class="edd_download_download_ratings"><?php esc_html_e( 'Ratings', 'kiwi' ); ?></th>
			<?php } ?>	
			<th><?php esc_html_e( 'Price', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Sales', 'kiwi' ) ?></th>
			<th><?php esc_html_e( 'Date added', 'kiwi' ); ?></th>
			<?php do_action('fes-product-table-column-title'); ?>
		</tr>
	</thead>
	<tbody>
		<?php
		if (count($products) > 0 ){
		foreach ( $products as $product ) : ?>
			<tr>
				<td class = "fes-product-list-td mp-thumbnail"><?php echo get_the_post_thumbnail( $product->ID, 'full', array( 'class' => 'kiwi-featured-image' )); ?></td>
				<td class = "fes-product-list-td mp-title">
					<a href="<?php the_permalink($product->ID); ?>"><?php echo esc_html( EDD_FES()->dashboard->product_list_title($product->ID) ); ?></a>
					<div class="mp-actions"><?php EDD_FES()->dashboard->product_list_actions($product->ID); ?></div>
				</td>
				<td class = "fes-product-list-td mp-status"><?php echo EDD_FES()->dashboard->product_list_status($product->ID); ?></td>
				
				
<?php if ( class_exists( 'EDD_Reviews' ) ) {
	
		echo '<td class="fes-product-list-td mp-ratings">';
	
		$args = array(
			'post_id' => $product->ID,
			'count' => true, 
			'meta_key' => 'edd_rating',
		);
		$comments = get_comments($args);

		$rating_count = 0;
		$total_rating = 0;
		/* */
		
		$reviews = get_comments(
			apply_filters(
				'widget_edd_per_product_reviews_args',
				array(
					'status' => 'approve',
					'post_status' => 'publish',
					'post_type' => 'download',
					'post_id' => $product->ID,
				)
			)
		);
	
		if ( $reviews ) {		

			foreach ( (array) $reviews as $review ) {				
				$total_rating = $total_rating + get_comment_meta( $review->comment_ID, 'edd_rating', true );
				$rating_count = $rating_count + 1;
			}
			
		if($total_rating != 0) {	
			$average = $total_rating / $comments;
			$number = sprintf('%0.2f', $average);
		} else {
			$number = 0;
		}
		
			echo  '<div class="edd_reviews_rating_box" role="img" aria-label="'. $number . ' ' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo  '<div class="edd_star_rating custom" style="float: none; width: ' . ( 19 * $number ) . 'px;"></div>';
			echo  '</div>';
			
		} else {
			
			echo  '<div class="edd_reviews_rating_box" role="img" aria-label="0' . esc_html__( 'stars', 'kiwi' ) .'">';
			echo  '<div class="edd_star_rating custom" style="float: none; width:0;"></div>';
			echo  '</div>';
		}
	} 
	
			echo '</td>';
?>				
				
				<td class = "fes-product-list-td mp-price"><?php echo EDD_FES()->dashboard->product_list_price($product->ID); ?></td>
				<td class = "fes-product-list-td mp-sales"><?php echo EDD_FES()->dashboard->product_list_sales_esc($product->ID); ?></td>
				<td class = "fes-product-list-td mp-item-date"><?php echo EDD_FES()->dashboard->product_list_date($product->ID); ?></td>
				<?php do_action('fes-product-table-column-value'); ?>
			</tr>
		<?php endforeach;
		}
		else{
			echo '<tr><td colspan="7" class = "fes-product-list-td" >'. sprintf( esc_html__('No %s found','kiwi'), EDD_FES()->helper->get_product_constant_name( $plural = true, $uppercase = false ) ).'</td></tr>';
		}
		?>
	</tbody>
</table>
</div>

<?php do_action( 'mp_dashboard_products_after_table' ); ?>
<?php EDD_FES()->dashboard->product_list_pagination();
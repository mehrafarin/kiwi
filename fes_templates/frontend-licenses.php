<h3><?php esc_html_e( 'Licenses', 'kiwi' ); ?></h3>
<?php do_action( 'mp_dashboard_licenses_before' ); ?>
<?php echo do_shortcode( '[edd_license_keys]' ); 
<?php
    $user = get_current_user_id();
   
	$products = EDD_FES()->vendors->get_all_products($user);
	
	$pending = EDD_FES()->vendors->get_all_products_count(get_current_user_id(), 'pending');
	$approved = EDD_FES()->vendors->get_all_products_count(get_current_user_id(), 'publish');
	
	$pending_item = _n( 'pending item', 'pending items', $pending, 'kiwi' );
	$approved_item = _n( 'approved item', 'approved items', $approved, 'kiwi' );
	
	if ( EDD_FES()->integrations->is_commissions_active() ){
		$unpaid_commission = edd_currency_filter( eddc_get_unpaid_totals( get_current_user_id() ) );
	} else {
		$unpaid_commission = 0;
		}

	
	$sales = 0;
	$earnings = 0;

	if (!empty($products)) { 
		foreach ($products as $product) : 
		$sales+= $product['sales']; 
		endforeach; 
	} 
	
	$message = sprintf( _n( '%s sale', '%s sales', $sales, 'kiwi' ), $sales );
?>
</div>	

<h3><?php esc_html_e( 'Dashboard', 'kiwi' ); ?></h3>

<?php
$vendor_announcement = EDD_FES()->helper->get_option( 'fes-dashboard-notification', '' );
if ( $vendor_announcement ) {
	?>
	<div id="fes-vendor-announcements"><?php echo apply_filters( 'fes_dashboard_content', do_shortcode( $vendor_announcement ) ); ?></div>
	<?php
}
?>

<div id="fes-vendor-store-link">
	<?php echo EDD_FES()->vendors->get_vendor_store_url_dashboard(); ?>
	<span><?php printf( esc_html__( 'and you\'ve been a member since: %s.', 'kiwi' ), date_i18n( get_option('date_format'), strtotime( ( get_the_author_meta('user_registered', $user) ) ) ) ); ?></span>
</div>

<div id="fes-metabox-products" class="group">

<?php do_action( 'mp_dashboard_account_before' ); ?>

<div class="mp-account-overview-wrapper">
<h4><?php esc_html_e( 'Account overview', 'kiwi' ); ?></h4>

<div class="mp-account-overview">
<div class="row">

<?php if ( EDD_FES()->integrations->is_commissions_active() ) { ?>
	<div class="col-md-6 col-sm-6 stats-finance">		
		<div class="row mp-stats-finance">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h5><?php esc_html_e( 'Total Income', 'kiwi' ); ?></h5>
					<div class="result text-success"><?php echo wp_kses( edd_currency_filter( eddc_get_paid_totals( get_current_user_id() ) ), wp_kses_allowed_html( 'post' ) ); ?></div>
					<div class="result text-subline"><?php echo wp_kses( $message, wp_kses_allowed_html( 'post' ) ); ?></div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-6 mp-second">
					<h5><?php esc_html_e( 'Total due', 'kiwi' ); ?></h5>
					<div class="result text-due"><?php echo wp_kses( $unpaid_commission, wp_kses_allowed_html( 'post' ) ); ?></div>
					<div class="result text-subline"><?php esc_html_e( 'since last payment', 'kiwi' ); ?></div>			
				</div>	
		</div>
	</div>
<?php } ?>
	
	<div class="col-md-6 col-sm-6 stats-products">	
			<div class="row mp-stats-products">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h5><?php esc_html_e( 'Portfolio', 'kiwi' ); ?></h5>
					<div class="result text-success"><?php echo esc_html( $approved ); ?></div>
					<div class="result text-subline"><?php echo esc_html( $approved_item ); ?></div>	
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-6 mp-second">
					<h5><?php esc_html_e( 'Awaiting', 'kiwi' ); ?></h5>
					<div class="result text-due"><?php echo esc_html( $pending ) ?></div>
					<div class="result text-subline"><?php echo esc_html( $pending_item ); ?></div>			
				</div>
			</div>	
	</div>
</div>

</div>
</div>

<?php do_action( 'mp_dashboard_account_after' ); ?>

<div class="fes-comments-wrap">
	<table id="fes-comments-table" >
		<tr>
			<th class="col-author"><?php esc_html_e( 'Author', 'kiwi' ); ?></th>
			<th class="col-content"><?php esc_html_e( 'Comment', 'kiwi' ); ?></th>
		</tr>
		<?php echo EDD_FES()->dashboard->render_comments_table( 10 ); ?>
	</table>
</div>
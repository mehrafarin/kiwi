<?php global $orders; ?>
<h3><?php esc_html_e( 'Orders', 'kiwi' ); ?></h3>

<div class="table-responsive">
<table class="table fes-table table-condensed  table-striped" id="fes-order-list">
	<thead>
		<tr>
			<th><?php esc_html_e( 'Order', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Status', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Total', 'kiwi' ); ?></th>
			<th><?php esc_html_e( 'Customer', 'kiwi' ) ?></th>
			<th><?php esc_html_e( 'View Order','kiwi') ?></th>
			<?php do_action('fes-order-table-column-title'); ?>
			<th><?php esc_html_e( 'Date', 'kiwi' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (count($orders) > 0 ){
		foreach ( $orders as $order ) : ?>
			<tr>
				<td class="fes-order-list-td"><?php echo esc_html( EDD_FES()->dashboard->order_list_title($order->ID) ); ?></td>
				<td class="fes-order-list-td"><?php echo EDD_FES()->dashboard->order_list_status($order->ID); ?></td>
				<td class="fes-order-list-td"><?php echo wp_kses( EDD_FES()->dashboard->order_list_total($order->ID), wp_kses_allowed_html( 'post' ) ); ?></td>
				<td class="fes-order-list-td"><?php echo esc_html( EDD_FES()->dashboard->order_list_customer($order->ID )); ?></td>
				<td class="fes-order-list-td"><?php echo esc_html( EDD_FES()->dashboard->order_list_actions($order->ID) ); ?></td>
				<?php do_action('fes-order-table-column-value', $order); ?>
				<td class="fes-order-list-td"><?php echo esc_html( EDD_FES()->dashboard->order_list_date($order->ID) ); ?></td>
				
				
				
				
				
				
				
			</tr>
		<?php endforeach;
		}
		else{
			echo '<tr><td colspan="6">'.esc_html__('No orders found','kiwi').'</td></tr>';
		}
		?>
	</tbody>
</table>
</div>
<?php EDD_FES()->dashboard->order_list_pagination();
<h3><?php esc_html_e( 'Points history', 'kiwi' ); ?></h3>
<?php do_action( 'mp_dashboard_points_before' ); ?>
<?php echo do_shortcode( '[edd_points_history]' ); 
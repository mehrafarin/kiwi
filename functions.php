<?php

load_theme_textdomain( 'kiwi', get_template_directory() . '/languages' );

/*===============================================================
	Theme Options Panel
===============================================================*/
if(!function_exists('redux_register_custom_extension_loader')) :
	function redux_register_custom_extension_loader($ReduxFramework) {
		$path = dirname( __FILE__ ) . '/framework/theme-options/extensions/';
		$folders = scandir( $path, 1 );
		foreach($folders as $folder) {
			if ($folder === '.' or $folder === '..' or !is_dir($path . $folder) ) {
				continue;
			}
			$extension_class = 'ReduxFramework_Extension_' . $folder;
			if( !class_exists( $extension_class ) ) {

				$class_file = $path . $folder . '/extension_' . $folder . '.php';
				$class_file = apply_filters( 'redux/extension/'.$ReduxFramework->args['opt_name'].'/'.$folder, $class_file );
				if( $class_file ) {
					require_once( $class_file );
					$extension = new $extension_class( $ReduxFramework );
				}
			}
		}
	}
	add_action("redux/extensions/kiwi_theme_option/before", 'redux_register_custom_extension_loader', 0);
endif;


if ( !isset( $kiwi_theme_option ) && file_exists( dirname( __FILE__ ) . '/framework/theme-options/kiwi-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/framework/theme-options/kiwi-config.php' );
}

function addAndOverridePanelCSS() {
  wp_register_style( 'redux-custom-css', get_template_directory_uri() . '/framework/theme-options/redux-admin.css', array( 'farbtastic' ),  time(), 'all' );
  wp_enqueue_style( 'redux-custom-css' );
}
add_action( 'redux/page/kiwi_theme_option/enqueue', 'addAndOverridePanelCSS' );

/* Fonts by wpyar.com for theme */
#---TAKEXPERT start changelog---#
/*
function wpyar_fonts_theme() {
    wp_enqueue_style( 'wpyar-fonts', get_template_directory_uri() . '/fonts/wpyar-fonts/fonts.css' );
}
add_action( 'wp_enqueue_scripts', 'wpyar_fonts_theme' );
*/
#---TAKEXPERT end changelog---#


/*===============================================================
		Wp enqueue scripts
===============================================================*/
function wp_add_scripts() {

global $kiwi_theme_option;

if (!is_admin()) {

	// custom lostpassword/resetpassword shortcodes
	wp_enqueue_script( 'ajax-forms', get_template_directory_uri() . '/js/theme-ajax.js', array(), null, false );

	wp_enqueue_style( 'kiwi-style', get_stylesheet_uri() );
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/fonts/bootstrap/bootstrap.min.css' );
	wp_enqueue_style( 'fontawesome-css', get_template_directory_uri() . '/fonts/fontawesome/font-awesome.min.css' );
	wp_enqueue_style( 'marketplace-icons-css', get_template_directory_uri() . '/fonts/marketplace/Marketplace-Additional-Icons.css' );

 if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
	wp_enqueue_style( 'marketplace-css-rtl', get_template_directory_uri() . '/css/marketplace-rtl.css' );
	wp_enqueue_style( 'theme-style-rtl', get_template_directory_uri() . '/css/style-rtl.css' );
 }

	wp_enqueue_style( 'postformat-css', get_template_directory_uri() . '/css/postformat.css' );
	wp_enqueue_style( 'navigation-css', get_template_directory_uri() . '/css/navigation.css' );
	wp_enqueue_style( 'visualcomposer-css', get_template_directory_uri() . '/css/visualcomposer.css' );
	wp_enqueue_style( 'marketplace-css', get_template_directory_uri() . '/css/marketplace.css' );
	wp_enqueue_style( 'styled', get_template_directory_uri() . '/css/styled.css' );

	wp_register_style('option-panel', get_template_directory_uri() . '/css/dynamic.css', 'style');
	wp_enqueue_style( 'option-panel');

	wp_enqueue_script( 'notifyBar', get_template_directory_uri() . '/js/jquery.notifyBar.js', array(), null, false );
	wp_enqueue_script( 'cookie', get_template_directory_uri() . '/js/js.cookie.js', array(), null, false );

	if (class_exists('TablePress')) {
		wp_enqueue_style( 'tablepress-css', get_template_directory_uri() . '/css/tablepress.css' );
	}


	if ( class_exists('Vc_Manager') && class_exists('Marketplace_Edd_Vcaddons') ) {
		wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.min.css' );
		wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array(), null, false );


		 wp_add_inline_script( 'theme-kiwi', "jQuery(function ($) {
				$(document).ready( function() {
					new WOW().init();
					$('.mp-vc-items .mp-equal-heights').matchHeight();
				});
			})"
		);

	}

	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), null, true );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array(), null, true );
	wp_enqueue_script( 'theme-kiwi', get_template_directory_uri() . '/js/theme-kiwi.js', array(), null, true );


	if ( ! class_exists('Marketplace_Edd_Vcaddons')) {
		wp_enqueue_script( 'trunk8', get_template_directory_uri() . '/js/trunk8.js', array(), null, true );
		wp_enqueue_style( 'slider-slick', get_template_directory_uri() . '/css/slick.css' );
		wp_enqueue_script( 'slider-slick-js', get_template_directory_uri() . '/js/slick.min.js', array(), null, false );
		wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), null, true );
	}


		if ( $kiwi_theme_option['header-sticky'] == '1' ) {
			wp_enqueue_script( 'headhesive', get_template_directory_uri() . '/js/stickUp.min.js', array(), null, true );
		}

		if ( $kiwi_theme_option['script-bsmenu'] == '0' ) {
			wp_enqueue_script( 'smartmenu', get_template_directory_uri() . '/js/jquery.smartmenus.min.js', array(), null, true );
			wp_enqueue_script( 'smartmenuwp', get_template_directory_uri() . '/js/jquery.smartmenus.bootstrap.min.js', array(), null, true );
		}

		if ( $kiwi_theme_option['script-masonry'] == '0' ) {
			wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), null, true );
		}

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
}

}
add_action( 'wp_enqueue_scripts', 'wp_add_scripts' );


/*===============================================================
		Bootstrap menu responsive
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/theme-options/extensions/wbc_importer/settings.php' ) ) {
	require_once dirname( __FILE__ ) . '/framework/theme-options/extensions/wbc_importer/settings.php';
}


/*===============================================================
	Extra styling inside admin area
===============================================================*/
function change_adminbar_css() {
   wp_enqueue_style( 'add-admin-stylesheet', get_template_directory_uri() . '/framework/theme-options/mp-admin-css.css');
}

add_action( 'admin_enqueue_scripts', 'change_adminbar_css' );

/*===============================================================
		Bootstrap menu responsive
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/wp_bootstrap_navwalker.php' ) ) {
	require_once( get_stylesheet_directory() . '/framework/wp_bootstrap_navwalker.php' );
} else {
	require_once dirname( __FILE__ ) . '/framework/wp_bootstrap_navwalker.php';
}



/*===============================================================
		Marketplace posttype
===============================================================*/
 if ( class_exists( 'Easy_Digital_Downloads' ) ){

	 if ( file_exists( get_stylesheet_directory() . '/framework/files/marketplace_edd.php' ) ) {
		 require_once( get_stylesheet_directory() . '/framework/files/marketplace_edd.php' );
	 } else {
		 require_once dirname( __FILE__ ) . '/framework/files/marketplace_edd.php';
	 }

 }

/*===============================================================
		Theme widgets
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/files/widgets.php' ) ) {
	require_once( get_stylesheet_directory() . '/framework/files/widgets.php' );
} else {
	require_once dirname( __FILE__ ) . '/framework/files/widgets.php';
}


/*===============================================================
		Breadcrumbs
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/files/breadcrumbs.php' ) ) {
	require_once( get_stylesheet_directory() . '/framework/files/breadcrumbs.php' );
} else {
	require_once dirname( __FILE__ ) . '/framework/files/breadcrumbs.php';
}

/*===============================================================
		Postformat - Chat
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/files/postformat-chat.php' ) ) {
	require_once( get_stylesheet_directory() . '/framework/files/postformat-chat.php' );
} else {
	require_once dirname( __FILE__ ) . '/framework/files/postformat-chat.php';
}


 /*===============================================================
		Social Media Header + Footer Redux
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/files/socialmedia.php' ) ) {
	require_once( get_stylesheet_directory() . '/framework/files/socialmedia.php' );
} else {
	require_once dirname( __FILE__ ) . '/framework/files/socialmedia.php';
}





 /*===============================================================
	Theme Kiwi :: VC Addons
===============================================================*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active('js_composer/js_composer.php') && file_exists( dirname( __FILE__ ) . '/framework/files/vc-addons.php' ) ) {
	// Extending Typography Options in vc_text_separator
	require_once dirname( __FILE__ ) . '/framework/files/vc-addons.php';
}

if ( is_plugin_active('js_composer/js_composer.php') && file_exists( dirname( __FILE__ ) . '/vc_templates/vc_mp_kiwi_animation.php' ) ) {
	// Extending VC with EDD Slider
	require_once dirname( __FILE__ ) . '/vc_templates/vc_mp_kiwi_animation.php';
}

if ( is_plugin_active('js_composer/js_composer.php') && file_exists( dirname( __FILE__ ) . '/vc_templates/vc_mp_kiwi_button.php' ) ) {
	// Extending VC with Buttons
	require_once dirname( __FILE__ ) . '/vc_templates/vc_mp_kiwi_button.php';
}





/*===============================================================
	GTmetrix snippets to improve score
===============================================================*/
if ( file_exists( get_stylesheet_directory() . '/framework/files/gtmetrix.php' ) ) {
	require_once( get_stylesheet_directory() . '/framework/files/gtmetrix.php' );
} else {
	require_once dirname( __FILE__ ) . '/framework/files/gtmetrix.php';
}


/*===============================================================
	Include the TGM_Plugin_Activation class
===============================================================*/
	require_once dirname( __FILE__ ) . '/framework/class-tgm-plugin-activation.php';
	add_action( 'tgmpa_register', 'kiwi_register_plugins' );


/*===============================================================
	Register the required plugins for this theme
===============================================================*/
function kiwi_register_plugins() {

    $plugins = array(

		array(
            'name'               => 'Easy Digital Downloads',
            'slug'               => 'easy-digital-downloads',
            'required'           => true,
            'version'            => '2.5.9',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'EDD Custom Dashboard',
            'slug'               => 'EDD-Customer-Dashboard',
            'source'             => get_template_directory() . '/framework/plugins/EDD-Customer-Dashboard.zip',
            'required'           => false,
            'version'            => '1.0.3',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'Marketplace EDD VC Addons',
            'slug'               => 'marketplace-edd-vcaddons',
            'source'             => get_template_directory() . '/framework/plugins/marketplace-edd-vcaddons.zip',
            'required'           => false,
            'version'            => '1.1.4',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'Contact Form 7',
            'slug'               => 'contact-form-7',
            'required'           => true,
            'version'            => '4.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'EDD Sale Price',
            'slug'               => 'edd-sale-price',
            'required'           => false,
            'version'            => '1.0.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'EDD Coming Soon',
            'slug'               => 'edd-coming-soon',
            'required'           => false,
            'version'            => '1.0.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		/* array(
            'name'               => 'EDD Hide Download',
            'slug'               => 'edd-hide-download',
            'required'           => false,
            'version'            => '1.2.7',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ), */

		array(
            'name'               => 'Redux Framework',
            'slug'               => 'redux-framework',
            'required'           => true,
            'version'            => '3.5.7',
            'force_activation'   => true,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'CMB 2',
            'slug'               => 'cmb2',
            'required'           => true,
            'version'            => '2.1.2',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'Widget Importer & Exporter',
            'slug'               => 'widget-importer-exporter',
            'required'           => true,
            'version'            => '1.2.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),

		array(
            'name'               => 'WordPress Importer',
            'slug'               => 'wordpress-importer',
            'required'           => true,
            'version'            => '0.6.1',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
    );

    $config = array(
        'id'           => 'tgmpa',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => false,
		'message'      => '',

    );
    tgmpa( $plugins, $config );
}


/*===============================================================
	Force Visual Composer to initialize as "built into the theme"
===============================================================*/
add_action( 'vc_before_init', 'your_prefix_vcSetAsTheme' );
	function your_prefix_vcSetAsTheme() {
		vc_set_as_theme();
}


/*===============================================================
		Disable wp footer thank you message
===============================================================*/
function remove_footer_admin () {
    echo '';
}
add_filter('admin_footer_text', 'remove_footer_admin');


/*===============================================================
	Disable VC Admin Link
===============================================================*/
global $kiwi_theme_option;

if($kiwi_theme_option['miscellaneous-vcedit-admin'] == '1') {

	function vc_remove_wp_admin_bar_button() {
		remove_action( 'admin_bar_menu', array( vc_frontend_editor(), 'adminBarEditLink' ), 1000 );
	}
	add_action( 'vc_after_init', 'vc_remove_wp_admin_bar_button' );

}

/*===============================================================
		Excerpt length
===============================================================*/
function kiwi_excerpt_length( $length ) {

	global $post, $kiwi_theme_option;

	if ( $post->post_type == 'download' && $kiwi_theme_option['marketplace-enableexcerpt'] == '1' ) {
		return $kiwi_theme_option['marketplace-excerptlength'];
	} elseif ( $post->post_type == 'download' && $kiwi_theme_option['marketplace-enableexcerpt'] == '0' ) {
		return false;
	} else {
		return $kiwi_theme_option['blog-excerptlength'];
	}
}
add_filter( 'excerpt_length', 'kiwi_excerpt_length', 999 );


function blog_excerpt( $more ) {
	global $post, $kiwi_theme_option;

	if ( $post->post_type == 'download' && $kiwi_theme_option['marketplace-enableexcerpt'] == '1' ) {
		return ''.$kiwi_theme_option['marketplace-excerptending'].' <a href="'. get_permalink($post->ID) . '" class="btn-readmore">'.$kiwi_theme_option['marketplace-readmore'].'</a>';
	} elseif ( $post->post_type == 'download' && $kiwi_theme_option['marketplace-enableexcerpt'] == '0' ) {
		return false;
	} else {
		return ''.$kiwi_theme_option['blog-excerptending'].' <a href="'. get_permalink($post->ID) . '" class="btn-readmore">'.$kiwi_theme_option['blog-readmore'].'</a>';
	}
}
add_filter('excerpt_more', 'blog_excerpt');



/* function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
 */









/*===============================================================
		Post format :: meta boxes :: Page Settings
===============================================================*/

function mp_metabox_page_settings() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Page Settings', 'kiwi' ),
		'object_types'  => array( 'page', ), // Post type
		'context'   	=> 'normal',
		'priority'   	=> 'high',
		//'show_on'       => array( 'key' => 'exclude-page-template', 'value' => array( 'search', 'index' )),
		//'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/full-width.php' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Header', 'kiwi' ),
		'desc' => esc_html__( 'Enable header overlapping.', 'kiwi' ),
		'id' => $prefix . 'header_overlap',
		'type' => 'checkbox'
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Transparency', 'kiwi' ),
		'desc' => esc_html__( 'Enable header transparency.', 'kiwi' ),
		'id' => $prefix . 'header_transparency',
		'type' => 'checkbox'
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Navigation color', 'kiwi' ),
		'desc' => esc_html__( 'Enable white menu color', 'kiwi' ),
		'id' => $prefix . 'header_menu_color',
		'type' => 'checkbox'
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Hide pagetitle bar', 'kiwi' ),
		'desc' => esc_html__( 'Check to hide the pagetitle bar.', 'kiwi' ),
		'id' => $prefix . 'header_hide',
		'type' => 'checkbox'
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Page title bar margin', 'kiwi' ),
		'desc'    => esc_html__( 'top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi' ),
		'default' => '0px 0px 50px 0px',
		'id'      => $prefix . 'ptmargin',
		'type'    => 'text'
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Hide top bar', 'kiwi' ),
		'desc' => esc_html__( 'Check to hide topbar.', 'kiwi' ),
		'id' => $prefix . 'header_topbarhide',
		'type' => 'checkbox'
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Content margin', 'kiwi' ),
		'desc'    => esc_html__( 'top, right, bottom, left. e.g. 5px 10px 5px 10px', 'kiwi' ),
		'default' => '80px auto 80px auto',
		'id'      => $prefix . 'ptcontent',
		'type'    => 'text'
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Search string', 'kiwi' ),
		'desc'    => esc_html__( 'Limit search results to marketplace items only (excluding posts and pages).', 'kiwi' ),
		'id'      => $prefix . 'mpsearchstring',
		'type'    => 'checkbox'
	) );

	/* $cmb_demo->add_field( array(
		'name'    => esc_html__( 'Notification bar', 'kiwi' ),
		'desc'    => esc_html__( 'Force notification bar to show on this page', 'kiwi' ),
		'id'      => $prefix . 'mpforce_notificationbar',
		'type'    => 'checkbox'
	) ); */



}
add_action( 'cmb2_init', 'mp_metabox_page_settings' );



function mp_metabox_audio() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_audio',
		'title'         => esc_html__( 'Post format :: Audio Settings', 'kiwi' ),
		'object_types'  => array( 'post', ), // Post type
		'context'   	=> 'normal',
		'priority'   	=> 'high',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Audio file', 'kiwi' ),
		'desc' => esc_html__( 'Upload an audio file or enter a URL.', 'kiwi' ),
		'id'   => $prefix . 'audiofile',
		'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Embed audio', 'kiwi' ),
		'desc' => esc_html__( 'Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'kiwi' ),
		'id'   => $prefix . 'audioembed',
		'type' => 'oembed',
		// 'repeatable' => true,
	) );

}
add_action( 'cmb2_init', 'mp_metabox_audio' );



function mp_metabox_link() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_link',
		'title'         => esc_html__( 'Post format :: Link Settings', 'kiwi' ),
		'object_types'  => array( 'post', ), // Post type
		'context'   	=> 'normal',
		'priority'   	=> 'high',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Website URL', 'kiwi' ),
	//	'desc' => esc_html__( 'field description (optional)', 'kiwi' ),
		'id'   => $prefix . 'linkurl',
		'type' => 'text_url',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Heading', 'kiwi' ),
		'desc' => esc_html__( 'Optional', 'kiwi' ),
		'id'   => $prefix . 'linkheading',
		'type' => 'text_medium',
		// 'repeatable' => true,
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Subheading', 'kiwi' ),
		'desc' => esc_html__( 'Optional', 'kiwi' ),
		'id'   => $prefix . 'linksubheading',
		'type' => 'text_medium',
		// 'repeatable' => true,
	) );

}
add_action( 'cmb2_init', 'mp_metabox_link' );



function mp_metabox_quote() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_quote',
		'title'         => esc_html__( 'Post format :: Quote Settings', 'kiwi' ),
		'object_types'  => array( 'post', ), // Post type
		'context'   	=> 'normal',
		'priority'   	=> 'high',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Quote', 'kiwi' ),
		'id'   => $prefix . 'quotetitle',
		'type' => 'textarea_small',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Author', 'kiwi' ),
		'id'   => $prefix . 'quoteauthor',
		'type' => 'text_medium',
	) );

}
add_action( 'cmb2_init', 'mp_metabox_quote' );


function mp_metabox_video() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_video',
		'title'         => esc_html__( 'Post format :: Video Settings', 'kiwi' ),
		'object_types'  => array( 'post', ),
		'context'   	=> 'normal',
		'priority'   	=> 'high',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Video file', 'kiwi' ),
		'desc' => esc_html__( 'Upload a video file or enter a URL.', 'kiwi' ),
		'id'   => $prefix . 'videofile',
		'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Embed video', 'kiwi' ),
		'desc' => esc_html__( 'Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'kiwi' ),
		'id'   => $prefix . 'videoembed',
		'type' => 'oembed',
	) );


}
add_action( 'cmb2_init', 'mp_metabox_video' );


function mp_metabox_gallery() {

	$prefix = '_cmb2_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_gallery',
		'title'         => esc_html__( 'Post format :: Gallery Settings', 'kiwi' ),
		'object_types'  => array( 'post', ),
		'context'   	=> 'normal',
		'priority'   	=> 'high',
	) );

	$cmb_demo->add_field( array(
		'name'         => esc_html__( 'Images', 'kiwi' ),
		'desc'         => esc_html__( 'Upload or add multiple images/attachments.', 'kiwi' ),
		'id'           => $prefix . 'galleryimages',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Enable slideshow', 'kiwi' ),
		'desc' => esc_html__( 'Check to enabe slideshow.', 'kiwi' ),
		'id' => $prefix . 'flexslider',
		'type' => 'checkbox'
	) );


}
add_action( 'cmb2_init', 'mp_metabox_gallery' );


function wpse14707_scripts() {
    wp_enqueue_script( 'jquery' );

    $script = '<script type="text/javascript">
        jQuery( document ).ready( function($) {
		// Starts by hiding the "Video Options" meta box
		$( "#metabox_video" ).addClass( "hidden" );
		$( "#metabox_audio" ).addClass( "hidden" );
		$( "#metabox_link" ).addClass( "hidden" );
		$( "#metabox_quote" ).addClass( "hidden" );
		$( "#metabox_gallery" ).addClass( "hidden" );

		if( $( "input#post-format-video" ).is(":checked") ){
			$( "#metabox_video" ).removeClass( "hidden" );
		}

		if( $( "input#post-format-audio" ).is(":checked") ){
			$( "#metabox_audio" ).removeClass( "hidden" );
		}

		if( $( "input#post-format-link" ).is(":checked") ){
			$( "#metabox_link" ).removeClass( "hidden" );
		}
		if( $( "input#post-format-quote" ).is(":checked") ){
			$( "#metabox_quote" ).removeClass( "hidden" );
		}
		if( $( "input#post-format-gallery" ).is(":checked") ){
			$( "#metabox_gallery" ).removeClass( "hidden" );
		}


		// If "Video" post format is selected, show the "Video Options" meta box
		$( "input#post-format-video" ).change( function() {
			if( $(this).is(":checked") ){
				$( "#metabox_video" ).removeClass( "hidden" );
			}
		} );

		$( "input#post-format-audio" ).change( function() {
			if( $(this).is(":checked") ){
				$( "#metabox_audio" ).removeClass( "hidden" );
			}
		} );

		$( "input#post-format-link" ).change( function() {
			if( $(this).is(":checked") ){
				$( "#metabox_link" ).removeClass( "hidden" );
			}
		} );
		$( "input#post-format-quote" ).change( function() {
			if( $(this).is(":checked") ){
				$( "#metabox_quote" ).removeClass( "hidden" );
			}
		} );
		$( "input#post-format-gallery" ).change( function() {
			if( $(this).is(":checked") ){
				$( "#metabox_gallery" ).removeClass( "hidden" );
			}
		} );
    }
        );
    </script>';

    return print $script;
}
add_action( 'admin_footer', 'wpse14707_scripts' );


/*===============================================================
	CMB2 :: Featured Item Metabox
===============================================================*/
 if ( file_exists( get_stylesheet_directory() . '/framework/files/cmb_featured.php' ) ) {
	 require_once( get_stylesheet_directory() . '/framework/files/cmb_featured.php' );
 } else {
	 require_once dirname( __FILE__ ) . '/framework/files/cmb_featured.php';
 }

/*===============================================================
		CMB2 :: File Information Metabox + Item Images Metabox
===============================================================*/

global $kiwi_theme_option;

if( $kiwi_theme_option['metabox-themes-enable'] == '1' ){

	if ( !class_exists( 'EDD_Front_End_Submissions' ) && file_exists( dirname( __FILE__ ) . '/framework/files/cmb_fileinformation.php' )) {
		require_once dirname( __FILE__ ) . '/framework/files/cmb_fileinformation.php';
	}

	if ( !class_exists( 'EDD_Front_End_Submissions' ) && file_exists( dirname( __FILE__ ) . '/framework/edd/edd-widget-fileinfo.php' )) {
		require_once dirname( __FILE__ ) . '/framework/edd/edd-widget-fileinfo.php';
	}

}

if( $kiwi_theme_option['metabox-images-enable'] == '1' ){

	if ( !class_exists( 'EDD_Front_End_Submissions' ) && file_exists( dirname( __FILE__ ) . '/framework/files/cmb_images.php' )) {
		require_once dirname( __FILE__ ) . '/framework/files/cmb_images.php';
	}

}

/*===============================================================
	Template file_list :: SlickSlider
===============================================================*/
function cmb2_flexslider( $file_list_meta_key, $img_size = 'medium' ) {

    $files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );

    echo '<div class="kiwi-slider-settings"> <ul class="slick-slider">';

    foreach ( (array) $files as $attachment_id => $attachment_url ) {
        echo '<li>';
        echo wp_get_attachment_image( $attachment_id, $img_size );
        echo '</li>';
    }
    echo '</ul>';

	echo '<ul class="flex-direction-nav">
			<li><a href="#" class="flex-prev"></a></li>
			<li><a href="#" class="flex-next"></a></li>
		</ul>';

	echo '</div>';
}


function cmb2_nongallery( $file_list_meta_key, $img_size = 'medium' ) {

    $files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );

    echo '<div class="file-list-wrap">';

    foreach ( (array) $files as $attachment_id => $attachment_url ) {
        echo '<div class="file-list-image">';
        echo wp_get_attachment_image( $attachment_id, $img_size );
        echo '</div>';
    }
    echo '</div>';
}

/*===============================================================
	Register sidebar
===============================================================*/
function kiwi_widgets_init() {

	global $kiwi_theme_option;

	$footer_reg_widget = $kiwi_theme_option['footer-columns'];

	register_sidebar( array(
		'name' => esc_html__( 'Post Sidebar 1', 'kiwi' ),
		'id' => 'post-sidebar',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Post Sidebar 2', 'kiwi' ),
		'id' => 'post-sidebar-two',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Page Sidebar 1', 'kiwi' ),
		'id' => 'page-sidebar',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Page Sidebar 2', 'kiwi' ),
		'id' => 'page-sidebar-two',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Marketplace Sidebar 1', 'kiwi' ),
		'id' => 'marketplace-sidebar',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Marketplace Sidebar 2', 'kiwi' ),
		'id' => 'marketplace-sidebar-two',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Marketplace Sidebar 3', 'kiwi' ),
		'id' => 'marketplace-sidebar-three',
		'description'   => 'Special sidebar for the item description page.',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<div class="widget_title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Archive Sidebar 1', 'kiwi' ),
		'id' => 'archive-sidebar',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => esc_html__( 'Archive Sidebar 2', 'kiwi' ),
		'id' => 'archive-sidebar-two',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '<div style="clear:both;"></div></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );


	if($footer_reg_widget >= '1'){
	register_sidebar(array(
			'name' => esc_html__( 'Footer Widget 1', 'kiwi' ),
			'id' => 'kiwi-footer-widget-1',
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}

	if($footer_reg_widget >= '2'){
			register_sidebar(array(
			'name' => esc_html__( 'Footer Widget 2', 'kiwi' ),
			'id' => 'kiwi-footer-widget-2',
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}

	if($footer_reg_widget >= '3'){
			register_sidebar(array(
			'name' => esc_html__( 'Footer Widget 3', 'kiwi' ),
			'id' => 'kiwi-footer-widget-3',
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}

	if($footer_reg_widget >= '4'){
			register_sidebar(array(
			'name' => esc_html__( 'Footer Widget 4', 'kiwi' ),
			'id' => 'kiwi-footer-widget-4',
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}

	if($footer_reg_widget >= '5' && $footer_reg_widget != '7'){
			register_sidebar(array(
			'name' => esc_html__( 'Footer Widget 5', 'kiwi' ),
			'id' => 'kiwi-footer-widget-5',
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}

	if($footer_reg_widget >= '6' && $footer_reg_widget != '7'){
			register_sidebar(array(
			'name' => esc_html__( 'Footer Widget 6', 'kiwi' ),
			'id' => 'kiwi-footer-widget-6',
			'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>',
			));
		}

}
add_action( 'widgets_init', 'kiwi_widgets_init' );


/*===============================================================
	WP 4.4 Title :: Remove blog title + separator
===============================================================*/
if ( ! function_exists( 'kiwi_title_name' ) ) :
	function kiwi_title_name($title) {
		unset( $title['site'] );
		return $title;
	}
	add_filter('document_title_parts', 'kiwi_title_name' );
endif;


if ( ! function_exists( 'kiwi_title_separator' ) ) :
	function kiwi_title_separator() {
		return '';
	}
	add_filter('document_title_separator', 'kiwi_title_separator');
endif;

/*===============================================================
		Shortcode support to sidebar widgets and excerpt
===============================================================*/
add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );
add_filter( 'the_excerpt', 'do_shortcode' );

/*===============================================================
		Theme support
===============================================================*/
function kiwi_setup() {

	global $kiwi_theme_option;

	//load_theme_textdomain( 'kiwi', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-formats', array( 'aside','image','link','quote','status','video','audio','gallery','chat' ) );

	add_theme_support( 'yoast-seo-breadcrumbs' );

	add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form', ) );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'featured-img', 768, 450, true );
	add_image_size( 'product-img', 540, 360, true );

	$mp_width = $kiwi_theme_option['marketplace-slider-thumb-width'];
	$mp_height = $kiwi_theme_option['marketplace-slider-thumb-height'];

	add_image_size( 'mp-slider-product-page', $mp_width, $mp_height, true );
}
add_action( 'after_setup_theme', 'kiwi_setup' );


/*===============================================================
	Fallback
===============================================================*/
if ( ! isset( $content_width ) ) $content_width = 733;

/*===============================================================
	Menu
===============================================================*/
global $kiwi_theme_option;

register_nav_menus( array(
	'main_navigation' 	=> esc_html__('Main Navigation','kiwi'),
	'top_navigation'	=> esc_html__('Top Navigation','kiwi'),
	'footer_navigation' => esc_html__('Footer Navigation','kiwi'),
	'404_navigation' 	=> esc_html__('404 Page Navigation','kiwi'),
	'header_nav_first' 	=> esc_html__('Header first half','kiwi'),
	'header_nav_second' => esc_html__('Header second half','kiwi'),
	));

if ( $kiwi_theme_option['marketplace-enable-userroles'] == '1' ) {
	register_nav_menus( array(
		'nonlogged_navigation' 	=> esc_html__('Top Navigation for non-logged in users','kiwi'),
		'logged_navigation' 	=> esc_html__('Top Navigation for logged in users','kiwi'),
		'vendor_navigation' 	=> esc_html__('Top Navigation for logged in vendors','kiwi'),
	));
}

/*===============================================================
	Full Screen Search Overlay :: Main Menu
===============================================================*/
if( $kiwi_theme_option['header-expandable-search'] == '1' ){

	global $kiwi_theme_option;

	function add_search_to_wp_menu ( $items, $args ) {
		if( $args->theme_location == 'main_navigation' ) {
			$items .= '<li class="menu-item menu-item-search search-form">';
			$items .= ' <a id="overlay-menu"><i class="search-field"></i></a>';
			$items .= '</li>';
		}
		return $items;
	}
	add_filter('wp_nav_menu_items','add_search_to_wp_menu', 10, 2);

}

/*===============================================================
	Return the post URL
===============================================================*/
function kiwi_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}


/*===============================================================
	Extend the default WordPress body classes
===============================================================*/
function kiwi_body_class( $classes ) {

	global $kiwi_theme_option;

	if ( ! is_multi_author() ) {
		$classes[] = 'single-author';
	}
	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() ) {
		$classes[] = 'sidebar';
	}
	if ( ! get_option( 'show_avatars' ) ) {
		$classes[] = 'no-avatars';
	}
	if ( $kiwi_theme_option['settings-sitelayout'] == '2' && !is_page_template( 'page-fullwidth.php' ) ) {
		$classes[] = 'boxed';
	}
	if ( !empty($kiwi_theme_option['styling-body']['background-color']) )  {
		$classes[] = 'styled-background';
	}
	if ( empty($kiwi_theme_option['styling-body']['background-color']) )  {
		$classes[] = 'standard-style';
	}

	if (class_exists( 'EDD_Front_End_Submissions' ) ){

		$fes_featured_video = get_post_meta( get_the_ID(), 'upload_featured_video', true );
		$fes_featured_audio = get_post_meta( get_the_ID(), 'upload_featured_audio', true );
		$fes_embedded_url = get_post_meta( get_the_ID(), 'upload_media_embed', true );

		if ( ! empty( $fes_featured_video ) && empty( $fes_featured_audio ) ) {
			$classes[] = 'marketplace-video';
		}

		if ( empty( $fes_featured_video ) && ! empty( $fes_featured_audio ) ) {
			$classes[] = 'marketplace-audio';
		}

		 if ( empty( $fes_featured_video ) && empty( $fes_featured_audio ) && !empty( $fes_embedded_url )) {
			$classes[] = 'marketplace-embedded-url';
		}

	}

	if ( class_exists( 'EDD_Cross_Sell_And_Upsell' ) ){

		if( edd_is_checkout() ) {
			$classes[] = 'mp-vc-items';
		}

	}

	return $classes;
}

add_filter( 'body_class', 'kiwi_body_class' );


/** Filter the page menu arguments.
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link. **/
function kiwi_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'kiwi_page_menu_args' );


/*===============================================================
	Pagination
===============================================================*/
if ( ! function_exists( 'kiwi_content_nav' ) ) :

function kiwi_content_nav() {
    global $wp_query;

    $big = 999999999;
    $tag = '<div class="kiwi-pagination">' . PHP_EOL;
    $tag .= paginate_links( array(
            'base'              => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'            => '?paged=%#%',
            'current'           => max( 1, get_query_var('paged') ),
            'total'             => $wp_query->max_num_pages,
            'prev_next'         => True,
            'prev_text'         => esc_html__('&laquo;','kiwi'),
            'next_text'         => esc_html__('&raquo;','kiwi'),
        ) ) . PHP_EOL;
    $tag .= '</div>' . PHP_EOL;
    echo $tag;
}
endif;


/*===============================================================
	Pagination :: Comments
===============================================================*/
function my_comments_nav($nav) {
	if (get_option('page_comments')) {
		$total_pages = get_comment_pages_count();
		$args = (array(
    			'echo' => false,
    			'prev_text'         => esc_html__('&laquo;','kiwi'),
				'next_text'         => esc_html__('&raquo;','kiwi'),
    			'add_fragment' => '#comments'
				));
		if ($total_pages > 1) {
			$nav = '<div id="comment_nav" class="kiwi-pagination"><p class="previous">';
			$nav .= paginate_comments_links($args);
			$nav .= "</p></div>\n\n";
			}
		}
	return $nav;
}

add_filter('paginate_comments_links', 'my_comments_nav');

/*===============================================================
	WP 4.3 :: Re-enable comments by default for pages
===============================================================*/
function wpdocs_open_comments_for_myposttype( $status, $post_type, $comment_type ) {
	if ( ( 'page' === $post_type ) && ( 'comment' === $comment_type ) ) {
		return 'open';
	}
	return $status;
}

add_filter( 'get_default_comment_status', 'wpdocs_open_comments_for_myposttype', 10, 3 );

/*===============================================================
	Pagination :: SEO rel="next" and rel="prev"
===============================================================*/
function slug_remove_rel_prev( $format ) {
	return str_replace( ' rel="prev"', '', $format );
}
add_filter( 'previous_post_link', 'slug_remove_rel_prev' );

function slug_remove_rel_next( $format ) {
	return str_replace( ' rel="next"', '', $format );
}
add_filter( 'next_post_link', 'slug_remove_rel_next' );

/*===============================================================
	Template for comments and pingbacks
===============================================================*/
if ( ! function_exists( 'kiwi_comment' ) ) :

function kiwi_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php esc_html_e( 'Pingback:', 'kiwi' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'kiwi' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

	<?php echo get_avatar( $comment, 64 ); ?>

		<article id="comment-<?php comment_ID(); ?>" class="comment">

			<header class="comment-meta comment-author vcard">
				<?php
					printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span>' . esc_html__( 'Post author', 'kiwi' ) . '</span>' : ''
					);?>

					<div class="reply-meta">
						<?php printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
							esc_url( get_comment_link( $comment->comment_ID ) ),
							esc_attr( get_comment_time( 'c' ) ),
							/* translators: 1: date, 2: time */
							sprintf( esc_html__( '%1$s at %2$s', 'kiwi' ), esc_attr( get_comment_date() ), esc_attr( get_comment_time() ) )
						);
						?>
						<span class="reply">
							<?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'kiwi' ), 'after' => ' ', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
						</span>
					</div>

			</header>
			<div class="clear"></div>

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'kiwi' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( esc_html__( 'Edit', 'kiwi' ), '<p class="edit-link">', '</p>' ); ?>
			</section>


		</article>
	<?php
		break;
	endswitch;
}
endif;


/*===============================================================
Meta info for current post: categories, tags, permalinks, author, and date.
===============================================================*/
if ( ! function_exists( 'kiwi_entry_meta' ) ) :

function kiwi_entry_meta() {

	$categories_list 	= get_the_category_list( esc_html__( ', ', 'kiwi' ) );
	$tag_list 			= get_the_tag_list( '', esc_html__( ', ', 'kiwi' ) );
	$txt_modified 		= esc_html__( 'Last modified on: ', 'kiwi' );

	if (get_post_time() < get_post_modified_time()) {
			$date = sprintf( '<time class="entry-date published" datetime="%3$s"><span itemprop="dateCreated datePublished">%4$s</span></time>',
				esc_url( get_permalink() ),
				esc_attr( get_the_time() ),
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_html( get_the_modified_date() )
			);

			$date_modified = sprintf( '<div>'.$txt_modified.'<time class="entry-date updated" datetime="%3$s"><span itemprop="dateModified">%5$s</span></time></div>',
				esc_url( get_permalink() ),
				esc_attr( get_the_time() ),
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_html( get_the_modified_date() )
			);

	} else {

		$date = sprintf( '<time class="entry-date published" datetime="%3$s"><span itemprop="dateCreated datePublished">%4$s</span></time>',
				esc_url( get_permalink() ),
				esc_attr( get_the_time() ),
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_html( get_the_modified_date() )
			);

		$date_modified = '';
	}


	$author = sprintf( '<span class="author vcard" itemprop="author"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( esc_html__( 'View all posts by %s', 'kiwi' ), get_the_author() ) ),
		get_the_author()
	);

	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
	if ( $tag_list ) {
		$utility_text = esc_html__( '%3$s / in %1$s / by %4$s %5$s', 'kiwi' );
	} elseif ( $categories_list ) {
		$utility_text = esc_html__( '%3$s / in %1$s / by %4$s %5$s', 'kiwi' );
	} else {
		$utility_text = esc_html__( '%3$s / by %4$s %5$s', 'kiwi' );
	}

	printf( $utility_text,$categories_list,$tag_list,$date,$author,$date_modified);

}
endif;

/*===============================================================
	Register postMessage support
===============================================================*/
/* Add postMessage support for site title and description for the Customizer. */
function kiwi_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'kiwi_customize_register' );

function kiwi_customize_preview_js() {
	wp_enqueue_script( 'kiwi-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20141120', true );
}
add_action( 'customize_preview_init', 'kiwi_customize_preview_js' );


/*===============================================================
		Post :: Meta data
===============================================================*/
if ( ! function_exists( 'kiwi_singlebottom_meta' ) ) :
/** Meta info on single posts **/
function kiwi_singlebottom_meta() {
	//global $wp_query;

	$posttags = get_the_tags();

	 ?>
		<div class="metadata-single">
			<div class="pull-left">
					<?php if ( !empty($posttags)) { ?>
						<b><?php esc_html_e( 'Tags:', 'kiwi' ); ?></b><?php the_tags( ' ', ', ', '<br />' ); ?>
					<?php } ?>
			</div>

			<div class="pull-right">
				<span class="comments"><i class="fa fa-comment"></i> <?php comments_number( '0', '1', '%' ); ?></span>

			</div>

			<div class="clear"></div>
		</div>
	<?php
}
endif;

if ( ! function_exists( 'kiwi_bootstrap_menu' ) ) :
/** Meta info on not single posts **/
function kiwi_bootstrap_menu() {
	global /* $wp_query, */ $kiwi_theme_option;
	 ?>

	 <?php if ( $kiwi_theme_option['miscellaneous-ubermenu'] == '0'  ) { ?>
	<nav class="navbar navbar-static-top" role="navigation">

	  <a class="kiwi-navbar-brand visible-xs main-logo" itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="logo" src="<?php echo esc_url( $kiwi_theme_option['settings-headerlogo']['url'] ); ?>" height="<?php echo '' . $kiwi_theme_option['settings-logoheight']; ?>" style="margin:<?php echo '' . $kiwi_theme_option['settings-logomargin']; ?>" alt=""></a>

		<div class="navbar-header">
		<?php #---TAKEXPERT start changelog---# ?>
          <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">-->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">منو
		<?php #---TAKEXPERT end changelog---# ?>
            <span class="sr-only"><?php esc_html_e( 'Toggle navigation', 'kiwi' ); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		</div>
        <?php #---TAKEXPERT start changelog---# ?>
        <div class="collapse navbar-collapse">
        <!--<div class="collapse in navbar-collapse">-->
        <?php #---TAKEXPERT end changelog---# ?>
		<?php } ?>
	<?php
}
endif;

if ( ! function_exists( 'kiwi_bootstrap_menu_end' ) ) :
/** Meta info on not single posts **/
function kiwi_bootstrap_menu_end() {
	global $wp_query, $kiwi_theme_option;
	 ?>
	  <?php if ( $kiwi_theme_option['miscellaneous-ubermenu'] == '0'  ) { ?>
	</div>
		</nav>
		<?php } ?>
	<?php
}
endif;

/*===============================================================
		Post :: Archives
===============================================================*/
if ( ! function_exists( 'kiwi_archive_meta' ) ) :
function kiwi_archive_meta() {
	global $kiwi_theme_option;
	 ?>
	 <?php if ( $kiwi_theme_option['page-pagetitle-archive-enable'] == '1' ) { ?>
		<div class="archive-page">
			<h3><?php
				if ( is_day() ) :
					printf( esc_html__( 'Daily Archives: %s', 'kiwi' ), '<span>' . esc_html( get_the_date() ) . '</span>' );
				elseif ( is_month() ) :
					printf( esc_html__( 'Monthly Archives: %s', 'kiwi' ), '<span>' . esc_attr( get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'kiwi' ) ) ) . '</span>' );
				elseif ( is_year() ) :
					printf( esc_html__( 'Yearly Archives: %s', 'kiwi' ), '<span>' . esc_attr( get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'kiwi' ) ) ) . '</span>' );
				else :
					esc_html_e( 'Archives', 'kiwi' );
				endif;
			?>
			</h3>
		</div>
	<?php } ?>
	<?php
}
endif;

/*===============================================================
	Removal of whitespace inside excerpt
===============================================================*/
function whitespace_excerpt( $excerpt ) {
    return preg_replace( '~^(\s*(?:&nbsp;)?)*~i', '', $excerpt );
}
add_filter( 'get_the_excerpt', 'whitespace_excerpt' );

/*===============================================================
	HTML output comment
===============================================================*/
function wpdocs_comment_form_defaults( $defaults ) {
  $defaults['title_reply'] = '<span>' . esc_html__( 'Leave a reply', 'kiwi' ). '</span>';
  return $defaults;
}
add_filter( 'comment_form_defaults', 'wpdocs_comment_form_defaults' );


/*===============================================================
	RTL Feature
===============================================================*/
if ( ! function_exists( 'kiwi_rtl' ) ) :
function kiwi_rtl() {

	global $kiwi_theme_option;

	if ( $kiwi_theme_option['miscellaneous-vc-rtl-support'] == '1' ) {
			echo ' dir="rtl"';
		} else {
			echo '';
	}
}
endif;

























/*===============================================================
	Lost password link url to page with custom shortcode
===============================================================*/
if ( ! function_exists( 'mp_lost_password_page' ) ) :

	function mp_lost_password_page( $lostpassword_url, $redirect ) {

		global $kiwi_theme_option;
		 
		if ( isset($kiwi_theme_option['marketplace-link-lost-password']) && !empty($kiwi_theme_option['marketplace-link-lost-password']) ) {
			return get_page_link ($kiwi_theme_option['marketplace-link-lost-password']);
		} else {
			return $lostpassword_url;
		}

	}
	add_filter( 'lostpassword_url', 'mp_lost_password_page', 10, 2 );


endif;


/*===============================================================
	Lost password link redirect back to the edd login page
===============================================================*/
if ( ! function_exists( 'mp_redirect_after_lost_password' ) ) :

	function mp_redirect_after_lost_password( $lostpassword_redirect ) {

		global $kiwi_theme_option;

		$url_id 	= $kiwi_theme_option['marketplace-enablelinks-login'];
		//$buyer_link = get_page_link ($url_id);

		if ( isset($url_id) && !empty($url_id) ) {
			return get_page_link ($url_id);
		} else {
			return home_url();
		}

	}
	add_filter( 'lostpassword_redirect', 'mp_redirect_after_lost_password' );


endif;



/*===============================================================
	Redirect after clicking on the reset password button
===============================================================*/
function wpse_lost_password_redirect() {

    // Check if have submitted
    $confirm = ( isset($_GET['action'] ) && $_GET['action'] == resetpass );

	global $kiwi_theme_option;

	$url_id 	= $kiwi_theme_option['marketplace-enablelinks-login'];
	//$buyer_link = get_page_link ($url_id);

	if( $confirm ) {
		if ( isset($url_id) && !empty($url_id) ) {
			wp_redirect( get_page_link ($url_id) );
			exit;
		} else {
			wp_redirect( home_url() );
			exit;
		}
	}

}
add_action('login_headerurl', 'wpse_lost_password_redirect');




/*===============================================================
	Shortcode :: [edd_lost_password]
===============================================================*/
if ( ! function_exists( 'mp_shortcode_lost_password' ) ) :

	function mp_shortcode_lost_password( $atts ) {

	$a = shortcode_atts( array(
       // 'foo' => 'something',
    ), $atts, 'edd_lost_password' );

		if ( is_user_logged_in() ) {
			wp_redirect( home_url('/') );
		}

		ob_start();

		get_template_part( 'edd_templates/shortcode', 'lost-password' );

		$output = ob_get_contents();

		ob_end_clean();

	return $output;

	}

	add_shortcode('edd_lost_password' , 'mp_shortcode_lost_password');



endif;



/*===============================================================
	Shortcode :: [edd_reset_password]
===============================================================*/
if ( ! function_exists( 'mp_shortcode_reset_password' ) ) :

	function mp_shortcode_reset_password( $atts ){

		$a = shortcode_atts( array(
		   // 'foo' => 'something',
		), $atts, 'edd_reset_password' );

		if ( is_user_logged_in() ) {
			wp_redirect( home_url('/') );
		}

		ob_start();

		get_template_part( 'edd_templates/shortcode', 'reset-password' );

		$output = ob_get_contents();

		ob_end_clean();

	return $output;

	}
	add_shortcode('edd_reset_password' , 'mp_shortcode_reset_password');

endif;





/*===============================================================
	Reset the user pass after validation
===============================================================*/
function reset_user_pass(){

	parse_str( $_POST['form_values'], $params );

	$user = check_password_reset_key($params['key'], $params['login']);

	$status='';

	// Check if key is valid
	if ( is_wp_error($user) ) {
		if ( $user->get_error_code() === 'expired_key' ){
			$status = 'expiredkey' ;
		} else{
			$status = 'invalidkey' ;
		}

		echo $status;
		die;
	}

	// check if keys match
	if ( isset($params['pass1']) && $params['pass1'] != $params['pass2'] ){
		$status = 'mismatch';
	} else {
	// Update the user pass
		reset_password($user, $params['pass1']);

		$status = 'success';
	}

	echo $status;
	die;

}
add_action( 'wp_ajax_nopriv_reset_user_pass', 'reset_user_pass' );



/*===============================================================
	Marketplace :: Hide content if empty
	Strips out HTML tags, removes non-breaking space entities, and trims all whitespace.
===============================================================*/
function empty_content($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}
